<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Photo extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
        date_default_timezone_set('Asia/Jakarta');

        $msg = array(
            'Message' => 'Selamat datang di Witness / Photo Api',
            'success' => true,

        );
        $this->response($msg, 200);
    }

    public function report_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('type', 'type', 'trim|required');
        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
        $this->form_validation->set_message('required', '{field} harus diisi');

        if ($this->form_validation->run() == true) {

        } else {
            $response = array(
                'message' => 'Maaf, gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            );
        }
        $this->response($response, 200);
    }

}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Komunitas extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Model_Komunitas']);
    }

    public function index_get()
    {
        date_default_timezone_set('Asia/Jakarta');

        $timestamp = time();
        $date_time = date("d-m-Y (D) H:i:s", $timestamp);

        $msg = array(
            'Message' => 'Selamat datang di i-Pangan/komunitas Api',
            'time' => getDateTime(strtotime(date("d-m-Y"))), //$date_time,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function list_get()
    {
        $searchtext = $this->get('s');
        $array_berita = array();
        $page = 1;
        if ($this->get('p')) {
            $page = $this->get('p');
        }
        $rows = 10;
        $offset = ($page - 1) * $rows;

        $this->db->select('*');
        $this->db->from('community');

        // $this->db->join('user', 'news.newsAdmiId=user.userId', 'LEFT');
        // $this->db->join('master_category', 'news.newsMsctId=master_category.msctId', 'LEFT');
        $this->db->where('commStatus', 1);
        $this->db->where('commStatusAprv', 1);
        //$this->db->order_by('newsId', 'DESC');
        if ($searchtext != '') {
            $this->db->like('commName', $searchtext, 'both');
        }
        $this->db->limit($rows, $offset);
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $array_berita[] = array(
                'commId' => $r->commId,
                'commName' => $r->commName,
                'commMsprId' => getField('master_province', 'msprName', ['msprId' => $r->commMsprId]),
                'commMsrgId' => getField('master_regency', 'msrgName', ['msrgId' => $r->commMsrgId]),
                'commArimId' => $r->commArimId,
                'commSaved' => $r->commSaved,
                'commStatus' => $r->commStatus,
                'commUserId' => $r->commUserId,
                'allMember' => count_all('community_member', ['cmmbCommId' => $r->commId]),
                'images' => $this->Model_Komunitas->get_all_images($r->commArimId),
            );
        }

        $msg = array(
            'data' => $array_berita,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function detail_get()
    {
        $array_berita = array();
        $idkom = $this->get('id');

        $this->db->select('*');
        $this->db->from('community');
        $this->db->where('commId', $idkom);
        // $this->db->join('user', 'news.newsAdmiId=user.userId', 'LEFT');
        // $this->db->join('master_category', 'news.newsMsctId=master_category.msctId', 'LEFT');
        // $this->db->where('newsStatus', 1);
        //$this->db->order_by('newsId', 'DESC');

        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $array_berita = array(
                'commId' => $r->commId,
                'commName' => $r->commName,
                'slug' => 'http://i-pangan.com/community/detail/'.$r->commId.'/'.slugify($r->commName).".html",
                'commMsprId' => getField('master_province', 'msprName', ['msprId' => $r->commMsprId]),
                'commMsrgId' => getField('master_regency', 'msrgName', ['msrgId' => $r->commMsrgId]),
                'commArimId' => $r->commArimId,
                'commSaved' => $r->commSaved,
                'timeAgo' => getDateTime(strtotime(date($r->commSaved))), // timeAgo($r->commSaved),
                'commStatus' => $r->commStatus,
                'commUserId' => $r->commUserId == null ? '' : $r->commUserId,
                'images' => $this->Model_Komunitas->get_all_images($r->commArimId),
                'organisasi' => $this->Model_Komunitas->getOrganisasi($r->commId),
                'anggota' => $this->Model_Komunitas->getMember($r->commId),
                'forum' => $this->Model_Komunitas->getForum($r->commId),
            );
        }

        $msg = array(
            'data' => $array_berita,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function listcomment_get()
    {
        $array_video = array();
        $id = $this->get('id');

        $page = 1;
        if ($this->get('p')) {
            $page = $this->get('p');
        }
        $rows = 5;
        $offset = ($page - 1) * $rows;

        $this->db->select('*');
        $this->db->from('community_comment');

        $this->db->where('communityId', $id);
        $this->db->order_by('ccId', 'DESC');
        $this->db->limit($rows, $offset);
        $query = $this->db->get()->result();

        if (!$query) {
            $msg = array(
                'data' => [],
                'success' => false,
            );
            $this->response($msg, 200);
            exit();
        }

        foreach ($query as $r) {
            $array_video[] = array(

                'ccId' => $r->ccId,
                'communityId' => $r->communityId,
                'communityUserId' => $r->communityUserId,
                'communityCommentText' => $r->communityCommentText,
                'communityCommentStatus' => $r->communityCommentStatus,
                'createDate' => getDateTime(strtotime(date($r->createDate))),
             
                'userName' => getField('user', 'userFullName', ['userId' => $r->communityUserId]),
                'userPict' => getField('user', 'userImgURL', ['userId' => $r->communityUserId]),
                // 'necoSaved' => getDateTime(strtotime(date($r->necoSaved))),//timeAgo($r->necoSaved),

            );
        }

        $msg = array(
            'data' => $array_video,
            'success' => true,
        );
        $this->response($msg, 200);
    }


    public function commentpost_post(){
        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('userid', 'userid', 'trim|required',
            array(
                'required' => 'userid harus diisi',
            )
        );

        $this->form_validation->set_rules('text', 'text', 'trim|required',
            array(
                'required' => 'text harus diisi',
            )
        );

        $this->form_validation->set_rules('communityId', 'comunity id', 'trim|required',
            array(
                'required' => 'comunity harus diisi',
            )
        );
        if ($this->form_validation->run() == true) {
            $data = array(
                'communityId' => @$post['communityId'],
                'communityUserId' => @$post['userid'],
                'communityCommentText' => @$post['text'],
            );

            $this->db->insert('community_comment', $data);
            $response = array(
                'message' => 'post berhasil',
                'success' => true,
            );
        } else {
            $response = array(
                'message' => 'Maaf, post response gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            );
        }
        $this->response($response, 200);
    }

    public function commentedit_post(){
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $this->form_validation->set_rules('ccId', 'ccId', 'trim|required',
            array(
                'required' => 'ccId harus diisi',
            )
         );


        $this->form_validation->set_rules('userid', 'userid', 'trim|required',
            array(
                'required' => 'userid harus diisi',
            )
        );

        $this->form_validation->set_rules('text', 'text', 'trim|required',
            array(
                'required' => 'text harus diisi',
            )
        );

        $this->form_validation->set_rules('communityId', 'comunity id', 'trim|required',
            array(
                'required' => 'comunity harus diisi',
            )
        );
        if ($this->form_validation->run() == true) {
            $data = array(
                'ccId'=> @$post['ccId'],
                'communityId' => @$post['communityId'],
                'communityUserId' => @$post['userid'],
                'communityCommentText' => @$post['text'],
            );

            $this->db->update('community_comment', $data,['ccId'=>$post['ccId']]);
            $response = array(
                'message' => 'post berhasil',
                'success' => true,
            );
        } else {
            $response = array(
                'message' => 'Maaf, post response gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            );
        }
        $this->response($response, 200);
    }

    public function commentdelete_post(){
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $idtodelete = $post['ccId'];
        $this->db->delete('community_comment', ['ccId' => $idtodelete]);
        $response = array(
            'message' => 'hapus berhasil',
            'success' => true,
        );
        $this->response($response, 200);
    }

    public function threaddetail_get()
    {
        $_arr = [];
        $idthread = $this->get('id');
        $this->db->select("*");
        $this->db->from('thread');
        if ($idthread != "") {
            $this->db->where('thrdId', $idthread);
        }
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $_arr = array(
                'thrdId' => $r->thrdId,
                'thrdUserId' => $r->thrdUserId,
                'thrdCommId' => $r->thrdCommId,
                'thrdTitle' => $r->thrdTitle,
                'thrdText' => $r->thrdText == null ? '' : $r->thrdText,
                'thrdSaved' => getDateTime(strtotime(date($r->thrdSaved))), // $r->thrdSaved,
                'thrdStatus' => $r->thrdStatus,
            );
        }

        $msg = array(
            'data' => $_arr,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function threadresponse_get()
    {
        $_arr = [];
        $idthread = $this->get('id');
        $page = 1;
        if ($this->get('p')) {
            $page = $this->get('p');
        }
        $rows = 10;
        $offset = ($page - 1) * $rows;

        $this->db->select("*");
        $this->db->from('thread_response');
        if ($idthread != "") {
            $this->db->where('tresThrdId', $idthread);
        }
        $this->db->order_by('tresId', 'DESC');
        $this->db->limit($rows, $offset);
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $_arr[] = array(
                'tresId' => $r->tresId,
                'tresThrdId' => $r->tresThrdId,
                'tresUserId' => $r->tresUserId,
                'userName' => getField('user', 'userFullName', ['userId' => $r->tresUserId]),
                'userImage' => getField('user', 'userImgURL', ['userId' => $r->tresUserId]),
                'tresText' => $r->tresText,
                'tresSaved' =>getDateTime(strtotime(date($r->tresSaved))), //timeAgo($r->tresSaved),
                'tresStatus' => $r->tresStatus,
            );
        }

        $msg = array(
            'data' => $_arr,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function threadresponselist_get()
    {
        $_arr = [];
        $idthread = $this->get('id');

        $page = 1;
        if ($this->get('p')) {
            $page = $this->get('p');
        }
        $rows = 5;
        $offset = ($page - 1) * $rows;

        $this->db->select("*");
        $this->db->from('thread_response');
        if ($idthread != "") {
            $this->db->where('tresThrdId', $idthread);
        }
        $this->db->limit($rows, $offset);
        $this->db->order_by('tresId', 'DESC');
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $_arr[] = array(
                'tresId' => $r->tresId,
                'tresThrdId' => $r->tresThrdId,
                'tresUserId' => $r->tresUserId,
                'userName' => getField('user', 'userFullName', ['userId' => $r->tresUserId]),
                'userImage' => getField('user', 'userImgURL', ['userId' => $r->tresUserId]),
                'tresText' => $r->tresText,
                'tresSaved' => timeAgo($r->tresSaved),
                'tresStatus' => $r->tresStatus,
            );
        }

        $msg = array(
            'data' => $_arr,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function threadresponsepost_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('userid', 'userid', 'trim|required',
            array(
                'required' => 'userid harus diisi',
            )
        );

        $this->form_validation->set_rules('text', 'text', 'trim|required',
            array(
                'required' => 'text harus diisi',
            )
        );

        $this->form_validation->set_rules('threadid', 'threadid', 'trim|required',
            array(
                'required' => 'tresThrdId harus diisi',
            )
        );
        if ($this->form_validation->run() == true) {
            $data = array(
                'tresThrdId' => @$post['threadid'],
                'tresUserId' => @$post['userid'],
                'tresText' => @$post['text'],
            );

            $this->db->insert('thread_response', $data);
            $response = array(
                'message' => 'post berhasil',
                'success' => true,
            );
        } else {
            $response = array(
                'message' => 'Maaf, post response gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            );
        }
        $this->response($response, 200);
    }

    public function threadresponsedelete_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $idtodelete = $post['id'];
        $this->db->delete('thread_response', ['tresId' => $idtodelete]);
        $response = array(
            'message' => 'hapus berhasil',
            'success' => true,
        );
        $this->response($response, 200);
    }

    public function terbaru_get()
    {

        $array_berita = array();

        $this->db->select('*');
        $this->db->from('news');

        $this->db->join('user', 'news.newsAdmiId=user.userId', 'LEFT');
        $this->db->join('master_category', 'news.newsMsctId=master_category.msctId', 'LEFT');
        $this->db->where('newsStatus', 1);
        $this->db->order_by('newsId', 'DESC');

        $this->db->limit(3);
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $array_berita[] = array(
                'newsId' => $r->newsId,
                'newsTitle' => $r->newsTitle,
                'newsMsctId' => $r->newsMsctId,
                'newsExcerpt' => $r->newsExcerpt,
                'newsDetail' => $r->newsDetail,
                'newsPublishTime' => $r->newsPublishTime,
                'newsSaved' => timeAgo($r->newsSaved),
                'newsAdmiId' => $r->newsAdmiId,
                'newsStatus' => $r->newsStatus,
                'newsSticky' => $r->newsSticky,
                'newsPushNotif' => $r->newsPushNotif,
                'images' => $this->Model_Berita->get_all_images($r->newsId),
            );
        }

        $msg = array(
            'data' => $array_berita,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function cekjoinorleft_get()
    {

        $f = getField('community_member', 'cmmbCommId', array('cmmbUserId' => $this->get('userId'), 'cmmbCommId' => $this->get('cid')));
        $msg = array(
            'status' => $f,
            //'data' => $array_berita,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function joinkomunitas_post()
    {
        $userid = $this->input->post('userId');
        $communityid = $this->input->post('communityId');

        if ($userid != '' && $communityid != '') {
            $this->db->insert('community_member', ['cmmbCommId' => $communityid, 'cmmbUserId' => $userid]);
            $msg = array(
                'status' => 'ok',
                'success' => true,
            );
            $this->response($msg, 200);
        }

    }

    public function leftkomunitas_post()
    {
        $userid = $this->input->post('userId');
        $communityid = $this->input->post('communityId');

        if ($userid != '' && $communityid != '') {
            $this->db->delete('community_member', ['cmmbCommId' => $communityid, 'cmmbUserId' => $userid]);
            $msg = array(
                'status' => 'ok',
                'success' => true,
            );
            $this->response($msg, 200);
        }
    }

    public function cekadminornot_get()
    {
        $userid = $this->get('userId');
        $msg = array(
            'access1' => getField('user', 'userAdmin', ['userId' => $userid]),
            'access2' => getField('user', 'userAdminAccess', ['userId' => $userid]),
            'success' => true,
        );
        $this->response($msg, 200);

    }

    public function createforum_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $userid = $post['userId'];
        $communityid = $post['communityId'];
        $ttitle = $post['namaforum'];
        $text = $post['deskripsiforum'];

        if ($userid != '' && $communityid != '' && $ttitle != '') {
            $this->db->insert('thread', [
                'thrdUserId' => $userid,
                'thrdCommId' => $communityid,
                'thrdTitle' => $ttitle,
                'thrdText' => $text,
            ]);
            $msg = array(
                'status' => 'ok',
                'success' => true,
            );
            $this->response($msg, 200);
        }
    }

    public function create_post()
    {

        $this->load->library('upload');
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        //print_r($this->session->userdata('data_login'));exit;
        //   $this->load->library('upload');
        //   $post = $this->input->post();
        //pathinfo(basename($image["name"]),PATHINFO_EXTENSION);
        $file_image = $_FILES['img_community'];
        $file_image_extension = pathinfo(basename($file_image['name']), PATHINFO_EXTENSION);

        $table = 'arsip_images';
        $data = array(
            'arimTitle' => '-',
            'arimCaption' => '-',
            'arimFileType' => $file_image_extension,
            'arimUserIdSaved' => 1,
            'arimSaved' => date("Y-m-d H:i:s"),
            'arimActive' => 1,
        );

        $upload_data_image_id = $this->Model_Komunitas->save_arsip_images($table, $data);

        $id = $upload_data_image_id; //id library;
        $splitId = str_split($id);
        //print_r($splitId);exit;

        foreach ($splitId as $k => $v) {
            $valArr[] = $v . "/";
        }

        //create recursive folder
        $structure = implode($valArr);
        if (!file_exists("../cms.i-pangan.com/uploads/library/" . $structure)) {
            mkdir("../cms.i-pangan.com/uploads/library/" . $structure, 0755, true);
        }
        $config['upload_path'] = "../cms.i-pangan.com/uploads/library/" . $structure;

        //upload image to recursive folder
        $max_width = '10288';
        $max_height = '7068';
        $config['allowed_types'] = "gif|jpg|png|jpeg|bmp";
        $config['max_size'] = '2048'; //max 2mb
        $config['max_width'] = $max_width; //max lebar
        $config['max_height'] = $max_height; //max tinggi
        $config['file_name'] = $id;
        $config['overwrite'] = true;

        $this->upload->initialize($config);

        if ($_FILES['img_community']['error'] == 0 && $this->upload->do_upload('img_community')) {
            //print_r($this->image_lib->display_errors());exit;

            if ($_FILES['img_community']['size'] > 2 * 1024 * 1024) {
                $msg = array(
                    'status' => 'berhasil',
                    'success' => true,
                );
            } else {

                $image_size = array(
                    array(
                        'width' => 224,
                        'height' => 153,
                    ),
                    array(
                        'width' => 263,
                        'height' => 180,
                    ),
                    array(
                        'width' => 300,
                        'height' => 206,
                    ),
                    array(
                        'width' => 512,
                        'height' => 351,
                    ),
                    array(
                        'width' => 683,
                        'height' => 468,
                    ),
                    array(
                        'width' => 840,
                        'height' => 576,
                    ),
                );
                $this->load->library('image_lib');

                foreach ($image_size as $k => $v) {
                    $img = $this->upload->data();
                    //print_r($img); exit;
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = '../cms.i-pangan.com/uploads/library/' . $structure . '/' . $img['file_name'];
                    $config['create_thumb'] = false;
                    $config['maintain_ratio'] = false;
                    $config['new_image'] = '../cms.i-pangan.com/uploads/library/' . $structure . '/' . $img['raw_name'] . "_" . $v['width'] . 'x' . $v['height'] . $img['file_ext'];
                    $config['width'] = $v['width'];
                    $config['height'] = $v['height'];

                    //100_224x153.png, //100_263x180.png, //100_300x206.png, //100_512x351.png, //100_683x468.png, 100_840x576.png

                    $this->image_lib->clear();
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                }

                $data_community = array(
                    'commName' => $post['name_community'],
                    'commMsprId' => $post['province_community'],
                    'commMsrgId' => $post['regency_community'],
                    'commArimId' => $upload_data_image_id,
                    'commUserId' => $post['userid'],
                    'commSaved' => date("Y-m-d H:i:s"),
                    'commStatus' => 1,
                    'commStatusAprv' => 0,
                );

                $table_community = 'community';

                $save_community = $this->Model_Komunitas->save_post($table_community, $data_community);

                if ($save_community) {
                    $msg = array(
                        'status' => 'berhasil',
                        'success' => true,
                    );
                } else {
                    $msg = array(
                        'status' => 'gagal',
                        'success' => false,
                    );
                }
            }
        } else {
            $msg = array(
                'status' => 'gagal',
                'success' => false,
            );
        }

        $this->response($msg, 200);
    }

    public function anggotalist_get()
    {
        $commid = $this->get('commid');
        $xx = array();

        $page = 1;
        if ($this->get('p')) {
            $page = $this->get('p');
        }
        $rows = 5;
        $offset = ($page - 1) * $rows;

        $this->db->select("community_member.*, user.userFullName as uName, user.userImgURL as imageUser, user.userMsprId as prof");
        $this->db->from('community_member');
        $this->db->join('user', 'community_member.cmmbUserId = user.userId');
        $this->db->where('cmmbCommId', $commid);
        $this->db->limit($rows, $offset);

        $query = $this->db->get()->result();
        $xx = array();
        foreach ($query as $r) {
            $xx[] = array(
                'cmmbId' => $r->cmmbId,
                'cmmbCommId' => $r->cmmbCommId,
                'cmmbUserId' => $r->cmmbUserId,
                'cmmbSaved' => $r->cmmbSaved,
                'cmmbStatus' => $r->cmmbStatus,
                'uName' => $r->uName,
                'provinsi' => getField('master_province', 'msprName', ['msprId' => $r->prof]),
                'imageUser' => $r->imageUser == null ? '' : $r->imageUser,
            );
        }
        $response = array(
            'data' => $xx,
            'success' => true,
        );
        $this->response($response, 200);
    }

    // public function create_forum_automatic($coid){
    //   $this->db->inser('thread',['thrdCommId'=>$coid,'thrdTitle'=>'Berbagi Info', 'thrdText'=>'Tempat Member Saling Tukar Info']);
    //   $this->db->inser('thread',['thrdCommId'=>$coid,'thrdTitle'=>'Diskusi', 'thrdText'=>'Tempat Member Saling Berdiskusi']);
    // }

    // public function tambahkomunitas_post()
    // {
    //     $post = $this->post();
    //     $this->form_validation->set_data($this->post());

    //     $this->form_validation->set_rules('namakomunitas', 'namakomunitas', 'trim|required');
    //     $this->form_validation->set_rules('provinsi', 'provinsi', 'trim|required');
    //     $this->form_validation->set_rules('kota', 'kota', 'trim|required');

    //     $this->form_validation->set_message('required', '{field} harus diisi');

    //     if ($this->form_validation->run() == true) {
    //         $data = array(
    //             'userFullName' => @$post['name'],
    //         );

    //         $response = array(
    //             'message' => 'Tambah Berhasil',
    //             'success' => true,
    //         );

    //     } else {

    //         $response = array(
    //             'message' => 'Maaf, tambah gagal',
    //             'errors' => $this->form_validation->error_array(),
    //             'success' => false,
    //         );

    //     }

    //     $this->response($response, 200);
    // }

    // public function video_terbaru_get()
    // {

    //     $this->db->select('*');
    //     $this->db->from('video');
    //     $this->db->where('videStatus', 1);
    //     $this->db->limit(5);
    //     $this->db->order_by('videId', 'DESC');
    //     $query = $this->db->get()->result();

    //     $msg = array(
    //         'data' => $query,
    //         'success' => true,
    //     );
    //     $this->response($msg, 200);
    // }

}
//http://cdn.i-pangan.com/library/1/7/17_224x153.png
//date('d-m-Y', strtotime($val->ptntBday))

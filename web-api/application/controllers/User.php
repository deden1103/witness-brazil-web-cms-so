<?php
header("Access-Control-Allow-Origin: *");
defined('BASEPATH') or exit('No direct script access allowed');

class User extends API_Controller
{
    public function index_get()
    {
        $response = array(
            'message' => 'Welcome Witness User API',
        );
        $this->response($response, 200);
    }

    public function detail_get() {
		$dataDetail = array();
		$query = $this->db->select('*')
			->from('user_komunitas')
			->where('ukomId', $this->jwtData->id)
			->get()->result();
		foreach ($query as $val) {
			// $photonya = $val->photoRelawan ? base_url('assets/uploads/profile/' . $val->photoRelawan) : base_url('assets/uploads/profile/noimage.png');
			$dataDetail = array(
				"ukomId" => $val->ukomId,
				"ukomKomtId" => getField('komunitas','komtName',['komtId' =>  $val->ukomKomtId]),
				"ukomName" => $val->ukomName,
				"ukomPassword" =>  $val->ukomPassword,
				"ukomEmail" => $val->ukomEmail,
				"token" =>  $val->token,
				"ukomPhoto" =>  $val->ukomPhoto,
				"ukomPhone"  =>  $val->ukomPhone,
				"ukomOneSignal"  =>  $val->ukomOneSignal,
				"vvvv"  =>  "kkkkk",
				// "ukomTtl": "1992-03-09",
				// "ukomGender": "1",
				// "ukomProvinsi": "32",
				// "ukomKabupaten": "3275",
				// "ukomKecamatan": "3275060",
				// "ukomKelurahan": "3275060005",
				// "ukomAlamat": "-",
				// "ukomSaved": "2019-05-20 09:37:56",
				// "ukomAdmiIdSaved": "1",
				// "ukomStatus": "1",
				// "lastLogin": "2019-07-31 19:28:19"
			);
		}
		$response = array(
			'detail' => $dataDetail,
			'success' => true,
		);
		$this->response($response, 200);
	}
	
	public function updatepict_post() {
		$this->load->library('uploads');
		$post = $this->post();
		$this->form_validation->set_data($this->post());

		$data = array();

		if (@$_FILES['pict']['name'] != '') {
			$file = $this->uploads->upload_image('pict', 'assets/uploads/profile/');
			if (@$file['error']) {
				$response = array(
					'message' => 'Gagal upload foto (max size 1MB only jpg,jpeg,png)',
					'success' => FALSE,
				);
				$this->response($response, 200);
				exit();
			}
			$data['ukomPhoto'] = base_url()."assets/uploads/profile/".$file['file_name'];
			$imageName = basename(getField('user_komunitas', 'ukomPhoto', array('ukomId' => $this->jwtData->id)));
			$dir = "assets/uploads/profile/" . $imageName;
			if (file_exists($dir)) {
				@unlink($dir);
			}

		}

		$update = $this->db->update('user_komunitas', $data, array('ukomId' => $this->jwtData->id));

		$response = array(
			'message' => 'Update Berhasil',
			'photoRelawan' => base_url()."assets/uploads/profile/".$file['file_name'],
			'success' => true,
		);

		$this->response($response, 200);
	}

	//https://upload.wikimedia.org/wikipedia/commons/6/66/An_up-close_picture_of_a_curious_male_domestic_shorthair_tabby_cat.jpg


    public function updateonesignal_post() {
		$post = $this->post();
		$this->form_validation->set_data($post);
		$data = array(
			'ukomOneSignal' => $post['osid'],
		);
		$update = $this->db->update('user_komunitas', $data, array('ukomId' => $this->jwtData->id));

		$msg = array(
			'message' => 'onesignal id berhasil di tambahkan',
			'success' => TRUE,
		);
		$this->response($msg, 200);
	}

	public function changepassword_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $this->form_validation->set_rules('password', 'password', 'trim|required');

        $this->form_validation->set_message('required', '{field} harus diisi');

        if ($this->form_validation->run() == true) {
            $data = array(
              "ukomPassword" =>SHA1(@$post['password'])
		);


        $this->db->update('user_komunitas', $data, array('ukomId' => $this->jwtData->id));
           

            $response = array(
                'message' => 'Update Berhasil',
                'success' => true,
            );
        } else {
            $response = array(
                'message' => 'Maaf, edit user gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            );
        }
        $this->response($response, 200);
    }


    
}


/*
3608a6d1a05aba23ea390e5f3b48203dbb7241f7

*/
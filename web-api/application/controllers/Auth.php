<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Auth extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_Auth');
    }
    public function index_get()
    {
        $response = [
            'message' => 'Welcome Auth API',
        ];
        $this->response($response, 200);
    }

    public function login_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $emailorphone = $post['emailorphone'];
        $password = SHA1($post['password']);

        $this->form_validation->set_rules(
            'emailorphone',
            'emailorphone',
            'trim|required',
            [
                'required' => 'emailorphone harus diisi',
            ]
        );

        $this->form_validation->set_rules(
            'password',
            'Password',
            'trim|required',
            [
                'required' => 'Kata Sandi harus diisi',
                'min_length' => 'Kata Sandi harus diisi minimal 8 karakter',
            ]
        );

        if ($this->form_validation->run() == true) {
            $generator = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $result = '';
            for ($i = 1; $i <= 4; $i++) {
                $result .= substr($generator, rand() % strlen($generator), 1);
            }
            $userData = $this->Model_Auth->login($emailorphone, $password);
            //$photonya = $userData->photoAnggota ? base_url('assets/uploads/profile/' . $userData->photoAnggota) : base_url('assets/uploads/profile/noimage.png');
            if ($userData) {
                if (strtolower($emailorphone) == 'dev@gmail.com') {
                    // $this->sendsms($userData->ukomPhone, $result);
                    $this->db->update(
                        'user_komunitas',
                        ['otp' => 'ABCD'],
                        ['ukomId' => $userData->ukomId]
                    );
                } else {
                    $this->sendsms($userData->ukomPhone, $result);
                    $this->db->update(
                        'user_komunitas',
                        ['otp' => $result],
                        ['ukomId' => $userData->ukomId]
                    );
                }

                $response = [
                    'userId' => $userData->ukomId,
                    'otp' => $result, //$userData->otp,
                    'userFullName' => $userData->ukomName,
                    'userEmail' => $userData->ukomEmail,
                    'userPhone' => $userData->ukomPhone,
                    'userPict' => $userData->ukomPhoto,
                    'komunitasId' => $userData->ukomKomtId,
                    'iccid' =>
                        $userData->simSerialNumber == '' ||
                        $userData->simSerialNumber == null
                            ? '11119999XXX'
                            : $userData->simSerialNumber,
                    'message' => 'Login Berhasil',
                    'success' => true,
                ];

                $token['id'] = $userData->ukomId;
                $token['email'] = $userData->ukomEmail;
                $token['phone'] = $userData->ukomPhone;
                $token['komunitasid'] = $userData->ukomKomtId;
                $token['logged'] = true;
                $token['iat'] = time();
                $token['exp'] = time() + 2592000;
                $jwt_token = JWT::encode($token, secretKey());
                $jwt = [
                    'token' => $jwt_token,
                ];

                $response = array_merge($response, $jwt);
                $this->db->update(
                    'user_komunitas',
                    ['lastLogin' => date('Y-m-d H:i:s'), 'token' => $jwt_token],
                    ['ukomId' => $userData->ukomId]
                );
                $this->db->update(
                    'user_komunitas',
                    ['token' => $jwt_token],
                    ['ukomId' => $userData->ukomId]
                );
            } else {
                $response = [
                    'message' => 'Login gagal, username/password anda salah',
                    'success' => false,
                ];
            }
        } else {
            $response = [
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            ];
        }

        $this->response($response, 200);
    }

    public function sendsms($nomor = '', $code = '')
    {
        if ($nomor == '' || $code == '') {
            // echo 'tidak ada nomor atau kode';
            // print_r('tidak ada nomor atau kode');
        } else {


            $userkey = '3d85f931865d';
            $passkey = 'b6a81fd161d62b2087bad008';
            $telepon = $nomor;
            $message = 'Masukkan ' . $code . " di aplikasi Tempo Witness.";
            $url = 'https://console.zenziva.net/wareguler/api/sendWA/';
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_URL, $url);
            curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
            curl_setopt($curlHandle, CURLOPT_POST, 1);
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, [
                'userkey' => $userkey,
                'passkey' => $passkey,
                'to' => $telepon,
                'message' => $message,
            ]);
            $results = json_decode(curl_exec($curlHandle), true);
            curl_close($curlHandle);

            // $userkey = '3d85f931865d';
            // $passkey = 'b6a81fd161d62b2087bad008';
            // $telepon = $nomor;
            // // $message = 'Masukkan ' . $code . " di aplikasi Tempo Witness.";
            // $message = $code;
            // // $url = 'https://console.zenziva.net/reguler/api/sendsms/';
            // $url = 'https://console.zenziva.net/wareguler/api/sendWA/';
            // $curlHandle = curl_init();
            // curl_setopt($curlHandle, CURLOPT_URL, $url);
            // curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            // curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
            // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
            // curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
            // curl_setopt($curlHandle, CURLOPT_POST, 1);
            // curl_setopt($curlHandle, CURLOPT_POSTFIELDS, [
            //     'userkey' => $userkey,
            //     'passkey' => $passkey,
            //     'to' => $telepon,
            //     'message' => $message,
            // ]);
            // $results = json_decode(curl_exec($curlHandle), true);
            // // print_r($results);
            // curl_close($curlHandle);
        }
    }

    public function loginotp_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $emailorphone = @$post['emailorphone'];
        $phone = @$post['phone'];
        $otp = @$post['otp'];

        $this->form_validation->set_rules(
            'emailorphone',
            'emailorphone',
            'trim|required',
            [
                'required' => 'emailorphone harus diisi',
            ]
        );

        $this->form_validation->set_rules('otp', 'otp', 'trim|required', [
            'required' => 'otp harus diisi',
        ]);

        if ($this->form_validation->run() == true) {
            $userData = $this->db
                ->select('*')
                ->from('user_komunitas')
                ->where('otp', $otp)
                ->where('ukomStatus', 1)
                ->group_start()
                ->where('ukomEmail', $emailorphone)
                ->or_where('ukomPhone', $phone)
                ->group_end()
                ->get()
                ->row();

            if ($userData) {
                $response = [
                    'userId' => $userData->ukomId,
                    'otp' => $userData->otp,
                    'userFullName' => $userData->ukomName,
                    'userEmail' => $userData->ukomEmail,
                    'userPhone' => $userData->ukomPhone,
                    'userPict' => $userData->ukomPhoto,
                    'komunitasId' => $userData->ukomKomtId,
                    'iccid' => 00,
                    'message' => 'Login Berhasil',
                    'success' => true,
                ];

                $token['id'] = $userData->ukomId;
                $token['email'] = $userData->ukomEmail;
                $token['phone'] = $userData->ukomPhone;
                $token['komunitasid'] = $userData->ukomKomtId;
                $token['logged'] = true;
                $token['iat'] = time();
                $token['exp'] = time() + 2592000;
                $jwt_token = JWT::encode($token, secretKey());
                $jwt = [
                    'token' => $jwt_token,
                ];

                $response = array_merge($response, $jwt);
                $this->db->update(
                    'user_komunitas',
                    ['lastLogin' => date('Y-m-d H:i:s'), 'token' => $jwt_token],
                    ['ukomId' => $userData->ukomId]
                );
                $this->db->update(
                    'user_komunitas',
                    ['token' => $jwt_token],
                    ['ukomId' => $userData->ukomId]
                );
            } else {
                $response = [
                    'message' => 'Login gagal, otp anda salah',
                    'success' => false,
                ];
            }
        } else {
            $response = [
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            ];
        }

        $this->response($response, 200);
    }

    public function registeremail_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $this->form_validation->set_rules('name', 'name', 'trim|required');
        //$this->form_validation->set_rules('email', 'email', 'trim|required|is_unique[anggota.emailAnggota]');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        //$this->form_validation->set_rules('phone', 'phone', 'trim');
        $this->form_validation->set_rules('password', 'password', 'trim');

        $this->form_validation->set_message('required', '{field} harus diisi');
        //$this->form_validation->set_message('is_unique', 'email sudah terdaftar');

        if ($this->form_validation->run() == true) {
            $data = [
                'userFullName' => @$post['name'],
                'userEmail' => @$post['email'],
                'userPhone' => @$post['phone'],
                'userPassword' => SHA1(@$post['password']),
            ];
            if (@$post['email'] != '') {
                $register = $this->Model_Auth->register($data);
                $response = [
                    'message' => 'Registrasi Berhasil',
                    'success' => true,
                ];
            } else {
                $response = [
                    'message' =>
                        'Registrasi Belum Berhasil data-data masih kosong',
                    'success' => false,
                ];
            }
        } else {
            $response = [
                'message' => 'Maaf, Registrasi gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            ];
        }

        $this->response($response, 200);
    }

    public function registerphone_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $this->form_validation->set_rules('name', 'name', 'trim|required');
        //$this->form_validation->set_rules('email', 'email', 'trim|required|is_unique[anggota.emailAnggota]');
        //$this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim');

        $this->form_validation->set_message('required', '{field} harus diisi');
        //$this->form_validation->set_message('is_unique', 'email sudah terdaftar');

        if ($this->form_validation->run() == true) {
            $data = [
                'userFullName' => @$post['name'],
                'userEmail' => @$post['email'],
                'userPhone' => @$post['phone'],
                'userPassword' => SHA1(@$post['password']),
            ];
            if (@$post['phone'] != '') {
                $register = $this->Model_Auth->register($data);
                $response = [
                    'message' => 'Registrasi Berhasil',
                    'success' => true,
                ];
            } else {
                $response = [
                    'message' =>
                        'Registrasi Belum Berhasil data-data masih kosong',
                    'success' => false,
                ];
            }
        } else {
            $response = [
                'message' => 'Maaf, Registrasi gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            ];
        }

        $this->response($response, 200);
    }

    public function register2_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $this->form_validation->set_rules('name', 'name', 'trim');
        $this->form_validation->set_rules(
            'email',
            'email',
            'trim|required|is_unique[anggota.emailAnggota]'
        );
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules(
            'password',
            'password',
            'trim|required'
        );
        $this->form_validation->set_rules(
            'confirm_password',
            'Confirm Password',
            'trim|required|matches[password]'
        );

        $this->form_validation->set_message(
            'matches',
            'Konfirmasi Password baru tidak sama'
        );
        $this->form_validation->set_message('required', '{field} harus diisi');
        $this->form_validation->set_message(
            'is_unique',
            'email sudah terdaftar'
        );

        if ($this->form_validation->run() == true) {
            $data = [
                'namaAnggota' => @$post['name'],
                'emailAnggota' => @$post['email'],
                'phoneAnggota' => @$post['phone'],
                'password' => hash_password(@$post['password']),
            ];
            if (@$post['name'] != '' || @$post['email'] != '') {
                $register = $this->Auth_Model->register($data);
                $response = [
                    'message' => 'Registrasi Berhasil',
                    'success' => true,
                ];
            } else {
                $response = [
                    'message' =>
                        'Registrasi Belum Berhasil data-data masih kosong',
                    'success' => false,
                ];
            }
        } else {
            $response = [
                'message' => 'Maaf, Registrasi gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            ];
        }

        $this->response($response, 200);
    }

    public function logingoogle_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $email = $post['email'];

        $userData = $this->Model_Auth->logingoogle($email);
        if ($userData) {
            $response = [
                'userId' => $userData->userId,
                'userFullName' => $userData->userFullName,
                'userEmail' => $userData->userEmail,
                'userPhone' => $userData->userPhone,
                'userPict' => $userData->userImgURL,
                'message' => 'Login Berhasil',
                'success' => true,
            ];

            $token['id'] = $userData->userId;
            $token['email'] = $userData->userEmail;
            $token['phone'] = $userData->userPhone;
            $token['logged'] = true;
            $token['iat'] = time();
            $token['exp'] = time() + 2592000;
            $jwt_token = JWT::encode($token, secretKey());
            $jwt = [
                'token' => $jwt_token,
            ];

            $response = array_merge($response, $jwt);
            $this->db->update(
                'user',
                ['userLastActivity' => date('Y-m-d H:i:s')],
                ['userId' => $userData->userId]
            );
            $this->db->update(
                'user',
                ['token' => $jwt_token],
                ['userId' => $userData->userId]
            );
        } else {
            $data = [
                'userFullName' => @$post['name'],
                'userEmail' => $email,
                'userImgURL' => @$post['photo'],
            ];

            $this->db->insert('user', $data);
            $dataUser = $this->Model_Auth->logingoogle($email);

            $response = [
                'userId' => $dataUser->userId,
                'userFullName' => $dataUser->userFullName,
                'userEmail' => $dataUser->userEmail,
                'userPhone' => $dataUser->userPhone,
                'userPict' => $dataUser->userImgURL,
                'message' => 'register dan Login Berhasil',
                'success' => true,
            ];

            $token['id'] = $dataUser->userId;
            $token['email'] = $dataUser->userEmail;
            $token['phone'] = $dataUser->userPhone;
            $token['logged'] = true;
            $token['iat'] = time();
            $token['exp'] = time() + 2592000;
            $jwt_token = JWT::encode($token, secretKey());
            $jwt = [
                'token' => $jwt_token,
            ];

            $response = array_merge($response, $jwt);
            $this->db->update(
                'user',
                ['userLastActivity' => date('Y-m-d H:i:s')],
                ['userId' => $dataUser->userId]
            );
            $this->db->update(
                'user',
                ['token' => $jwt_token],
                ['userId' => $dataUser->userId]
            );
        }

        $this->response($response, 200);
    }

    // public function sendemail2_post()
    // {
    //     $post = $this->post();
    //     $this->form_validation->set_data($this->post());

    //     $acak = random_password();
    //     $pass = SHA1($acak);

    //     $c['protocol'] = 'smtp';
    //     $c['smtp_host'] = 'ssl://smtp.gmail.com';
    //     $c['smtp_port'] = '465';
    //     $c['smtp_timeout'] = '7';
    //     $c['smtp_user'] = 'ipanganindonesia@gmail.com';
    //     $c['smtp_pass'] = 'BismiLLahberkah999';
    //     $c['charset'] = 'utf-8';
    //     $c['newline'] = "\r\n";
    //     $c['mailtype'] = 'html'; // or html
    //     $c['validation'] = true; // bool whether to validate email or not
    //     $this->load->library('email',$c);
    //     $this->email->from('cs@ipangan.com', 'i-Pangan');
    //     $this->email->to($post['email']);
    //     $this->email->subject('Reset Password Akun i-Pangan');
    //     $this->email->message('<h4>  i-Pangan </h4>
    //         <h4>Password Anda adalah :' . $acak . '</h4><h4>Anda bisa ganti password di aplikasi anda</h4>');

    //     $data = array(
    //         'userPassword' => $pass,
    //     );

    //     if (!$this->email->send()) {
    //         $response = array(
    //             'detail' => 'email not send',
    //             'success' => false,
    //             'error'=>$this->email->print_debugger()
    //         );
    //     } else {
    //         //$this->db->update('user', $data, array('userEmail' => $post['email']));
    //         $response = array(
    //             'detail' => 'email success to send',
    //             'password_baru' => $acak,
    //             'success' => true,
    //             'error'=>$this->email->print_debugger()
    //         );
    //     }

    //     $this->response($response, 200);
    // }

    // public function oldpassword_check($old_password = "")
    // {
    //     $old_password_hash = SHA1($old_password);
    //     if ($old_password != '' && $old_password_hash != getField('user', 'userPassword', array('userId' => $this->jwtData->id))) {
    //         $this->form_validation->set_message('oldpassword_check', 'Password lama anda salah');
    //         return false;
    //     } else {
    //         return true;
    //     }
    // }

    public function changepassword_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($post);

        // $this->form_validation->set_rules('old_password', 'Password Lama', 'trim|required|callback_oldpassword_check');
        $this->form_validation->set_rules(
            'new_password',
            'Password Baru',
            'trim|required'
        );
        $this->form_validation->set_rules(
            'confirm_password',
            'Konfirmasi Password Baru',
            'trim|required|matches[new_password]'
        );

        $this->form_validation->set_message(
            'min_length',
            'Password minimal harus 8 karakter'
        );
        $this->form_validation->set_message(
            'matches',
            'Konfirmasi Password baru tidak sama'
        );
        $this->form_validation->set_message('required', '{field} harus diisi');

        if ($this->form_validation->run() == true) {
            $data = [
                'userPassword' => SHA1($post['confirm_password']),
            ];

            $update = $this->db->update('user', $data, [
                'userId' => $post['userId'],
            ]);

            if ($update) {
                $msg = [
                    'message' => 'Password anda berhasil diubah',
                    'success' => true,
                ];
            } else {
                $msg = [
                    'message' => 'Password anda gagal diubah',
                    'success' => false,
                ];
            }
        } else {
            $msg = [
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            ];
        }

        $this->response($msg, 200);
    }

    public function sendemail_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $acak = random_password();
        $pass = SHA1($acak);

        $cekuser = $this->db
            ->select('userFullname')
            ->from('user')
            ->where('userEmail', $post['email'])
            ->get()
            ->row();
        if ($cekuser) {
            $arr = [
                'nama' => $cekuser,
                'password' => $acak,
            ];
            $template = $this->load->view('forgot', $arr, true);

            $this->load->library('email');
            $this->email->from('cs@ipangan.com', 'i-Pangan');
            $this->email->to($post['email']);
            $this->email->subject('Reset Password Akun i-Pangan');
            $this->email->message($template);

            $data = [
                'userPassword' => $pass,
            ];

            if (!$this->email->send()) {
                $response = [
                    'detail' => 'email not send',
                    'success' => false,
                ];
            } else {
                $this->db->update('user', $data, [
                    'userEmail' => $post['email'],
                ]);
                $response = [
                    'detail' => 'email success to send',
                    'password_baru' => $acak,
                    'success' => true,
                ];
            }
        } else {
            $response = [
                'detail' => 'email tidak terdaftar',
                'success' => false,
            ];
        }

        $this->response($response, 200);
    }

    public function sendsms_get()
    {
        $userkey = '3d85f931865d';
        $passkey = 'b6a81fd161d62b2087bad008';
        $telepon = '083807345096';
        $message = 'Hi John Doe, have a nice day.';
        $url = 'https://console.zenziva.net/wareguler/api/sendWA/';
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, [
            'userkey' => $userkey,
            'passkey' => $passkey,
            'to' => $telepon,
            'message' => $message,
        ]);
        $results = json_decode(curl_exec($curlHandle), true);
        curl_close($curlHandle);
        $response = [
            'result' => $results,
            'detail' => 'sms sms sms',
            'success' => true,
        ];
        $this->response($response, 200);
    }
}

/*

userId
userFullName
userMsidId
userIdentityNumber
userBRIAccountNum
userBornDate
userMsgdId
userPhone
userEmail
userPassword
token
userMsltId
userLoanAmount
userMltrId
userMsbsId
userBusinessAddress
userMsprId
userMsrgId
userMssdId
userMspcdId
userOwnerName
userHistory
userManagement
userMsptId
userMsneId
userMspcId
userOmzet
userProduced
userTargetArea
userMsbeId
userMsedId
userIsBRILink
userIsBUMDes
userLastActivity
userImgURL
userPoin
userLocation
userFacebook
userWeb
userLine
userInstagram
userSaved
userStatus
userActivationKey
userAdmin
userAdminAccess
userRefferer
userPass
userAddress
userJob
 */

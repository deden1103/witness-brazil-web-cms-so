<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Profile extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
        date_default_timezone_set('Asia/Jakarta');

        $timestamp = time();
        $date_time = date("d-m-Y (D) H:i:s", $timestamp);

        $msg = array(
            'Message' => 'Selamat datang di i-Pangan/Profile Api',
            'time' => getDateTime(strtotime(date("d-m-Y"))), //$date_time,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function updatepict_post()
    {
        $this->load->library('uploads');
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $data = array();

        // $check = getimagesize($_FILES["pict"]["tmp_name"]);
        // $base = base64_encode(file_get_contents($_FILES["pict"]["tmp_name"]));
        // $lengkap = "data:" . $check["mime"] . ";base64," . $base;
        // $data['userImgURL'] = $lengkap;

        if (@$_FILES['pict']['name'] != '') {
            $split_img = str_split($post['userId']);
            $implode_img = implode("/", $split_img);

            $file = $this->uploads->upload_image('pict', '../i-pangan.com/assets/images/user/'.$implode_img.'/');
            if (@$file['error']) {
                $response = array(
                    'message' => 'Gagal upload foto (max size 1MB only jpg,jpeg,png)',
                    'success' => false,
                );
                $this->response($response, 200);
                exit();
            }
    
            $imageName = getField('user', 'userImgURL', array('userId' => $post['userId']));
            $page = basename($imageName);

            $dir = "../i-pangan.com/assets/images/user/".$implode_img."/"  . $page;
            $data['userImgURL'] = 'http://i-pangan.com/assets/images/user/'.$implode_img.'/' . $file['file_name'];
            if (file_exists($dir)) {
                @unlink($dir);
            }

        }

        $this->db->update('user', $data, array('userId' => $post['userId']));
        $response = array(
            'img' => 'http://i-pangan.com/assets/images/user/'.$implode_img.'/' . $file['file_name'],
            'message' => 'Update Photo Berhasil',
            'success' => true,
        );
        $this->response($response, 200);
    }

    public function generateImage($img)
    {

        $folderPath = "images/";

        $image_parts = explode(";base64,", $img);

        $image_type_aux = explode("image/", $image_parts[0]);

        $image_type = $image_type_aux[1];

        $image_base64 = base64_decode($image_parts[1]);

        $file = $folderPath . uniqid() . '.png';

        file_put_contents($file, $image_base64);

    }

    public function upload_post()
    {
        if (!empty($_FILES['image_file'])) {
            $extension = 'jpg';
            if ($_FILES['image_file']['type'] == 'image/jpeg' || $_FILES['image_file']['type'] == 'image/jpg') {
                $extension = 'jpg';
            } else if ($_FILES['image_file']['type'] == 'image/gif') {
                $extension = 'gif';
            } else if ($_FILES['image_file']['type'] == 'image/png') {
                $extension = 'png';
            }
            $new_file_name = strtotime('now') . '.' . $extension;
            $move_file = move_uploaded_file($_FILES['image_file']['tmp_name'], '/opt/lampp/htdocs/elks-api.dampaksosial.com/temp_857/' . $new_file_name);
            if ($move_file) {
                $upload_image = $this->curl_post('http://elks-cms.dampaksosial.com/api/image/', array('title' => @$post['institute_name'], 'caption' => @$post['institute_name'], 'image' => curl_file_create('/opt/lampp/htdocs/elks-api.dampaksosial.com/temp_857/' . $new_file_name)));

            }
        }
    }

    public function curl_post($url = '', $request = '')
    {
        $data = array();
        if (!empty($url)) {
            $ch = curl_init($url);
            $options = array(
                CURLOPT_RETURNTRANSFER => true, // return web page
                CURLOPT_HEADER => false, // don't return headers
                CURLOPT_FOLLOWLOCATION => false, // follow redirects
                // CURLOPT_ENCODING           => "utf-8",           // handle all encodings
                CURLOPT_AUTOREFERER => true, // set referer on redirect
                CURLOPT_CONNECTTIMEOUT => 20, // timeout on connect
                CURLOPT_TIMEOUT => 20, // timeout on response
                CURLOPT_POST => 1, // i am sending post data
                //CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POSTFIELDS => $request, // this are my post vars
                CURLOPT_SSL_VERIFYHOST => 0, // don't verify ssl
                CURLOPT_SSL_VERIFYPEER => false, //
                CURLOPT_VERBOSE => 1,
            );
            curl_setopt_array($ch, $options);
            $data = curl_exec($ch);
            $curl_errno = curl_errno($ch);
            $curl_error = curl_error($ch);
            //echo 'error no : ' . $curl_errno;
            //echo 'error : ' . $curl_error;
            //print_r($data); exit();
            curl_close($ch);
        }
        return $data;
    }

}
//date('d-m-Y', strtotime($val->ptntBday))
/*
 
  if(!empty($name_of_file_image)){
            $split_img = str_split($id);
            $implode_img = implode("/", $split_img);
            
            $img_url = $this->uploads->upload_image($name_of_file_image, '../i-pangan.com/assets/images/user/'.$implode_img.'/');
           
            if(@$img_url['error']){
                $upload_image = 0;
            } else {
                $upload_image = 1;

                //$data['userImgURL'] = $file['file_name'];
                $imageName_before = getField('user', 'userImgURL', array('userId' => $id));
                $link_array = explode("/", $imageName_before);
                $getEnd_url = end($link_array);
                $dir = "../i-pangan.com/assets/images/user/".$implode_img.'/'.$getEnd_url;

                if(file_exists($dir)){
                    @unlink($dir);
                }

                $imageName = $img_url['file_name'];
                $userImgURL = base_url().'assets/images/user/'.$implode_img.'/'.$img_url['file_name'];
            }
        }


 */
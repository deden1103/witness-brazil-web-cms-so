<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') or exit('No direct script access allowed');

class Article extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Model_Komunitas', 'Model_Article']);
    }
    public function index_get()
    {
        $response = [
            'message' => 'Welcome Witness token API',
        ];
        $this->response($response, 200);
    }

    public function list_get()
    {
        $dataDetail = [];

        $page = 1;
        if ($this->get('p')) {
            $page = $this->get('p');
        }
        $rows = 3;
        $offset = ($page - 1) * $rows;

        $this->db->select('*');
        $this->db->from('article');
        // $this->db->where('artcKomtId', $this->jwtData->komunitasid);
        $this->db->where('artcUkomIdSaved', $this->jwtData->id);
        $this->db->where('artcStatus !=', 0);
        $this->db->where('artcStatus', 1);
        // artcUkomIdSaved
        $this->db->join(
            'user_komunitas',
            'article.artcUkomIdSaved = user_komunitas.ukomId',
            'LEFT'
        );
        $this->db->order_by('artcId', 'DESC');

        $this->db->limit($rows, $offset);

        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($r->artcId);
            $path_folder_image = implode('/', $split_id);
            $typpe = null;
            if ($r->artcMediaFileType == 'aac') {
                $typpe = 'sound';
            } elseif (
                $r->artcMediaFileType == 'mp4' ||
                $r->artcMediaFileType == 'mov'
            ) {
                $typpe = 'video';
            } elseif (
                $r->artcMediaFileType !== null ||
                $r->artcMediaFileType !== 'mp4' ||
                $r->artcMediaFileType !== 'aac'
            ) {
                $typpe = 'image';
            }
            $dataDetail[] = [
                'articleId' => $r->artcId,
                'title' => $r->artcTitle, // substr($r->artcTitle,0,20),
                'shortDesc' => $r->artcExcerpt,
                'longDesc' => $r->artcDetail,
                'mediaType' => $r->artcMediaFileType,
                'media' =>
                    CDN_MEDIA .
                    "{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}?" .
                    rand(),
                // "media" => CDN_MEDIA . "{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}?".rand(),
                'thumb' =>
                    'https://www.pnglot.com/pngfile/detail/494-4941278_play-button-coral-icon-video-player-icon-png.png',
                'height' =>
                    $r->heightImage == 0 || $r->heightImage == '0'
                        ? 500
                        : (int) $r->heightImage,
                'type' => $typpe, //$r->artcMediaFileType ? $this->checktype("assets/library/{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}") : null ,
                'isExist' => $r->artcMediaFileType ? true : false,

                // "artcMediaId"=> "0",
                // "artcKeyword"=> "a:5:{i:0;s:1:\"5\";i:1;s:3:\"125\";i:2;s:3:\"302\";i:3;s:3:\"303\";i:4;s:3:\"306\";}",
                // "artcMscvId"=> "64",
                'artcEmbedURL' => $r->artcEmbedURL,
                'ytid' => $this->getYoutubeId($r->artcEmbedURL),
                'artcKomtId' => $r->artcKomtId,
                'artcUkomIdSaved' => $r->artcUkomIdSaved,
                'userPict' => $r->ukomPhoto,
                // "artcSaved"=> "2019-05-09 04:00:02",
                // "artcPublishTime"=> "2019-05-25 12:00:00",
                // "artcUserIdApproved"=> null,
                'userUpload' => $r->ukomName,
                'status' => $r->artcStatus,
            ];
        }
        $response = [
            'detail' => $dataDetail,
            'success' => true,
        ];
        $this->response($response, 200);
    }

    public function notif_get()
    {
        $dataDetail = [];
        $this->db->select(
            'artcUserIdApproved,artcPublishTime,artcTitle,artcStatus'
        );
        $this->db->from('article');
        $this->db->where('artcKomtId', $this->jwtData->komunitasid);
        $this->db->where('artcStatus', 2);
        // $this->db->where('artcUkomIdSaved', $this->jwtData->id);

        // $this->db->join('user_komunitas', 'article.artcUkomIdSaved = user_komunitas.ukomId');
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $dataDetail[] = [
                // "userApproveId" => $r->artcUserIdApproved,
                // "userApproveDetail" => $this->Model_Komunitas->getDetailUser($r->artcUserIdApproved)
                'username' => getField('admin', 'admiRealName', [
                    'admiId' => $r->artcUserIdApproved,
                ]),
                'artcTitle' => $r->artcTitle,
                'artcPublishTime' => $r->artcPublishTime,
            ];
        }

        $sorted = $this->array_orderby(
            $dataDetail,
            'artcPublishTime',
            SORT_DESC
        );

        $response = [
            'detail' => $sorted,
            'success' => true,
        ];
        $this->response($response, 200);
    }

    public function mynotif_get()
    {
        $dataDetail = [];
        $this->db->select(
            'artcUserIdApproved,artcPublishTime,artcTitle,artcStatus'
        );
        $this->db->from('article');
        $this->db->where('artcUkomIdSaved', $this->jwtData->id);
        $this->db->where('artcKomtId', $this->jwtData->komunitasid);
        $this->db->where('artcStatus', 2);

        // $this->db->join('user_komunitas', 'article.artcUkomIdSaved = user_komunitas.ukomId');
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $dataDetail[] = [
                // "userApproveId" => $r->artcUserIdApproved,
                // "userApproveDetail" => $this->Model_Komunitas->getDetailUser($r->artcUserIdApproved)
                'username' => getField('admin', 'admiRealName', [
                    'admiId' => $r->artcUserIdApproved,
                ]),
                'artcTitle' => $r->artcTitle,
                'artcPublishTime' => $r->artcPublishTime,
            ];
        }

        $sorted = $this->array_orderby(
            $dataDetail,
            'artcPublishTime',
            SORT_DESC
        );

        $response = [
            'detail' => $sorted,
            'success' => true,
        ];
        $this->response($response, 200);
    }

    public function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = [];
                foreach ($data as $key => $row) {
                    $tmp[$key] = $row[$field];
                }

                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }

    public function mylist_get()
    {
        $dataDetail = [];
        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('artcUkomIdSaved', $this->jwtData->id);
        $this->db->join(
            'user_komunitas',
            'article.artcUkomIdSaved = user_komunitas.ukomId'
        );
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($r->artcId);
            $path_folder_image = implode('/', $split_id);
            $dataDetail[] = [
                'articleId' => $r->artcId,
                'title' => $r->artcTitle,
                'shortDesc' => $r->artcExcerpt,
                'longDesc' => $r->artcDetail,
                'mediaType' => $r->artcMediaFileType,
                'media' =>
                    CDN_MEDIA .
                    "{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}",
                // "media" => CDN_MEDIA . "{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}?".rand(),
                // "type" => $this->checktype("assets/library/{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}"),
                // "artcMediaId"=> "0",
                // "artcKeyword"=> "a:5:{i:0;s:1:\"5\";i:1;s:3:\"125\";i:2;s:3:\"302\";i:3;s:3:\"303\";i:4;s:3:\"306\";}",
                // "artcMscvId"=> "64",
                // "artcEmbedURL"=> "https://players.brightcove.net/4077388032001/default_default/index.html?videoId=6034016273001 ",
                'artcKomtId' => $r->artcKomtId,
                'artcUkomIdSaved' => $r->artcUkomIdSaved,
                // "artcSaved"=> "2019-05-09 04:00:02",
                // "artcPublishTime"=> "2019-05-25 12:00:00",
                // "artcUserIdApproved"=> null,
                'userUpload' => $r->ukomName,
                'status' => $r->artcStatus,
            ];
        }
        $response = [
            'detail' => $dataDetail,
            'success' => true,
        ];
        $this->response($response, 200);
    }

    public function detail_get()
    {
        $dataDetail = [];
        $idArtc = $this->get('id');

        $this->db->select('*');
        $this->db->from('article');
        // $this->db->where('artcKomtId', $this->jwtData->komunitasid);
        $this->db->where('artcId', $idArtc);
        $this->db->join(
            'user_komunitas',
            'article.artcUkomIdSaved = user_komunitas.ukomId'
        );
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($r->artcId);
            $path_folder_image = implode('/', $split_id);
            $typpe = null;
            if ($r->artcMediaFileType == 'aac') {
                $typpe = 'sound';
            } elseif ($r->artcMediaFileType == 'mp4') {
                $typpe = 'video';
            } elseif (
                $r->artcMediaFileType !== null ||
                $r->artcMediaFileType !== 'mp4' ||
                $r->artcMediaFileType !== 'aac'
            ) {
                $typpe = 'image';
            }
            $dataDetail[] = [
                'articleId' => $r->artcId,
                'title' => $r->artcTitle,
                'shortDesc' => $r->artcExcerpt,
                'longDesc' => $r->artcDetail,
                'mediaType' => $r->artcMediaFileType,
                'media' =>
                    CDN_MEDIA .
                    "{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}",
                'type' => $typpe, //$r->artcMediaFileType ? $this->checktype("assets/library/{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}") : null ,
                'isExist' => $r->artcMediaFileType ? true : false,
                'height' => (int) $r->heightImage,
                // "artcMediaId"=> "0",
                // "artcKeyword"=> "a:5:{i:0;s:1:\"5\";i:1;s:3:\"125\";i:2;s:3:\"302\";i:3;s:3:\"303\";i:4;s:3:\"306\";}",
                // "artcMscvId"=> "64",
                // "artcEmbedURL"=> "https://players.brightcove.net/4077388032001/default_default/index.html?videoId=6034016273001 ",
                'artcKomtId' => $r->artcKomtId,
                'artcUkomIdSaved' => $r->artcUkomIdSaved,
                // "artcSaved"=> "2019-05-09 04:00:02",
                // "artcPublishTime"=> "2019-05-25 12:00:00",
                // "artcUserIdApproved"=> null,
                'userUpload' => $r->ukomName,
                'status' => $r->artcStatus,
            ];
        }
        $response = [
            'detail' => $dataDetail,
            'success' => true,
        ];
        $this->response($response, 200);

        // $array_article = array();
        // $idArtc = $this->get('id');

        // $this->db->select('*');
        // $this->db->from('article');
        // $this->db->where('artcId', $idArtc);
        // $query = $this->db->get()->result();

        // foreach ($query as $r) {

        //     $array_article = array(
        //         "articleId" => $r->artcId,
        //         "title" => $r->artcTitle,
        //         "shortDesc" => $r->artcExcerpt,
        //         "longDesc" => $r->artcDetail,
        //         "artcId" => $r->artcDetail,
        //         "artcTitle" => $r->artcTitle,
        //         "artcExcerpt" => $r->artcExcerpt,
        //         "artcDetail" => $r->artcDetail,
        //         "artcMediaId" => $r->artcMediaId,
        //         "artcMediaFileType" => $r->artcMediaFileType,
        //         "artcKeyword" => $r->artcKeyword,
        //         "artcMscvId" => $r->artcMscvId,
        //         "artcEmbedURL" => $r->artcEmbedURL,
        //         "artcKomtId" => $r->artcKomtId,
        //         "artcUkomIdSaved"=> $r->artcUkomIdSaved,
        //         "artcSaved"=> $r->artcSaved,
        //         "artcPublishTime"=> $r->artcPublishTime,
        //         "artcUserIdApproved"=> $r->artcUserIdApproved,
        //         "artcStatus"=> $r->artcStatus,
        //     );
        // }

        // $response = array(
        //     'detail' => $array_article,
        //     'success' => true,
        // );
        // $this->response($response, 200);
    }

    public function add_post()
    {
        $this->load->library('upload');
        $post = $this->post();
        $heightImage = @$post['hg'];
        $isShowUser = @$post['isshow'] == '1' ? '0' : '1';
        $this->form_validation->set_data($this->post());
        $structure = '';
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('desc', 'desc', 'trim');
        //$this->form_validation->set_rules('tyep', 'type', 'trim|required');

        $this->form_validation->set_message('required', '{field} harus diisi');

        if ($this->form_validation->run() == true) {
            $data = [
                'artcTitle' => @$post['title'],
                'artcDetail' => @$post['desc'],
                'artcExcerpt' => substr(@$post['desc'], 0, 150),
                'artcKomtId' => $this->jwtData->komunitasid,
                'artcUkomIdSaved' => $this->jwtData->id,
                'latitude' => @$post['latitude'],
                'longitude' => @$post['longitude'],
                'heightImage' => @$post['hg'],
                'isShowUser' => $isShowUser,
            ];
            if (@$_FILES['file']['name'] != '') {
                $file_image = @$_FILES['file'];
                $file_image_extension = pathinfo(
                    basename($file_image['name']),
                    PATHINFO_EXTENSION
                );

                $data['artcMediaFileType'] = $file_image_extension;
                $data['heightImage'] = $heightImage;
            }
            // $this->db->insert('article', $data);

            $upload_data_image_id = $this->Model_Komunitas->save_arsip_images(
                'article',
                $data
            );

            $id = $upload_data_image_id;
            $splitId = str_split($id);

            if (@$_FILES['file']['name'] != '') {
                foreach ($splitId as $k => $v) {
                    $valArr[] = $v . '/';
                }
                $structure = implode($valArr);
                if (!file_exists('assets/library/' . $structure)) {
                    mkdir('assets/library/' . $structure, 0777, true);
                }
                $config['upload_path'] = 'assets/library/' . $structure;

                //upload image to recursive folder
                $max_width = '10288';
                $max_height = '7068';
                $config['allowed_types'] = '*';
                $config['max_size'] = '120000048'; //max 2mb
                $config['max_width'] = $max_width; //max lebar
                $config['max_height'] = $max_height; //max tinggi
                $config['file_name'] = $id;
                $config['overwrite'] = true;

                $this->upload->initialize($config);
                $this->upload->do_upload('file');
            }


            $queryListEditor = $this->Model_Article->list_editor($this->jwtData->komunitasid);
            foreach ($queryListEditor as $k => $v) {
                if ($v['phone']) {
                    $this->send_notif_new_article($v['phone'],$v['komtName']);
                }
            }

            $response = [
                'message' => 'input  Berhasil',
                // 'str' => $structure,
                'id' => $upload_data_image_id,
                'success' => true,
            ];
        } else {
            $response = [
                'message' => 'Maaf, Registrasi gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            ];
        }
        $this->response($response, 200);
    }

    public function add_ios_post()
    {
        $this->load->library('upload');
        $post = $this->post();
        $heightImage = @$post['hg'];
        $isShowUser = @$post['isshow'] == '1' ? '0' : '1';
        $this->form_validation->set_data($this->post());
        $structure = '';
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('desc', 'desc', 'trim');
        //$this->form_validation->set_rules('tyep', 'type', 'trim|required');

        $this->form_validation->set_message('required', '{field} harus diisi');

        if ($this->form_validation->run() == true) {
            $data = [
                'artcTitle' => @$post['title'],
                'artcDetail' => @$post['desc'],
                'artcExcerpt' => substr(@$post['desc'], 0, 150),
                'artcKomtId' => $this->jwtData->komunitasid,
                'artcUkomIdSaved' => $this->jwtData->id,
                'latitude' => @$post['latitude'],
                'longitude' => @$post['longitude'],
                'heightImage' => 0, //@$post['hg'],
                'isShowUser' => $isShowUser,
            ];
            if (@$_FILES['file']['name'] != '') {
                $file_image = @$_FILES['file'];
                $file_image_extension = pathinfo(
                    basename($file_image['name']),
                    PATHINFO_EXTENSION
                );

                $data['artcMediaFileType'] = $file_image_extension;
                // $data['heightImage'] = $heightImage;
            }

            $upload_data_image_id = $this->Model_Komunitas->save_arsip_images(
                'article',
                $data
            );
            $id = $upload_data_image_id;
            $splitId = str_split($id);

            if (@$_FILES['file']['name'] != '') {
                foreach ($splitId as $k => $v) {
                    $valArr[] = $v . '/';
                }
                $structure = implode($valArr);
                if (!file_exists('assets/library/' . $structure)) {
                    mkdir('assets/library/' . $structure, 0777, true);
                }
                $config['upload_path'] = 'assets/library/' . $structure;

                //upload image to recursive folder
                $max_width = '10288';
                $max_height = '10288';
                $config['allowed_types'] = '*';
                $config['max_size'] = '1200000000000048'; //max 2mb
                $config['max_width'] = $max_width; //max lebar
                $config['max_height'] = $max_height; //max tinggi
                $config['file_name'] = $id;
                $config['overwrite'] = true;

                $this->upload->initialize($config);
                $this->upload->do_upload('file');
            }


            $queryListEditor = $this->Model_Article->list_editor($this->jwtData->komunitasid);
            foreach ($queryListEditor as $k => $v) {
                if ($v['phone']) {
                    $this->send_notif_new_article($v['phone'],$v['komtName']);
                }
            }

            $response = [
                'message' => 'input  Berhasil',
                'success' => true,
            ];
        } else {
            $response = [
                'message' => 'Maaf, Input gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            ];
        }
        $this->response($response, 200);
    }

    public function add_background_post()
    {
        $this->load->library('upload');
        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $structure = '';
        // $this->form_validation->set_rules('tyep', 'type', 'trim|required');
        // 'arid' => $this->input->get_request_header('arid')
        $articleId = $this->input->get_request_header('arid');
        $heightImage = @$post['hg'];
        $data = [];
        if (@$_FILES['file']['name'] != '') {
            $file_image = @$_FILES['file'];
            $file_image_extension = pathinfo(
                basename($file_image['name']),
                PATHINFO_EXTENSION
            );

            $data['artcMediaFileType'] = $file_image_extension;
            // $data['heightImage'] = $heightImage;

            // $extt = $file_image_extension;
            // $himg = $heightImage;
        }

        $this->db->update('article', $data, ['artcId' => $articleId]);

        // $id='';
        // if ($articleId) {
        //     $id = $articleId;
        //     $this->db->update('article',
        //         array(
        //             'artcMediaFileType' => $extt,
        //             'heightImage' => $himg,
        //         ), array('artcId' => $articleId));
        // } else {
        //     $upload_data_image_id = $this->Model_Komunitas->save_arsip_images('article', $data);
        //     $id = $upload_data_image_id;
        // }

        $splitId = str_split($articleId);

        if (@$_FILES['file']['name'] != '') {
            foreach ($splitId as $k => $v) {
                $valArr[] = $v . '/';
            }
            $structure = implode($valArr);
            if (!file_exists('assets/library/' . $structure)) {
                mkdir('assets/library/' . $structure, 0777, true);
            }
            $config['upload_path'] = 'assets/library/' . $structure;

            //upload image to recursive folder
            $max_width = '10288';
            $max_height = '10288';
            $config['allowed_types'] = '*';
            $config['max_size'] = '120000000000000048'; //max 2mb
            $config['max_width'] = $max_width; //max lebar
            $config['max_height'] = $max_height; //max tinggi
            $config['file_name'] = $articleId;
            $config['overwrite'] = true;

            $this->upload->initialize($config);
            $this->upload->do_upload('file');
        }

        $response = [
            'message' => 'input File Berhasil',
            // 'str' => $structure,
            //'id' => $articleId ? $articleId : $id,
            'success' => true,
        ];

        $this->response($response, 200);
    }

    public function edit_post()
    {
        $this->load->library('upload');
        $post = $this->post();
        $articleId = @$post['article_id'];
        $this->form_validation->set_data($this->post());
        $structure = '';
        // $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules(
            'article_id',
            'article_id',
            'trim|required'
        );
        //$this->form_validation->set_rules('tyep', 'type', 'trim|required');

        $this->form_validation->set_message('required', '{field} harus diisi');

        // $file_image = $_FILES['file'];
        // $file_image_extension = pathinfo(basename($file_image['name']), PATHINFO_EXTENSION);
        // $extForDelete = getField('article', 'artcMediaFileType', array('artcId' => $articleId));
        if ($this->form_validation->run() == true) {
            $data = [
                'artcTitle' => @$post['title'],
                'artcDetail' => @$post['desc'],
                'artcExcerpt' => substr(@$post['desc'], 0, 150),
                'artcKomtId' => $this->jwtData->komunitasid,
                'artcUkomIdSaved' => $this->jwtData->id,
                // 'latitude' => @$post['latitude'],
                // 'longitude' => @$post['longitude'],
            ];

            if ($post['latitude'] != '' && $post['longitude'] != '') {
                $data['latitude'] = $post['latitude'];
                $data['longitude'] = $post['longitude'];
            }
            if (@$_FILES['file']['name'] != '') {
                $file_image = $_FILES['file'];
                $file_image_extension = pathinfo(
                    basename($file_image['name']),
                    PATHINFO_EXTENSION
                );
                $extForDelete = getField('article', 'artcMediaFileType', [
                    'artcId' => $articleId,
                ]);
                $data['artcMediaFileType'] = $file_image_extension;
            }
            // $this->db->insert('article', $data);

            // $upload_data_image_id = $this->Model_Komunitas->save_arsip_images('article', $data);

            $this->db->update('article', $data, ['artcId' => $articleId]);

            $id = $articleId;
            $splitId = str_split($id);

            if (@$_FILES['file']['name'] != '') {
                foreach ($splitId as $k => $v) {
                    $valArr[] = $v . '/';
                }
                $structure = implode($valArr);
                // if (!file_exists("assets/library/" . $structure)) {
                //     mkdir("assets/library/" . $structure, 0777, true);
                // }
                $dir =
                    'assets/library/' .
                    $structure .
                    $articleId .
                    '.' .
                    $extForDelete;
                if (file_exists($dir)) {
                    @unlink($dir);
                }
                $config['upload_path'] = 'assets/library/' . $structure;

                //upload image to recursive folder
                $max_width = '10288';
                $max_height = '7068';
                $config['allowed_types'] = '*';
                $config['max_size'] = '120000048'; //max 2mb
                $config['max_width'] = $max_width; //max lebar
                $config['max_height'] = $max_height; //max tinggi
                $config['file_name'] = $id;
                $config['overwrite'] = true;

                $this->upload->initialize($config);
                $this->upload->do_upload('file');
            }

            $response = [
                'message' => 'edit  Berhasil',
                // 'str' => $structure,
                // 'dir' => $dir,
                'success' => true,
            ];
        } else {
            $response = [
                'message' => 'Maaf, edit gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            ];
        }
        $this->response($response, 200);
    }

    public function delete_get()
    {
        $ai = $this->get('id');
        $this->db->delete('article', ['artcId' => $ai]);
        $response = [
            'message' => 'berhasil dihapus',
            'success' => true,
        ];
        $this->response($response, 200);
    }

    public function checktype2($typ)
    {
        // $mimeType = mime_content_type($fle);
        // if (strstr($mimeType, "video/")) {
        //     $filetype = "video";
        // } else if (strstr($mimeType, "image/")) {
        //     $filetype = "image";
        // } else if (strstr($mimeType, "audio/")) {
        //     $filetype = "audio";
        // }
        // return $filetype;
        $filetype = '';
        $imageType = ['Mac', 'NT', 'Irix', 'Linux'];
        $videoType = ['Mac', 'NT', 'Irix', 'Linux'];
        $soundType = ['Mac', 'NT', 'Irix', 'Linux'];
        if (in_array($typ, $imageType)) {
            $filetype = 'image';
        }
        if (in_array($typ, $videoType)) {
            $filetype = 'video';
        }
        if (in_array($typ, $soundType)) {
            $filetype = 'sound';
        }
        return $filetype;
    }

    public function checktype($fle)
    {
        $mimeType = mime_content_type($fle);
        if (strstr($mimeType, 'video/')) {
            $filetype = 'video';
        } elseif (strstr($mimeType, 'image/')) {
            $filetype = 'image';
        } elseif (strstr($mimeType, 'audio/')) {
            $filetype = 'sound';
        }
        return $filetype;
    }

    public function getYoutubeId($url)
    {
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
        $longUrlRegex =
            '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';
        $youtube_id = '';
        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }

        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return $youtube_id;
    }

    public function list_editor_get()
    {
        $queryListEditor = $this->Model_Article->list_editor($this->jwtData->komunitasid);
        // foreach ($queryListEditor as $k => $v) {
        //     if ($v['phone']) {
        //         $this->send_notif_new_article($v['phone'],$v['komtName']);
        //     }
        // }

        $response = [
            'data' => $queryListEditor,
            'success' => true,
            'message' => 'List editor API',
        ];
        $this->response($response, 200);
    }

    public function send_notif_new_article($nomor = '',$komunitas='')
    {
        $messageText = "Editor, ada laporan JR ({$komunitas}), sila diedit. Terima kasih.";
        if ($nomor == '') {
            
        } else {
            $userkey = '3d85f931865d';
            $passkey = 'b6a81fd161d62b2087bad008';
            $telepon = $nomor;
            $message = $messageText;
            $url = 'https://console.zenziva.net/wareguler/api/sendWA/';
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_URL, $url);
            curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
            curl_setopt($curlHandle, CURLOPT_POST, 1);
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, [
                'userkey' => $userkey,
                'passkey' => $passkey,
                'to' => $telepon,
                'message' => $message,
            ]);
            $results = json_decode(curl_exec($curlHandle), true);
            curl_close($curlHandle);
        }
    }
}

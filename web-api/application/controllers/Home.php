<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Home extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Model_Berita']);
    }

    public function index_get()
    {
        date_default_timezone_set('Asia/Jakarta');

        $timestamp = time();
        $date_time = date("d-m-Y (D) H:i:s", $timestamp);

        $msg = array(
            'Message' => 'Selamat datang di i-Pangan/home Api',
            'time' => getDateTime(strtotime(date("d-m-Y"))), //$date_time,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function berita_terbaru_get()
    {
        $array_berita = [];
        $this->db->select('*');
        $this->db->from('news');
        $this->db->where('newsStatus', 1);
        $this->db->limit(5);
        $this->db->order_by('newsId', 'DESC');
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $array_berita[] = array(
                'newsId' => $r->newsId,
                'newsTitle' => $r->newsTitle,
                'newsMsctId' => $r->newsMsctId,
                'newsExcerpt' => $r->newsExcerpt,
                'newsDetail' => $r->newsDetail,
                'newsPublishTime' => $r->newsPublishTime,
                'newsSaved' => timeAgo($r->newsSaved),
                'newsAdmiId' => $r->newsAdmiId,
                'newsStatus' => $r->newsStatus,
                'newsSticky' => $r->newsSticky,
                'newsPushNotif' => $r->newsPushNotif,
                'images' => $this->Model_Berita->get_all_images($r->newsId),
            );
        }

        $msg = array(
            'data' => $array_berita,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function video_terbaru_get()
    {
        $array_vid[] = [];
        $this->db->select('*');
        $this->db->from('video');
        // $this->db->where('videStatus',1);
        $this->db->limit(5);
        $this->db->order_by('videId', 'DESC');
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $array_vid[] = [
                'videId' => $r->videId,
                'videMsctId' => $r->videMsctId,
                'videTitle' => $r->videTitle,
                'videExcerpt' => $r->videExcerpt,
                'videStart' => $r->videStart,
                'videEnd' => $r->videEnd,
                'videStreamURL' => $r->videStreamURL,
                'videSaved' => timeAgo($r->videSaved),
                'videAdmiId' => $r->videAdmiId,
                'videStatus' => $r->videStatus,
                'videoPushNotif' => $r->videoPushNotif,
            ];
        }

        $msg = array(
            'data' => $array_vid,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function investasi_terbaru_get()
    {
        $array_inv = [];
        $this->db->select('*');
        $this->db->from('investasi');
        // $this->db->where('videStatus', 1);
        $this->db->limit(5);
        $this->db->order_by('investasiId', 'DESC');
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $array_inv[] = [
                'investasiId' => $r->investasiId,
                'investasiName' => $r->investasiName,
                'investasiFirstResult' => $r->investasiFirstResult,
                'investasiNominal' => $r->investasiNominal,
                'investasiPanen' => $r->investasiPanen,
                'investasiProfit' => $r->investasiProfit,
                'investasiContract' => $r->investasiContract,
                'investasiType' => $r->investasiType,
                'investasiSlot' => $r->investasiSlot,
                'createDate' => $r->createDate,
                'updateDate' => $r->updateDate,
                'createDateAgo' => timeAgo($r->createDate),
                'updateDateAgo' => timeAgo($r->updateDate),
            ];
        }

        $msg = array(
            'data' => $array_inv,
            'success' => true,
        );
        $this->response($msg, 200);
    }


    public function faq_get()
    {
        $array_inv = [];
        $this->db->select('*');
        $this->db->from('faq');
        // $this->db->where('videStatus', 1);
        // $this->db->limit(5);
        // $this->db->order_by('investasiId', 'DESC');
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $array_inv[] = [
                'faqId' => $r->faqId,
                'question' => $r->question,
                'answer' => $r->answer,
                'faqImage' => $r->faqImage,
                'bg' => $r->bg
            ];
        }

        $msg = array(
            'data' => $array_inv,
            'success' => true,
        );
        $this->response($msg, 200);
    }

}
//date('d-m-Y', strtotime($val->ptntBday))

<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Main extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
        // phpinfo();
        // $this->load->library('Ffmpeglib');
        // Ffmpeglib::create_thumb('./assets/library/9/9/99.mp4');
        date_default_timezone_set('Asia/Jakarta');

        // $link = $_SERVER['PHP_SELF'];assets/library/9/9/99.mp4
        // $link_array = explode('/','http://i-pangan.com/news/detail/128/jahe-jadi-andalan-kabupaten-garut.html');
        // $page = end($link_array);

        $path = "/home/httpd/html/index.php";
        $file = basename($path); // $file is set to "index.php"
        // $file = basename($path, ".php");

        $timestamp = time();
        $date_time = date("d-m-Y (D) H:i:s", $timestamp);
        $jam = explode(' ', date("Y-m-d H:i:s"));
        $msg = array(
            'Message' => 'Selamat datang di Witness Api',
            'time' => getDateTime(strtotime(date("d-m-Y"))), //$date_time,
            // 'file' => $file,
            //'now' => date("Y-m-d H:i:s"), //$date_time,
            //'jam' => $jam[1],
            'success' => true,
            //'firstname' => $this->getFirstName('deden ramdani hjgg ghghjg gh'),
            //'yt' =>basename('http://i-pangan.com/news/detail/128/jahe-jadi-andalan-kabupaten-garut.html')// $this->get_video_thumbnail('https://www.youtube.com/watch?v=7booLy9reio'),
        );
        $this->response($msg, 200);
    }

    public function list_get()
    {
        // $searchtext = $this->get('s');
        // $array_berita = array();
        // $page = 1;
        // if ($this->get('p')) {
        //     $page = $this->get('p');
        // }
        // $rows = 10;
        // $offset = ($page - 1) * $rows;

        $this->db->select('*');
        $this->db->from('article');

        $query = $this->db->get()->result();

        $msg = array(
            'data' => $query,
            'success' => true,
        );
        $this->response($msg, 200);
    }

    public function getFirstName($name)
    {
        $trim = trim($name);
        $nameParts = explode(' ', $trim);
        $firstName = $nameParts[0];
        return $firstName;
    }

    public function get_video_thumbnail($src)
    {
        $url_pieces = explode('/', $src);
        if ($url_pieces[2] == 'dai.ly') {
            $id = $url_pieces[3];
            $hash = json_decode(file_get_contents('https://api.dailymotion.com/video/' . $id . '?fields=thumbnail_large_url'), true);
            $thumbnail = $hash['thumbnail_large_url'];
        } else if ($url_pieces[2] == 'www.dailymotion.com') {
            $id = $url_pieces[4];
            $hash = json_decode(file_get_contents('https://api.dailymotion.com/video/' . $id . '?fields=thumbnail_large_url'), true);
            $thumbnail = $hash['thumbnail_large_url'];
        } else if ($url_pieces[2] == 'vimeo.com') { // If Vimeo
            $id = $url_pieces[3];
            $hash = unserialize(file_get_contents('http://vimeo.com/api/v2/video/' . $id . '.php'));
            $thumbnail = $hash[0]['thumbnail_large'];
        } elseif ($url_pieces[2] == 'youtu.be') { // If Youtube
            $extract_id = explode('?', $url_pieces[3]);
            $id = $extract_id[0];
            $thumbnail = 'http://img.youtube.com/vi/' . $id . '/mqdefault.jpg';
        } else if ($url_pieces[2] == 'player.vimeo.com') { // If Vimeo
            $id = $url_pieces[4];
            $hash = unserialize(file_get_contents('http://vimeo.com/api/v2/video/' . $id . '.php'));
            $thumbnail = $hash[0]['thumbnail_large'];
        } elseif ($url_pieces[2] == 'www.youtube.com') { // If Youtube
            $extract_id = explode('=', $url_pieces[3]);
            $id = $extract_id[1];
            $thumbnail = 'http://i3.ytimg.com/vi/' . $id . '/mqdefault.jpg';
        } else {
            $thumbnail = tim_thumb_default_image('video-icon.png', null, 147, 252);
        }
        return $thumbnail;
    }

    public function editcomment_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $table = @$post['table'];
        $tableid = @$post['tableid'];
        $tabletext = @$post['tabletext'];
        $id = @$post['id'];
        $text = @$post['text'];

        $this->form_validation->set_rules('id', 'id', 'trim|required',
            array(
                'required' => 'userid harus diisi',
            )
        );

        $this->form_validation->set_rules('text', 'text', 'trim|required',
            array(
                'required' => 'text harus diisi',
            )
        );

        if ($this->form_validation->run() == true) {
            $data = array(
                $tableid => $id,
                $tabletext => $text,
            );

            $this->db->update($table, $data, [$tableid => $id]);
            $response = array(
                'message' => 'post berhasil',
                'success' => true,
            );
        } else {
            $response = array(
                'message' => 'Maaf, post response gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            );
        }
        $this->response($response, 200);
    }

    public function pushnotif_get()
    {
        $content = array(
            "en" => 'testing',
        );

        $fields = array(
            'app_id' => "04f752ea-ae43-42a1-8488-75b945eb9447",
            'included_segments' => array(
                'All',
            ),
            'data' => array(
                "foo" => "bar",
            ),
            'contents' => $content,

        );

        $fields = json_encode($fields);
        // print("\nJSON sent:\n");
        // print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic YTEyNmNhYTgtOTI0Yy00NGNjLWFmMmUtNzQyMWJiYzE4N2M5',
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);
    }

    public function get_token_get()
    {
        $username = $this->get('username');
        date_default_timezone_set("Asia/Jakarta");
        $tanggal = date('Ymd');
        $login = strtoupper($username);
        $toooo = MD5('golfjgc123' . $tanggal . $login . $tanggal . '123golfjgc');

        $response = array(
            'data' => $toooo,
            'success' => true,
        );

        $this->response($response, 200);
    }

    public function tes_bni_post()
    {
        $this->load->library('BniEnc');

        $client_id = '00001';
        $secret_key = '4654564654654654645';
        $url = 'https://apibeta.bni-ecollection.com/';

        $data_asli = array(
            'client_id' => $client_id,
            'trx_id' => mt_rand(), // fill with Billing ID
            'trx_amount' => 10000,
            'billing_type' => 'c',
            'datetime_expired' => date('c', time() + 2 * 3600), // billing will be expired in 2 hours
            'virtual_account' => '',
            'customer_name' => 'Mr. X',
            'customer_email' => '',
            'customer_phone' => '',
        );

        $hashed_string = BniEnc::encrypt(
            $data_asli,
            $client_id,
            $secret_key
        );

        $data = array(
            'client_id' => $client_id,
            'data' => $hashed_string,
        );

        $response = $this->get_content($url, json_encode($data));
        $response_json = json_decode($response, true);

        if ($response_json['status'] !== '000') {
            // handling jika gagal
            var_dump($response_json);
        } else {
            $data_response = BniEnc::decrypt($response_json['data'], $client_id, $secret_key);
            // $data_response will contains something like this:
            // array(
            //     'virtual_account' => 'xxxxx',
            //     'trx_id' => 'xxx',
            // );
            var_dump($data_response);
        }

        // $response = array(
        //     'data' =>  'bni',
        //     'success' => true,
        // );

        // $this->response($response, 200);
    }

    public function get_content($url, $post = '')
    {
        $usecookie = __DIR__ . "/cookie.txt";
        $header[] = 'Content-Type: application/json';
        $header[] = "Accept-Encoding: gzip, deflate";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        // curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");

        if ($post) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $rs = curl_exec($ch);

        if (empty($rs)) {
            var_dump($rs, curl_error($ch));
            curl_close($ch);
            return false;
        }
        curl_close($ch);
        return $rs;
    }

    public function sendsms($nomor = "", $code = "")
    {
        if ($nomor == "" || $code == "") {
            echo 'tidak ada nomor atau kode';
        } else {
            // $userkey = '3d85f931865d';
            // $passkey = 'b6a81fd161d62b2087bad008';
            // $telepon = $nomor;
            // $message = 'Kode OTP anda adalah '.$code." jangan dibagikan kepada orang lain. Terima kasih.";
            // $url = 'https://console.zenziva.net/reguler/api/sendsms/';
            // $curlHandle = curl_init();
            // curl_setopt($curlHandle, CURLOPT_URL, $url);
            // curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            // curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
            // curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
            // curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
            // curl_setopt($curlHandle, CURLOPT_POST, 1);
            // curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
            //     'userkey' => $userkey,
            //     'passkey' => $passkey,
            //     'to' => $telepon,
            //     'message' => $message,
            // ));
            // $results = json_decode(curl_exec($curlHandle), true);
            // print_r($results);
            // curl_close($curlHandle);
        }

    }

}
//date('d-m-Y', strtotime($val->ptntBday))

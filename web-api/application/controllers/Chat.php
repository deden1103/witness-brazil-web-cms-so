<?php
header("Access-Control-Allow-Origin: *");
defined('BASEPATH') or exit('No direct script access allowed');

class Chat extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Model_Komunitas'));
    }
    public function index_get()
    {
        $response = array(
            'message' => 'Welcome Witness Chat API',
        );
        $this->response($response, 200);
    }

    public function list_get(){
        $dataDetail = array();
        $artcId = $this->get('artcid');
        $page = 1;
        if ($this->get('p')) {
            $page = $this->get('p');
        }
        $rows = 10;
        $offset = ($page - 1) * $rows;


        $this->db->select('*');
        $this->db->from('chat');
        $this->db->where('chatArtcId',$artcId);
        // $this->db->join('user_komunitas', 'article.artcUkomIdSaved = user_komunitas.ukomId');
        // $this->db->order_by('artcId','DESC');

        // $this->db->limit($rows, $offset);

        
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $dataDetail[] = array(
                "chatId" => $r->chatId,
                "nama" =>  $r->chatAdmiId ? 'admin' : getField('user_komunitas','ukomName',array('ukomId' => $r->chatUkomId)),
                "pesan" =>  $r->chatFilteredMessage,
                "time" => $r->chatSaved,
                "isShowAvatar" => '',
                "artcid" => $r->chatArtcId,

                "chatArtcId"=> $r->chatArtcId,
                "chatUkomId"=> $r->chatUkomId,
                "chatAdmiId"=> $r->chatAdmiId,
                "chatRealMessage"=> $r->chatRealMessage,
                "chatFilteredMessage"=> $r->chatFilteredMessage,
                "chatSaved"=> $r->chatSaved,
                "chatStatus"=> $r->chatStatus,
            );
        }
        $response = array(
            'title' => getField('article', 'artcTitle', array('artcId' => $artcId)),
            'detail' => $dataDetail,
            'success' => true,
        );
        $this->response($response, 200);
    }

    public function chat_delete_get(){
        $ai = $this->get('id');
        $this->db->delete('chat', array('chatArtcId' => $ai));
        $response = array(
            'message' => 'berhasil dihapus',
            'success' => true,
        );
        $this->response($response, 200);
    }

   
}

/*

 public function list_get()
    {
        $dataDetail = array();

        $page = 1;
        if ($this->get('p')) {
            $page = $this->get('p');
        }
        $rows = 10;
        $offset = ($page - 1) * $rows;


        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('artcKomtId', $this->jwtData->komunitasid);
        $this->db->join('user_komunitas', 'article.artcUkomIdSaved = user_komunitas.ukomId');
        $this->db->order_by('artcId','DESC');

        $this->db->limit($rows, $offset);

        
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($r->artcId);
            $path_folder_image = implode('/', $split_id);
            $typpe = null;
            if($r->artcMediaFileType == "aac"){
                $typpe = "sound";
            }else if($r->artcMediaFileType == "mp4"){
                $typpe = "video";
            }else if($r->artcMediaFileType !== null || $r->artcMediaFileType !== "mp4" || $r->artcMediaFileType !== "aac" ){
                $typpe = "image";
            }
            $dataDetail[] = array(
                "articleId" => $r->artcId,
                "title" =>$r->artcTitle,// substr($r->artcTitle,0,20),
                "shortDesc" => $r->artcExcerpt,
                "longDesc" => $r->artcDetail,
                "mediaType" => $r->artcMediaFileType,
                "media" =>  CDN_MEDIA . "{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}",
                "type" =>  $typpe, //$r->artcMediaFileType ? $this->checktype("assets/library/{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}") : null ,
                "isExist" => $r->artcMediaFileType ? true : false,

                // "artcMediaId"=> "0",
                // "artcKeyword"=> "a:5:{i:0;s:1:\"5\";i:1;s:3:\"125\";i:2;s:3:\"302\";i:3;s:3:\"303\";i:4;s:3:\"306\";}",
                // "artcMscvId"=> "64",
                // "artcEmbedURL"=> "https://players.brightcove.net/4077388032001/default_default/index.html?videoId=6034016273001 ",
                "artcKomtId" => $r->artcKomtId,
                "artcUkomIdSaved" => $r->artcUkomIdSaved,
                "userPict" => $r->ukomPhoto,
                // "artcSaved"=> "2019-05-09 04:00:02",
                // "artcPublishTime"=> "2019-05-25 12:00:00",
                // "artcUserIdApproved"=> null,
                'userUpload' => $r->ukomName,
                "status" => $r->artcStatus,
            );
        }
        $response = array(
            'detail' => $dataDetail,
            'success' => true,
        );
        $this->response($response, 200);
    }

    public function notif_get(){

        $dataDetail = array();
        $this->db->select('artcUserIdApproved,artcPublishTime');
        $this->db->from('article');
        $this->db->where('artcUkomIdSaved', $this->jwtData->id);
       
        // $this->db->join('user_komunitas', 'article.artcUkomIdSaved = user_komunitas.ukomId');
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $dataDetail[] = array(
                // "userApproveId" => $r->artcUserIdApproved,
                // "userApproveDetail" => $this->Model_Komunitas->getDetailUser($r->artcUserIdApproved)
                "username" => getField('user_komunitas' , "ukomName" , array("ukomId" =>  $r->artcUserIdApproved)),
                "artcPublishTime" => $r->artcPublishTime
            );
        }

        $sorted = $this->array_orderby($dataDetail, 'artcPublishTime', SORT_DESC);

        $response = array(
            'detail' => $sorted,
            'success' => true,
        );
        $this->response($response, 200);
    }

    public function array_orderby() {
		$args = func_get_args();
		$data = array_shift($args);
		foreach ($args as $n => $field) {
			if (is_string($field)) {
				$tmp = array();
				foreach ($data as $key => $row) {
					$tmp[$key] = $row[$field];
				}

				$args[$n] = $tmp;
			}
		}
		$args[] = &$data;
		call_user_func_array('array_multisort', $args);
		return array_pop($args);
	}


    public function mylist_get()
    {
        $dataDetail = array();
        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('artcUkomIdSaved', $this->jwtData->id);
        $this->db->join('user_komunitas', 'article.artcUkomIdSaved = user_komunitas.ukomId');
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($r->artcId);
            $path_folder_image = implode('/', $split_id);
            $dataDetail[] = array(
                "articleId" => $r->artcId,
                "title" => $r->artcTitle,
                "shortDesc" => $r->artcExcerpt,
                "longDesc" => $r->artcDetail,
                "mediaType" => $r->artcMediaFileType,
                "media" => CDN_MEDIA . "{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}",
                // "type" => $this->checktype("assets/library/{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}"),
                // "artcMediaId"=> "0",
                // "artcKeyword"=> "a:5:{i:0;s:1:\"5\";i:1;s:3:\"125\";i:2;s:3:\"302\";i:3;s:3:\"303\";i:4;s:3:\"306\";}",
                // "artcMscvId"=> "64",
                // "artcEmbedURL"=> "https://players.brightcove.net/4077388032001/default_default/index.html?videoId=6034016273001 ",
                "artcKomtId" => $r->artcKomtId,
                "artcUkomIdSaved" => $r->artcUkomIdSaved,
                // "artcSaved"=> "2019-05-09 04:00:02",
                // "artcPublishTime"=> "2019-05-25 12:00:00",
                // "artcUserIdApproved"=> null,
                'userUpload' => $r->ukomName,
                "status" => $r->artcStatus,
            );
        }
        $response = array(
            'detail' => $dataDetail,
            'success' => true,
        );
        $this->response($response, 200);
    }

    public function detail_get()
    {

        $dataDetail = array();
        $idArtc = $this->get('id');

        $this->db->select('*');
        $this->db->from('article');
        // $this->db->where('artcKomtId', $this->jwtData->komunitasid);
        $this->db->where('artcId', $idArtc);
        $this->db->join('user_komunitas', 'article.artcUkomIdSaved = user_komunitas.ukomId');
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($r->artcId);
            $path_folder_image = implode('/', $split_id);
            $typpe = null;
            if($r->artcMediaFileType == "aac"){
                $typpe = "sound";
            }else if($r->artcMediaFileType == "mp4"){
                $typpe = "video";
            }else if($r->artcMediaFileType !== null || $r->artcMediaFileType !== "mp4" || $r->artcMediaFileType !== "aac" ){
                $typpe = "image";
            }
            $dataDetail[] = array(
                "articleId" => $r->artcId,
                "title" => $r->artcTitle,
                "shortDesc" => $r->artcExcerpt,
                "longDesc" => $r->artcDetail,
                "mediaType" => $r->artcMediaFileType,
                "media" =>  CDN_MEDIA . "{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}",
                "type" =>  $typpe, //$r->artcMediaFileType ? $this->checktype("assets/library/{$path_folder_image}/{$r->artcId}.{$r->artcMediaFileType}") : null ,
                "isExist" => $r->artcMediaFileType ? true : false,
                // "artcMediaId"=> "0",
                // "artcKeyword"=> "a:5:{i:0;s:1:\"5\";i:1;s:3:\"125\";i:2;s:3:\"302\";i:3;s:3:\"303\";i:4;s:3:\"306\";}",
                // "artcMscvId"=> "64",
                // "artcEmbedURL"=> "https://players.brightcove.net/4077388032001/default_default/index.html?videoId=6034016273001 ",
                "artcKomtId" => $r->artcKomtId,
                "artcUkomIdSaved" => $r->artcUkomIdSaved,
                // "artcSaved"=> "2019-05-09 04:00:02",
                // "artcPublishTime"=> "2019-05-25 12:00:00",
                // "artcUserIdApproved"=> null,
                'userUpload' => $r->ukomName,
                "status" => $r->artcStatus,
            );
        }
        $response = array(
            'detail' => $dataDetail,
            'success' => true,
        );
        $this->response($response, 200);

        // $array_article = array();
        // $idArtc = $this->get('id');

        // $this->db->select('*');
        // $this->db->from('article');
        // $this->db->where('artcId', $idArtc);
        // $query = $this->db->get()->result();

        // foreach ($query as $r) {

        //     $array_article = array(
        //         "articleId" => $r->artcId,
        //         "title" => $r->artcTitle,
        //         "shortDesc" => $r->artcExcerpt,
        //         "longDesc" => $r->artcDetail,
        //         "artcId" => $r->artcDetail,
        //         "artcTitle" => $r->artcTitle,
        //         "artcExcerpt" => $r->artcExcerpt,
        //         "artcDetail" => $r->artcDetail,
        //         "artcMediaId" => $r->artcMediaId,
        //         "artcMediaFileType" => $r->artcMediaFileType,
        //         "artcKeyword" => $r->artcKeyword,
        //         "artcMscvId" => $r->artcMscvId,
        //         "artcEmbedURL" => $r->artcEmbedURL,
        //         "artcKomtId" => $r->artcKomtId,
        //         "artcUkomIdSaved"=> $r->artcUkomIdSaved,
        //         "artcSaved"=> $r->artcSaved,
        //         "artcPublishTime"=> $r->artcPublishTime,
        //         "artcUserIdApproved"=> $r->artcUserIdApproved,
        //         "artcStatus"=> $r->artcStatus,
        //     );
        // }

        // $response = array(
        //     'detail' => $array_article,
        //     'success' => true,
        // );
        // $this->response($response, 200);
    }

    public function add_post()
    {
        $this->load->library('upload');
        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $structure = '';
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('desc', 'desc', 'trim');
        //$this->form_validation->set_rules('tyep', 'type', 'trim|required');

        $this->form_validation->set_message('required', '{field} harus diisi');

       

        if ($this->form_validation->run() == true) {

            $data = array(
                'artcTitle' => @$post['title'],
                'artcDetail' => @$post['desc'],
                'artcExcerpt' => substr(@$post['desc'], 0, 150),
                'artcKomtId' => $this->jwtData->komunitasid,
                'artcUkomIdSaved' => $this->jwtData->id,

            );
            if (@$_FILES['file']['name'] != '') {
                $file_image = @$_FILES['file'];
                $file_image_extension = pathinfo(basename($file_image['name']), PATHINFO_EXTENSION);
                
                $data['artcMediaFileType'] = $file_image_extension;
            }
            // $this->db->insert('article', $data);

            $upload_data_image_id = $this->Model_Komunitas->save_arsip_images('article', $data);

            $id = $upload_data_image_id;
            $splitId = str_split($id);

            if (@$_FILES['file']['name'] != '') {
                foreach ($splitId as $k => $v) {
                    $valArr[] = $v . "/";
                }
                $structure = implode($valArr);
                if (!file_exists("assets/library/" . $structure)) {
                    mkdir("assets/library/" . $structure, 0777, true);
                }
                $config['upload_path'] = "assets/library/" . $structure;

                //upload image to recursive folder
                $max_width = '10288';
                $max_height = '7068';
                $config['allowed_types'] = "*";
                $config['max_size'] = '12048'; //max 2mb
                $config['max_width'] = $max_width; //max lebar
                $config['max_height'] = $max_height; //max tinggi
                $config['file_name'] = $id;
                $config['overwrite'] = true;

                $this->upload->initialize($config);
                $this->upload->do_upload('file');
            }

            $response = array(
                'message' => 'input  Berhasil',
                // 'str' => $structure,
                'success' => true,
            );
        } else {
            $response = array(
                'message' => 'Maaf, Registrasi gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            );
        }
        $this->response($response, 200);
    }

    public function edit_post()
    {
        $this->load->library('upload');
        $post = $this->post();
        $articleId = @$post['article_id'];
        $this->form_validation->set_data($this->post());
        $structure = '';
        // $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('article_id', 'article_id', 'trim|required');
        //$this->form_validation->set_rules('tyep', 'type', 'trim|required');

        $this->form_validation->set_message('required', '{field} harus diisi');

        // $file_image = $_FILES['file'];
        // $file_image_extension = pathinfo(basename($file_image['name']), PATHINFO_EXTENSION);
        // $extForDelete = getField('article', 'artcMediaFileType', array('artcId' => $articleId));
        if ($this->form_validation->run() == true) {

            $data = array(
                'artcTitle' => @$post['title'],
                'artcDetail' => @$post['desc'],
                'artcExcerpt' => substr(@$post['desc'], 0, 150),
                'artcKomtId' => $this->jwtData->komunitasid,
                'artcUkomIdSaved' => $this->jwtData->id,

            );
            if (@$_FILES['file']['name'] != '') {
                $file_image = $_FILES['file'];
                $file_image_extension = pathinfo(basename($file_image['name']), PATHINFO_EXTENSION);
                $extForDelete = getField('article', 'artcMediaFileType', array('artcId' => $articleId));
                $data['artcMediaFileType'] = $file_image_extension;
            }
            // $this->db->insert('article', $data);

            // $upload_data_image_id = $this->Model_Komunitas->save_arsip_images('article', $data);

            $this->db->update('article', $data, array('artcId' => $articleId));

            $id = $articleId;
            $splitId = str_split($id);

            if (@$_FILES['file']['name'] != '') {

               


                foreach ($splitId as $k => $v) {
                    $valArr[] = $v . "/";
                }
                $structure = implode($valArr);
                // if (!file_exists("assets/library/" . $structure)) {
                //     mkdir("assets/library/" . $structure, 0777, true);
                // }
                $dir = "assets/library/" . $structure . $articleId . "." . $extForDelete;
                if (file_exists($dir)) {
                    @unlink($dir);
                }
                $config['upload_path'] = "assets/library/" . $structure;

                //upload image to recursive folder
                $max_width = '10288';
                $max_height = '7068';
                $config['allowed_types'] = "*";
                $config['max_size'] = '12048'; //max 2mb
                $config['max_width'] = $max_width; //max lebar
                $config['max_height'] = $max_height; //max tinggi
                $config['file_name'] = $id;
                $config['overwrite'] = true;

                $this->upload->initialize($config);
                $this->upload->do_upload('file');
            }

            $response = array(
                'message' => 'edit  Berhasil',
                'str' => $structure,
                'dir' => $dir,
                'success' => true,
            );
        } else {
            $response = array(
                'message' => 'Maaf, edit gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            );
        }
        $this->response($response, 200);
    }

    public function delete_get()
    {
        $ai = $this->get('id');
        $this->db->delete('article', array('artcId' => $ai));
        $response = array(
            'message' => 'berhasil dihapus',
            'success' => true,
        );
        $this->response($response, 200);
    }

    public function checktype2($typ)
    {
        // $mimeType = mime_content_type($fle);
        // if (strstr($mimeType, "video/")) {
        //     $filetype = "video";
        // } else if (strstr($mimeType, "image/")) {
        //     $filetype = "image";
        // } else if (strstr($mimeType, "audio/")) {
        //     $filetype = "audio";
        // }
        // return $filetype;
        $filetype = '';
        $imageType = array("Mac", "NT", "Irix", "Linux");
        $videoType = array("Mac", "NT", "Irix", "Linux");
        $soundType = array("Mac", "NT", "Irix", "Linux");
        if (in_array($typ, $imageType)) {
            $filetype = 'image';
        }
        if (in_array($typ, $videoType)) {
            $filetype = 'video';
        }
        if (in_array($typ, $soundType)) {
            $filetype = 'sound';
        }
        return $filetype;
        
    }

    public function checktype($fle)
    {
        $mimeType = mime_content_type($fle);
        if (strstr($mimeType, "video/")) {
            $filetype = "video";
        } else if (strstr($mimeType, "image/")) {
            $filetype = "image";
        } else if (strstr($mimeType, "audio/")) {
            $filetype = "sound";
        }
        return $filetype;
        
    }


 */

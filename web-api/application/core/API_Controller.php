<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class API_Controller extends REST_Controller {

	public function __construct()
	{
		parent::__construct();

		$token = $this->input->get_request_header('Authorization');		

		if ($token) {

			$token = explode("Bearer ", $token);

			$jwt = JWT::decode($token[1],secretKey(),true);

			//	if (@$jwt->logged AND time() < @$jwt->exp) {
				$this->jwtData = $jwt;
			// }else{
			// 	$msg = array(
			// 		'message' => 'Failed to authenticate token',
			// 		'status' => 'failed'
			// 	);
			// 	$this->response($msg, 400);
			// }

		}else{
			$msg = array(
				'message' => 'Failed to authenticate token',
				'status' => 'failed'				
			);
			$this->response($msg, 400);
		}

	}

	public function _validateType($type)
	{

		if (!is_array($type)) {
			$type = array($type);
		}
		
		if (in_array(getCustomer('customerType'), $type)) {
			return TRUE;
		}else{
			return FALSE;
		}

	}

}

/* End of file API_Controller.php */
/* Location: ./application/core/API_Controller.php */
<?php
class Model_Belanja extends CI_Model
{

    public function get_all_images($id)
    {
        $arr_img = array();
        $query = $this->db->select('*')->from('ecommerce_image')
            ->join('arsip_images', 'ecommerce_image.ecimArimId=arsip_images.arimId', 'LEFT')
            ->where("ecimEcomId", $id)->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($r->ecimArimId);
            $path_folder_image = implode('/', $split_id);
            $arr_img[] = array(
                'img1' => CDN_IMG . "{$path_folder_image}/{$r->ecimArimId}_224x153.{$r->arimFileType}",
                'img2' => CDN_IMG . "{$path_folder_image}/{$r->ecimArimId}_263x180.{$r->arimFileType}",
                'img3' => CDN_IMG . "{$path_folder_image}/{$r->ecimArimId}_300x206.{$r->arimFileType}",
                'img4' => CDN_IMG . "{$path_folder_image}/{$r->ecimArimId}_512x351.{$r->arimFileType}",
                'img5' => CDN_IMG . "{$path_folder_image}/{$r->ecimArimId}_683x468.{$r->arimFileType}",
                'img6' => CDN_IMG . "{$path_folder_image}/{$r->ecimArimId}_840x576.{$r->arimFileType}",
            );

        }

        return $arr_img;
    }

    public function get_image_cover($id)
    {
        $arr_img = array();
        $query = $this->db->select('*')->from('ecommerce_image')
            ->join('arsip_images', 'ecommerce_image.ecimArimId=arsip_images.arimId', 'LEFT')
            ->where("ecimEcomId", $id)->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($r->ecimArimId);
            $path_folder_image = implode('/', $split_id);
            $arr_img[] = array(
                'img1' => CDN_IMG . "{$path_folder_image}/{$r->ecimArimId}_224x153.{$r->arimFileType}",
            );

        }

        if(count($arr_img) > 0){
            return $arr_img[0]['img1'];
        }else{
            return '';
        }

       
    }

    // public function register($data) {
    //     $this->db->insert('anggota', $data);
    //     if ($this->db->affected_rows() == 1) {
    //         return TRUE;
    //     } else {
    //         return FALSE;
    //     }
    // }

    // function login($emailorphone, $password) {
    //     $query = $this->db->select('*')->from('relawan')
    //     ->where('password', $password)
    //     ->where("status",1)
    //     ->group_start()
    //     ->where("emailRelawan", $emailorphone)
    //     ->or_where("handphone", $emailorphone)
    //     ->group_end()
    //     ->get();
    //     //echo $this->db->last_query();
    //     return $query->row();
    // }

    // function logingoogle($email) {
    //     $where = array(
    //         'emailAnggota' => $email,
    //     );
    //     $this->db->where($where);
    //     $query = $this->db->get('anggota');
    //     return $query->row();
    // }

}

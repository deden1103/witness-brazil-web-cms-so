<?php
class Model_Article extends CI_Model {

	public function list_editor($komunitasId) {
        $sql="SELECT * FROM admin";
		$sql.=" LEFT JOIN komunitas ON admin.komunitasId = komunitas.komtId";
		$sql.=" WHERE komunitasId = {$komunitasId} AND notifikasiActive = 1 OR admiId =28";
        $query=$this->db->query($sql)->result_array();
		return $query;
	}

	function login($emailorphone, $password) {
		$query = $this->db->select('*')->from('user_komunitas')
		->where('ukomPassword', $password)
		->where("ukomStatus",1)
		->group_start()
		->where("ukomEmail", $emailorphone)
		->or_where("ukomPhone", $emailorphone)
		->group_end()
		->get();
		//echo $this->db->last_query(); 
		return $query->row();
	}

	function logingoogle($email) {
		$where = array(
			'userEmail' => $email,
		);
		$this->db->where($where);
		$query = $this->db->get('user');
		return $query->row();
	}

}

<?php
class Model_Komunitas extends CI_Model
{

    public function get_all_images($newsId)
    {
        $arr_img = array();
        $query = $this->db->select('*')->from('arsip_images')
        // ->join('arsip_images', 'news_images.neimArimId=arsip_images.arimId', 'LEFT')
            ->where("arimId", $newsId)->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($newsId);
            $path_folder_image = implode('/', $split_id);
            $arr_img = array(
                'img1' => CDN_IMG . "{$path_folder_image}/{$newsId}_224x153.{$r->arimFileType}",
                'img2' => CDN_IMG . "{$path_folder_image}/{$newsId}_263x180.{$r->arimFileType}",
                'img3' => CDN_IMG . "{$path_folder_image}/{$newsId}_300x206.{$r->arimFileType}",
                'img4' => CDN_IMG . "{$path_folder_image}/{$newsId}_512x351.{$r->arimFileType}",
                'img5' => CDN_IMG . "{$path_folder_image}/{$newsId}_683x468.{$r->arimFileType}",
                'img6' => CDN_IMG . "{$path_folder_image}/{$newsId}_840x576.{$r->arimFileType}",
            );

        }

        return $arr_img;
    }

    public function getMember($id)
    {
        $this->db->select("community_member.*, user.userFullName as uName, user.userImgURL as imageUser");
        $this->db->from('community_member');
        $this->db->join('user', 'community_member.cmmbUserId = user.userId');
        $this->db->where('cmmbCommId', $id);
        //  $this->db->join('community_member');
        $query = $this->db->get()->result();
        $xx = array();
        foreach ($query as $r) {
            $xx[] = array(
                'cmmbId' => $r->cmmbId,
                'cmmbCommId' => $r->cmmbCommId,
                'cmmbUserId' => $r->cmmbUserId,
                'cmmbSaved' => $r->cmmbSaved,
                'cmmbStatus' => $r->cmmbStatus,
                'uName' => $r->uName,
                'imageUser' => $r->imageUser == null ? '' : $r->imageUser,
            );
        }

        return $xx;
    }

    public function getForum($id)
    {
        $_arr = [];
        $this->db->select("*");
        $this->db->from('thread');
        // $this->db->join('user','community_member.cmmbUserId = user.userId');
        $this->db->where('thrdCommId', $id);
        //  $this->db->join('community_member');
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $_arr[] = array(
                'thrdId' => $r->thrdId,
                'thrdUserId' => $r->thrdUserId,
                'thrdCommId' => $r->thrdCommId,
                'thrdTitle' => $r->thrdTitle,
                'thrdText' => $r->thrdText == null ? '' : $r->thrdText,
                'thrdSaved' => $r->thrdSaved,
                'timeAgo' => getDateTime(strtotime(date($r->thrdSaved))),//timeAgo($r->thrdSaved),
                'thrdStatus' => $r->thrdStatus,
                // 'comment' => $this->getcomment(),
            );
        }

        return $_arr;
    }

    public function getOrganisasi($id = '')
    {
        $_arr = [];
        $this->db->select("*");
        $this->db->from('community_organisasi');
        // $this->db->join('user','community_member.cmmbUserId = user.userId');
        $this->db->where('commId', $id);
        //  $this->db->join('community_member');
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $_arr[] = array(
                'commOrgId' => $r->commOrgId,
                'commId' => $r->commId,
                'organisasiName' => $r->organisasiName,
                'organisasiMember' => $r->organisasiMember,
                'timeAgo' => timeAgo($r->createDate),
                'allMember' => count_all('community_organisasi_member', ['comOrgId' => $r->commId]),
                'createDate' => $r->createDate,
            );
        }

        return $_arr;
    }

    public function getcomment()
    {
        $_arr = [];
        $this->db->select("*");
        $this->db->from('thread_response');
        // $this->db->join('user','community_member.cmmbUserId = user.userId');
        // $this->db->where('thrdCommId', $id);
        //  $this->db->join('community_member');
        $query = $this->db->get()->result();

        // foreach ($query as $r) {
        //     $_arr[] = array(
        //         'thrdId' => $r->thrdId,
        //         'thrdUserId' => $r->thrdUserId,
        //         'thrdCommId' => $r->thrdCommId,
        //         'thrdTitle' => $r->thrdTitle,
        //         'thrdText' => $r->thrdText,
        //         'thrdSaved' => $r->thrdSaved,
        //         'timeAgo' => timeAgo($r->thrdSaved),
        //         'thrdStatus' => $r->thrdStatus,
        //     );
        // }

        return $query;
    }

    public function save_arsip_images($table, $data_register)
    {
        $this->db->insert($table, $data_register);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }
    public function save_post($table, $data_register)
    {
        return $this->db->insert($table, $data_register);
    }

    // public function register($data) {
    //     $this->db->insert('anggota', $data);
    //     if ($this->db->affected_rows() == 1) {
    //         return TRUE;
    //     } else {
    //         return FALSE;
    //     }
    // }

    // function login($emailorphone, $password) {
    //     $query = $this->db->select('*')->from('relawan')
    //     ->where('password', $password)
    //     ->where("status",1)
    //     ->group_start()
    //     ->where("emailRelawan", $emailorphone)
    //     ->or_where("handphone", $emailorphone)
    //     ->group_end()
    //     ->get();
    //     //echo $this->db->last_query();
    //     return $query->row();
    // }

    // function logingoogle($email) {
    //     $where = array(
    //         'emailAnggota' => $email,
    //     );
    //     $this->db->where($where);
    //     $query = $this->db->get('anggota');
    //     return $query->row();
    // }

    public function getDetailUser($id){
        $_arr = [];
        $this->db->select("*");
        $this->db->from('user_komunitas');
        $this->db->where('ukomId', $id);
        $query = $this->db->get()->result();

        foreach ($query as $r) {
            $_arr = array(
                "ukomId" => $r->ukomId,
                "ukomKomtId" => $r->ukomKomtId,
                "ukomName" => $r->ukomName,
                "ukomEmail" => $r->ukomEmail,
            );
        }

        return $_arr;
    }

}

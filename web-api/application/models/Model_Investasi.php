<?php
class Model_Investasi extends CI_Model
{

    public function get_all_images($inid)
    {
        $arr_img = array();
        $query = $this->db->select('*')->from('investasi_images')
        ->join('arsip_images', 'investasi_images.investasiArsipImageId=arsip_images.arimId', 'LEFT')
        ->where("investasiId", $inid)->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($r->investasiArsipImageId);
            $path_folder_image = implode('/', $split_id);
            $arr_img = array(
                'img1' => CDN_IMG."{$path_folder_image}/{$r->investasiArsipImageId}_224x153.{$r->arimFileType}",
                'img2' => CDN_IMG."{$path_folder_image}/{$r->investasiArsipImageId}_263x180.{$r->arimFileType}",
                'img3' => CDN_IMG."{$path_folder_image}/{$r->investasiArsipImageId}_300x206.{$r->arimFileType}",
                'img4' => CDN_IMG."{$path_folder_image}/{$r->investasiArsipImageId}_512x351.{$r->arimFileType}",
                'img5' => CDN_IMG."{$path_folder_image}/{$r->investasiArsipImageId}_683x468.{$r->arimFileType}",
                'img6' => CDN_IMG."{$path_folder_image}/{$r->investasiArsipImageId}_840x576.{$r->arimFileType}"
            );

        }

        return $arr_img;
    }

    // public function register($data) {
    //     $this->db->insert('anggota', $data);
    //     if ($this->db->affected_rows() == 1) {
    //         return TRUE;
    //     } else {
    //         return FALSE;
    //     }
    // }

    // function login($emailorphone, $password) {
    //     $query = $this->db->select('*')->from('relawan')
    //     ->where('password', $password)
    //     ->where("status",1)
    //     ->group_start()
    //     ->where("emailRelawan", $emailorphone)
    //     ->or_where("handphone", $emailorphone)
    //     ->group_end()
    //     ->get();
    //     //echo $this->db->last_query();
    //     return $query->row();
    // }

    // function logingoogle($email) {
    //     $where = array(
    //         'emailAnggota' => $email,
    //     );
    //     $this->db->where($where);
    //     $query = $this->db->get('anggota');
    //     return $query->row();
    // }

}

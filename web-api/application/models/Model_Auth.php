<?php
class Model_Auth extends CI_Model {

	public function register($data) {
		$this->db->insert('user', $data);
		if ($this->db->affected_rows() == 1) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function login($emailorphone, $password) {
		$query = $this->db->select('*')->from('user_komunitas')
		->where('ukomPassword', $password)
		->where("ukomStatus",1)
		->group_start()
		->where("ukomEmail", $emailorphone)
		->or_where("ukomPhone", $emailorphone)
		->group_end()
		->get();
		//echo $this->db->last_query(); 
		return $query->row();
	}

	function logingoogle($email) {
		$where = array(
			'userEmail' => $email,
		);
		$this->db->where($where);
		$query = $this->db->get('user');
		return $query->row();
	}

}

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http, { origins: '*:*'});
var port = process.env.PORT || 3088;
var striptags = require('striptags');
var moment = require('moment');

// Bad Words
var Filter = require('bad-words');
var filter = new Filter();
filter.addWords('anjing', 'babi', 'monyet', 'kunyuk', 'bajingan', 'asu', 'bangsat', 'kampret', 'kontol', 'memek', 'ngentot', 'ngewe', 'ngehe', 'sepong', 'jembut', 'jembot', 'jemboot', 'jilmet', 'coli', 'onani', 'orgasme', 'doggy', 'titit', 'toket', 'boker', 'berak', 'perek', 'pecun', 'bencong', 'banci', 'jablay', 'maho', 'bispak', 'bego', 'goblok', 'goblog', 'idiot', 'geblek', 'orang gila', 'gila', 'sinting', 'tolol', 'sarap', 'udik', 'kampungan', 'kamseupay', 'sempak', 'bacot', 'meki', 'ngepet', 'oon', 'buta', 'budek', 'bolot', 'jelek', 'setan', 'iblis', 'keparat', 'bejad', 'gembel', 'brengsek', 'tai', 'tae', 'sompret', 'viagra', 'penis', 'vagina', 'obat kuat', 'sex', 'sex toys', 'seks');

// Mysql Database Connection
var mysql = require('mysql');
var db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '135_4y34y3',
    database: 'tempowitness',
    port:'3306'
});
db.connect();


app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.set('origins', '*:*');

// One Signal Notification
var sendNotification = function(data) {
  var headers = {
    "Content-Type": "application/json; charset=utf-8"
  };
  
  var options = {
    host: "onesignal.com",
    port: 443,
    path: "/api/v1/notifications",
    method: "POST",
    headers: headers
  };
  
  var https = require('https');
  var req = https.request(options, function(res) {  
    res.on('data', function(data) {
      console.log("Response:");
      console.log(JSON.parse(data));
    });
  });
  
  req.on('error', function(e) {
    console.log("ERROR:");
    console.log(e);
  });
  
  req.write(JSON.stringify(data));
  req.end();
};

// Var Apps Online
var apps_online = {};

io.on('connection', function(socket){
	// Join
	socket.on('join', function (ukomId, myId, artcId, isCms) {
		// User Komunitas ID (ukomId) sebagai parameter root
		socket.room = ukomId;
		// myId sebagai parameter id yg memasuki room
		socket.myId = myId;
        // artcId sebagai parameter tambahan article id
        socket.artcId = artcId;
        // UPDATE 2019 09 04, room terbaru adalah artcId
        socket.newRoom = artcId;
        // is CMS adalah parameter apakah chatting dari CMS atau bukan
        socket.isCMs = isCms;
		// Join
		//socket.join(ukomId);
        // UPDATE 2019 09 04, room terbaru adalah artcId
		socket.join(artcId);
        // Add Apps Online
        if(typeof isCms === "undefined") {
            apps_online[myId] = true
        }
        console.log('connect')
        console.log(apps_online)
	});
	
	// Send Chat
	socket.on('sendchat', function (data) {
		// Filter
		var filtered = realed = striptags(data);
        filtered = filter.clean(filtered);
		// Moment Date Now
		moment.locale('id');
		var dateNow = moment().format('DD MMMM YYYY HH:mm:ss');
		// Cek apakah room dan myId itu sama,, jika sama maka adalah user komunitas
		//if(socket.room == socket.myId) {
        if(typeof socket.isCMs === "undefined") {
			// Get Detail User Komunitas
			db.query('SELECT * FROM `user_komunitas` WHERE `ukomId`=?', [socket.myId], function (err, result) {
				if(err) {
					console.log('\x1b[33m%s\x1b[0m', 'ERROR QUERY UKOM');
					console.log(err)
				}
				// Emit message
				//io.in(socket.room).emit('updatechat', socket.myId, dateNow, result[0]['ukomName'], filtered);
				// UPDATE 2019 09 04, room terbaru adalah artcId
                io.in(socket.newRoom).emit('updatechat', socket.myId, dateNow, result[0]['ukomName'], filtered);
				// Save To Table Chat
				db.query('INSERT INTO `chat` (`chatArtcId`, `chatUkomId`, `chatRealMessage`, `chatFilteredMessage`) VALUES (?,?,?,?)', [socket.artcId, socket.myId, realed, filtered]);
			})
		} else {
			// Get Detail User Admin
			db.query('SELECT * FROM `admin` WHERE `admiId`=?', [socket.myId], function (err, result) {
				if(err) {
					console.log('\x1b[33m%s\x1b[0m', 'ERROR QUERY ADMIN');
					console.log(err)
				}
                let adminName = result[0]['admiRealName'];
				// Emit message
				//io.in(socket.room).emit('updatechat', socket.myId, dateNow, result[0]['admiRealName'], filtered);
				// UPDATE 2019 09 04, room terbaru adalah artcId
                io.in(socket.newRoom).emit('updatechat', socket.myId, dateNow, result[0]['admiRealName'], filtered);
				// Save To Table Chat
				db.query('INSERT INTO `chat` (`chatArtcId`, `chatUkomId`, `chatAdmiId`, `chatRealMessage`, `chatFilteredMessage`) VALUES (?,?,?,?,?)', [socket.artcId, socket.room, socket.myId, realed, filtered]);
			    
                if(typeof apps_online[socket.myId] === "undefined") {
                    // Get Data User Komunitas
                    db.query('SELECT * FROM `user_komunitas` WHERE `ukomId`=?', [socket.room], function (err, result) {
                        if(err) {
                            console.log('\x1b[33m%s\x1b[0m', 'ERROR QUERY ADMIN GET UKOM');
                            console.log(err)
                        }
                        // One signal notification
                        var message_os = { 
                          app_id: "04f752ea-ae43-42a1-8488-75b945eb9447",
                          contents: {"en": filtered},
                          headings: {"en": adminName},
                          data:{"artcid": socket.artcId},
                          include_player_ids: [result[0]['ukomOneSignal']]
                        };

                        sendNotification(message_os);
                    })
                }
            })
		}
		
    });
	
	// Debuging
    socket.on('chat message', function(msg){
		// Filter
        msg = striptags(msg);
        msg = filter.clean(msg);
        io.emit('chat message', msg);
    });
    
    // Disconnect
    socket.on('disconnect', function () {
        console.log('disconnect')
        if(typeof socket.isCMs === "undefined") {
            delete apps_online[socket.myId];   
        }
        console.log(apps_online)
    });
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});

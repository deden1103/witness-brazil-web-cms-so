<?php defined('SYSPATH') or die('No direct script access.');

class Model_Mascatv extends Model {
	
	public function count_all() {
		
		$return = 0;
		
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('master_category_video')
					->where('mscvStatus','=',1)
					->execute()
					->as_array();
		
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function list_data($limit = '', $offset = '') {
		
		$exec = DB::select(
					array('mscvId', 'id'),
					array('mscvName', 'name'),
					array('mscvSaved', 'saved'),
					array('mscvStatus', 'status')
				)
				->from('master_category_video')
				->where('mscvStatus','=',1)
				->order_by('mscvId', 'DESC')
				->limit($limit)
				->offset($offset)
				->execute()
				->as_array();
		
		return $exec;
		
	}
	
	public function save_data($data = array(), $user_id = '') {
		
		$query =	DB::insert('master_category_video', array(
						'mscvName',
						'mscvAdmiIdSaved'
					))
					->values(array(
						$data['name'],
						$user_id
					))
					->execute();
		return $query;
	}
	
	public function data_by_id($id = '') {
		
		$return  = array();
		
		$exec = DB::select(
					array('mscvId', 'id'),
					array('mscvName', 'name')
				)
				->from('master_category_video')
				->where('mscvStatus','=',1)
				->where('mscvId','=',$id)
				->execute()
				->as_array();
		
		if(!empty($exec[0])) {
			$return = $exec[0];
		}
		
		return $return;
		
	}
	
	public function update_data($id = '', $data = '') {
		
		$query =	DB::update('master_category_video')
						->set(array('mscvName' => $data['name']))
						->where('mscvId', '=', $id)
						->execute();
		
	}
	
	public function delete_data($id = '') {
		$query =	DB::update('master_category_video')
					->set(array('mscvStatus' => 0))
					->where('mscvId', '=', $id)
					->execute();
	}
	
	public function count_search_data($search = '') {
		
		$return = 0;
		
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('master_category_video')
					->where('master_category_video.mscvName', 'LIKE', '%' . $search . '%')
					->execute()
					->as_array();
		
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function search_data($search = '', $limit = '', $offset = '') {
		
		
		$exec = DB::select(
					array('mscvId', 'id'),
					array('mscvName', 'name'),
					array('mscvSaved', 'saved'),
					array('mscvStatus', 'status')
				)
				->from('master_category_video')
				->where('mscvStatus','=',1)
				->where('master_category_video.mscvName', 'LIKE', '%' . $search . '%')
				->order_by('mscvId', 'DESC')
				->limit($limit)
				->offset($offset)
				->execute()
				->as_array();
		
		return $exec;
		
	}

	
	
}
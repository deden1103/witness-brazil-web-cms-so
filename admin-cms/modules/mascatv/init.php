<?php

// Route List
Route::set('mascatvlist', 'mascatv/list(/<page>)')
	->defaults(array(
		'controller' => 'mascatv',
		'action'     => 'list',
	));

// Route Search
Route::set('mascatvsearch', 'mascatv/search(/<search>(/<page>))')
	->defaults(array(
		'controller' => 'mascatv',
		'action'     => 'search',
	));
	
// Route input
Route::set('mascatvinput', 'mascatv/new(/<action>(/<page>))')
	->defaults(array(
		'controller' => 'mascatv',
		'action'     => 'new',
	));
	
// Route Edit
Route::set('mascatvedit', 'mascatv/edit(/<id>)')
	->defaults(array(
		'controller' => 'mascatv',
		'action'     => 'edit',
	));
	
// Route Cahnge Password
Route::set('mascatvcpas', 'mascatv/cpas(/<id>)')
	->defaults(array(
		'controller' => 'mascatv',
		'action'     => 'cpas',
	));
	
// Route Delete
Route::set('mascatvdelete', 'mascatv/delete(/<id>)')
	->defaults(array(
		'controller' => 'mascatv',
		'action'     => 'delete',
	));
	
// Route default
Route::set('mascatv', 'mascatv(/<action>(/<page>))')
	->defaults(array(
		'controller' => 'mascatv',
		'action'     => 'index',
	));
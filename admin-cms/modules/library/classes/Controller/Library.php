<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Library extends Controller_Backend {

	private $custom_footer;

	public function before() {

		parent::before();
		
		$level = Controller_Backend::check_level();
		if($level == 2){
			$this->redirect(URL::Base().'/keranjang');
		}
		// Javascript for delete data
		$this->custom_footer = '
			<script type="text/javascript">
				function del_confirm(url) {
					var dialog = confirm("' . __('Are you sure for delete this data ?') . '");
					if (dialog == true) {
						window.location.href=url;
					}
				}
			</script>
		';

	}

	public function action_index() {
		$session = Session::instance();
		
		if(!empty($_GET['img-gallery'])){
			if($_GET['img-gallery'] == 1){
				$data['main_title'] = __('Library');
				$data['menu_active'] = 'gallery';
				$data['menu_active_child'] = 'image';
				$data['menu_active_child_1'] = 'list';
			}
		} else {
			$data['main_title'] = __('Library | List');
			$data['menu_active'] = 'Library';
			$data['menu_active_child'] = 'list';
		}
		
		$config = Kohana::$config->load('path');
		// $data['options'] = $config->get('cdn');
		$data['options'] = "/uploads";

		$session = Session::instance();
		if(isset($_POST['search'])) $session->set('search_image', $_POST['search']);
		$data['search'] = $search = $session->get("search_image", "");
		
		// Page from parameter
		$page = intval($this->request->param('page'));
		if(empty($page)) $page = 1;
		$data['page'] = $page;

		$id = intval($this->request->param('id'));
		if(empty($id)) $id = 0;
		$data['id'] = $id;

		$data['dom'] = $dom = $this->request->param('dom');
		if(!empty($dom)) $dom = '/'.$dom;

		// Load data from model
		$library_model = new Model_Library();

		//is_gallery?
		$is_gallery='';
		if(!empty($_GET['img-gallery']))
			$is_gallery = $_GET['img-gallery'];

		// Pagination				
		if(isset($_GET['img-gallery'])){
			$count_all = $library_model->count_all($search, $is_gallery);
			//print_r($count_all); exit;
			$pagination = 	Pagination::factory(array(
							'total_items'    		=> $count_all,
							'items_per_page'	=> 1,
							'current_page'		=> $page,
							'suffix'					=> "{$id}{$dom}?img-gallery={$_GET['img-gallery']}",
							'base_url'				=> "/library/index/",
							'view'					=> 'pagination/admin'
						));
		} else {
			$count_all = $library_model->count_all($search);
		
			$pagination = 	Pagination::factory(array(
							'total_items'    		=> $count_all,
							'items_per_page'	=> 20,
							'current_page'		=> $page,
							'suffix'					=> "{$id}{$dom}",
							'base_url'				=> "/library/index/",
							'view'					=> 'pagination/admin'
						));
		}
		$data['list'] = $library_model->list_data($pagination->items_per_page, $pagination->offset, $search, $is_gallery);

		$data['pagination'] =  $pagination->render();

		$data['custom_header'] = '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.5/dist/sweetalert2.min.css">';

		$data['custom_footer'] = $this->custom_footer;	
		$data['custom_footer'] .=  '<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.5/dist/sweetalert2.all.min.js"></script>';
		$data['custom_footer'] .=  '
		<script>
			function delLib(swal_id){
				swal({
					title: "Are you sure?",
					text: "You won\'t be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3085d6",
					cancelButtonColor: "#d33",
					confirmButtonText: "Yes, delete it!"
				  }).then((result) => {
					if (result.value) {
						$.ajax({
							url: "/library/delete/",
							dataType: "JSON",
							type: "POST",
							data: {swal_id : swal_id}, 
							success: function(res){
								$("#row"+swal_id).remove()
								swal(
									"Deleted!",
									"Your file has been deleted.",
									"success"
								  )
							},error: function(){
								swal(
									"Something Wrong Happen !!",
									"",
									"error"
								  )
							}
						})
					}
				  })
			}
		</script>';

		$member = $session->get('member');
		$data['member'] = $member;

		if(empty($dom)){
			$view = Briliant::admin_template('library/' . Kohana::$config->load('path.main_template') . '/list', $data);
		}else{
			$view = Briliant::admin_template_plugin('library/' . Kohana::$config->load('path.main_template') . '/list', $data);
		}
		$this->response->body($view);

	}

	public function action_delete() {
		if(isset($_POST["swal_id"])){
			$id = $_POST['swal_id'];
			$global_model = new Model_Globalmodel();
			$update = $global_model->update('arsip_image',['arimStatus' => 0],['arimId' => $id]);
			print_r($update);
			if ($update == 1) {
				echo json_encode($update);
			}else{
				http_response_code(500);
			}

			
		}
	}

	public function action_new() {
		$library_model = new Model_Library();
		
		// Page from parameter
		$page = intval($this->request->param('page'));
		if(empty($page)) $page = 1;
		$data['page'] = $page;

		$id = intval($this->request->param('id'));
		if(empty($id)) {
			$id = 0;
		}else{
			$data['library'] = $library_model->data_by_id($id);
		}
		$data['id'] = $id;

		$data['dom'] = $dom = $this->request->param('dom');
		
		//edited by AN
		$data_category = $library_model->list_category_gallery();
		$data['category'] = $data_category;

		if ($this->request->method() == Request::POST) {
			
			// print_r($_FILES['fileName']);exit;

			$validation = 	Validation::factory($this->request->post())
							->rule('title', 'not_empty')
							->rule('caption', 'not_empty');

			if($validation->check()) {
				$session = Session::instance();
				$user_id = $session->get('adminId');

				$param['id'] = $this->request->post('id');
				$data['library']['title'] = $param['title'] = $this->request->post('title');
				$data['library']['caption'] = $param['caption'] = $this->request->post('caption');
				$param['userId'] = $user_id;
				$data['library']['gallery'] = $param['gallery'] = $this->request->post('arimSetCategory');
				$data['library']['gallery_ctg'] = $param['gallery_ctg'] = $this->request->post('arimCategory');
				
				// Edit By IRUL FZ
				if(!empty($this->request->post('is_edit'))) {

					if(!empty($this->request->post('image_editor'))) {
						$filename = $this->base64_to_jpeg_and_save_image($param, $this->request->post('image_editor'));
					}
					else{
						if (!empty($_FILES['fileName']['size'])) {
							$filename = $this->_save_image($param, $_FILES['fileName']);
						}
					}
					$filename = $library_model->save_data($param);

					header("location: " . URL::Base() . "library/index/{$page}/0/{$dom}");
					exit;
				} else {
					if(!empty($this->request->post('image_editor'))) {
						//print_r($_FILES['fileName']); 
						$filename = $this->base64_to_jpeg_and_save_image($param, $this->request->post('image_editor'));
					}
					else{
						if (!empty($_FILES['fileName'])) {
							$filename = $this->_save_image($param, $_FILES['fileName']);
						}elseif(!empty($param['id'])){
							$filename = $library_model->save_data($param);
						}
					}

					if (empty($filename)) {
						$data['errors'][] = ' There was a problem while uploading the image.
							Make sure it is uploaded and must be JPG/PNG/GIF file.';
					}else{
						if(!empty($param['id'])){
							header("location: " . URL::Base() . "library/index/{$page}/{$id}/{$dom}");
							exit;
						}else{
							header("location: " . URL::Base() . "library/index/{$page}/0/{$dom}");
							exit;
						}
					}
				}

			}else{
				$data['errors'] = $validation->errors('validation');
			}
		}
		
		if(!empty($_GET['img-gallery'])){
			if($_GET['img-gallery'] == 1){
				$data['menu_active'] = 'gallery';
				$data['menu_active_child'] = 'image';
				$data['menu_active_child_1'] = 'add';
			}
		} else {
			$data['main_title'] = __('Library | Add New Data');
			$data['menu_active'] = 'Library';
			$data['menu_active_child'] = 'add';
		}
	
		if(empty($dom)){
			$view = Briliant::admin_template('library/' . Kohana::$config->load('path.main_template') . '/edit', $data);
		}else{
			$view = Briliant::admin_template_plugin('library/' . Kohana::$config->load('path.main_template') . '/edit', $data);
		}
		$this->response->body($view);

	}
 
	protected function base64_to_jpeg_and_save_image($param, $base64_string) {
		// open the output file for writing
		$output_file = getcwd()."/uploads/editor/temp.jpg";
		$ifp = fopen( $output_file, 'wb' ); 

		// split the string on commas
		// $data[ 0 ] == "data:image/png;base64"
		// $data[ 1 ] == <actual base64 string>
		$data = explode( ',', $base64_string );

		// we could add validation here with ensuring count( $data ) > 1
		fwrite( $ifp, base64_decode( $data[ 1 ] ) );

		// clean up the file resource
		fclose( $ifp ); 

		$param['fileType'] = $imageFileType = "jpg";

		$library_model = new Model_Library();
		$arimId = $library_model->save_data($param);
		if(empty($arimId)) return FALSE;

		$directory = DOCROOT.'uploads/library';
		$arimId_arr = str_split($arimId);
		foreach($arimId_arr as $v){
			$directory.="/{$v}";
			if(!is_dir($directory)) mkdir($directory, 0777);
		}
		
		$filename_title = Briliant::slugify($this->request->post('title'));
		//$filename = "{$arimId}.{$imageFileType}";
		$filename = "{$filename_title}.{$imageFileType}";
        if (copy($output_file, $directory."/".$filename)){
            //return $this->_resize($imageFileType, $directory, $arimId);
            return $this->_resize($imageFileType, $directory, $filename_title);
        }
        return FALSE;
	}

	protected function _save_image($param, $image){
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif'))
			) {
            return FALSE;
        }

		$param['fileType'] = $imageFileType = pathinfo(basename($image["name"]),PATHINFO_EXTENSION);

		$library_model = new Model_Library();
		$arimId = $library_model->save_data($param);
		if(empty($arimId)) return FALSE;

		$directory = DOCROOT.'uploads/library';
		$arimId_arr = str_split($arimId);
		foreach($arimId_arr as $v){
			$directory.="/{$v}";
			if(!is_dir($directory)) mkdir($directory, 0777);
		}

		/* $files = glob($directory.'/*'); // get all file names
		foreach($files as $file){ // iterate files
		  if(is_file($file))
			@unlink($file); // delete file
		} */
		
		$filename_title = Briliant::slugify($this->request->post('title'));
		//$filename = "{$arimId}.{$imageFileType}";
		$filename = "{$filename_title}.{$imageFileType}";
        if ($file = Upload::save($image, $filename, $directory))
        {

            // Delete the temporary file
            //unlink($file);
            //return $this->_resize($imageFileType, $directory, $arimId);
            return $this->_resize($imageFileType, $directory, $filename_title);
        }

        return FALSE;
    }

	//protected function _resize($imageFileType, $directory, $arimId) {
	protected function _resize($imageFileType, $directory, $filename_title) {
		//$filename = "{$arimId}.{$imageFileType}";
		$filename = "{$filename_title}.{$imageFileType}";
		$file = "{$directory}/{$filename}";

		$size_image = array();
		$size_image[] = array(360, 180);
		$size_image[] = array(278, 139);
		$size_image[] = array(263, 132);
		$size_image[] = array(180, 90);

		if($imageFileType=='gif'){
			$filenameJPG = "{$arimId}_360x180.jpg";
			Image::factory($file)->resize(360, 180, Image::AUTO)->save("{$directory}/{$filenameJPG}");
			list($orig_width, $orig_height, $type) = getimagesize($file);
			$coalesce = "{$directory}/{$arimId}_coalesce.gif";
			exec("convert {$file} -coalesce {$coalesce}", $output);
			foreach($size_image as $z){
				/* $imagick = new Imagick($file);
				$imagick = $imagick->coalesceImages();
				$file_name = "{$arimId}_{$z[0]}x{$z[1]}.{$imageFileType}";
				foreach ($imagick as $frames) {
				  $frames->thumbnailImage($z[0], $z[1]);
				  $frames->setImagePage($z[0], $z[1], 0, 0);
				}
				$imagick = $imagick->deconstructImages();
				$imagick->writeImages("{$directory}/{$file_name}", true); */

				$file_name = "{$arimId}_{$z[0]}x{$z[1]}.{$imageFileType}";
				error_log("convert -size {$orig_width}x{$orig_height} {$coalesce} -resize {$z[0]}x{$z[1]} {$directory}/{$file_name}", 3, "./../temp/{$arimId}_{$z[0]}x{$z[1]}.sh");
			}
		}else{
			$size_image[] = array(1140, 570);
			foreach($size_image as $z){
				//$filename = "{$arimId}_{$z[0]}x{$z[1]}.{$imageFileType}";
				$filename = "{$filename_title}_{$z[0]}x{$z[1]}.{$imageFileType}";
				Image::factory($file)
					->resize($z[0], $z[1], Image::AUTO)
					//->render(NULL, 60)
					->save("{$directory}/{$filename}");
			}
		}
		// Engine_Arsipimage::send_to_web($arimId);
		// Engine_Library::send_to_web($arimId, $directory);
		return $filename;
	}

	/* 
		public function action_delete() {

		$page = intval($this->request->param('page'));
		if(empty($page)) $page = 1;

		$id = intval($this->request->param('id'));
		if(empty($id)) $id = 0;

		$dom = $this->request->param('dom');

		// Execute model
		$library_model = new Model_Library();
		$library_model->delete_data($id);

		$directory = DOCROOT.'uploads/library';
		$arimId_arr = str_split($id);
		foreach($arimId_arr as $v){
			$directory.="/{$v}";
		}

		$files = glob($directory.'/*'); // get all file names
		foreach($files as $file){ // iterate files
		  if(is_file($file))
			@unlink($file); // delete file
		}

		// Redirect to category list
		header("location: /library/index/{$page}/{$id}/{$dom}");
		// header("location: " . URL::Base() . "/library/index/{$page}/0/{$dom}");
		exit;
	} */

	public function action_rename_file(){

		for($i = 107; $i<=402; $i++){
			$id = $i;
			//echo($id);
			$library_model = new Model_Library(); 
			$setDatalibrary = $library_model->data_by_id($id);
			
			$setId = str_split($id);
			$count_split = count($setId);
			$path_global = $_SERVER['DOCUMENT_ROOT'];

			$pathId_f = $path_global.'/uploads/library/';
			$pathId = '';
			$pathId_new = '';
			$new_name = Briliant::slugify($setDatalibrary['title']);
			
			$path_id_now = [];
			$path_id_now_new = [];

			//print_r($new_name);

			foreach($setId as $k=>$v){
				//print_r($v);
				if($k == 0){
					$pathId = $pathId_f.$v.'/';
					$pathId_new = $pathId_f.$v.'/';
				} else {
					$pathId = $v.'/';
					$pathId_new = $v.'/';
				}

				//print_r($pathId_new);

				if($k+1 == $count_split){
					//$pathId .= $id.'.'.$setDatalibrary['fileType'];
					//$pathId .= $id.'_1140x570.'.$setDatalibrary['fileType'];
					//$pathId .= $id.'_180x90.'.$setDatalibrary['fileType'];
					//$pathId .= $id.'_263x132.'.$setDatalibrary['fileType'];
					//$pathId .= $id.'_278x139.'.$setDatalibrary['fileType'];
					$pathId .= $id.'_360x180.'.$setDatalibrary['fileType'];

					//$pathId_new .= $new_name.'.'.$setDatalibrary['fileType'];
					//$pathId_new .= $new_name.'_1140x570.'.$setDatalibrary['fileType'];
					//$pathId_new .= $new_name.'_180x90.'.$setDatalibrary['fileType'];
					//$pathId_new .= $new_name.'_263x132.'.$setDatalibrary['fileType'];
					//$pathId_new .= $new_name.'_278x139.'.$setDatalibrary['fileType'];
					$pathId_new .= $new_name.'_360x180.'.$setDatalibrary['fileType'];
				}

				$path_id_now[] = $pathId;
				$path_id_now_new[] = $pathId_new;

				//print_r($path_id_now.' '.$path_id_now_new);
				//shell_exec('mv '.$path_id_now.' '.$path_id_now_new);
			}

			//print_r($path_id_now_new);
			$path_now_fix = $path_id_now[0].$path_id_now[1].$path_id_now[2];
			$path_now_fix_new = $path_id_now_new[0].$path_id_now_new[1].$path_id_now_new[2];

			//echo($path_now_fix.'<br>');
			//echo($path_now_fix_new.'<br>');

			shell_exec('mv '.$path_now_fix.' '.$path_now_fix_new);
		} 

		exit;
	}
}

<!-- Content Wrapper. Contains page content -->
<?php if(empty($data['dom'])):
	$add_new = 'library/new/';
	?>
	<div class="content-wrapper">
<?php else:
	$add_new = 'library/new/1/0/'.$data['dom'];
	?>
	<script>
			function paste(dom_id, id){
				window.opener.document.getElementById(dom_id).value = id;
				window.opener.<?php echo $data['dom']; ?>();
				window.close();
			}

		</script>
	<div class="content-wrapper" style="margin-left: 0;">
<?php endif;

		$is_gallery='';
		if(!empty($_GET['img-gallery'])){
			if($_GET['img-gallery'] == 1){
				$is_gallery = 1;
			}
		}
	?>
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php
				if($is_gallery == 1):
					echo __('Image Gallery');
				else:
					echo __('Library');
				endif;
			?>
		</h1>
		<?php //if(!empty($data['dom'])): ?>
			<ol class="breadcrumb">
				<li><a href="/"><?php echo __('Home'); ?></a></li>
				<li class="active"><a href="<?php echo URL::Base(); ?>library?img-gallery=1"><?php echo __('Library'); ?></a></li>
			</ol>
		<?php //endif; ?>
	</section>

	<section class="content">

		<?Php
			if($is_gallery != 1){
		?>

		<div class="box box-primary">
			<div class="box-body">
				<a href="<?php echo URL::Base().$add_new; ?>"><button style="width:150px" class="btn btn-block btn-primary btn-sm"><?php echo __('Add New Data'); ?></button></a>
			</div>
		</div>

		<?php
			}
		?>

		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('List Data'); ?></h3>
						<div class="box-tools">
							<form method="post">
								<div class="input-group" style="width: 150px;">
									<input type="text" name="search" class="form-control input-sm pull-right" placeholder="<?php echo __('Search'); ?>" value="<?php echo !empty($data['search']) ? $data['search'] : ''; ?>" minlength=3>
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php echo !empty($data['pagination']) ? $data['pagination'] : ''; ?>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<?php if(empty($data['dom'])): ?>
								<th><?php echo __('Título'); ?></th>
								<th><?php echo __('Image'); ?></th>
								<th><?php echo __('Caption'); ?></th>
								<th><?php echo __('Ações'); ?></th>
								<?php else: ?>
								<th></th>
								<th><?php echo __('Ações'); ?></th>
								<?php endif; ?>
							</tr>
							<?php
							if(!empty($data['list'])) {
								foreach($data['list'] as $v_list) {

									$img='';
									$img_url='';
									if(!empty($v_list['fileType'])){
										$split_id = str_split($v_list['id']);

										$title = ucwords($v_list['title']);
										$caption = ucwords($v_list['caption']);

										$caption = substr($caption, 0, 100);;

										if(!empty($data['search'])){
											$search = str_replace("'","",$data['search']);
											$search = explode(",",$search);
											foreach($search as $v ){
												$title = str_ireplace($v, "<span style='color:red;'><b><i>{$v}</i></b></span>", $title);
												$caption = str_ireplace($v, "<span style='color:red;'><b><i>{$v}</i></b></span>", $caption);
											}
										}

										if(empty($data['dom'])){

											$size='_360x180';
											$title_img = Briliant::slugify($v_list['title']);
											
											//$img = "<img style='width:200px;height:auto;' src='/uploads/library/".implode('/', $split_id)."/{$v_list['id']}{$size}.{$v_list['fileType']}'>";
											$img = "<img style='width:200px;height:auto;' src='/uploads/library/".implode('/', $split_id)."/{$title_img}{$size}.{$v_list['fileType']}'>";

											if($is_gallery != 1){
												echo '
													<tr id="row'.$v_list['id'].'">
														<td>' . $title . '</td>
														<td>' . $img . '</td>
														<td>' . $caption . '</td>
														<td>
														<a href="'.URL::Base().'library/new/' . "{$data['page']}/{$v_list['id']}" . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a>
														<br/>

														<button type="button" onclick="delLib('.$v_list['id'].')" class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button>
														</td>

													</tr>
												';
											} else {
												echo '
													<tr>
														<td>' . $title . '</td>
														<td>' . $img . '</td>
														<td>' . $caption . '</td>
														<td>
														<a href="'.URL::Base().'library/new/' . "{$data['page']}/{$v_list['id']}" . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a>
														<br/>

														</td>

													</tr>
												';
											}
										}else{
											$cdn = Kohana::$config->load('path.cdn');
											$title_img = Briliant::slugify($v_list['title']);
											
											//$img_url="{$cdn}/library/".implode('/', $split_id)."/{$v_list['id']}_360x180.{$v_list['fileType']}";
											//$img_paste="{$cdn}/library/".implode('/', $split_id)."/{$v_list['id']}_1140x570.{$v_list['fileType']}";

											$img_url="{$cdn}/library/".implode('/', $split_id)."/{$title_img}_360x180.{$v_list['fileType']}";
											$img_paste="{$cdn}/library/".implode('/', $split_id)."/{$title_img}_1140x570.{$v_list['fileType']}";

											$img = "<a href='javascript:paste(\"{$data['dom']}\",\"{$img_paste}\")'><img style='width:200px;height:auto;' src='{$img_url}'></a>";
											$copy = '<a href="javascript:paste(\''.$data['dom'].'\', \''.$img_paste.'\')"><button class="btn btn-block btn-success btn-xs">' . __('Set as Image') . '</button></a><br/>';

											echo '
											<tr>
												<td>' . "{$title}<br/>{$img}<br/>{$caption}" . '</td>
												<td>
												'.$copy.'
												<a><button disabled="disabled" class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a>
												<br/>

												<a><button disabled="disabled" class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button>
												</a>

												</td>
											</tr>';

										}
									}
								}
							}
							?>
						</table>
					</div>
					<!-- /.box-body -->
					<?php echo !empty($data['pagination']) ? $data['pagination'] : ''; ?>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>

		<?Php
			if($is_gallery != 1){
		?>

		<div class="box box-primary">
			<div class="box-body">
				<a href="<?php echo URL::Base(); ?>library/new/"><button style="width:150px" class="btn btn-block btn-primary btn-sm"><?php echo __('Add New Data'); ?></button></a>
			</div>
		</div>

		<?php
			}
		?>

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

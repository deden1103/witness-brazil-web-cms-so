<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Corretor de informação'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Corretor de informação'); ?></li>
			<li class="active"><a href="/userkom"><?php echo __('Novo'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Novo Data Corretor de informação'); ?></h3>
					</div>
					<form role="form" method="post" action="<?php echo URL::Base(); ?>userkom/save">
						<div class="box-body">
						
							<!-- Hidden Text -->
							<div class="form-group">
								<label><?php echo __('Nama') ?></label>
								<input type="text" name="title"  class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Password') ?></label>
								<input type="password" name="password"  maxlength="60" placeholder="" class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Email') ?></label>
								<input type="email" name="email"  maxlength="60"  class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Telefone') ?></label>
								<input type="text" name="phone"  maxlength="60"  class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Número de série do SIM') ?></label>
								<input type="text" name="simserial"  maxlength="60"  class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Género') ?></label>
								<select class="form-control" name="gender">
								<option>------Género-----</option>
									<?php
										foreach($data["master_gender"] as $v){
											$selected = "";
										
											echo '<option value="'.$v['msgdId'].'" '.$selected.'>'.$v['msgdName'].'</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label><?php echo __('Data de nascimento') ?></label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" name="ttl" class="form-control pull-right" id="datepicker" value="">
								</div>
							</div>
							<div class="form-group">
								<label>Province</label>
								<select class="form-control" name="province_id" id="province">
									<option>-----Escolher a opção----</option>
									<?php 
										foreach($data['province'] as $v){
											$selected = "";
											
											echo '<option value="'.$v['id'].'" '.$selected.'>'.$v['name'].'</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label>Regência</label>
								<select class="form-control" name="regency_id" id="regency">
							
									<?php 
										foreach($data['regency'] as $v){
											$selected = "";
											
											echo '<option value="'.$v['id'].'" '.$selected.'>'.$v['name'].'</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label>Distrito</label>
								<select class="form-control" name="district_id" id="district">
							
									<?php 
										foreach($data['district'] as $v){
											$selected = "";
											
											echo '<option value="'.$v['id'].'" '.$selected.'>'.$v['name'].'</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label>Aldeia</label>
								<select class="form-control" name="village_id" id="village">
								
									<?php 
										foreach($data['village'] as $v){
											$selected = "";
											
											echo '<option value="'.$v['id'].'" '.$selected.'>'.$v['name'].'</option>';
										}
									?>
								</select>
							</div>

							<div class="form-group">
								<label><?php echo __('Endereço') ?></label>
								<textarea class="form-control" rows="3" name="alamat" maxlength="200"  minlength="3" required><?php echo !empty($data['detail']['alamat']) ? $data['detail']['alamat'] : ''; ?></textarea>
							</div>
							
							<?php 
								$sl_category = '';
								if(!empty($data['detail']['category_id'])) {
									$sl_category = $data['detail']['category_id'];
								}
								echo Userkom::select_list('category', 1, $sl_category); 
							?>
							
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Guardar os dados'); ?></button>
							<a href="<?= URL::base() ?>userkom/search"><button type="button" class="btn btn-danger"><?php echo __('Cancelar'); ?></button></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('User Community'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('User Community'); ?></li>
			<li class="active"><a href="/userkom"><?php echo __('Edit'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Edit Data User Community'); ?></h3>
					</div>
					<form role="form" method="post" action="<?php echo URL::Base(); ?>userkom/update">
						<div class="box-body">
						
							<!-- Hidden Text -->
							<input type="hidden" name="id" value="<?php echo !empty($data['detail']['id']) ? $data['detail']['id'] : ''; ?>" />
							<input type="hidden" name="status" value="<?php echo !empty($data['detail']['status']) ? $data['detail']['status'] : ''; ?>" />
							<input type="hidden" name="is_edit" value="1" />
							<div class="form-group">
								<label><?php echo __('Nama') ?></label>
								<input type="text" name="title" value="<?php echo !empty($data['detail']['title']) ? $data['detail']['title'] : ''; ?>" maxlength="60" placeholder="<?php echo __('Only 60 Characters'); ?>" class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Email') ?></label>
								<input type="text" name="email" value="<?php echo !empty($data['detail']['email']) ? $data['detail']['email'] : ''; ?>" maxlength="60" placeholder="<?php echo __('Only 60 Characters'); ?>" class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Telefone') ?></label>
								<input type="text" name="phone" value="<?php echo !empty($data['detail']['phone']) ? $data['detail']['phone'] : ''; ?>" maxlength="60" placeholder="<?php echo __('Only 60 Characters'); ?>" class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Número de série do SIM') ?></label>
								<input type="text" name="simserial" value="<?php echo !empty($data['detail']['simserial']) ? $data['detail']['simserial'] : ''; ?>" maxlength="60" placeholder="<?php echo __('Only 60 Characters'); ?>" class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Género') ?></label>
								<select class="form-control" name="gender">
									<?php
										foreach($data["master_gender"] as $v){
											$selected = "";
											if($v['msgdId'] == $data['detail']['gender']){
												$selected = "selected";
											}
											echo '<option value="'.$v['msgdId'].'" '.$selected.'>'.$v['msgdName'].'</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label><?php echo __('Data de nascimento') ?></label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" name="ttl" class="form-control pull-right" id="datepicker" value="<?php echo date("d/m/Y", strtotime($data['detail']['ttl'])); ?>">
								</div>
							</div>
							<div class="form-group">
								<label>Province</label>
								<select class="form-control" name="province_id" id="province">
									<?php 
										foreach($data['province'] as $v){
											$selected = "";
											if($v['id'] == $data['detail']['province_id']){
												$selected = "selected";
											}
											echo '<option value="'.$v['id'].'" '.$selected.'>'.$v['name'].'</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label>Regência</label>
								<select class="form-control" name="regency_id" id="regency">
									<?php 
										foreach($data['regency'] as $v){
											$selected = "";
											if($v['id'] == $data['detail']['regency_id']){
												$selected = "selected";
											}
											echo '<option value="'.$v['id'].'" '.$selected.'>'.$v['name'].'</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label>Distrito</label>
								<select class="form-control" name="district_id" id="district">
									<?php 
										foreach($data['district'] as $v){
											$selected = "";
											if($v['id'] == $data['detail']['district_id']){
												$selected = "selected";
											}
											echo '<option value="'.$v['id'].'" '.$selected.'>'.$v['name'].'</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label>Aldeia</label>
								<select class="form-control" name="village_id" id="village">
									<?php 
										foreach($data['village'] as $v){
											$selected = "";
											if($v['id'] == $data['detail']['village_id']){
												$selected = "selected";
											}
											echo '<option value="'.$v['id'].'" '.$selected.'>'.$v['name'].'</option>';
										}
									?>
								</select>
							</div>

							<div class="form-group">
								<label><?php echo __('Endereço') ?></label>
								<textarea class="form-control" rows="3" name="alamat" maxlength="200"  minlength="3" required><?php echo !empty($data['detail']['alamat']) ? $data['detail']['alamat'] : ''; ?></textarea>
							</div>
							
							<?php 
								$sl_category = '';
								if(!empty($data['detail']['category_id'])) {
									$sl_category = $data['detail']['category_id'];
								}
								echo Userkom::select_list('category', 1, $sl_category); 
							?>

							<!-- <div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Tetapkan sebagai editor') ?></label>
								<select name="isEditor" id="isEditor" class="form-control">
									<option disabled>--Pilih--</option>
									<option  value="0">Tidak</option>
									<option  value="1">YA</option>
								</select>
							</div>

							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Aktivkan Notifikasi WA') ?></label>
								<select name="isActiveNotif" id="isActiveNotif" class="form-control">
									<option disabled>--Pilih--</option>
									<option  value="0">Tidak</option>
									<option  value="1">YA</option>
								</select>
							</div> -->
							
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Atualizar os dados'); ?></button>
							<a href="<?= URL::base() ?>userkom/search"><button type="button" class="btn btn-danger"><?php echo __('Cancelar'); ?></button></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>

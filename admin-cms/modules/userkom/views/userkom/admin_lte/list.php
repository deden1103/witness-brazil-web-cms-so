<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Corretor de informação'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Community'); ?></li>
			<li class="active"><a href="/userkom"><?php echo __('Corretor de informação'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
						<!-- Date range -->

						<div style="display:flex;flex-direction:row;">
							<div class="form-group">
								<form method="post" id="catForm">
								<label>Community</label>
								<br>
									<?php echo Userkom::select_list('category', 1, @$data['category'], "onChange=document.getElementById('catForm').submit();", 'lite'); ?>
								</form>
							</div>
							&nbsp;&nbsp;
							<div class="form-group">
								<form method="post" id="province_form">
								<label>Província</label>
								<br>
									<?php echo Userkom::select_list_provinsi("onChange=document.getElementById('province_form').submit();", 'lite'); ?>
								</form>
							</div>
						</div>

						


					</div>
					
				</div>
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $data['count_all'] . ' ' .__('Dados encontrados'); ?></h3>
						<div class="box-tools">
							<form method="post" action="/userkom/search">
								<div class="input-group" style="width: 150px;">
									<input type="text" name="search" class="form-control input-sm pull-right" placeholder="<?php echo __('Search'); ?>" value="<?php echo !empty($data['search']) ? $data['search'] : ''; ?>">
									<input type="hidden" name="date_range" value="<?php echo !empty($_GET['date_range']) ? $_GET['date_range'] : ''; ?>" />
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php 
					echo !empty($data['pagination']) ? $data['pagination'] : '';
					if($data['version']=='lite'){
						include('lite_template.php');
					}else{
						include('full_template.php');
					}
					echo !empty($data['pagination']) ? $data['pagination'] : ''; 
					?>
				</div>
			</div>
		</div>
	</section>
</div>

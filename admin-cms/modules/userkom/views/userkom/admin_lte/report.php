<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Relatório'); ?>
		</h1>
		
		<ol class="breadcrumb">
			<li><?php echo __('Relatório'); ?></li>
			<li class="active"><a href="/userkom"><?php echo __('Report'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Relatório'); ?></h3>
					</div>
					<form role="form" method="post" action="<?php echo URL::Base(); ?>userkom/report_submit">
						<div class="box-body">
						
							<!-- Hidden Text -->
						
							
							
							
							
						
							<!-- <h1 class="box-title" style="color:red;"><?php echo __('MAINTENANCE'); ?></h1> -->
						
						
							
						
							
							<?php 
								$sl_category = '';
								if(!empty($data['detail']['category_id'])) {
									$sl_category = $data['detail']['category_id'];
								}
								echo Userkom::select_list('category', 1, $sl_category); 
							?>


                            <div class="form-group">
								<label><?php echo __('Editor') ?></label>
								<select class="form-control" name="adminId">
                                <option value="">--Selecionar o editor/admin</option>
									<?php
										foreach($data["master_gender"] as $v){
											$selected = "";
											// if($v['msgdId'] == $data['detail']['gender']){
											// 	$selected = "selected";
											// }
											echo '<option value="'.$v['admiId'].'" '.$selected.'>'.$v['admiRealName'].'</option>';
										}
									?>
								</select>
							</div>


							<!-- <div class="form-group">
								<label><?php echo __('Tanggal') ?></label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" name="tanggal" class="form-control pull-right" id="datepicker" value="">
								</div>
							</div> -->

							
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Descarregar o EXCEL'); ?></button>
							<a href="<?= URL::base() ?>userkom/search"><button type="button" class="btn btn-danger"><?php echo __('Cancelar'); ?></button></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Corretor de informação'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Corretor de informação'); ?></li>
			<li class="active"><a href="/userkom/changepass"><?php echo __('Mudar senha'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
						<!-- Date range -->
						<div class="form-group">
							
							<form method="post" id="catForm">
							<label>Community</label>
							<br>
								<?php echo Userkom::select_list('category', 1, @$data['category'], "onChange=document.getElementById('catForm').submit();", 'lite'); ?>
							</form>
						</div>
					</div>
					
				</div>
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $data['count_all'] . ' ' .__('Dados encontrados'); ?></h3>
						<div class="box-tools">
							<form method="post" action="/userkom/changepass">
								<div class="input-group" style="width: 150px;">
									<input type="text" name="search" class="form-control input-sm pull-right" placeholder="<?php echo __('Search'); ?>" value="<?php echo !empty($data['search']) ? $data['search'] : ''; ?>">
									<input type="hidden" name="date_range" value="<?php echo !empty($_GET['date_range']) ? $_GET['date_range'] : ''; ?>" />
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
                    <div class="box-body">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th style="text-align:center;"><?php echo __('Nama'); ?></th>
                                <th style="text-align:center;"><?php echo __('Email'); ?></th>
                                <th style="text-align:center;"><?php echo __('Community'); ?></th>
                                <th colspan="2" style="width:20%;text-align:center;"><?php echo __('Ações'); ?></th>
                            </tr>
                            <?php
                            if(!empty($data['list'])) {
                                foreach($data['list'] as $v_list) {
                                    $now = date('Y-m-d H:i:s');
                                    
                                    // Image article
                                    $img_video = '' . __('[ No Image Available ]') . '';
                                    if(!empty($v_list['images'][0]['image_id'])) {
                                        $split_id = str_split($v_list['images'][0]['image_id']);
                                        $path_folder_image = implode('/', $split_id);
                                        $img_title = Briliant::slugify($v_list['images'][0]['image_title']);
                                        //$img_video = '<img src="' . URL::base() .'uploads/library/' . $path_folder_image . '/' . $v_list['images'][0]['image_id'] . '_278x139.' . $v_list['images'][0]['image_type'] . '" />';
                                        $img_video = '<img src="' . URL::base() .'uploads/library/' . $path_folder_image . '/' . $img_title . '_278x139.' . $v_list['images'][0]['image_type'] . '" />';
                                    }

                                    // Status
                                    $video_status = '';
                                    if($v_list['status'] == 1) {
                                        $video_status = '<span class="label label-info">Elaborado</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
                                    } else if($v_list['status'] == 0) {
                                        $video_status = '<span class="label label-danger">Deleted</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
                                    } else if($v_list['status'] == 2) {
                                        //$url_live = Kohana::$config->load('path.arah')."/video/{$v_list['id']}/".URL::title($v_list['title']).'.html';
                                        $video_status = '<span class="label label-success">Published</span> <b>' . date('d M Y H:i', strtotime($v_list['publish'])) . '</b>';
                                        //$video_status .= '<br><br><b><i>Approved By :<br>' . Video::get_user_approver($v_list['id']) . '<i></b>';
                                    }
                                    
                                    // Publish schedule
                                    if($v_list['publish'] > $now) {
                                        $video_status .= '<br/></br><span class="label bg-maroon-active color-palette">Schedule</span> <b>' . date('d M Y H:i', strtotime($v_list['publish'])) . '</b>';
                                    }

                                    // Default Button
                                    $level = Controller_Backend::check_level();
                                    $category = Video::slugfy($v_list['category_name']);
                                    if($level == 3 or $level == 1){
                                        if($v_list['status'] == 2){
                                            $button = '
                                            <a href="'.URL::Base().'userkom/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
                                            <a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a><br><br>
                                        ';
                                        }else{
                                            $button = '
                                            <a href="'.URL::Base().'userkom/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
                                            <a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a><br><br>
                                        ';
                                        }
                                        
                                    }else{
                                        $button = '
                                                <a href="'.URL::Base().'userkom/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
                                                <a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a><br><br>
                                        ';
                                    }

                                    
                                    
                                    // Button If Status 0
                                    if($v_list['status'] == 0) {
                                        $button = '
                                                <a href="javascript:void()"><button disabled class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
                                                <a href="javascript:void()"><button disabled class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a>
                                        ';
                                    }
                                    
                                    $article_title = $v_list['title'];
                                    if(!empty($data['search'])) {
                                        $ex_search = explode(',',$data['search']);
                                        foreach($ex_search as $v_search) {
                                            $article_title = str_ireplace(trim($v_search), '<span style="color:red"><b><i>' . trim($v_search). '</i></b></span>', $article_title);
                                        }
                                    }
                                    // if(!empty($url_live)){
                                    // 	$article_title = "<a href='{$url_live}' target='_BLANK'>{$article_title}</a>";
                                    // }

                                    //level
                                    if ($v_list['level'] == 1) {
                                        $level = "Admin";
                                    }elseif ($v_list['level'] == 2) {
                                        $level = "Redaksi";
                                    }else{
                                        $level = "Editor";
                                    }

                                
                                    echo '
                                        <tr>
                                            <td>
                                                ' . $article_title . '
                                            </td>
                                            <td>
                                                    ' . $v_list['description'] . '
                                            </td>
                                            <td>
                                                    ' . $v_list['category_name'] . '
                                            </td>
                                        
                                            <td><a href="'.URL::Base().'userkom/cpas/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Mudar senha') . '</button></a></td>
                                        </tr>
                                    ';
                                }
                            }
                            ?>
                        </table>
                    </div>
				</div>
			</div>
		</div>
	</section>
</div>

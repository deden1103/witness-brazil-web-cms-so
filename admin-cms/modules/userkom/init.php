<?php

// Route index
Route::set('userkom_index', 'userkom/index(/<page>)')
	->defaults(array(
		'controller' => 'Userkom',
		'action'     => 'index',
	));


	// Route search
Route::set('userkom_search', 'userkom/search(/<page>)')
->defaults(array(
	'controller' => 'Userkom',
	'action'     => 'search',
));

// Route delete
Route::set('userkom_delete', 'userkom/delete(/<id>)')
	->defaults(array(
		'controller' => 'Userkom',
		'action'     => 'delete',
	));


// Route edit
Route::set('userkom_edit', 'userkom/edit(/<id>)')
	->defaults(array(
		'controller' => 'Userkom',
		'action'     => 'edit',
	));
	

	
// Route new
Route::set('userkom_new', 'userkom/new')
	->defaults(array(
		'controller' => 'Userkom',
		'action'     => 'new',
	));
	
// Route save
Route::set('userkom_save', 'userkom/save')
	->defaults(array(
		'controller' => 'Userkom',
		'action'     => 'save',
	));


	
// Route update
Route::set('userkom_update', 'userkom/update')
	->defaults(array(
		'controller' => 'Userkom',
		'action'     => 'update',
	));

// Route Report
Route::set('userkom_report', 'userkom/report')
	->defaults(array(
		'controller' => 'Userkom',
		'action'     => 'report',
	));
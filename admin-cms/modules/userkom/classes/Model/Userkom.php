<?php defined('SYSPATH') or die('No direct script access.');

class Model_Userkom extends Model
{
    public function read_data($table = '', $column_sort = '')
    {

        $exec = DB::select()
            ->from($table)
            ->order_by($column_sort, 'ASC')
            ->execute()
            ->as_array();

        return $exec;

    }

    public function ajax($table = '', $column = '', $column_sort = '', $where = '')
    {

        $exec = DB::select()
            ->from($table)
            ->where($column, '=', $where)
            ->order_by($column_sort, 'ASC')
            ->execute()
            ->as_array();

        return $exec;

    }

    public function count_search($date1 = '', $date2 = '', $search = '')
    {
        $session = Session::instance();
        $publish = $session->get("publish_video", "ukomSaved");
        $return = 0;
        //print_r($publish .' '. $date1 .' sampai '. $date2); exit;

        $query = DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
            ->from('user_komunitas');

        if (!empty($date1) and !empty($date2)) {
            $query = $query->where(DB::expr('DATE(' . $publish . ')'), '>=', $date1)
                ->where(DB::expr('DATE(' . $publish . ')'), '<=', $date2);
        }

        $ex_search = explode(',', $search);
        foreach ($ex_search as $v_search) {
            $query = $query->where('ukomName', 'LIKE', '%' . trim($v_search) . '%');
        }

        $category = $session->get("category_userkom", "");
        if (!empty($category)) {
            $query = $query->where('ukomKomtId', '=', $category);
        }

        // echo Debug::vars((string) $query);
        $query = $query->order_by('ukomId', 'DESC')
            ->where('ukomStatus', '!=', 0)
            ->execute()
            ->as_array();

        if (!empty($query[0]['COUNT'])) {
            $return = $query[0]['COUNT'];
        }

        return $return;

    }

    public function list_search($date1 = '', $date2 = '', $search = '', $limit = '', $offset = '')
    {
        $session = Session::instance();
        $publish = $session->get("publish_video", "ukomSaved");

        $exec = DB::select(
            array('ukomId', 'id'),
            array('ukomName', 'title'),
            array('ukomEmail', 'description'),
            array('ukomSaved', 'publish'),
            array('ukomSaved', 'saved'),
            array('ukomAdmiIdSaved', 'savedId'),
            array('ukomStatus', 'status'),
            array('ukomKomtId', 'category_id'),
            array('komtName', 'category_name'),
            array('admiRealName', 'user_name'),
            array('admiLvl', 'level')

        )
            ->from('user_komunitas')
            ->join('admin', 'LEFT')
            ->on('user_komunitas.ukomAdmiIdSaved', '=', 'admin.admiId')
            ->join('komunitas', 'LEFT')
            ->on('user_komunitas.ukomKomtId', '=', 'komunitas.komtId');

        if (!empty($date1) and !empty($date2)) {
            $exec = $exec->where(DB::expr('DATE(' . $publish . ')'), '>=', $date1)
                ->where(DB::expr('DATE(' . $publish . ')'), '<=', $date2);
        }

        $exec = $exec->where('ukomName', 'LIKE', '%' . $search . '%')
            ->order_by('ukomId', 'DESC')
            ->where('ukomStatus', '!=', 0);

        $ex_search = explode(',', $search);
        foreach ($ex_search as $v_search) {
            $exec = $exec->where('ukomName', 'LIKE', '%' . trim($v_search) . '%');
        }

        $category = $session->get("category_userkom", "");
        //print_r($category); exit;
        if (!empty($category)) {
            $exec = $exec->where('ukomKomtId', '=', $category);
        }


        $province_id = $session->get("province_id", "");
        //print_r($category); exit;
        if (!empty($province_id)) {
            $exec = $exec->where('ukomProvinsi', '=', $province_id);
        }

        $exec = $exec->limit($limit)
            ->offset($offset)
            ->execute()
            ->as_array();

        return $exec;

    }

    public function save_data($data = '', $user_id = '')
    {

        $date = explode("/", $data['ttl']);
        $date = $date[2] . "-" . $date[1] . "-" . $date[0];

        $query = DB::insert('user_komunitas', array(
            'ukomName',
            'ukomPassword',
            'ukomEmail',
            'ukomKomtId',
            'ukomProvinsi',
            'ukomKabupaten',
            'ukomKecamatan',
            'ukomKelurahan',
            'ukomGender',
            'ukomAlamat',
            'ukomTtl',
            'ukomPhone',
            'simSerialNumber',
            'ukomAdmiIdSaved',
        ))
            ->values(array(
                $data['title'],
                SHA1($data['password']),
                $data['email'],
                $data['category'],
                $data['province_id'],
                $data['regency_id'],
                $data['district_id'],
                $data['village_id'],
                $data['gender'],
                $data['alamat'],
                $date,
                $data['phone'],
                $data['simserial'],
                $user_id,
            ))
            ->execute();
        return $query;

    }

    public function detail_data($id = '')
    {

        $return = '';

        $exec = DB::select(
            array('ukomId', 'id'),
            array('ukomName', 'title'),
            array('ukomEmail', 'email'),
            array('ukomSaved', 'saved'),
            array('ukomAdmiIdSaved', 'savedId'),
            array('ukomStatus', 'status'),
            array('ukomKomtId', 'category_id'),
            array('ukomProvinsi', 'province_id'),
            array('ukomKabupaten', 'regency_id'),
            array('ukomKecamatan', 'district_id'),
            array('ukomKelurahan', 'village_id'),
            array('ukomGender', 'gender'),
            array('ukomAlamat', 'alamat'),
            array('ukomTtl', 'ttl'),
            array('ukomPhone', 'phone'),
            array('simSerialNumber', 'simserial'),
            array('komtName', 'category_name'),
            array('admiRealName', 'user_name')
        )
            ->from('user_komunitas')
            ->join('admin', 'LEFT')
            ->on('user_komunitas.ukomAdmiIdSaved', '=', 'admin.admiId')
            ->join('komunitas', 'LEFT')
            ->on('user_komunitas.ukomKomtId', '=', 'komunitas.komtId')
            ->join('provinces', 'LEFT')
            ->on('user_komunitas.ukomProvinsi', '=', 'provinces.id')
            ->join('regencies', 'LEFT')
            ->on('user_komunitas.ukomKabupaten', '=', 'regencies.id')
            ->join('districts', 'LEFT')
            ->on('user_komunitas.ukomKecamatan', '=', 'districts.id')
            ->join('villages', 'LEFT')
            ->on('user_komunitas.ukomKelurahan', '=', 'villages.id')
            ->join('master_gender', 'LEFT')
            ->on('user_komunitas.ukomGender', '=', 'master_gender.msgdId')
            ->where('ukomId', '=', $id)
            ->execute()
            ->as_array();

        if (!empty($exec[0])) {

            $return = $exec[0];

        }

        return $return;

    }

    public function data_by_id($id = '')
    {

        $return = array();

        $exec = DB::select(
            array('ukomId', 'id'),
            array('ukomName', 'name'),
            array('ukomEmail', 'email'),
            array('ukomPassword', 'password')
        )
            ->from('user_komunitas')
            ->where('ukomId', '=', $id)
            ->execute()
            ->as_array();
        if (!empty($exec[0])) {
            $return = $exec[0];
        }

        return $return;

    }

    public function change_password($id = '', $password = '')
    {
        $query = DB::update('user_komunitas')
            ->set(
                array('ukomPassword' => SHA1($password))
            )
            ->where('ukomId', '=', $id)
            ->execute();
    }

    public function update_data($data = '', $user_id = '', $action = '')
    {
        // Id video
        $id_video = $data['id'];

        // Start Transaction
        Database::instance()->begin();

        // Check old image

        // Update date video
        $date = explode("/", $data['ttl']);
        $date = $date[2] . "-" . $date[1] . "-" . $date[0];

        $update = DB::update('user_komunitas')
            ->set(array('ukomName' => $data['title']))
            ->set(array('ukomEmail' => $data['email']))
            ->set(array('ukomKomtId' => $data['category']))
            ->set(array('ukomProvinsi' => $data['province_id']))
            ->set(array('ukomKabupaten' => $data['regency_id']))
            ->set(array('ukomKecamatan' => $data['district_id']))
            ->set(array('ukomKelurahan' => $data['village_id']))
            ->set(array('ukomGender' => $data['gender']))
            ->set(array('ukomAlamat' => $data['alamat']))
            ->set(array('ukomTtl' => $date))
            ->set(array('ukomPhone' => $data['phone']))
            ->set(array('simSerialNumber' => $data['simserial']))
            ->set(array('ukomAdmiIdSaved' => $user_id));

        $update_1 = $update->where('ukomId', '=', $id_video)->execute();

        // Commit transaction
        Database::instance()->commit();

    }

    public function delete_data($id = '')
    {

        DB::update('user_komunitas')
            ->set(array('ukomStatus' => 0))
            ->where('ukomId', '=', $id)
            ->execute();

    }

    public function data_list_report($komId = '', $admin = '', $mulai = '', $akhir = "")
    {

        $return = array();

        if ($admin !== '') {
            $exec = DB::select(
                array('ukomId', 'id'),
                array('ukomKomtId', 'ukomKomtId'),
                array('ukomName', 'ukomName'),
                array('ukomEmail', 'ukomEmail'),
                array('ukomPhone', 'ukomPhone'),
                array('ukomAlamat', 'ukomAlamat'),
                array('komtName', 'komtName'),
                array('artcTitle', 'artcTitle'),
                array('artcDetail', 'artcDetail'),
                array('admiRealName', 'admiRealName'),
                array('admiEmail', 'admiEmail'),
                array('artcSaved', 'artcSaved'),
                array('artcPublishTime', 'artcPublishTime')

            )
                ->from('user_komunitas')
                ->join('komunitas', 'LEFT')
                ->on('user_komunitas.ukomKomtId', '=', 'komunitas.komtId')
                ->join('article', 'LEFT')
                ->on('user_komunitas.ukomId', '=', 'article.artcUkomIdSaved')
                ->join('admin', 'LEFT')
                ->on('article.artcUserIdApproved', '=', 'admin.admiId')
                ->where('ukomKomtId', '=', $komId)
                ->where('artcUserIdApproved', '=', $admin)
            // ->where('artcSaved', '<=', $mulai)
            // ->where('artcSaved', '>', $akhir)
                ->group_by('ukomId')
            // ->limit(10)
                ->execute()
                ->as_array();
        } else {

            if ($komId !== '') {
                $exec = DB::select(
                    array('ukomId', 'id'),
                    array('ukomKomtId', 'ukomKomtId'),
                    array('ukomName', 'ukomName'),
                    array('ukomEmail', 'ukomEmail'),
                    array('ukomPhone', 'ukomPhone'),
                    array('ukomAlamat', 'ukomAlamat'),
                    array('komtName', 'komtName'),
                    array('artcTitle', 'artcTitle'),
                    array('artcDetail', 'artcDetail'),
                    array('admiRealName', 'admiRealName'),
                    array('admiEmail', 'admiEmail'),
                    array('artcSaved', 'artcSaved'),
                    array('artcPublishTime', 'artcPublishTime')

                )
                    ->from('user_komunitas')
                    ->join('komunitas', 'LEFT')
                    ->on('user_komunitas.ukomKomtId', '=', 'komunitas.komtId')
                    ->join('article', 'LEFT')
                    ->on('user_komunitas.ukomId', '=', 'article.artcUkomIdSaved')
                    ->join('admin', 'LEFT')
                    ->on('article.artcUserIdApproved', '=', 'admin.admiId')
                    ->where('ukomKomtId', '=', $komId)
                // ->where('artcSaved', '<=', $mulai)
                // ->where('artcSaved', '>', $akhir)
                    ->group_by('ukomId')
                // ->limit(10)
                    ->execute()
                    ->as_array();
            } else {
                $exec = DB::select(
                    array('ukomId', 'id'),
                    array('ukomKomtId', 'ukomKomtId'),
                    array('ukomName', 'ukomName'),
                    array('ukomEmail', 'ukomEmail'),
                    array('ukomPhone', 'ukomPhone'),
                    array('ukomAlamat', 'ukomAlamat'),
                    array('komtName', 'komtName'),
                    array('artcTitle', 'artcTitle'),
                    array('artcDetail', 'artcDetail'),
                    array('admiRealName', 'admiRealName'),
                    array('admiEmail', 'admiEmail'),
                    array('artcSaved', 'artcSaved'),
                    array('artcPublishTime', 'artcPublishTime')

                )
                    ->from('user_komunitas')
                    ->join('komunitas', 'LEFT')
                    ->on('user_komunitas.ukomKomtId', '=', 'komunitas.komtId')
                    ->join('article', 'LEFT')
                    ->on('user_komunitas.ukomId', '=', 'article.artcUkomIdSaved')
                    ->join('admin', 'LEFT')
                    ->on('article.artcUserIdApproved', '=', 'admin.admiId')
                // ->where('ukomKomtId', '=', $komId)
                // ->where('artcSaved', '<=', $mulai)
                // ->where('artcSaved', '>', $akhir)
                    ->group_by('ukomId')
                // ->limit(10)
                    ->execute()
                    ->as_array();
            }

        }

        /*

        [ukomId] => 7
        [ukomKomtId] => 2
        [ukomName] => nanu
        [ukomPassword] => 3608a6d1a05aba23ea390e5f3b48203dbb7241f7
        [ukomEmail] => nanu@gmail.com
        [token] => eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjciLCJlbWFpbCI6Im5hbnVAZ21haWwuY29tIiwicGhvbmUiOiIwODU2ODcxNTY3ODEiLCJrb211bml0YXNpZCI6IjIiLCJsb2dnZWQiOnRydWUsImlhdCI6MTU2NzU3OTIwNCwiZXhwIjoxNTcwMTcxMjA0fQ.hnmI6Myz__X9UqbiY4Ek5xWCoGPLBMfs2qqr-Xqt4cc
        [ukomPhoto] => http://witness-api.hahabid.com/assets/uploads/profile/38193b1c1bf6d1fb2b08592d15e98114.jpg
        [ukomPhone] => 085687156781
        [simSerialNumber] =>
        [ukomOneSignal] => b69120cd-0ea3-48ce-ac7f-e59dbe2c7d3e
        [ukomTtl] => 1992-03-09
        [ukomGender] => 1
        [ukomProvinsi] => 32
        [ukomKabupaten] => 3275
        [ukomKecamatan] => 3275060
        [ukomKelurahan] => 3275060005
        [ukomAlamat] => -
        [ukomSaved] => 2019-05-20 09:37:56
        [ukomAdmiIdSaved] => 1
        [ukomStatus] => 1
        [lastLogin] => 2019-09-04 13:40:04
        [komtId] => 2
        [komtName] => Palmerah TEMPO
        [komtSaved] => 2019-05-17 14:34:05
        [komtAdmiIdSaved] => 1
        [komtStatus] => 1
        [artcId] => 169
        [artcTitle] => komputer canggih
        [artcTitleReal] =>
        [latitude] => -6.2513957
        [longitude] => 106.8292716
        [artcExcerpt] => mantul gan
        [artcDetail] => mantul gan
        [artcDetailReal] =>
        [artcMediaId] => 0
        [artcMediaFileType] => jpg
        [heightImage] => 750
        [artcKeyword] =>
        [artcMscvId] => 0
        [artcHotTopicId] =>
        [artcEmbedURL] =>
        [artcKomtId] => 2
        [artcUkomIdSaved] => 7
        [artcSaved] => 2019-09-04 13:20:53
        [artcPublishTime] => 2019-09-04 13:38:13
        [artcUserIdApproved] => 1
        [artcStatus] => 0
        [isShowUser] => 0
        [isEditing] => 0
        [admiId] => 1
        [admiEmail] => admin
        [admiPassword] => 3608a6d1a05aba23ea390e5f3b48203dbb7241f7
        [admiRealName] => administrator
        [admiSaved] => 2021-03-24 10:57:58
        [admiLvl] => 1
        [admiStatus] => 1

         */

        return $exec;

    }

    public function data_list_report_nodate($komId = '', $admin = '')
    {

        $return = array();

        if ($admin !== '') {
            $exec = DB::select(
                array('ukomId', 'id'),
                array('ukomKomtId', 'ukomKomtId'),
                array('ukomName', 'ukomName'),
                array('ukomEmail', 'ukomEmail'),
                array('ukomPhone', 'ukomPhone'),
                array('ukomAlamat', 'ukomAlamat'),
                array('komtName', 'komtName'),
                array('artcTitle', 'artcTitle'),
                array('artcDetail', 'artcDetail'),
                array('admiRealName', 'admiRealName'),
                array('admiEmail', 'admiEmail'),
                array('artcSaved', 'artcSaved'),
                array('artcPublishTime', 'artcPublishTime')

            )
                ->from('user_komunitas')
                ->join('komunitas', 'LEFT')
                ->on('user_komunitas.ukomKomtId', '=', 'komunitas.komtId')
                ->join('article', 'LEFT')
                ->on('user_komunitas.ukomId', '=', 'article.artcUkomIdSaved')
                ->join('admin', 'LEFT')
                ->on('article.artcUserIdApproved', '=', 'admin.admiId')
            // ->where('ukomKomtId', '=', $komId)
            // ->where('artcUserIdApproved', '=', $admin)
                ->group_by('ukomId')
            // ->limit(10)
                ->execute()
                ->as_array();
        } else {
            $exec = DB::select(
                array('ukomId', 'id'),
                array('ukomKomtId', 'ukomKomtId'),
                array('ukomName', 'ukomName'),
                array('ukomEmail', 'ukomEmail'),
                array('ukomPhone', 'ukomPhone'),
                array('ukomAlamat', 'ukomAlamat'),
                array('komtName', 'komtName'),
                array('artcTitle', 'artcTitle'),
                array('artcDetail', 'artcDetail'),
                array('admiRealName', 'admiRealName'),
                array('admiEmail', 'admiEmail'),
                array('artcSaved', 'artcSaved'),
                array('artcPublishTime', 'artcPublishTime')

            )
                ->from('user_komunitas')
                ->join('komunitas', 'LEFT')
                ->on('user_komunitas.ukomKomtId', '=', 'komunitas.komtId')
                ->join('article', 'LEFT')
                ->on('user_komunitas.ukomId', '=', 'article.artcUkomIdSaved')
                ->join('admin', 'LEFT')
                ->on('article.artcUserIdApproved', '=', 'admin.admiId')
            // ->where('ukomKomtId', '=', $komId)
                ->group_by('ukomId')
            // ->limit(10)
                ->execute()
                ->as_array();
        }

        return $exec;

    }

    public function getJumlahArticle($artcUkomIdSaved = '', $artcStatus = '')
    {
        // $query = DB::query(Database::SELECT, 'SELECT * FROM article WHERE artcUkomIdSaved = 1 AND artcStatus = 2')->execute()->as_array();
        // return $query;
        $sql = "SELECT count(artcId) as jum FROM article WHERE artcUkomIdSaved = {$artcUkomIdSaved} AND artcStatus = {$artcStatus}";
        $query = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $query[0]['jum'];
    }


    public function getJumlahAllArticle($artcUkomIdSaved = '')
    {
        // $query = DB::query(Database::SELECT, 'SELECT * FROM article WHERE artcUkomIdSaved = 1 AND artcStatus = 2')->execute()->as_array();
        // return $query;
        $sql = "SELECT count(artcId) as jum FROM article WHERE artcUkomIdSaved = {$artcUkomIdSaved}";
        $query = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $query[0]['jum'];
    }

}
/*

SELECT * FROM `user_komunitas`
LEFT JOIN `komunitas` ON `user_komunitas`.`ukomKomtId` = `komunitas`.`komtId`
LEFT JOIN `article` ON `user_komunitas`.`ukomId` = `article`.`artcUkomIdSaved`

SELECT `ukomName`,`komtName`,`artcId` FROM `user_komunitas`
LEFT JOIN `komunitas` ON `user_komunitas`.`ukomKomtId` = `komunitas`.`komtId`
LEFT JOIN `article` ON `user_komunitas`.`ukomId` = `article`.`artcUkomIdSaved`;

SELECT `ukomName` as `nama`,`komtName` as `nama komunitas`, COUNT(artcId) as `jumlah laporan dikirim` FROM `user_komunitas`
LEFT JOIN `komunitas` ON `user_komunitas`.`ukomKomtId` = `komunitas`.`komtId`
LEFT JOIN `article` ON `user_komunitas`.`ukomId` = `article`.`artcUkomIdSaved`
WHERE `artcStatus` = 1
GROUP BY `ukomId`;

SELECT `ukomName` as `nama`,`komtName` as `nama komunitas`, COUNT(artcId) as `jumlah laporan diterbitkan` FROM `user_komunitas`
LEFT JOIN `komunitas` ON `user_komunitas`.`ukomKomtId` = `komunitas`.`komtId`
LEFT JOIN `article` ON `user_komunitas`.`ukomId` = `article`.`artcUkomIdSaved`
WHERE `artcStatus` = 2
GROUP BY `ukomId`

 */

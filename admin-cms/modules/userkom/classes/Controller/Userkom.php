<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Userkom extends Controller_Backend {

	private $custom_header_form;
	private $custom_footer_form;
	private $video_model;
	private $global_model;

	public function before() {

		parent::before();
		$level = Controller_Backend::check_level();

		$level = Controller_Backend::check_level();
		if($level == 2){
			$this->redirect(URL::Base().'/keranjang');
		}elseif($level == 3){
			$this->redirect(URL::Base().'/home');
			
		}

		

		//set global model
		$this->global_model = new Model_Globalmodel();
		$this->video_model = new Model_Userkom();

		// Header Multiple SElect
		$this->custom_header_form = '
			<link rel="stylesheet" href="'.URL::Base().'assets/plugins/select2/select2.min.css">
			<link rel="stylesheet" href="'.URL::Base().'assets/dist/css/AdminLTE.min.css">
		';

		// Custom header for daterangepicker
		$this->custom_header_form .= '<link rel="stylesheet" href="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		// Tinymce
		$this->custom_footer_form = '
			<script src="'.URL::Base().'assets/tinymce/tinymce.min.js"></script>
			<script>
				tinymce.init({
					paste_data_images: true,
					valid_elements : \'*[*]\',
					selector: \'.f_tinymce\',
					theme: \'modern\',
                    templates: [{title: \'Berita Lainnya\', description: \'Berita Lainnya\', url: \'/template/terkait.html\'},{title: \'Button Selengkapnya\', description: \'Button Selengkapnya\', url: \'/template/selengkapnya.html\'},{title: \'Sharing\', description: \'Sharing Information\', url: \'/template/sharing.html\'},{title: \'Mutiara\', description: \'Informasi Mutiara\', url: \'/template/komunitas.html\'},{title: \'Download Arena\', description: \'Link Download Aplikasi Arena\', url: \'/template/arena.html\'}],
					plugins: [
						\'example advlist autolink lists link image charmap print preview hr anchor pagebreak\',
						\'searchreplace wordcount visualblocks visualchars code fullscreen\',
						\'insertdatetime media nonbreaking save table contextmenu directionality\',
						\'emoticons template paste textcolor colorpicker textpattern imagetools\'
					],
					toolbar1: \'insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image\',
					toolbar2: \'fullscreen preview media | forecolor backcolor emoticons | pagebreak | my_image\',
					image_advtab: true,
                                        pagebreak_separator: "<!--PAGE_SEPARATOR-->",
                                        pagebreak_split_block: true,
                                        setup: function (editor) {
                                        editor.addButton(\'my_image\', {
                                            text: \'Add Image From Gallery\',
                                            icon: false,
                                            onclick: function () {
                                                open_popup_img_tmce();
                                            }
                                        });
                                      },
				});
				function my_voice(texthtml) {
					tinyMCE.activeEditor.insertContent(texthtml);
				}
			</script>
		';

		// Multiple SElect
		$this->custom_footer_form .= '
			<script src="'.URL::Base().'assets/plugins/select2/select2.full.min.js"></script>
			<script>
				$(".select2").select2({allowClear: true, placeholder : "-- Choose Keyword --"});
			</script>
		';

		// Custom footer for daterangepicker
		$this->custom_footer_form .= '
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				//Date range picker
				$(function() {
					$(\'.publish_time\').daterangepicker({
						format: \'DD/MM/YYYY HH:mm:ss\',
						timePicker: true,
						singleDatePicker: true,
						timePickerIncrement: 1,
						timePicker12Hour: false,
						//minDate: \'' . date('d-m-Y H:i:s') . '\'
					});
				});
				$("#myButton").click(function() {
					$("#kotak1").data(\'daterangepicker\').toggle();
				});
			</script>
			<!--Tinymce hidden for append image textarea-->
			<input type="hidden" id="image_popup_tmce" />
			<script>
				function open_popup_img() {
					PopupCenter("/library/index/1/0/image_popup", "google", "640", "480");
				}

				function open_popup_img_tmce() {
					PopupCenter("/library/index/1/0/image_popup_tmce", "google", "640", "480");
				}

				function image_popup_tmce() {
						var str = $(\'#image_popup_tmce\').val();
						var n = str.lastIndexOf(".");
						var res = str.substring(0, n)+str.substring(n);
						tinyMCE.activeEditor.insertContent(\'<img src="\' + res + \'" width="100%" />\');
				}

				function image_popup() {
					$(\'#previewImage\').html(\'\');
					$(\'#previewImage\').append(\'<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreview();">X</span></center></div>\');
					$(\'#previewImage\').append(\'<img src="\' + $(\'#image_popup\').val() + \'" style="width:200px;"/>\');
				}

				function delPreview() {
					$(\'#previewImage\').html(\'\');
					$(\'#image_popup\').val(\'\');
				}

				// GIF IMAGE JS FUNCTION
				function open_popup_img_gif() {
					PopupCenter("/library/index/1/0/image_popup_gif", "google", "640", "480");
				}

				function image_popup_gif() {
					$(\'#previewImageGif\').html(\'\');
					$(\'#previewImageGif\').append(\'<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreviewGif();">X</span></center></div>\');
					$(\'#previewImageGif\').append(\'<img src="\' + $(\'#image_popup_gif\').val() + \'" style="width:200px;"/>\');
				}

				function delPreviewGif() {
					$(\'#previewImageGif\').html(\'\');
					$(\'#image_popup_gif\').val(\'\');
				}

			</script>
		';

	}

	public function action_index() {

		$this->redirect('/userkom/search');

	}

	public function action_search() {
	

		$session = Session::instance();

		//print_r('test');die;

		if(isset($_POST['version'])) $session->set('version_userkom', $_POST['version']);
		$data['version'] = $version = $session->get("version_userkom", "");

		if(isset($_POST['category'])) $session->set('category_userkom', $_POST['category']);
		$data['category'] = $category = $session->get("category_userkom", "");

		if(isset($_POST['province_id'])) $session->set('province_id', $_POST['province_id']);
		$data['province_id'] = $province_id = $session->get("province_id", "");

		if(isset($_POST['search'])) $session->set('search_userkom', $_POST['search']);
		$data['search'] = $search = $session->get("search_userkom", "");

	
		$data['main_title'] = __('User Community');
		$data['menu_active'] = 'komunitas';
		$data['menu_active_child'] = 'userkom';
		$data['menu_active_child_1'] = 'list_ukom';

		// Custom header for daterangepicker
		$data['custom_header'] = '<link rel="stylesheet" href="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		// Custom footer for daterangepicker
		$data['custom_footer'] = '
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				//Date range picker
				$(function() {
					$(\'#reservation\').daterangepicker(
						{
							format: \'DD/MM/YYYY\'
						}, function (start, end) {
							window.location = "/userkom/search/?date_range=" + $(\'#reservation\').val();
						}
					);
				});
			</script>
		';

		// Yes No Confirm
		$data['custom_footer'] .= '
			<script type="text/javascript">
				function del_confirm(id) {
					var dialog = confirm("' . __('Are you sure for delete this data ?') . '");
					if (dialog == true) {
						window.location.href="/userkom/delete/" + id;
					}
				}
			</script>
		';

		// Date parameter
		if(empty($date_range)) {
			$date1 = "";
			$date2 = "";

		} else {
			$ex_range = explode(' - ', $date_range);
			if(!empty($ex_range)) {
				$date1 = DateTime::createFromFormat('d/m/Y', $ex_range[0]);
				$date1 = $date1->format('Y-m-d');
				$date2 = DateTime::createFromFormat('d/m/Y', $ex_range[1]);
				$date2 = $date2->format('Y-m-d');

				// Date adding time
				$date1 = $date1 . ' 00:00:00';
				$date2 = $date2 . ' 00:00:00';
			}
		}


		// Page
		$page = intval($this->request->param('page'));
		if(empty($page)) {
			$page = 1;
		}

		// Count all data
		$count_all = $this->video_model->count_search($date1, $date2, $search);
		$data['count_all'] = $count_all;

		// Pagination
		$pagination = 	Pagination::factory(array(
							'total_items'    		=> $count_all,
							'items_per_page'		=> 10,
							'current_page'			=> $page,
							'base_url'				=> '/userkom/search/',
							//'suffix'				=> '?date_range=' . $date_range,
							'view'					=> 'pagination/admin'
						));

		$data['list'] = $this->video_model->list_search($date1, $date2, $search, $pagination->items_per_page, $pagination->offset);
		

		$data['pagination'] = $pagination->render();

		$view = Briliant::admin_template('userkom/' . Kohana::$config->load('path.main_template') . '/list', $data);

		$this->response->body($view);

	}

	public function action_changepass() {
	

		$session = Session::instance();

		//print_r('test');die;

		if(isset($_POST['version'])) $session->set('version_userkom', $_POST['version']);
		$data['version'] = $version = $session->get("version_userkom", "");

		if(isset($_POST['category'])) $session->set('category_userkom', $_POST['category']);
		$data['category'] = $category = $session->get("category_userkom", "");

		if(isset($_POST['search'])) $session->set('search_userkom', $_POST['search']);
		$data['search'] = $search = $session->get("search_userkom", "");

	
		$data['main_title'] = __('User Community');
		$data['menu_active'] = 'komunitas';
		$data['menu_active_child'] = 'userkom';
		$data['menu_active_child_1'] = 'cp_ukom';

		// Custom header for daterangepicker
		$data['custom_header'] = '<link rel="stylesheet" href="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		// Custom footer for daterangepicker
		$data['custom_footer'] = '
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				//Date range picker
				$(function() {
					$(\'#reservation\').daterangepicker(
						{
							format: \'DD/MM/YYYY\'
						}, function (start, end) {
							window.location = "/userkom/changepass/?date_range=" + $(\'#reservation\').val();
						}
					);
				});
			</script>
		';

		// Yes No Confirm
		$data['custom_footer'] .= '
			<script type="text/javascript">
				function del_confirm(id) {
					var dialog = confirm("' . __('Are you sure for delete this data ?') . '");
					if (dialog == true) {
						window.location.href="/userkom/delete/" + id;
					}
				}
			</script>
		';

		// Date parameter
		if(empty($date_range)) {
			$date1 = "";
			$date2 = "";

		} else {
			$ex_range = explode(' - ', $date_range);
			if(!empty($ex_range)) {
				$date1 = DateTime::createFromFormat('d/m/Y', $ex_range[0]);
				$date1 = $date1->format('Y-m-d');
				$date2 = DateTime::createFromFormat('d/m/Y', $ex_range[1]);
				$date2 = $date2->format('Y-m-d');

				// Date adding time
				$date1 = $date1 . ' 00:00:00';
				$date2 = $date2 . ' 00:00:00';
			}
		}


		// Page
		$page = intval($this->request->param('page'));
		if(empty($page)) {
			$page = 1;
		}

		// Count all data
		$count_all = $this->video_model->count_search($date1, $date2, $search);
		$data['count_all'] = $count_all;

		// Pagination
		$pagination = 	Pagination::factory(array(
							'total_items'    		=> $count_all,
							'items_per_page'		=> 10,
							'current_page'			=> $page,
							'base_url'				=> '/userkom/changepass/',
							//'suffix'				=> '?date_range=' . $date_range,
							'view'					=> 'pagination/admin'
						));

		$data['list'] = $this->video_model->list_search($date1, $date2, $search, $pagination->items_per_page, $pagination->offset);
		

		$data['pagination'] = $pagination->render();

		$view = Briliant::admin_template('userkom/' . Kohana::$config->load('path.main_template') . '/changepass', $data);

		$this->response->body($view);

	}
	
	public function action_cpas() {
		
		// Id from parameter
		$id = intval($this->request->param('id'));
		$data = $this->video_model->data_by_id($id);
		
		$data['main_title'] = __('User Community');
		$data['menu_active'] = 'komunitas';
		$data['menu_active_child'] = 'userkom';
		$data['menu_active_child_1'] = 'cp_ukom';
		
		$view = Briliant::admin_template('userkom/' . Kohana::$config->load('path.main_template') . '/cpas', $data);
		$this->response->body($view);
	}
	
	public function action_cpas_submit(){
		// Id from parameter
		$id = intval($this->request->post('id'));
		
		 // Load Model Users
		$data = $this->video_model->data_by_id($id);
		
		$data['main_title'] = __('User Community');
		$data['menu_active'] = 'komunitas';
		$data['menu_active_child'] = 'userkom';
		$data['menu_active_child_1'] = 'cp_ukom';
		
        $validation = Validation::factory($this->request->post())
                        ->rule('password', 'not_empty')
                        ->rule('retype_password', 'not_empty')
                        ->rule('retype_password',  'matches', array(':validation', 'retype_password', 'password'));
        
        if($validation->check()) {
            
           
			$password = $this->request->post('password');
                
			// Change password
			$this->video_model->change_password($id, $this->request->post('password'));
			
			$data['success'] = 1;
                
        } else { // Validation Error
            
            $data['errors'] = $validation->errors('validation');
            
        }

		$view = Briliant::admin_template('userkom/' . Kohana::$config->load('path.main_template') . '/cpas', $data);
		$this->response->body($view);
	}

	public function action_new() {
		$session = Session::instance();

		$data['main_title'] = __('User Community | Add New Data');
		$data['menu_active'] = 'komunitas';
		$data['menu_active_child'] = 'userkom';
		$data['menu_active_child_1'] = 'add_ukom';

	
		$data['custom_header'] = $this->custom_header_form;
	
		$data['list_keyword'] = $this->global_model->select('master_keyword_video',['vkeyId as id', 'vkeyName as name'],['vkeyStatus >=' => 1]);
		$data['province'] = $this->video_model->read_data('provinces','name');
		$data['regency'] =  $this->video_model->ajax('regencies','province_id','name','');
		$data['district'] = $this->video_model->ajax('districts','regency_id','name','');
		$data['village'] =  $this->video_model->ajax('villages','district_id','name','');
		$data['master_gender'] = $this->video_model->read_data("master_gender","msgdName");
		$data['custom_footer'] = '
												<script>
												//Date picker
													$("#datepicker").datepicker({
														format: "dd/mm/yyyy",
														autoclose: true
													});
												
													$("#province").change(function () {

														$("#regency").empty();
														
														$("#province option:selected").each(function() {
															$.get("/userkom/ajax_regency/" + $( this ).val(), function( data ) {
																$.each(data, function (i, item) {
																	$("#regency").append($("<option>", { 
																		value: item.id,
																		text : item.name
																	}));
																});
															}, "json");
														});
													});

													$("#regency").change(function () {
													
														$("#district").empty();
														
														$("#regency option:selected").each(function() {
															$.get("/userkom/ajax_district/" + $( this ).val(), function( data ) {
																$.each(data, function (i, item) {
																	$("#district").append($("<option>", { 
																		value: item.id,
																		text : item.name
																	}));
																});
															}, "json");
														});
													});

													$("#district").change(function () {
													
														$("#village").empty();
														
														$("#district option:selected").each(function() {
															$.get("/userkom/ajax_village/" + $( this ).val(), function( data ) {
																$.each(data, function (i, item) {
																	$("#village").append($("<option>", { 
																		value: item.id,
																		text : item.name
																	}));
																});
															}, "json");
														});
													});
													
													
												

												</script>
											';
		
		$view = Briliant::admin_template('userkom/' . Kohana::$config->load('path.main_template') . '/new', $data);
		$this->response->body($view);

	}

	public function action_save() {
		$session = Session::instance();

		// Validation
			$validation = TRUE;

		if($validation !== TRUE) { // Error Validation

			$data['main_title'] = __('User Community | Add New Data');
			$data['menu_active'] = 'komunitas';
			$data['menu_active_child'] = 'userkom';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['post'] = $this->request->post();
			$view = Briliant::admin_template('userkom/' . Kohana::$config->load('path.main_template') . '/new', $data);
			$this->response->body($view);

		} else { // Validation Success

			// Get user id from session login
			$user_id = $session->get('adminId');
			$post = $this->request->post();

			// Save Data
			// print_r($this->request->post());exit;
			$save_data = $this->video_model->save_data($post, $user_id);

			// Redirect
			$this->redirect('/userkom/search');

		}

	}

	

	public function action_edit() {
		$session = Session::instance();

		$data['main_title'] = __('User Community | Edit Data');
		$data['menu_active'] = 'komunitas';
		$data['menu_active_child'] = 'userkom';

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;

		// Paramter ID
		$id = $this->request->param('id');

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('adminId');

		// Detail Data
		$video_detail = $this->video_model->detail_data($id);
		$data['detail'] = $video_detail;
		$data['province'] = $this->video_model->read_data('provinces','name');
		$data['regency'] =  $this->video_model->ajax('regencies','province_id','name',$data['detail']['province_id']);
		$data['district'] = $this->video_model->ajax('districts','regency_id','name',$data['detail']['regency_id']);
		$data['village'] =  $this->video_model->ajax('villages','district_id','name',$data['detail']['district_id']);
		$data['master_gender'] = $this->video_model->read_data("master_gender","msgdName");
		$data['custom_footer'] = '
												<script>
												//Date picker
													$("#datepicker").datepicker({
														format: "dd/mm/yyyy",
														autoclose: true
													});
												
													$("#province").change(function () {

														$("#regency").empty();
														
														$("#province option:selected").each(function() {
															$.get("/userkom/ajax_regency/" + $( this ).val(), function( data ) {
																$.each(data, function (i, item) {
																	$("#regency").append($("<option>", { 
																		value: item.id,
																		text : item.name
																	}));
																});
															}, "json");
														});
													});

													$("#regency").change(function () {
													
														$("#district").empty();
														
														$("#regency option:selected").each(function() {
															$.get("/userkom/ajax_district/" + $( this ).val(), function( data ) {
																$.each(data, function (i, item) {
																	$("#district").append($("<option>", { 
																		value: item.id,
																		text : item.name
																	}));
																});
															}, "json");
														});
													});

													$("#district").change(function () {
													
														$("#village").empty();
														
														$("#district option:selected").each(function() {
															$.get("/userkom/ajax_village/" + $( this ).val(), function( data ) {
																$.each(data, function (i, item) {
																	$("#village").append($("<option>", { 
																		value: item.id,
																		text : item.name
																	}));
																});
															}, "json");
														});
													});
													
													
												

												</script>
											';
		
		
		$view = Briliant::admin_template('userkom/' . Kohana::$config->load('path.main_template') . '/edit', $data);

		$this->response->body($view);

	}

	public function action_update() {

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('adminId');

		// Check Permission
		$id_parameter = $this->request->post('id');

		// Validation
		$validation = TRUE;
		if($validation !== TRUE) { // Error Validation

			$data['main_title'] = __('User Community | Edit Data');
			$data['menu_active'] = 'komunitas';
			$data['menu_active_child'] = 'userkom';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['detail'] = $this->request->post();
			if(!empty($data['detail']['category'])) {
				$data['detail']['category_id'] = $data['detail']['category'];
			}

			$view = Briliant::admin_template('userkom/' . Kohana::$config->load('path.main_template') . '/edit', $data);

			$this->response->body($view);

		} else { // Validation Success

			// Update Data
			$update_data = $this->video_model->update_data($this->request->post(), $user_id);


			$this->redirect('/userkom/search');

		}

	}

	public function action_delete() {

		// ID Paramter
		$id = $this->request->param('id');

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('user_id');

		// Check Permission
		// DVH::check_permission('video', 'vdeoId', $id, $user_id, 'vdeoUserIdSaved');

		// Change status to 0
		$this->video_model->delete_data($id);

		// Redirect
		$this->redirect('/userkom/search');

	}

	
	
	public function action_ajax_regency() {
		$id = $this->request->param('id');
		$res = $this->video_model->ajax('regencies','province_id','name',$id);
		print_r(json_encode($res));
	}
	public function action_ajax_district() {
		$id = $this->request->param('id');
		$res = $this->video_model->ajax('districts','regency_id','name',$id);
		print_r(json_encode($res));
	}
	public function action_ajax_village() {
		$id = $this->request->param('id');
		$res = $this->video_model->ajax('villages','district_id','name',$id);
		print_r(json_encode($res));
	}

	public function action_report() {
		$session = Session::instance();

		$data['main_title'] = __('Report | report');
		$data['menu_active'] = 'report';
		$data['menu_active_child'] = 'userkom';
		$data['menu_active_child_1'] = 'report';

	
		$data['custom_header'] = $this->custom_header_form;
	
		$data['list_keyword'] = $this->global_model->select('master_keyword_video',['vkeyId as id', 'vkeyName as name'],['vkeyStatus >=' => 1]);
		$data['province'] = $this->video_model->read_data('provinces','name');
		$data['regency'] =  $this->video_model->ajax('regencies','province_id','name','');
		$data['district'] = $this->video_model->ajax('districts','regency_id','name','');
		$data['village'] =  $this->video_model->ajax('villages','district_id','name','');
		$data['master_gender'] = $this->video_model->read_data("admin","admiRealName");
		$data['custom_footer'] = '
												<script>
												//Date picker
													$("#datepicker").datepicker({
														format: "dd/mm/yyyy",
														autoclose: true
													});
												
													$("#province").change(function () {

														$("#regency").empty();
														
														$("#province option:selected").each(function() {
															$.get("/userkom/ajax_regency/" + $( this ).val(), function( data ) {
																$.each(data, function (i, item) {
																	$("#regency").append($("<option>", { 
																		value: item.id,
																		text : item.name
																	}));
																});
															}, "json");
														});
													});

													$("#regency").change(function () {
													
														$("#district").empty();
														
														$("#regency option:selected").each(function() {
															$.get("/userkom/ajax_district/" + $( this ).val(), function( data ) {
																$.each(data, function (i, item) {
																	$("#district").append($("<option>", { 
																		value: item.id,
																		text : item.name
																	}));
																});
															}, "json");
														});
													});

													$("#district").change(function () {
													
														$("#village").empty();
														
														$("#district option:selected").each(function() {
															$.get("/userkom/ajax_village/" + $( this ).val(), function( data ) {
																$.each(data, function (i, item) {
																	$("#village").append($("<option>", { 
																		value: item.id,
																		text : item.name
																	}));
																});
															}, "json");
														});
													});
													
													
												

												</script>
											';
		
		$view = Briliant::admin_template('userkom/' . Kohana::$config->load('path.main_template') . '/report', $data);
		$this->response->body($view);
	}

	public function action_report_submit() {

		
		
		include APPPATH . 'mylib/PHPExcel/PHPExcel.php';
		
		$session = Session::instance();
			$validation = TRUE;

			

			$data['main_title'] = __('User Community | Add New Data');
			$data['menu_active'] = 'komunitas';
			$data['menu_active_child'] = 'userkom';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['post'] = $this->request->post();
			$user_id = $session->get('adminId');
			$post = $this->request->post();

			

			// if(@$post['tanggal']){
			// 	$date = explode("/", @$post['tanggal']);
			// 	$datem = $date[2] + 1 . "-" . $date[1] . "-" . $date[0];
			// 	$dateakhir = $date[2] - 1 . "-" . $date[1] . "-" . $date[0];
				// $datalist = $this->video_model->data_list_report($post['category'],$post['adminId'],$datem,$dateakhir);
			// }else{
				$datalist = $this->video_model->data_list_report($post['category'],$post['adminId']);

			// }

			

			foreach ($datalist as $key => $value) {
				$datalist[$key]['jumlah_noacc'] = $this->video_model->getJumlahAllArticle($value['id']);
				$datalist[$key]['jumlah_acc'] = $this->video_model->getJumlahArticle($value['id'],2);
			}
			$data['list'] = $datalist;

			// $testt = $this->video_model->getJumlahArticle(1,0);
			// $testt2 = $this->video_model->getJumlahArticle(1,1);
			// $testt3 = $this->video_model->getJumlahArticle(1,2);
			// print_r($testt);
			// echo "----";
			// print_r($testt2);
			// echo "----";
			// print_r($testt3);

			

			// print_r($datalist);
			// die();
			


		$view = View::factory('userkom/' . Kohana::$config->load('path.main_template') . '/reportexcel')->bind('data', $data);

		$this->response->body($view);

		// $view = Briliant::admin_template('userkom/' . Kohana::$config->load('path.main_template') . '/reportexcel', $data);
		// $this->response->body($view);

	}
	

}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Newsroom Video'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Sala de imprensa'); ?></li>
			<li class="active"><a href="/video"><?php echo __('Video'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Edit Data Video'); ?></h3>
					</div>
					<form role="form" method="post" action="<?php echo URL::Base(); ?>video/update">
						<div class="box-body">
							<?php
							if($data['detail']['status'] == 2) {
								?>
								<div class="alert alert-success alert-dismissable">
									<h4><i class="icon fa fa-check"></i> Alert!</h4>
									This video article was published
								</div>
								<?php
							}
							?>
							<?php
							if(!empty($data['errors'])) {
								?>
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<h4><i class="icon fa fa-ban"></i> <?php echo __('Alert!'); ?></h4>
									<?php
									foreach($data['errors'] as $v_errors) {
										echo ucfirst($v_errors) . '</br>';
									}
									?>
								</div>
								<?php
							}
							
							if(empty($data['detail']['is_edit'])) { // If detail from database
                                                            // Change format date for form detail
									if(Briliant::date_is_yyyy_mm_dd_hh_mm_ss($data['detail']['publish_time']) === true) {
										$data['detail']['publish_time'] = date('d/m/Y H:i:s', strtotime($data['detail']['publish_time']));
									}
							}
							
							// Style Background
							$style_bg = '';
							if($data['detail']['status'] == 2) {
								$style_bg = 'style="background-color: rgba(4, 255, 4, 0.29);" disabled';
							}
							
							// Status Publish
							$text_status_publish = '';
							$hidden_text_publish = '';
							if($data['detail']['status'] == 2) {
								$text_status_publish = '<span><i>(' . __('Was Published') . ')</i></span>';
								$hidden_text_publish = '<input type="hidden" name="publish_time" value="' . $data['detail']['publish_time'] . '" class="form-control">';
							}
							
							?>
							<!-- Hidden Text -->
							<input type="hidden" name="id" value="<?php echo !empty($data['detail']['id']) ? $data['detail']['id'] : ''; ?>" />
							<input type="hidden" name="status" value="<?php echo !empty($data['detail']['status']) ? $data['detail']['status'] : ''; ?>" />
							<input type="hidden" name="is_edit" value="1" />
							<div class="form-group">
								<label><?php echo __('Horário') ?></label>
								<div class="input-group">
									<input <?php echo $style_bg; ?> type="text" name="publish_time" value="<?php echo !empty($data['detail']['publish_time']) ? $data['detail']['publish_time'] : ''; ?>" placeholder="<?php echo __('Start'); ?>" class="form-control publish_time">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label><?php echo __('Título') ?></label>
								<input type="text" name="title" value="<?php echo !empty($data['detail']['title']) ? $data['detail']['title'] : ''; ?>" maxlength="60" placeholder="<?php echo __('Only 60 Characters'); ?>" class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Image') ?></label>
								<div id="previewImage" style="width: 100%; height: auto; margin-bottom: 10px;">
									
									<?php
									$val_hidden_image = '';
									// Image preview edit
									if(!empty($data['detail']['images'][0]['image_id'])) {
										$split_id = str_split($data['detail']['images'][0]['image_id']);
										$path_folder_image = implode('/', $split_id);

										//echo '<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreview();">X</span></center></div><img src="' . URL::Base() . 'uploads/library/' . $path_folder_image . '/' . $data['detail']['images'][0]['image_id'] . '_278x139.' . $data['detail']['images'][0]['image_type'] . '"  style="width:200px;">';
										echo '<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreview();">X</span></center></div><img src="' . URL::Base() . 'uploads/library/' . $path_folder_image . '/' . Briliant::slugify($data['detail']['images'][0]['image_title']) . '_278x139.' . $data['detail']['images'][0]['image_type'] . '"  style="width:200px;">';
										
										//$val_hidden_image = URL::Base() . 'uploads/library/' . $path_folder_image . '/' . $data['detail']['images'][0]['image_id'] . '.' . $data['detail']['images'][0]['image_type'] . '';
										$val_hidden_image = URL::Base() . 'uploads/library/' . $path_folder_image . '/' . Briliant::slugify($data['detail']['images'][0]['image_title']) . '.' . $data['detail']['images'][0]['image_type'] . '';
									
									} else if(!empty($data['detail']['image'])) {
										// Get id image from full url image
										$base_image = basename($data['detail']['image']).PHP_EOL;
										if(!empty($base_image)) {
											$ex_base = explode('.', $base_image);
											list($id_image, $file_type) = $ex_base;
										}
										$split_id = str_split($id_image);
										$path_folder_image = implode('/', $split_id);
										$data['detail']['images'][0]['image_id'] = $id_image;
										$data['detail']['images'][0]['image_type'] = $file_type;
										
										echo '<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreview();">X</span></center></div><img src="' . URL::Base() . 'uploads/library/' . $path_folder_image . '/' . $data['detail']['images'][0]['image_id'] . '_278x139.' . $data['detail']['images'][0]['image_type'] . '" style="width:200px;">';

										$val_hidden_image = URL::Base() . 'uploads/library/' . $path_folder_image . '/' . $data['detail']['images'][0]['image_id'] . '.' . $data['detail']['images'][0]['image_type'] . '';
									}
									?>
								</div>
								
								<input type="hidden" name="image" id="image_popup" value="<?php echo $val_hidden_image; ?>" class="form-control">
								<input type="hidden" name="image_old" value="<?php echo $val_hidden_image; ?>" class="form-control">
								<a href="javascript:open_popup_img()"><button type="button" class="btn btn-block btn-default" id="image_popup_btn" style="width:200px"><?php echo __('Browse'); ?></button></a>
							</div>
							<div class="form-group">
								<label><?php echo __('Descrição') ?></label>
								<textarea class="form-control" rows="3" name="description" maxlength="140" placeholder="<?php echo __('Only 140 Characters'); ?>"><?php echo !empty($data['detail']['description']) ? $data['detail']['description'] : ''; ?></textarea>
							</div>
							<?php 
								$sl_category = '';
								if(!empty($data['detail']['category_id'])) {
									$sl_category = $data['detail']['category_id'];
								}
								echo Video::select_list('category', 1, $sl_category); 
							?>
							<div class="form-group">
								<label><?php echo __('Palavra-chave') ?></label>
								<select style="width: 100%" class="form-control select2" name="keyword[]" id="" multiple>
								<?php
                                        foreach ($data['list_keyword'] as $key) {
											$selected = "";
											if(!empty($data['detail']['keyword_id'])){
												$lvl = unserialize($data['detail']['keyword_id']);
												foreach($lvl as $kk){
													if($key['id'] == $kk){
														$selected = "selected";
													}
												}
											}
                                            echo '
                                                <option '.$selected.' value="'.$key['id'].'">'.$key['name'].'</option>
                                            ';
                                        }
                                    ?>
								</select>
							</div>
							<div class="form-group">
								<label><?php echo __('Palavra-chave Alternative') ?></label>
								<input maxlength="255" type="text" name="keyword_alt" value="<?php echo !empty($data['detail']['keyword_alt']) ? $data['detail']['keyword_alt'] : ''; ?>"  placeholder="<?php echo __('Only 255 Characters'); ?>"  class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Video Embed URL') ?></label>
								<textarea class="form-control" rows="3" name="embed_url" placeholder="<?php echo __('Video Embed URL From Youtube or Other Online Media Player'); ?>"><?php echo !empty($data['detail']['embed_url']) ? $data['detail']['embed_url'] : ''; ?></textarea>
							</div>
							<div class="form-group">
								<label><?php echo __('Detalhes') ?></label>
								<textarea id="wysiwyg" class="form-control f_tinymce" rows="15" name="detail" minlength="3" required><?php echo !empty($data['detail']['detail']) ? $data['detail']['detail'] : ' '; ?></textarea>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Atualizar os dados'); ?></button>
							<a href="<?= URL::base() ?>video/search"><button type="button" class="btn btn-danger"><?php echo __('Cancelar'); ?></button></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>

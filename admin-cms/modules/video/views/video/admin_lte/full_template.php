<div class="box-body">
	<table class="table table-bordered table-striped">
		<tr>
			<th style="text-align:center;"><?php echo __('Image'); ?></th>
			<th style="text-align:center;"><?php echo __('Video Detail'); ?></th>
			<th style="text-align:center;"><?php echo __('Carregador'); ?></th>
			<th style="text-align:center;width:18%;"><?php echo __('Status'); ?></th>
			<th colspan="2" style="width:20%;text-align:center;"><?php echo __('Ações'); ?></th>
		</tr>
		<?php
		if(!empty($data['list'])) {
			foreach($data['list'] as $v_list) {
				$now = date('Y-m-d H:i:s');
				
				// Image article
				$img_video = '' . __('[ No Image Available ]') . '';
				if(!empty($v_list['images'][0]['image_id'])) {
					$split_id = str_split($v_list['images'][0]['image_id']);
					$path_folder_image = implode('/', $split_id);
					$img_title = Briliant::slugify($v_list['images'][0]['image_title']);
					//$img_video = '<img src="' . URL::base() .'uploads/library/' . $path_folder_image . '/' . $v_list['images'][0]['image_id'] . '_278x139.' . $v_list['images'][0]['image_type'] . '" />';
					$img_video = '<img src="' . URL::base() .'uploads/library/' . $path_folder_image . '/' . $img_title . '_278x139.' . $v_list['images'][0]['image_type'] . '" />';
				}

				// Status
				$video_status = '';
				if($v_list['status'] == 1) {
					$video_status = '<span class="label label-info">Elaborado</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
				} else if($v_list['status'] == 0) {
					$video_status = '<span class="label label-danger">Deleted</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
				} else if($v_list['status'] == 2) {
					//$url_live = Kohana::$config->load('path.arah')."/video/{$v_list['id']}/".URL::title($v_list['title']).'.html';
					$video_status = '<span class="label label-success">Published</span> <b>' . date('d M Y H:i', strtotime($v_list['publish'])) . '</b>';
					//$video_status .= '<br><br><b><i>Approved By :<br>' . Video::get_user_approver($v_list['id']) . '<i></b>';
				}
				
				// Publish schedule
				if($v_list['publish'] > $now) {
					$video_status .= '<br/></br><span class="label bg-maroon-active color-palette">Schedule</span> <b>' . date('d M Y H:i', strtotime($v_list['publish'])) . '</b>';
				}

				// Default Button
				$level = Controller_Backend::check_level();
				$category = Video::slugfy($v_list['category_name']);
				if($level == 3 or $level == 1){
					if($v_list['status'] == 2){
						$button = '
						<a href="'.URL::Base().'video/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
						<a href="'.URL::Base().'video/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
						<a href="'.URL::Base().'video/unpublish/' . $v_list['id'] . '/'.$category.'"><button class="btn btn-block btn-danger btn-xs">' . __('Unpublish') . '</button></a></br>
						<a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a><br><br>
					';
					}else{
						$button = '
						<a href="'.URL::Base().'video/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
						<a href="'.URL::Base().'video/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
						<a href="'.URL::Base().'video/publish/' . $v_list['id'] . '/'.$category.'"><button class="btn btn-block btn-info btn-xs">' . __('Publicar') . '</button></a></br>
						<a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a><br><br>
					';
					}
					
				}else{
					$button = '
							<a href="'.URL::Base().'video/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
							<a href="'.URL::Base().'video/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
							<a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a><br><br>
					';
				}

				
				
				// Button If Status 0
				if($v_list['status'] == 0) {
					$button = '
							<a href="'.URL::Base().'video/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
							<a href="javascript:void()"><button disabled class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
							<a href="javascript:void()"><button disabled class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a>
					';
				}
				
				$article_title = $v_list['title'];
				if(!empty($data['search'])) {
					$ex_search = explode(',',$data['search']);
					foreach($ex_search as $v_search) {
						$article_title = str_ireplace(trim($v_search), '<span style="color:red"><b><i>' . trim($v_search). '</i></b></span>', $article_title);
					}
				}
				// if(!empty($url_live)){
				// 	$article_title = "<a href='{$url_live}' target='_BLANK'>{$article_title}</a>";
				// }
				//Get keyword
				if(!empty($v_list['keyword_id'])){
					$keyword = null;
					$keyword_id = unserialize($v_list['keyword_id']);
					//print_r($keyword_id);
					// foreach($keyword_id as $k){
					// 	// echo $k.'<br>';
					// 	print_r($k);
					// }
					foreach($data['list_keyword'] as $kk){
						$idK = $kk['id'];
						$nameK = $kk['name'];
						foreach($keyword_id as $k){
							if ($k == $idK) {
								$keyword .= $nameK.', ';
							}
						}
					}
				}else{
					$keyword = "-";
				}

				//level
				if ($v_list['level'] == 1) {
					$level = "Admin";
				}elseif ($v_list['level'] == 2) {
					$level = "Redaksi";
				}else{
					$level = "Editor";
				}

				$keyword_alt = 'Keyword Alternative : -';
				if(!empty($v_list['keyword_alt'])){
					$keyword_alt = 'Keyword Alternative : '.$v_list['keyword_alt'];
				}
				
				echo '
					<tr>
						<td><center>'.$img_video.'</center></td>
						<td>
							<strong>' . $article_title . '</strong></br>
							<i>' . $v_list['description'] . '</i> </br></br>
							Categoria : <strong><i>' . $v_list['category_name'] . '</i></strong><br>
							Keyword : '.ucwords(substr($keyword,0,-2)).'
							'.$keyword_alt.'
						</td>
						<td>
							' . ucfirst($v_list['user_name']) . ' <br>- 
							' . ucfirst($level). '
						</td>
						<td>' . $video_status . '</td>
						<td>' . $button . '</td>
					</tr>
				';
			}
		}
		?>
	</table>
</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Newsroom Video'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Sala de imprensa'); ?></li>
			<li class="active"><a href="/video"><?php echo __('Video'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
						<!-- Date range -->
						<div class="form-group">
							<form method="post" id="publishForm">
							<label>Range Saved Date</label>
							</form>
							
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" name="date_range" required value="<?php echo !empty($_GET['date_range']) ? $_GET['date_range'] : date('d/m/Y') . ' - ' . date('d/m/Y') ; ?>" class="form-control pull-right" id="reservation">
							</div>
							<br/>
							
							<form method="post" id="catForm">
								<?php echo Video::select_list('category', 1, @$data['category'], "onChange=document.getElementById('catForm').submit();", 'lite'); ?>
							</form>
						</div>
					</div>
					<div class="box-body">
						<a href="/video/new"><button type="button" style="width:150px" class="btn btn-success btn-sm"><?php echo __('Add New Video'); ?></button></a>
					</div>
				</div>
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $data['count_all'] . ' ' .__('Dados encontrados'); ?></h3>
						<div class="box-tools">
							<form method="post" action="/video/search">
								<div class="input-group" style="width: 150px;">
									<input type="text" name="search" class="form-control input-sm pull-right" placeholder="<?php echo __('Search'); ?>" value="<?php echo !empty($data['search']) ? $data['search'] : ''; ?>">
									<input type="hidden" name="date_range" value="<?php echo !empty($_GET['date_range']) ? $_GET['date_range'] : ''; ?>" />
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php 
					echo !empty($data['pagination']) ? $data['pagination'] : '';
					if($data['version']=='lite'){
						include('lite_template.php');
					}else{
						include('full_template.php');
					}
					echo !empty($data['pagination']) ? $data['pagination'] : ''; 
					?>
				</div>
			</div>
		</div>
	</section>
</div>

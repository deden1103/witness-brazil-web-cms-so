<?php defined('SYSPATH') or die('No direct script access.');

class Model_Video extends Model {

	public function count_search($date1 = '', $date2 = '', $search = '') {
		$session = Session::instance();
		$publish = $session->get("publish_video", "vdeoPublishTime");
		$return = 0;
		//print_r($publish .' '. $date1 .' sampai '. $date2); exit;
		
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('video');
		
		if (!empty($date1) and !empty($date2)){
			$query = $query->where(DB::expr('DATE('.$publish.')'), '>=', $date1)
						->where(DB::expr('DATE('.$publish.')'), '<=', $date2);
		}
		
		$ex_search = explode(',', $search);
		foreach($ex_search as $v_search) {
			$query = $query->where('vdeoTitle', 'LIKE', '%' . trim($v_search) . '%');
		}
		
		$category = $session->get("category_video", "");
		if(!empty($category)){
			$query = $query->where('vdeoMscvId','=', $category);
		}
		
		// echo Debug::vars((string) $query);
		$query = $query->order_by('vdeoId', 'DESC')
					->where('vdeoStatus','!=', 0)
					->execute()
					->as_array();
					
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function list_search($date1 = '', $date2 = '', $search = '', $limit = '', $offset = '') {
		$session = Session::instance();
		$publish = $session->get("publish_video", "vdeoPublishTime");
		
		$exec = DB::select(
					array('vdeoId', 'id'),
					array('vdeoTitle', 'title'),
					array('vdeoExcerpt', 'description'),
					array('vdeoEmbedURL', 'embed_url'),
					array('vdeoPublishTime', 'publish'),
					array('vdeoSaved', 'saved'),
					array('vdeoAdmiIdSaved', 'savedId'),
					array('vdeoStatus', 'status'),
					array('vdeoMscvId', 'category_id'),
					array('mscvName', 'category_name'),
					array('admiRealName', 'user_name'),
					array('admiLvl', 'level'),
					array('vdeoKeyword','keyword_id'),
					array('vdeoKeywordAlt','keyword_alt')
				)
				->from('video')
				->join('admin', 'LEFT')
				->on('video.vdeoAdmiIdSaved', '=', 'admin.admiId')
				->join('master_category_video', 'LEFT')
				->on('video.vdeoMscvId', '=', 'master_category_video.mscvId');

		if (!empty($date1) and !empty($date2)){
			$exec = $exec->where(DB::expr('DATE('.$publish.')'), '>=', $date1)
						->where(DB::expr('DATE('.$publish.')'), '<=', $date2);
		}


		$exec = $exec->where('vdeoTitle', 'LIKE', '%' . $search . '%')
				->order_by('vdeoId', 'DESC')
				->where('vdeoStatus','!=', 0);
				
		$ex_search = explode(',', $search);
		foreach($ex_search as $v_search) {
			$exec = $exec->where('vdeoTitle', 'LIKE', '%' . trim($v_search) . '%');
		}
		
		$category = $session->get("category_video", "");
		//print_r($category); exit;
		if(!empty($category)){
			$exec = $exec->where('vdeoMscvId','=', $category);
		}
		
		$exec = $exec->limit($limit)
				->offset($offset)
				->execute()
				->as_array();
				if(!empty($exec)) {

					foreach($exec as $k_exec => $v_exec) {
						
						if(!empty($v_exec['id'])) {
		
							// Get images
							$exec[$k_exec]['images'] = 	DB::select(
															array('vidArimId', 'image_id'),
															array('arimTitle', 'image_title'),
															array('arimFileType', 'image_type')
														)
														->from('video_image')
														->join('arsip_image', 'LEFT')
														->on('arsip_image.arimId', '=', 'video_image.vidArimId')
														->where('vidVdeoId', '=', $v_exec['id'])
														->execute()
														->as_array();
		
						}
		
					}
		
				}		
		
		return $exec;
		
	}

	public function save_data($data = '', $user_id = '') {
		// Start Transaction
		Database::instance()->begin();
		
		$publish_time = date('Y-m-d H:i:s'); // Default data now
		if(!empty($data['publish_time'])) {
			$publish_time = DateTime::createFromFormat('d/m/Y H:i:s', $data['publish_time']);
			$publish_time = $publish_time->format('Y-m-d H:i:s');
			
			$saved_time = date('Y-m-d H:i:s');
		} else {
			$saved_time = $publish_time;
		}

		$keyword = serialize($data['keyword']);
		$video 		=	DB::insert('video', array(
						'vdeoTitle',
						'vdeoExcerpt',
						'vdeoDetail',
						'vdeoKeyword',
						'vdeoKeywordAlt',
						'vdeoMscvId', // Categpry or Kanal
						'vdeoEmbedURL',
						'vdeoPublishTime',
						'vdeoSaved',
						'vdeoAdmiIdSaved'
					))
					->values(array(
						$data['title'],
						$data['description'],
						$data['detail'],
						$keyword,
						$data['keyword_alt'],
						$data['category'],
						$data['embed_url'],
						$publish_time,
						$saved_time,
						$user_id
					));
		
		list($lastid_video_1, $rows_inserted_1) = $video->execute();

		if(!empty($lastid_video_1)) {
			// Save article images
			if(!empty($data['image'])) {
				$explode_id_image = explode("/", $data['image']);
				
				$id_image_2 = array();
				foreach($explode_id_image as $k_id_img=>$v_id_img){
					if(is_numeric(($v_id_img))){
						$id_image_2_arr = $v_id_img;
					} else {
						$id_image_2_arr = '';
					}

					array_push($id_image_2, $id_image_2_arr);
				} 
				$id_image_2_implode = implode("", $id_image_2);
					
				// Get id image from full url image
				$base_image = basename($data['image']).PHP_EOL;
				
				if(!empty($base_image)) {
					$ex_base = explode('_', $base_image);
					list($id_image, $file_type) = $ex_base;
				}

				if(!empty($id_image)) { 
					$image = 	DB::insert('video_image', array(
									'vidVdeoId',
									'vidArimId',
									'vidAdmiIdSaved'
								))
								->values(array(
									$lastid_video_1,
									$id_image_2_implode,
									$user_id
								));
					$add_image_1 = $image->execute();
				}
			}
		}
			

			// Start Transaction for portal database
			Database::instance('portal')->begin();
			
			list($lastid_video_2, $rows_inserted_2) = $video->execute('portal');
			if(!empty($lastid_video_2)) {
				// Save article images
				if(!empty($data['image'])) {
					$explode_id_image = explode("/", $data['image']);
				
					$id_image_2 = array();
					foreach($explode_id_image as $k_id_img=>$v_id_img){
						if(is_numeric(($v_id_img))){
							$id_image_2_arr = $v_id_img;
						} else {
							$id_image_2_arr = '';
						}

						array_push($id_image_2, $id_image_2_arr);
					} 
					$id_image_2_implode = implode("", $id_image_2);

					// Get id image from full url image
					$base_image = basename($data['image']).PHP_EOL;
					if(!empty($base_image)) {
						$ex_base = explode('_', $base_image);
						list($id_image, $file_type) = $ex_base;
					}

					if(!empty($id_image)) {
						$image = 	DB::insert('video_image', array(
										'vidVdeoId',
										'vidArimId',
										'vidAdmiIdSaved'
									))
									->values(array(
										$lastid_video_2,
										//$id_image_2,
										$id_image_2_implode,
										$user_id
									));
						$add_image_2 = $image->execute('portal');
					}
				}
			}

			if($add_image_1 and $add_image_2){
				// Commit transaction
				Database::instance()->commit();
				Database::instance('portal')->commit();
			}
	}
	
	public function detail_data($id = '') {
		
		$return = '';
		
		$exec = DB::select(
					array('vdeoId', 'id'),
					array('vdeoTitle', 'title'),
					array('vdeoExcerpt', 'description'),
					array('vdeoEmbedURL', 'embed_url'),
					array('vdeoPublishTime', 'publish_time'),
					array('vdeoSaved', 'saved'),
					array('vdeoAdmiIdSaved', 'savedId'),
					array('vdeoStatus', 'status'),
					array('vdeoMscvId', 'category_id'),
					array('mscvName', 'category_name'),
					array('admiRealName', 'user_name'),
					array('vdeoDetail', 'detail'),
					array('vdeoKeyword', 'keyword_id'),
					array('vdeoKeywordAlt', 'keyword_alt')
				)
				->from('video')
				->join('admin', 'LEFT')
				->on('video.vdeoAdmiIdSaved', '=', 'admin.admiId')
				->join('master_category_video', 'LEFT')
				->on('video.vdeoMscvId', '=', 'master_category_video.mscvId')
				->where('vdeoId', '=', $id)
				->execute()
				->as_array();

			if(!empty($exec)) {

					foreach($exec as $k_exec => $v_exec) {
		
						if(!empty($v_exec['id'])) {
		
							// Get images
							$exec[$k_exec]['images'] = 	DB::select(
															array('vidArimId', 'image_id'),
															array('arimTitle', 'image_title'),
															array('arimFileType', 'image_type')
														)
														->from('video_image')
														->join('arsip_image', 'LEFT')
														->on('arsip_image.arimId', '=', 'video_image.vidArimId')
														->where('vidVdeoId', '=', $v_exec['id'])
														->execute()
														->as_array();
														
						}
		
					}
		
				}
		
		if(!empty($exec[0])) {
			
			$return = $exec[0];
			
		}
		
		return $return;
		
	}
	
	public function update_data($data = '', $user_id = '', $action='') {
		// Id video
		$id_video = $data['id'];
		
		// Start Transaction
		Database::instance()->begin();

		// Check old image
		if($data['image'] != @$data['image_old']) {

			// Delete Data Old
			$del1 = DB::delete('video_image')
				->where('vidVdeoId', '=', $id_video)
				->execute();
			$del2 = DB::delete('video_image')
				->where('vidVdeoId', '=', $id_video)
				->execute('portal');

			// Check new image
			if(!empty($data['image'])) {
				//get image id
				$explode_id_image = explode("/", $data['image']);
				$id_image_2 = array();
				foreach($explode_id_image as $k_id_img=>$v_id_img){
					if(is_numeric(($v_id_img))){
						$id_image_2_arr = $v_id_img;
					} else {
						$id_image_2_arr = '';
					}

					array_push($id_image_2, $id_image_2_arr);
				} 
				$id_image_2_implode = implode("", $id_image_2);

				// Get id image from full url image
				$base_image = basename($data['image']).PHP_EOL;
				if(!empty($base_image)) {
					$ex_base = explode('_', $base_image);
					list($id_image, $file_type) = $ex_base;
				}

					if(!empty($id_image)) {
						$image = 	DB::insert('video_image', array(
										'vidVdeoId',
										'vidArimId',
										'vidAdmiIdSaved'
									))
									->values(array(
										$id_video,
										//$id_image,
										$id_image_2_implode,
										$user_id
									));
						$up_image_1 = $image->execute();			
						$up_image_2 = $image->execute('portal');	
					}

			}

		}

		// Update date video
		$publish_time = date('Y-m-d H:i:s'); // Default data now
		if(!empty($data['publish_time'])) {
			$publish_time = DateTime::createFromFormat('d/m/Y H:i:s', $data['publish_time']);
			$publish_time = $publish_time->format('Y-m-d H:i:s');
			
			$saved_time = date('Y-m-d H:i:s');
		} else {
			$saved_time = $publish_time;
		}
		
		if(!empty($data['finish_time'])) {
			$finish_time = DateTime::createFromFormat('d/m/Y H:i:s', $data['finish_time']);
			$finish_time = $finish_time->format('Y-m-d H:i:s');
		}

		if(!empty($data['keyword'])){
			$keyword = serialize($data['keyword']);
		}
		
		$update = DB::update('video')
			->set(array('vdeoTitle' => $data['title']))
			->set(array('vdeoExcerpt' => $data['description']))
			->set(array('vdeoDetail' => $data['detail']))
			->set(array('vdeoKeyword' => $keyword))
			->set(array('vdeoKeywordAlt' => $data['keyword_alt']))
			->set(array('vdeoEmbedURL' => $data['embed_url']))
			->set(array('vdeoMscvId' => $data['category']))
			->set(array('vdeoPublishTime' => $publish_time))
			->set(array('vdeoSaved' => $saved_time))
			->set(array('vdeoAdmiIdSaved' => $user_id));
			
		$update_1 = $update->where('vdeoId', '=', $id_video)->execute();
		$update_2 = $update->where('vdeoId', '=', $id_video)->execute('portal');
		
		// Commit transaction
		Database::instance()->commit();
		
	}
	
	public function delete_data($id = '') {
		
		DB::update('video')
			->set(array('vdeoStatus' => 0))
			->where('vdeoId', '=', $id)
			->execute();

		DB::update('video')
			->set(array('vdeoStatus' => 0))
			->where('vdeoId', '=', $id)
			->execute('portal');
		
	}

	public function add_category($data){
		Database::instance()->begin();
		$session = Session::instance();
		$save_category = DB::insert('master_category_video', array(
							'mscvName',
							'mscvAdmiIdSaved'
						))
						->values(array(
							$data['name'],
							$session->get('adminId')
						));
		$exec_cms = $save_category->execute();
		$exec_portal = $save_category->execute('portal');

		if ($exec_cms and $exec_portal) {
			$table = Video::slugfy($data['name']);
			$query = '
				CREATE TABLE video_cat_'.$table.'  (
					vdeoId int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
					vdeoTitle varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
					vdeoExcerpt varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
					vdeoDetail text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
					vdeoKeyword varchar(165) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
					vdeoMscvId int(10) UNSIGNED NOT NULL,
					vdeoEmbedURL text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
					vdeoCount int(10) NOT NULL DEFAULT 0,
					vdeoAdmiIdSaved int(10) UNSIGNED NOT NULL,
					vdeoSaved timestamp(0) NOT NULL DEFAULT current_timestamp(),
					vdeoPublishTime datetime(0) NOT NULL,
					PRIMARY KEY (vdeoId) USING BTREE,
					INDEX fk_video_master_category_video_idx(vdeoMscvId) USING BTREE,
					INDEX fk_video_admin_saved_idx(vdeoAdmiIdSaved) USING BTREE
				) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
			';
			$create_table_cms = DB::query(NULL,$query)->execute();
			$create_table_portal = DB::query(NULL,$query)->execute('portal');
			return $create_table_portal;
		}
		
		if($create_table_portal){
			Database::instance()->commit();	
			return "Success";
		}
	}

	public function update_category($data){
		Database::instance()->begin();
		$session = Session::instance();
			$update = DB::update('master_category_video')
					->set(array('mscvName' => $data['name']))
					->set(array('mscvAdmiIdSaved' => $session->get('adminId')))
					->where('mscvId', '=', $data['id'])->execute();

			if ($update) {
				$table_name = Video::slugfy($data['name']);
				$rename_table = Database::instance()->query(NULL, '
					RENAME TABLE video_cat_'.$data['old_cat'].' TO video_cat_'.$table_name.';
				');
			}

			if($rename_table){
				Database::instance()->commit();	
				return "Success";
			}
	}

	public function delete_category($id){
		//Database::instance()->begin();	
		//Database::instance()->commit();
		Database::instance()->begin();
		$session = Session::instance();
			$query = DB::update('master_category_video')
					->set(array('mscvStatus' => 0))
					->where('mscvId', '=', $id);
			$del_1 = $query->execute();		
			$del_2 = $query->execute('portal');		

			if($del_1 and $del_2){
				Database::instance()->commit();	
				return "Success";
			}
		

	}

	public function add_keyword($data){
		$session = Session::instance();
		$save_keyword = DB::insert('master_keyword_video', array(
			'vkeyName',
			'vkeyAdmiId'
		))
		->values(array(
			$data['name'],
			$session->get('adminId')
		));
		$exec_cms = $save_keyword->execute();
		$exec_portal = $save_keyword->execute('portal');
	}

	public function update_keyword($data){
		Database::instance()->begin();
		$session = Session::instance();
			$query = DB::update('master_keyword_video')
					->set(array('vkeyName' => $data['name']))
					->set(array('vkeyAdmiId' => $session->get('adminId')))
					->where('vkeyId', '=', $data['id']);
			$update_1 = $query->execute();		
			$update_2 = $query->execute('portal');		

			if($update_1 and $update_2){
				Database::instance()->commit();	
				return "Success";
			}
	}

	public function delete_keyword($id){
		Database::instance()->begin();
		$session = Session::instance();
			$query = DB::update('master_keyword_video')
					->set(array('vkeyStatus' => 0))
					->where('vkeyId', '=', $id);
			$del_1 = $query->execute();		
			$del_2 = $query->execute('portal');		

			if($del_1 and $del_2){
				Database::instance()->commit();	
				return "Success";
			}
	}

	public function publish_video($id_video, $cat_table, $data, $adminId){
		Database::instance()->begin();

		$now = date('Y-m-d H:i:s');
		$key = array_keys($data);
		$val = array_values($data);

		/* //save video category
		$save_video_cat = DB::insert('video_cat_'.$cat_table, $key)
							->values($val);
		list($last_save_video_cat, $rows_inserted_1) = $save_video_cat->execute();
		$status = 'video cat = '.$last_save_video_cat;

		if(!empty($last_save_video_cat)){
			//save video front page
			$save_video_front = DB::insert('video_front_page', $key)
			->values($val);
			list($last_save_video_front, $rows_inserted_2) = $save_video_front->execute();
			$status .= ' ,video front = '.$last_save_video_front;
		}

		if(!empty($last_save_video_front)){ */
			//update status video
			$update = DB::update('video')
						->set(array('vdeoStatus' => '2'))
						->set(array('vdeoUserIdApproved' => $adminId))
						->where('vdeoId', '=', $id_video);
			
			$update1 = $update->execute();
			$update2 = $update->execute('portal');
			$status = ' ,update video = '.$update1;

		// }
		// Commit transaction		
		if($update1 and $update2){
			Database::instance()->commit();	
			return $status;
		}
	
	}

	public function unpublish_video($id_video, $cat_table){
		Database::instance()->begin();
			// //delete video category
			// $delete_cat_video =  DB::delete('video_cat_'.$cat_table)
			// 						->where('vdeoId', '=', $id_video)
			// 						->execute();
			// if($delete_cat_video){
			// 	//delete video front
			// 	$delete_front_video =  DB::delete('video_front_page')
			// 						->where('vdeoId', '=', $id_video)
			// 						->execute();
			// }		
			
			// if($delete_front_video){
				//update video
				$update = DB::update('video')
				->set(array('vdeoStatus' => '1'))
				->set(array('vdeoUserIdApproved' => null));
				
				$update_1 = $update->where('vdeoId', '=', $id_video)->execute();
				$update_2 = $update->where('vdeoId', '=', $id_video)->execute('portal');
				
			// }

			if($update){
				Database::instance()->commit();	
				return $update;
			}
		
	}
}
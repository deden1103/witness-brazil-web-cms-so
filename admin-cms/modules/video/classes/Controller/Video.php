<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Video extends Controller_Backend {

	private $custom_header_form;
	private $custom_footer_form;
	private $video_model;
	private $global_model;

	public function before() {

		parent::before();
		$level = Controller_Backend::check_level();

		if($level == 2){
			$this->redirect(URL::Base().'/keranjang');
		}

		//set global model
		$this->global_model = new Model_Globalmodel();
		$this->video_model = new Model_Video();

		// Header Multiple SElect
		$this->custom_header_form = '
			<link rel="stylesheet" href="'.URL::Base().'assets/plugins/select2/select2.min.css">
			<link rel="stylesheet" href="'.URL::Base().'assets/dist/css/AdminLTE.min.css">
		';

		// Custom header for daterangepicker
		$this->custom_header_form .= '<link rel="stylesheet" href="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		// Tinymce
		$this->custom_footer_form = '
			<script src="'.URL::Base().'assets/tinymce/tinymce.min.js"></script>
			<script>
				tinymce.init({
					paste_data_images: true,
					valid_elements : \'*[*]\',
					selector: \'.f_tinymce\',
					theme: \'modern\',
                    templates: [{title: \'Berita Lainnya\', description: \'Berita Lainnya\', url: \'/template/terkait.html\'},{title: \'Button Selengkapnya\', description: \'Button Selengkapnya\', url: \'/template/selengkapnya.html\'},{title: \'Sharing\', description: \'Sharing Information\', url: \'/template/sharing.html\'},{title: \'Mutiara\', description: \'Informasi Mutiara\', url: \'/template/komunitas.html\'},{title: \'Download Arena\', description: \'Link Download Aplikasi Arena\', url: \'/template/arena.html\'}],
					plugins: [
						\'example advlist autolink lists link image charmap print preview hr anchor pagebreak\',
						\'searchreplace wordcount visualblocks visualchars code fullscreen\',
						\'insertdatetime media nonbreaking save table contextmenu directionality\',
						\'emoticons template paste textcolor colorpicker textpattern imagetools\'
					],
					toolbar1: \'insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image\',
					toolbar2: \'fullscreen preview media | forecolor backcolor emoticons | pagebreak | my_image\',
					image_advtab: true,
                                        pagebreak_separator: "<!--PAGE_SEPARATOR-->",
                                        pagebreak_split_block: true,
                                        setup: function (editor) {
                                        editor.addButton(\'my_image\', {
                                            text: \'Add Image From Gallery\',
                                            icon: false,
                                            onclick: function () {
                                                open_popup_img_tmce();
                                            }
                                        });
                                      },
				});
				function my_voice(texthtml) {
					tinyMCE.activeEditor.insertContent(texthtml);
				}
			</script>
		';

		// Multiple SElect
		$this->custom_footer_form .= '
			<script src="'.URL::Base().'assets/plugins/select2/select2.full.min.js"></script>
			<script>
				$(".select2").select2({allowClear: true, placeholder : "-- Choose Keyword --"});
			</script>
		';

		// Custom footer for daterangepicker
		$this->custom_footer_form .= '
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				//Date range picker
				$(function() {
					$(\'.publish_time\').daterangepicker({
						format: \'DD/MM/YYYY HH:mm:ss\',
						timePicker: true,
						singleDatePicker: true,
						timePickerIncrement: 1,
						timePicker12Hour: false,
						//minDate: \'' . date('d-m-Y H:i:s') . '\'
					});
				});
				$("#myButton").click(function() {
					$("#kotak1").data(\'daterangepicker\').toggle();
				});
			</script>
			<!--Tinymce hidden for append image textarea-->
			<input type="hidden" id="image_popup_tmce" />
			<script>
				function open_popup_img() {
					PopupCenter("/library/index/1/0/image_popup", "google", "640", "480");
				}

				function open_popup_img_tmce() {
					PopupCenter("/library/index/1/0/image_popup_tmce", "google", "640", "480");
				}

				function image_popup_tmce() {
						var str = $(\'#image_popup_tmce\').val();
						var n = str.lastIndexOf(".");
						var res = str.substring(0, n)+str.substring(n);
						tinyMCE.activeEditor.insertContent(\'<img src="\' + res + \'" width="100%" />\');
				}

				function image_popup() {
					$(\'#previewImage\').html(\'\');
					$(\'#previewImage\').append(\'<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreview();">X</span></center></div>\');
					$(\'#previewImage\').append(\'<img src="\' + $(\'#image_popup\').val() + \'" style="width:200px;"/>\');
				}

				function delPreview() {
					$(\'#previewImage\').html(\'\');
					$(\'#image_popup\').val(\'\');
				}

				// GIF IMAGE JS FUNCTION
				function open_popup_img_gif() {
					PopupCenter("/library/index/1/0/image_popup_gif", "google", "640", "480");
				}

				function image_popup_gif() {
					$(\'#previewImageGif\').html(\'\');
					$(\'#previewImageGif\').append(\'<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreviewGif();">X</span></center></div>\');
					$(\'#previewImageGif\').append(\'<img src="\' + $(\'#image_popup_gif\').val() + \'" style="width:200px;"/>\');
				}

				function delPreviewGif() {
					$(\'#previewImageGif\').html(\'\');
					$(\'#image_popup_gif\').val(\'\');
				}

			</script>
		';

	}

	public function action_index() {

		$this->redirect('/video/search');

	}

	public function action_search() {

		$session = Session::instance();

		if(isset($_POST['version'])) $session->set('version_video', $_POST['version']);
		$data['version'] = $version = $session->get("version_video", "");

		if(isset($_POST['publisher'])) $session->set('publish_video', $_POST['publisher']);
		$data['publish'] = $publish = $session->get("publish_video", "artcPublishTime");

		if(isset($_POST['uploader'])) $session->set('uploader_video', $_POST['uploader']);
		$data['uploader'] = $uploader = $session->get("uploader_video", "");

		if(isset($_POST['category'])) $session->set('category_video', $_POST['category']);
		$data['category'] = $category = $session->get("category_video", "");

		if(isset($_POST['search'])) $session->set('search_video', $_POST['search']);
		$data['search'] = $search = $session->get("search_video", "");

		if(isset($_GET['date_range'])) $session->set('date_range_video', $_GET['date_range']);
		$data['date_range'] = $date_range = $session->get("date_range_video", "");

		$data['main_title'] = __('Video');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'video';
		$data['menu_active_child_1'] = 'list';

		// Custom header for daterangepicker
		$data['custom_header'] = '<link rel="stylesheet" href="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		// Custom footer for daterangepicker
		$data['custom_footer'] = '
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				//Date range picker
				$(function() {
					$(\'#reservation\').daterangepicker(
						{
							format: \'DD/MM/YYYY\'
						}, function (start, end) {
							window.location = "/video/search/?date_range=" + $(\'#reservation\').val();
						}
					);
				});
			</script>
		';

		// Yes No Confirm
		$data['custom_footer'] .= '
			<script type="text/javascript">
				function del_confirm(id) {
					var dialog = confirm("' . __('Are you sure for delete this data ?') . '");
					if (dialog == true) {
						window.location.href="/video/delete/" + id;
					}
				}
			</script>
		';

		// Date parameter
		if(empty($date_range)) {
			$date1 = "";
			$date2 = "";

		} else {
			$ex_range = explode(' - ', $date_range);
			if(!empty($ex_range)) {
				$date1 = DateTime::createFromFormat('d/m/Y', $ex_range[0]);
				$date1 = $date1->format('Y-m-d');
				$date2 = DateTime::createFromFormat('d/m/Y', $ex_range[1]);
				$date2 = $date2->format('Y-m-d');

				// Date adding time
				$date1 = $date1 . ' 00:00:00';
				$date2 = $date2 . ' 00:00:00';
			}
		}


		// Page
		$page = intval($this->request->param('page'));
		if(empty($page)) {
			$page = 1;
		}

		// Count all data
		$count_all = $this->video_model->count_search($date1, $date2, $search);
		$data['count_all'] = $count_all;

		// Pagination
		$pagination = 	Pagination::factory(array(
							'total_items'    		=> $count_all,
							'items_per_page'		=> 10,
							'current_page'			=> $page,
							'base_url'				=> '/video/search/',
							//'suffix'				=> '?date_range=' . $date_range,
							'view'					=> 'pagination/admin'
						));

		$data['list'] = $this->video_model->list_search($date1, $date2, $search, $pagination->items_per_page, $pagination->offset);
		$data['list_keyword'] = $this->global_model->select(
			'master_keyword_video',['vkeyId as id', 'vkeyName as name'], //select
			['vkeyStatus >=' => 1] //table
		);


		$data['pagination'] = $pagination->render();

		$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/list', $data);

		$this->response->body($view);

	}

	public function action_new() {
		$session = Session::instance();

		$data['main_title'] = __('Video | Add New Data');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'video';
		$data['menu_active_child_1'] = 'add';

		$keranjang_mentah = $this->request->post();
		if(isset($keranjang_mentah)){
			$data['post']['title'] = $this->request->post('title');
			$data['post']['detail'] =  $this->request->post('detail');
		}

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;
		$data['list_keyword'] = $this->global_model->select('master_keyword_video',['vkeyId as id', 'vkeyName as name'],['vkeyStatus >=' => 1]);

		$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/new', $data);
		$this->response->body($view);

	}

	public function action_save() {
		$session = Session::instance();

		// Validation
		$validation = Video::validation($this->request->post());

		if($validation !== TRUE) { // Error Validation

			$data['main_title'] = __('Video | Add New Data');
			$data['menu_active'] = 'video';
			$data['menu_active_child'] = 'video';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['post'] = $this->request->post();
			$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/new', $data);
			$this->response->body($view);

		} else { // Validation Success

			// Get user id from session login
			$user_id = $session->get('adminId');
			$post = $this->request->post();

			// Save Data
			// print_r($this->request->post());exit;
			$save_data = $this->video_model->save_data($post, $user_id);

			// Redirect
			$this->redirect('/video/search');

		}

	}

	public function action_detail() {
		$session = Session::instance();

            $data['main_title'] = __('Video | Detail Data');
            $data['menu_active'] = 'video';
            $data['menu_active_child'] = 'video';

            $id = $this->request->param('id');

            // Detail data
			$data['detail'] = $this->video_model->detail_data($id);
			$data['list_keyword'] = $this->global_model->select('master_keyword_video',['vkeyId as id', 'vkeyName as name'],['vkeyStatus >=' => 1]);

			$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/detail', $data);
            $this->response->body($view);

	}

	public function action_edit() {
		$session = Session::instance();

		$data['main_title'] = __('Video | Edit Data');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'video';

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;

		// Paramter ID
		$id = $this->request->param('id');

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('adminId');

		// Detail Data
		$video_detail = $this->video_model->detail_data($id);
		$data['detail'] = $video_detail;
		$data['list_keyword'] = $this->global_model->select('master_keyword_video',['vkeyId as id', 'vkeyName as name'],['vkeyStatus >=' => 1]);
		$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/edit', $data);

		$this->response->body($view);

	}

	public function action_update() {

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('adminId');

		// Check Permission
		$id_parameter = $this->request->post('id');

		// Validation
		$validation = Video::validation($this->request->post());

		if($validation !== TRUE) { // Error Validation

			$data['main_title'] = __('Newsroom | Newsroom | News | Edit Data');
			$data['menu_active'] = 'newsroom';
			$data['menu_active_child'] = 'video';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['detail'] = $this->request->post();
			if(!empty($data['detail']['category'])) {
				$data['detail']['category_id'] = $data['detail']['category'];
			}

			$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/edit', $data);

			$this->response->body($view);

		} else { // Validation Success

			// Update Data
			$update_data = $this->video_model->update_data($this->request->post(), $user_id);


			$this->redirect('/video/search');

		}

	}

	public function action_delete() {

		// ID Paramter
		$id = $this->request->param('id');

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('user_id');

		// Check Permission
		// DVH::check_permission('video', 'vdeoId', $id, $user_id, 'vdeoUserIdSaved');

		// Change status to 0
		$this->video_model->delete_data($id);

		// Redirect
		$this->redirect('/video/search');

	}

	public function action_publish(){
		$session = Session::instance();
		$level = Controller_Backend::check_level();
		if($level == 3 or $level == 1){
			$id_video = $this->request->param('id');
			$adminId = $session->get('adminId');
			$table_name = $this->request->param('cat');

			$get_data_video = $this->global_model->select(
				'video', //table
				['vdeoId','vdeoTitle','vdeoExcerpt','vdeoDetail','vdeoKeyword','vdeoMscvId','vdeoEmbedURL','vdeoCount','vdeoAdmiIdSaved','vdeoSaved','vdeoPublishTime'], //select
				['vdeoId' => $id_video] //where
			);
			$publish =  $this->video_model->publish_video($id_video, $table_name, $get_data_video[0], $adminId);
			
			if($publish){
				// Redirect
				$this->redirect('/video/search');
			}
		}
	}

	public function action_unpublish(){
		$level = Controller_Backend::check_level();
		if($level == 3 or $level == 1){
			$id_video = $this->request->param('id');
			$table_name = $this->request->param('cat');
			$unpublish = $this->video_model->unpublish_video($id_video, $table_name);
			if($unpublish){
				// Redirect
				$this->redirect('/video/search');
			}
		}
	}

	public function action_category(){
		$data['main_title'] = __('Video | Config | Configurar a categoria');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'config';
		$data['menu_active_child_1'] = 'category';

		$data['ctg_category'] = $this->global_model->select(
			'master_category_video', //table
			['mscvId as id', 'mscvName as name'], //select
			['mscvStatus >=' => 1],
			'', //limit
			'', //offset
			['mscvId' => 'DESC'] //order
		); //where

		$data['custom_header'] = '
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.min.css">
		';
		$data['custom_footer'] = '
			<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.all.min.js"></script>
		';

		//alert
		$data['custom_footer'] .= '
		<script>
			function del(id){
				var swal_id = id
				swal({
					title: "Are you sure?",
					text: "You won\'t be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3085d6",
					cancelButtonColor: "#d33",
					confirmButtonText: "Yes, delete it!"
				  }).then((result) => {
					if (result.value) {
						$.ajax({
							url: "/video/delete_category/",
							dataType: "JSON",
							type: "POST",
							data: {swal_id : swal_id},
							success: function(res){
								$("#row"+swal_id).remove()
								swal(
									"Deleted!",
									"Your file has been deleted.",
									"success"
								  )
							},error: function(){
								swal(
									"Something Wrong Happen !!",
									"",
									"error"
								  )
							}
						})

					}
				  })
			}
		</script>
		';

		$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/config_category', $data);
		//print_r($data); exit;

		$this->response->body($view);
	}

	public function action_editcategory(){
		$data['main_title'] = __('Video | Config | Configurar a categoria');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'config';
		$data['menu_active_child_1'] = 'category';

		$id = $this->request->param('id');
		$data['ctg_category'] = $this->global_model->select(
				'master_category_video',
				['mscvId as id', 'mscvName as name'],
				['mscvId' => $id,'mscvStatus >=' => 1]
			);

		$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/edit_category', $data);
		//print_r($data); exit;

		$this->response->body($view);
	}

	function action_add_category(){
		$session = Session::instance();
		$save = $this->request->post('save');
		if (isset($save)) {
			$data = $this->request->post();
			$save_data = $this->video_model->add_category($data);

			$this->redirect('/video/category');
		}
	}

	function action_update_category(){
		
		$session = Session::instance();
		$save = $this->request->post('save');
		if (isset($save)) {
			$data = $this->request->post();
			$update = $this->video_model->update_category($data);

			
			$this->redirect('/video/category');
		}
	}

	function action_delete_category() {
		if(isset($_POST["swal_id"])){
			$id = $_POST['swal_id'];
			$update = $this->video_model->delete_category($id);
			if ($update == "Success") {
				echo json_encode($update);
			}else{
				http_response_code(500);
			}
		}
	}

	public function action_keyword(){
		$data['main_title'] = __('Video | Config | Config Keyword');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'config';
		$data['menu_active_child_1'] = 'keyword';

		$data['ctg_keyword'] = $this->global_model->select(
					'master_keyword_video', //from
					['vkeyId as id', 'vkeyName as name'], //select
					['vkeyStatus >=' => 1], //where
					'', //limit
					'', //offset
					['vkeyId' => 'DESC'] //order
				);

		$data['custom_header'] = '
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.min.css">
		';
		$data['custom_footer'] = '
			<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.all.min.js"></script>
		';

		//alert
		$data['custom_footer'] .= '
		<script>
			function del(id){
				var swal_id = id
				swal({
					title: "Are you sure?",
					text: "You won\'t be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3085d6",
					cancelButtonColor: "#d33",
					confirmButtonText: "Yes, delete it!"
				  }).then((result) => {
					if (result.value) {
						$.ajax({
							url: "/video/delete_keyword/",
							dataType: "JSON",
							type: "POST",
							data: {swal_id : swal_id},
							success: function(res){
								$("#row"+swal_id).remove()
								swal(
									"Deleted!",
									"Your file has been deleted.",
									"success"
								  )
							},error: function(){
								swal(
									"Something Wrong Happen !!",
									"",
									"error"
								  )
							}
						})

					}
				  })
			}
		</script>
		';

		$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/config_keyword', $data);
		//print_r($data); exit;

		$this->response->body($view);
	}

	public function action_editkeyword(){
		$data['main_title'] = __('Video | Config | Config Keyword');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'config';
		$data['menu_active_child_1'] = 'keyword';

		$id = $this->request->param('id');
		$data['ctg_keyword'] = $this->global_model->select(
				'master_keyword_video',
				['vkeyId as id', 'vkeyName as name'],
				['vkeyId' => $id,'vkeyStatus >=' => 1]
			);

		$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/edit_keyword', $data);
		//print_r($data); exit;

		$this->response->body($view);
	}

	function action_add_keyword(){
		$session = Session::instance();
		$save = $this->request->post('save');
		if (isset($save)) {
			$data = $this->request->post();
			$save_data = $this->video_model->add_keyword($data);
			$this->redirect('/video/keyword');
		}
	}

	function action_update_keyword(){
		$save = $this->request->post('save');
		if (isset($save)) {
			/* $id = $_POST['id'];
			$data = array('vkeyName' => $this->request->post('name'),'vkeyAdmiId' => $session->get('adminId'));
			$update = $this->global_model->update('master_keyword_video',$data,['vkeyId' => $id]); */
			$data = $this->request->post();
			$update_data = $this->video_model->update_keyword($data);
			$this->redirect('/video/keyword');
		}
	}

	function action_delete_keyword() {
		if(isset($_POST["swal_id"])){
			$id = $_POST['swal_id'];
			$update = $this->video_model->delete_keyword($id);
			if ($update == "Success") {
				echo json_encode($update);
			}else{
				http_response_code(500);
			}
		}
	}

}

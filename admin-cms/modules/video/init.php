<?php

// Route publish
Route::set('video_publish', 'video/publish(/<id>(/<cat>))')
	->defaults(array(
		'controller' => 'Video',
		'action'     => 'publish',
	));

// Route unpublish
Route::set('video_unpublish', 'video/unpublish(/<id>(/<cat>))')
	->defaults(array(
		'controller' => 'Video',
		'action'     => 'unpublish',
	));


// Route delete
Route::set('video_delete', 'video/delete(/<id>)')
	->defaults(array(
		'controller' => 'Video',
		'action'     => 'delete',
	));

// Route detail
Route::set('video_detail', 'video/detail(/<id>)')
	->defaults(array(
		'controller' => 'Video',
		'action'     => 'detail',
	));

// Route edit
Route::set('video_edit', 'video/edit(/<id>)')
	->defaults(array(
		'controller' => 'Video',
		'action'     => 'edit',
	));
	
// Route index
Route::set('video_index', 'video/index(/<page>)')
	->defaults(array(
		'controller' => 'Video',
		'action'     => 'index',
	));
	
// Route new
Route::set('video_new', 'video/new')
	->defaults(array(
		'controller' => 'Video',
		'action'     => 'new',
	));
	
// Route save
Route::set('video_save', 'video/save')
	->defaults(array(
		'controller' => 'Video',
		'action'     => 'save',
	));

// Route search
Route::set('video_search', 'video/search(/<page>)')
	->defaults(array(
		'controller' => 'Video',
		'action'     => 'search',
	));
	
// Route update
Route::set('video_update', 'video/update')
	->defaults(array(
		'controller' => 'Video',
		'action'     => 'update',
	));
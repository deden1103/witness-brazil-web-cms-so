<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Keranjang extends Controller_Backend {

	private $custom_header_form;

	private $custom_footer_form;

	private $global_model;

	public function before() {

		parent::before();

		//set global model
		$this->global_model = new Model_Globalmodel();

		// Header Multiple SElect
		$this->custom_header_form = '
			<link rel="stylesheet" href="'.URL::Base().'assets/plugins/select2/select2.min.css">
			<link rel="stylesheet" href="'.URL::Base().'assets/dist/css/AdminLTE.min.css">
		';

		// Custom header for daterangepicker
		$this->custom_header_form .= '<link rel="stylesheet" href="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		// Tinymce
		$this->custom_footer_form = '
			<script src="'.URL::Base().'assets/tinymce/tinymce.min.js"></script>
			<script>
				tinymce.init({
					paste_data_images: true,
					valid_elements : \'*[*]\',
					selector: \'.f_tinymce\',
					theme: \'modern\',
                    templates: [{title: \'Berita Lainnya\', description: \'Berita Lainnya\', url: \'/template/terkait.html\'},{title: \'Button Selengkapnya\', description: \'Button Selengkapnya\', url: \'/template/selengkapnya.html\'},{title: \'Sharing\', description: \'Sharing Information\', url: \'/template/sharing.html\'},{title: \'Mutiara\', description: \'Informasi Mutiara\', url: \'/template/komunitas.html\'},{title: \'Download Arena\', description: \'Link Download Aplikasi Arena\', url: \'/template/arena.html\'}],
					plugins: [
						\'example advlist autolink lists link image charmap print preview hr anchor pagebreak\',
						\'searchreplace wordcount visualblocks visualchars code fullscreen\',
						\'insertdatetime media nonbreaking save table contextmenu directionality\',
						\'emoticons template paste textcolor colorpicker textpattern imagetools\'
					],
					toolbar1: \'insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image\',
					toolbar2: \'fullscreen preview media | forecolor backcolor emoticons | pagebreak | my_image\',
					image_advtab: true,
                                        pagebreak_separator: "<!--PAGE_SEPARATOR-->",
                                        pagebreak_split_block: true,
                                        setup: function (editor) {
                                        editor.addButton(\'my_image\', {
                                            text: \'Add Image From Gallery\',
                                            icon: false,
                                            onclick: function () {
                                                open_popup_img_tmce();
                                            }
                                        });
                                      },
				});
				function my_voice(texthtml) {
					tinyMCE.activeEditor.insertContent(texthtml);
				}
			</script>
		';

		// Multiple SElect
		$this->custom_footer_form .= '
			<script src="'.URL::Base().'assets/plugins/select2/select2.full.min.js"></script>
			<script>
				$(".select2").select2({allowClear: true, placeholder : "-- Choose Keyword --"});
			</script>
		';

		// Custom footer for daterangepicker
		$this->custom_footer_form .= '
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				//Date range picker
				$(function() {
					$(\'.publish_time\').daterangepicker({
						format: \'DD/MM/YYYY HH:mm:ss\',
						timePicker: true,
						singleDatePicker: true,
						timePickerIncrement: 1,
						timePicker12Hour: false,
						//minDate: \'' . date('d-m-Y H:i:s') . '\'
					});
				});
				$("#myButton").click(function() {
					$("#kotak1").data(\'daterangepicker\').toggle();
				});
			</script>
			<!--Tinymce hidden for append image textarea-->
			<input type="hidden" id="image_popup_tmce" />
			<script>
				function open_popup_img() {
					PopupCenter("/library/index/1/0/image_popup", "google", "640", "480");
				}

				function open_popup_img_tmce() {
					PopupCenter("/library/index/1/0/image_popup_tmce", "google", "640", "480");
				}

				function image_popup_tmce() {
						var str = $(\'#image_popup_tmce\').val();
						var n = str.lastIndexOf(".");
						var res = str.substring(0, n)+str.substring(n);
						tinyMCE.activeEditor.insertContent(\'<img src="\' + res + \'" width="100%" />\');
				}

				function image_popup() {
					$(\'#previewImage\').html(\'\');
					$(\'#previewImage\').append(\'<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreview();">X</span></center></div>\');
					$(\'#previewImage\').append(\'<img src="\' + $(\'#image_popup\').val() + \'" style="width:200px;"/>\');
				}

				function delPreview() {
					$(\'#previewImage\').html(\'\');
					$(\'#image_popup\').val(\'\');
				}

				// GIF IMAGE JS FUNCTION
				function open_popup_img_gif() {
					PopupCenter("/library/index/1/0/image_popup_gif", "google", "640", "480");
				}

				function image_popup_gif() {
					$(\'#previewImageGif\').html(\'\');
					$(\'#previewImageGif\').append(\'<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreviewGif();">X</span></center></div>\');
					$(\'#previewImageGif\').append(\'<img src="\' + $(\'#image_popup_gif\').val() + \'" style="width:200px;"/>\');
				}

				function delPreviewGif() {
					$(\'#previewImageGif\').html(\'\');
					$(\'#image_popup_gif\').val(\'\');
				}

			</script>
		';

	}

	public function action_index() {

		$this->redirect('/keranjang/search');

	}

	public function action_search() {

		$session = Session::instance();

		if(isset($_POST['version'])) $session->set('version_keranjang', $_POST['version']);
		$data['version'] = $version = $session->get("version_keranjang", "");

		if(isset($_POST['search'])) $session->set('search_keranjang', $_POST['search']);
		$data['search'] = $search = $session->get("search_keranjang", "");

		if(isset($_GET['date_range'])) $session->set('date_range_keranjang', $_GET['date_range']);
		$data['date_range'] = $date_range = $session->get("date_range_keranjang", "");

		$data['main_title'] = __('Cart');
		$data['menu_active'] = 'keranjang';
		$data['menu_active_child'] = 'list';

		// Custom header for daterangepicker
		$data['custom_header'] = '<link rel="stylesheet" href="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		$data['custom_header'] .= '
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.min.css">
		';

		// Custom footer for daterangepicker
		$data['custom_footer'] = '
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				//Date range picker
				$(function() {
					$(\'#reservation\').daterangepicker(
						{
							format: \'DD/MM/YYYY\'
						}, function (start, end) {
							window.location = "/keranjang/search/?date_range=" + $(\'#reservation\').val();
						}
					);
				});
			</script>
		';

		$data['custom_footer'] .= '
			<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.all.min.js"></script>
		';

		//alert
		$data['custom_footer'] .= '
		<script>
			function del(id){
				var swal_id = id
				var total_data = $("#total-data").text();
				var new_total = parseInt(total_data)

				swal({
					title: "Are you sure?",
					text: "You won\'t be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3085d6",
					cancelButtonColor: "#d33",
					confirmButtonText: "Yes, delete it!"
				  }).then((result) => {
					if (result.value) {
						$.ajax({
							url: "/keranjang/delete/",
							dataType: "JSON",
							type: "POST",
							data: {swal_id : swal_id},
							success: function(res){
								$("#row"+swal_id).remove()
								$("#total-data").text(new_total-1 +" Data Found")
								swal(
									"Deleted!",
									"Your file has been deleted.",
									"success"
								  )
							},error: function(){
								swal(
									"Something Wrong Happen !!",
									"",
									"error"
								  )
							}
						})

					}
				  })

			}
		</script>
		';

		// Date parameter
		if(empty($date_range)) {
			$date1 = "";
			$date2 = "";

		} else {
			$ex_range = explode(' - ', $date_range);
			if(!empty($ex_range)) {
				$date1 = DateTime::createFromFormat('d/m/Y', $ex_range[0]);
				$date1 = $date1->format('Y-m-d');
				$date2 = DateTime::createFromFormat('d/m/Y', $ex_range[1]);
				$date2 = $date2->format('Y-m-d');

				// Date adding time
				$date1 = $date1 . ' 00:00:00';
				$date2 = $date2 . ' 00:00:00';
			}
		}


		// Page
		$page = intval($this->request->param('page'));
		if(empty($page)) {
			$page = 1;
		}

		// Load model video
		$keranjang_model = new Model_Keranjang();

		// Count all data
		$count_all = $keranjang_model->count_search($date1, $date2, $search);
		$data['count_all'] = $count_all;

		// Pagination
		$pagination = 	Pagination::factory(array(
							'total_items'    		=> $count_all,
							'items_per_page'		=> 10,
							'current_page'			=> $page,
							'base_url'				=> '/keranjang/search/',
							//'suffix'				=> '?date_range=' . $date_range,
							'view'					=> 'pagination/admin'
						));

		$data['list'] = $keranjang_model->list_search($date1, $date2, $search, $pagination->items_per_page, $pagination->offset);

		$data['pagination'] = $pagination->render();

		$view = Briliant::admin_template('keranjang/' . Kohana::$config->load('path.main_template') . '/list', $data);

		$this->response->body($view);

	}

	public function action_new() {
		$session = Session::instance();

		$data['main_title'] = __('Keranjang | Add New Data');
		$data['menu_active'] = 'keranjang';
		$data['menu_active_child'] = 'add';

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;

		$view = Briliant::admin_template('keranjang/' . Kohana::$config->load('path.main_template') . '/new', $data);
		$this->response->body($view);

	}

	public function action_save() {
		$session = Session::instance();

		// Validation
        $validation =  Kohana_Keranjang::validation($this->request->post());


		if($validation !== TRUE) { // Error Validation

			$data['main_title'] = __('Keranjang | Add New Data');
			$data['menu_active'] = 'keranjang';
			$data['menu_active_child'] = 'add';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['post'] = $this->request->post();
			$view = Briliant::admin_template('keranjang/' . Kohana::$config->load('path.main_template') . '/new', $data);
			$this->response->body($view);

		} else { // Validation Success

			// Get user id from session login
			$user_id = $session->get('adminId');
            $post = $this->request->post();

            $data_saved = [
                'cartTitle' => $post['title'],
                'cartDetail' => $post['detail'],
                'cartAdmiId' => $user_id
            ];

            $save = $this->global_model->save('cart',$data_saved);
            if($save){
                // Redirect
                $this->redirect('/keranjang/search');
            }

		}

	}

	public function action_detail() {
		$session = Session::instance();

            $data['main_title'] = __('Keranjang | Detail Data');
            $data['menu_active'] = 'keranjang';
            $data['menu_active_child'] = 'list';

            $id = $this->request->param('id');

            // Load model keranjang
            $keranjang_model = new Model_Keranjang();

            // Detail data
			$data['detail'] = $keranjang_model->detail_data($id);

			$view = Briliant::admin_template('keranjang/' . Kohana::$config->load('path.main_template') . '/detail', $data);
            $this->response->body($view);

	}

	public function action_edit() {
		$session = Session::instance();

		$data['main_title'] = __('Keranjang | Edit Data');
		$data['menu_active'] = 'keranjang';
		$data['menu_active_child'] = 'list';

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;

		// Paramter ID
		$id = $this->request->param('id');

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('adminId');

		// Load Model
		$keranjang_model = new Model_Keranjang();

		// Detail Data
		$detail = $keranjang_model->detail_data($id);
		$data['detail'] = $detail;
		$view = Briliant::admin_template('keranjang/' . Kohana::$config->load('path.main_template') . '/edit', $data);

		$this->response->body($view);

	}

	public function action_update() {

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('adminId');

		// Check Permission
		$id_parameter = $this->request->post('id');

		// Validation
		$validation = Kohana_Keranjang::validation($this->request->post());

		if($validation !== TRUE) { // Error Validation

			$data['main_title'] = __('Keranjang | Edit Data');
            $data['menu_active'] = 'keranjang';
            $data['menu_active_child'] = 'list';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['detail'] = $this->request->post();

			$view = Briliant::admin_template('keranjang/' . Kohana::$config->load('path.main_template') . '/edit', $data);

			$this->response->body($view);

		} else { // Validation Success

			// Load Model
            $post = $this->request->post();
            $update_data = $this->global_model->update(
                'cart',
                ['cartTitle' => $post['title'], 'cartDetail' => $post['detail'], 'cartAdmiId' => $user_id],
                ['cartId' => $post['id']]
            );
            if ($update_data) {
			    $this->redirect('/keranjang/search');
            }
		}

	}

	public function action_delete() {
		if(isset($_POST["swal_id"])){
			$id = $_POST['swal_id'];
			$update = $this->global_model->update('cart',['cartStatus' => 0],['cartId' => $id]);
			if ($update == 1) {
				echo json_encode($update);
			}else{
				http_response_code(500);
			}
		}

	}

}

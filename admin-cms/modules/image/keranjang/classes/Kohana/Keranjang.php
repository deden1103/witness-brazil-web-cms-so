<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_Keranjang {

	public static function validation($post) {
		
		$validation = 	Validation::factory($post)
						->rule('title', 'not_empty')
						->rule('title', 'max_length', array(':value', '65'))
						->rule('detail', 'not_empty')
						;
						
		if($validation->check()) {
			
			$return = TRUE;
		
		} else {
		
			$return = $validation->errors('validation');
		
		}
		
		return $return;
		
	}
    
    public static function get_image($video_id = '') {
        
        $return = '';
        
        $exec = DB::select(
                        array('arimId', 'image_id'),
                        array('arimFileType', 'image_type')
                )
                ->from('video_image_gif')
                ->join('arsip_images', 'LEFT')
                ->on('arsip_images.arimId', '=', 'video_image_gif.vgifArimId')
                ->where('vgifVdeoId', '=', $video_id)
                ->limit(1)
                ->execute()
                ->as_array();

        if(!empty($exec[0]['image_id'])) {
            $split = str_split($exec[0]['image_id']);
            $path = implode('/', $split);
            $return = '/uploads/library/' . $path . '/' . $exec[0]['image_id'] . '.' . $exec[0]['image_type'];
        }
        
        return $return;
        
    }
    
    public static function detail($video_id = '') {
        
        // Load MOdel
        $video_model = new Model_Video();
        
        return $video_model->detail_data($video_id);
        
    }
	
    
}
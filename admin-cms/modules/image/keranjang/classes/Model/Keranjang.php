<?php defined('SYSPATH') or die('No direct script access.');

class Model_Keranjang extends Model {

	public function count_search($date1 = '', $date2 = '', $search = '') {
		$session = Session::instance();
		$publish = $session->get("saved_cart", "cartSaved");
		$return = 0;
		//print_r($publish .' '. $date1 .' sampai '. $date2); exit;
		
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('cart');
		
		if (!empty($date1) and !empty($date2)){
			$query = $query->where(DB::expr('DATE('.$publish.')'), '>=', $date1)
						->where(DB::expr('DATE('.$publish.')'), '<=', $date2);
		}
		
		$ex_search = explode(',', $search);
		foreach($ex_search as $v_search) {
			$query = $query->where('cartTitle', 'LIKE', '%' . trim($v_search) . '%');
		}
		
		// $category = $session->get("category_video", "");
		// if(!empty($category)){
		// 	$query = $query->where('vdeoMscvId','=', $category);
		// }
		
		// echo Debug::vars((string) $query);
		$query = $query->order_by('cartId', 'DESC')
					->where('cartStatus','!=', 0)
					->execute()
					->as_array();
					
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function list_search($date1 = '', $date2 = '', $search = '', $limit = '', $offset = '') {
		$session = Session::instance();
		$publish = $session->get("saved_cart", "cartSaved");
		
		$exec = DB::select(
					array('cartId', 'id'),
					array('cartTitle', 'title'),
					array('cartSaved', 'saved'),
					array('cartAdmiId', 'savedId'),
					array('cartStatus', 'status'),
					array('admiRealName', 'user_name'),
					array('admiLvl', 'level')
				)
				->from('cart')
				->join('admin', 'LEFT')
				->on('cart.cartAdmiId', '=', 'admin.admiId');

		if (!empty($date1) and !empty($date2)){
			$exec = $exec->where(DB::expr('DATE('.$publish.')'), '>=', $date1)
						->where(DB::expr('DATE('.$publish.')'), '<=', $date2);
		}


		$exec = $exec->where('cartTitle', 'LIKE', '%' . $search . '%')
				->order_by('cartId', 'DESC')
				->where('cartStatus','!=', 0);
				
		$ex_search = explode(',', $search);
		foreach($ex_search as $v_search) {
			$exec = $exec->where('cartTitle', 'LIKE', '%' . trim($v_search) . '%');
		}
		
		// $category = $session->get("category_video", "");
		// //print_r($category); exit;
		// if(!empty($category)){
		// 	$exec = $exec->where('vdeoMscvId','=', $category);
		// }
		
		$exec = $exec->limit($limit)
				->offset($offset)
				->execute()
				->as_array();
				if(!empty($exec)) {

					/* foreach($exec as $k_exec => $v_exec) {
						
						if(!empty($v_exec['id'])) {
		
							// Get images
							$exec[$k_exec]['images'] = 	DB::select(
															array('vidArimId', 'image_id'),
															array('arimFileType', 'image_type')
														)
														->from('video_image')
														->join('arsip_image', 'LEFT')
														->on('arsip_image.arimId', '=', 'video_image.vidArimId')
														->where('vidVdeoId', '=', $v_exec['id'])
														->execute()
														->as_array();
		
						}
		
					} */
		
				}		
		
		return $exec;
		
	}

	public function save_data($data = '', $user_id = '') {
		// Start Transaction
		Database::instance()->begin();
		
		$publish_time = date('Y-m-d H:i:s'); // Default data now
		if(!empty($data['publish_time'])) {
			$publish_time = DateTime::createFromFormat('d/m/Y H:i:s', $data['publish_time']);
			$publish_time = $publish_time->format('Y-m-d H:i:s');
			
			$saved_time = date('Y-m-d H:i:s');
		} else {
			$saved_time = $publish_time;
		}

		$keyword = serialize($data['keyword']);
		$video 		=	DB::insert('video', array(
						'vdeoTitle',
						'vdeoExcerpt',
						'vdeoDetail',
						'vdeoKeyword',
						'vdeoMscvId', // Categpry or Kanal
						'vdeoEmbedURL',
						'vdeoPublishTime',
						'vdeoSaved',
						'vdeoAdmiIdSaved'
						// 'vdeoAdmiIdApproved'
					))
					->values(array(
						$data['title'],
						$data['description'],
						$data['detail'],
						$keyword,
						$data['category'],
						$data['embed_url'],
						$publish_time,
						$saved_time,
						$user_id
						// $user_id
					));
		
		$video->execute();;

		
		
		// Commit transaction
		Database::instance()->commit();
		
	}
	
	public function detail_data($id = '') {
		
		$return = '';
		
		$exec = DB::select(
					array('cartId', 'id'),
					array('cartTitle', 'title'),
					array('cartSaved', 'saved'),
					array('cartAdmiId', 'savedId'),
					array('cartStatus', 'status'),
					array('admiRealName', 'user_name'),
					array('cartDetail', 'detail')
				)
				->from('cart')
				->join('admin', 'LEFT')
				->on('cart.cartAdmiId', '=', 'admin.admiId')
				->where('cartId', '=', $id)
				->execute()
				->as_array();
		
		if(!empty($exec[0])) {
			
			$return = $exec[0];
			
		}
		
		return $return;
		
	}
	
	public function update_data($data = '', $user_id = '', $action='') {
		
		// Id video
		$id_video = $data['id'];
		
		// Start Transaction
		Database::instance()->begin();

		// Check old image
		if($data['image'] != @$data['image_old']) {

			// Delete Data Old
			DB::delete('video_image')
				->where('vidVdeoId', '=', $id_video)
				->execute();

			// Check new image
			if(!empty($data['image'])) {

				// Get id image from full url image
				$base_image = basename($data['image']).PHP_EOL;
				if(!empty($base_image)) {
					$ex_base = explode('.', $base_image);
					list($id_image, $file_type) = $ex_base;
				}

					if(!empty($id_image)) {
						$image = 	DB::insert('video_image', array(
										'vidVdeoId',
										'vidArimId',
										'vidAdmiIdSaved'
									))
									->values(array(
										$id_video,
										$id_image,
										$user_id
									))
									->execute();
					}

			}

		}

		// Update date video
		$publish_time = date('Y-m-d H:i:s'); // Default data now
		if(!empty($data['publish_time'])) {
			$publish_time = DateTime::createFromFormat('d/m/Y H:i:s', $data['publish_time']);
			$publish_time = $publish_time->format('Y-m-d H:i:s');
			
			$saved_time = date('Y-m-d H:i:s');
		} else {
			$saved_time = $publish_time;
		}
		
		if(!empty($data['finish_time'])) {
			$finish_time = DateTime::createFromFormat('d/m/Y H:i:s', $data['finish_time']);
			$finish_time = $finish_time->format('Y-m-d H:i:s');
		}

		if(!empty($data['keyword'])){
			$keyword = serialize($data['keyword']);
		}
		
		$update = DB::update('video')
			->set(array('vdeoTitle' => $data['title']))
			->set(array('vdeoExcerpt' => $data['description']))
			->set(array('vdeoDetail' => $data['detail']))
			->set(array('vdeoKeyword' => $keyword))
			->set(array('vdeoEmbedURL' => $data['embed_url']))
			->set(array('vdeoMscvId' => $data['category']))
			->set(array('vdeoPublishTime' => $publish_time))
			->set(array('vdeoSaved' => $saved_time))
			->set(array('vdeoAdmiIdSaved' => $user_id));
			
		$update = $update->where('vdeoId', '=', $id_video)->execute();
		
		// Commit transaction
		Database::instance()->commit();
		
	}
	
	public function delete_data($id = '') {
		
		DB::update('video')
			->set(array('vdeoStatus' => 0))
			->where('vdeoId', '=', $id)
			->execute();
		
	}
}
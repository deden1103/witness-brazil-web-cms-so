<?php

// Route delete
Route::set('keranjang_delete', 'keranjang/delete(/<id>)')
	->defaults(array(
		'controller' => 'Keranjang',
		'action'     => 'delete',
	));

// Route detail
Route::set('keranjang_detail', 'keranjang/detail(/<id>)')
	->defaults(array(
		'controller' => 'Keranjang',
		'action'     => 'detail',
	));

// Route edit
Route::set('keranjang_edit', 'keranjang/edit(/<id>)')
	->defaults(array(
		'controller' => 'Keranjang',
		'action'     => 'edit',
	));
	
// Route index
Route::set('keranjang_index', 'keranjang/index(/<page>)')
	->defaults(array(
		'controller' => 'Keranjang',
		'action'     => 'index',
	));
	
// Route new
Route::set('keranjang_new', 'keranjang/new')
	->defaults(array(
		'controller' => 'Keranjang',
		'action'     => 'new',
	));
	
// Route save
Route::set('keranjang_save', 'keranjang/save')
	->defaults(array(
		'controller' => 'Keranjang',
		'action'     => 'save',
	));

// Route search
Route::set('keranjang_search', 'keranjang/search(/<page>)')
	->defaults(array(
		'controller' => 'Keranjang',
		'action'     => 'search',
	));
	
// Route update
Route::set('keranjang_update', 'keranjang/update')
	->defaults(array(
		'controller' => 'Keranjang',
		'action'     => 'update',
	));
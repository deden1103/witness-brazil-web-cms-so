
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo __('Edit Category'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/home"><?php echo __('Painel de controlo'); ?></a></li>
			<li><a href="/video"><?php echo __('Video'); ?></a></li>
			<li><a href="#"><?php echo __('Config'); ?></a></li>
			<li class="active"><a href="#"><?php echo __('Edit Category'); ?></a></li>
		</ol>
	</section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
                    <form action="<?php echo URL::Base(); ?>video/update_category" method="post">
                        <div class="form-group">
                            <label><?php echo __('Nome de categoria') ?></label>
                            <input autofocus type="text" name="name" class="form-control" value="<?php echo $data['ctg_category'][0]['name']; ?>" required>
                            <input type="hidden" name="id" value="<?php echo $data['ctg_category'][0]['id']; ?>">
                        </div>
                    </div>
                    <div class="box-footer">
							<button type="submit" name="save" value="update" class="btn btn-primary"><?php echo __('Guardar'); ?></button>
                            <a href="/video/category" class="btn btn-danger">Cancel</a>
						</div>
					</form>
                </div>
            </div>
        </div>
    </section>
</div>  

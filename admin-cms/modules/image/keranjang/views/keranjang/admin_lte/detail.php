<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo __('Newsroom Video Detail'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Sala de imprensa'); ?></li>
			<li class="active"><a href="/video"><?php echo __('Video'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
					<div class="table-responsive">
						<table class="table">
							<tr>
								<td width="15%"><b><?php echo __('Status'); ?></b></td>
								<td width="5px">:</td>
								<td>
									<?php 
										if($data['detail']['status'] == 1) { // Saved
											echo '<span class="text-light-blue" style=" font-weight: bolder; ">** SAVED **</span>';
										} else if($data['detail']['status'] == 2) {
											echo '<span class="text-green" style=" font-weight: bolder; ">** PUBLISHED **</span>';
										} else {
											echo '<span class="text-red" style=" font-weight: bolder; ">** DELETED **</span>';
										}
									?>
								</td>
							</tr>
							<tr>
								<td width="15%"><b><?php echo __('Carregador'); ?></b</td>
								<td width="5px">:</td>
								<td><?php echo $data['detail']['user_name']; ?></td>
							</tr>
							<tr>
								<td width="15%"><b><?php echo __('Título'); ?></b</td>
								<td width="5px">:</td>
								<td><?php echo $data['detail']['title']; ?></td>
							</tr>
							<tr>
								<td width="15%"><b><?php echo __('Detalhes'); ?></b</td>
								<td width="5px">:</td>
								<td><?php echo $data['detail']['detail']; ?></td>
							</tr>
						</table>
						</div>
					</div>
					<div class="box-footer">
						<form action="<?=  URL::Base(); ?>video/new" method="post">
							<input type="hidden" name="title" value="<?= $data['detail']['title']; ?>">
							<input type="hidden" name="detail" value="<?= $data['detail']['detail']; ?>">
							<a href="javascript:history.go(-1);"><button type="button" class="btn btn-info"><?php echo __('Back'); ?></button></a>
							<button class="btn btn-success" type="submit">Copy Data</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo __('Configurar a categoria'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/home"><?php echo __('Painel de controlo'); ?></a></li>
			<li><a href="/user"><?php echo __('User'); ?></a></li>
			<li><a href="#"><?php echo __('Config'); ?></a></li>
			<li class="active"><a href="#"><?php echo __('Configurar a categoria'); ?></a></li>
		</ol>
	</section>
    <section class="content">
        <div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
                    <div class="box-header with-border">
						<h3 class="box-title">Add Data</h3>
					</div>
					<div class="box-body">
                    <form action="<?php echo URL::Base(); ?>video/add_category" method="post">
                        <div class="form-group">
                            <label><?php echo __('Nome de categoria') ?></label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                    </div>
                    <div class="box-footer">
							<button type="submit" name="save" value="save" class="btn btn-primary"><?php echo __('Guardar'); ?></button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
						</div>
					</form>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nome de categoria</th>
                                    <th colspan="2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                 $no = 1;
                                 foreach ($data['ctg_category'] as $key) {
                                    echo '
                                        <tr id="row'.$key['id'].'">   
                                            <td>'.$key['name'].'</td>
                                            <td>
                                                <a href="'.URL::Base().'video/editcategory/' . $key['id'] . '" class="btn btn-xs btn-block btn-warning"> Update </a>
                                            </td>
                                            <td>
                                                <button onclick="del(\''.$key['id'].'\')" class="btn btn-xs btn-block btn-danger"> Delete </buttin>
                                            </td>
                                        </tr>
                                    ';
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>  

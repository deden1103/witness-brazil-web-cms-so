<div class="box-body">
	<table class="table table-bordered table-striped">
		<tr>
			<th style="width:30%; text-align:center;"><?php echo __('Título'); ?></th>
			<th style="text-align:center;"><?php echo __('Carregador'); ?></th>
			<th style="text-align:center;"><?php echo __('Status'); ?></th>
			<th colspan="2" style="width:30%;text-align:center;"><?php echo __('Ações'); ?></th>
		</tr>
		<?php
		if(!empty($data['list'])) {
			foreach($data['list'] as $v_list) {

				// Image article
				// $img_video = '' . __('[ No Image Available ]') . '';
				// if(!empty($v_list['images'][0]['image_id'])) {
				// 	$split_id = str_split($v_list['images'][0]['image_id']);
				// 	$path_folder_image = implode('/', $split_id);
				// 	$img_video = '<img src="' . URL::base() .'uploads/library/' . $path_folder_image . '/' . $v_list['images'][0]['image_id'] . '_278x139.' . $v_list['images'][0]['image_type'] . '" />';
				// }

				// Status
				$status = '';
				if($v_list['status'] == 1) {
					$status = '<span class="label label-info">Saved</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
				} else if($v_list['status'] == 0) {
					$status = '<span class="label label-danger">Deleted</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
				} else if($v_list['status'] == 2) {
					//$url_live = Kohana::$config->load('path.arah')."/video/{$v_list['id']}/".URL::title($v_list['title']).'.html';
					$status = '<span class="label label-success">Published</span> <b>' . date('d M Y H:i', strtotime($v_list['publish'])) . '</b>';
					//$video_status .= '<br><br><b><i>Approved By :<br>' . Video::get_user_approver($v_list['id']) . '<i></b>';
				}
				
				$level = Controller_Backend::check_level();
				// Default Button
					$button = '
							<a href="'.URL::Base().'keranjang/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
							<a href="'.URL::Base().'keranjang/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
							<button onclick="del('.$v_list['id'].')" class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button><br><br>
					';
				
				// Button If Status 0
				if($v_list['status'] == 0) {
					$button = '
							<a href="'.URL::Base().'video/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
							<a href="javascript:void()"><button disabled class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
							<a href="javascript:void()"><button disabled class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a>
					';
				}
				
				$article_title = $v_list['title'];
				if(!empty($data['search'])) {
					$ex_search = explode(',',$data['search']);
					foreach($ex_search as $v_search) {
						$article_title = str_ireplace(trim($v_search), '<span style="color:red"><b><i>' . trim($v_search). '</i></b></span>', $article_title);
					}
				}

				
				//level
				if ($v_list['level'] == 1) {
					$level = "Admin";
				}elseif ($v_list['level'] == 2) {
					$level = "Redaksi";
				}else{
					$level = "Editor";
				}
				
				echo '
					<tr id="row'.$v_list['id'].'">
						<td><center>' . $article_title . '</center></td>
						<td>
							' . ucfirst($v_list['user_name']) . ' <br>- 
							' . ucfirst($level). '
						</td>
						<td>'.$status.'</td>
						<td>' . $button . '</td>
					</tr>
				';
			}
		}
		?>
	</table>
</div>
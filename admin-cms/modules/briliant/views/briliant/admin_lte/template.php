<?php $level = Controller_Backend::check_level(); ?>
<!DOCTYPE html>
<html>
	<!-- interesting with this code? ---- syempunaatgmaildotcom --- -->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<META NAME="robots" CONTENT="noindex,nofollow">
		<title><?php echo !empty($main_title) ? $main_title : __('Dashboard'); ?></title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="<?php echo URL::base(); ?>assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo URL::base(); ?>assets/plugins/datepicker/datepicker3.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo URL::base(); ?>assets/dist/css/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
			folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="<?php echo URL::base(); ?>assets/dist/css/skins/_all-skins.min.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo URL::base(); ?>assets/favicon.ico" type="image/x-icon" />
		<?php echo $custom_header; ?>

	</head>
	<body class="hold-transition skin-blue sidebar-mini fixed">
		<!-- Site wrapper -->
		<div class="wrapper">
			<header class="main-header">
				<!-- Logo -->
				<a href="/" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b><?php echo __('S6'); ?></b></span>
					<!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b><?php echo __('CMS'); ?></b></span>
				</a>
				<!-- Header Navbar: style can be found in header.less -->
				<nav class="navbar navbar-static-top" role="navigation">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>

					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- Control Sidebar Toggle Button -->
							<li>
								<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<!-- =============================================== -->
			<!-- Left side column. contains the sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
					<div class="user-panel">
						<div class="pull-left image">
							<?php
							$user_avatar = '/assets/images/user/default.png';
							if(!empty($user_detail['userAvatar'])) {
								$user_avatar = $user_detail['userAvatar'];
							}
							?>
							<img src="<?php echo $user_avatar; ?>" class="img-circle" alt="User Image">
						</div>
						<div class="pull-left info">
							<p><?php echo ucfirst($user_detail['userRealName']) ?></p>
							<?php
								if ($level == 1) {
									$lvl = "Admin";
								}elseif ($level == 2 || $level == 4) {
									$lvl = "Redaksi";
								}else{
									$lvl = "Editor";
								}
							?>
							<span><?php echo $lvl; ?></span>
						</div>
					</div>
					<!-- Sidebar user panel -->
					<ul class="sidebar-menu">
						<li class="header"><?php echo __('Navegação principal'); ?></li>
						<?php
							if($level == 1 or $level == 3){
						?>
						<li class="treeview <?php echo ($menu_active == 'dashboard') ? 'active' : ''; ?>">
							<a href="<?php echo URL::base(); ?>home">
								<i class="fa fa-dashboard"></i> <span><?php echo __('Painel de controlo'); ?></span>
							</a>
						</li>
						<?php
							}
							if($level == 1 or $level == 4){
						?>
						<li class="treeview <?php echo ($menu_active == 'users') ? 'active' : ''; ?>">
							<a href="">
								<i class="fa fa-user-secret"></i> <span><?php echo __('Users'); ?></span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="treeview <?php echo ($menu_active_child == 'list') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>users/list">
										<i class="fa fa-circle-o"></i>
										<span><?php echo __('List'); ?></span>
									</a>
								</li>

								<li class="treeview <?php echo ($menu_active_child == 'new') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>users/new">
										<i class="fa fa-circle-o"></i>
										<span><?php echo __('Novo'); ?></span>
									</a>
								</li>

							</ul>
						</li>
						<li class="treeview <?php echo ($menu_active == 'komunitas') ? 'active' : ''; ?>">
							<a href="">
							
							<i class="fa fa-users"></i> <span><?php echo __('Comunidade'); ?></span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="treeview <?php echo ($menu_active_child_1 == 'list') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>komunitas/search"><i class="fa fa-circle-o"></i> List</a>
								</li>
								<li class="treeview <?php echo ($menu_active_child_1 == 'new') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>komunitas/new"><i class="fa fa-circle-o"></i> New</a>
								</li>
							
								<li class="treeview <?php echo ($menu_active_child == 'userkom') ? 'active' : ''; ?>">
									<a href="">
										<i class="fa fa-cog"></i> Corretor de informação
										<i class="fa fa-angle-left pull-right"></i>
									</a>
									<ul class="treeview-menu">
										<li class="treeview <?php echo ($menu_active_child_1 == 'list_ukom') ? 'active' : ''; ?>">
											<a href="<?php echo URL::base(); ?>userkom/search">
												<i class="fa fa-circle-o"></i>
												<span><?php echo __('List'); ?></span>
											</a>
										</li>
										<li class="treeview <?php echo ($menu_active_child_1 == 'new_ukom') ? 'active' : ''; ?>">
											<a href="<?php echo URL::base(); ?>userkom/new">
												<i class="fa fa-circle-o"></i>
												<span><?php echo __('Novo'); ?></span>
											</a>
										</li>
										<li class="treeview <?php echo ($menu_active_child_1 == 'cp_ukom') ? 'active' : ''; ?>">
											<a href="<?php echo URL::base(); ?>userkom/changepass">
												<i class="fa fa-circle-o"></i>
												<span><?php echo __('Mudar senha'); ?></span>
											</a>
										</li>
										
										</li>
									</ul>
								</li>

								<li class="treeview <?php echo ($menu_active_child_1 == 'report') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>userkom/report"><i class="fa fa-circle-o"></i>Relatório</a>
								</li>

							</ul>
						</li>
						<li class="treeview <?php echo ($menu_active == 'witness') ? 'active' : ''; ?>">
							<a href="">
							
							<i class="fa fa-cog"></i> <span><?php echo __('Testemunha'); ?></span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="treeview <?php echo ($menu_active_child == 'about') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>witness/about"><i class="fa fa-circle-o"></i> Sobre nós</a>
								</li>
								<li class="treeview <?php echo ($menu_active_child == 'pms') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>witness/pms"><i class="fa fa-circle-o"></i> Diretrizes de serviço cibernético</a>
								</li>
								<li class="treeview <?php echo ($menu_active_child == 'layanan') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>witness/layanan"><i class="fa fa-circle-o"></i> Termos de serviço</a>
								</li>
							</ul>
						</li>


						<?php }
							if($level == 1 or $level == 3 or $level == 4){
						?>
						<li class="treeview <?php echo ($menu_active == 'video') ? 'active' : ''; ?>">
							<a href="">
								<i class="fa fa-newspaper-o"></i> <span><?php echo __('Relatório'); ?></span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="treeview <?php echo ($menu_active_child_1 == 'list') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>laporan/search"><i class="fa fa-circle-o"></i> List</a>
								</li>
								<li class="treeview <?php echo ($menu_active_child == 'config') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>laporan/config">
										<i class="fa fa-cog"></i> Config
										<i class="fa fa-angle-left pull-right"></i>
									</a>
									<ul class="treeview-menu">
										<li class="treeview <?php echo ($menu_active_child_1 == 'category') ? 'active' : ''; ?>">
											<a href="<?php echo URL::base(); ?>laporan/category">
												<i class="fa fa-circle-o"></i>
												<span><?php echo __('Category'); ?></span>
											</a>
										</li>
										<li class="treeview <?php echo ($menu_active_child_1 == 'keyword') ? 'active' : ''; ?>">
											<a href="<?php echo URL::base(); ?>laporan/keyword">
												<i class="fa fa-circle-o"></i>
												<span><?php echo __('Palavra-chave'); ?></span>
											</a>
										</li>
										</li>
									</ul>
								</li>
							</ul>
						</li>

			
						<?php } ?>

						<li class="treeview <?php echo ($menu_active == 'hot') ? 'active' : ''; ?>">
							<a href="">
								<i class="fa fa-book"></i> <span><?php echo __('Tópico quente'); ?></span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="treeview <?php echo ($menu_active_child == 'list') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>hot/index">
										<i class="fa fa-circle-o"></i>
										<span><?php echo __('List'); ?></span>
									</a>
								</li>

								

							</ul>
						</li>

						<li class="treeview <?php echo ($menu_active == 'featured') ? 'active' : ''; ?>">
							<a href="">
								<i class="fa fa-book"></i> <span><?php echo __('Artigo em destaque'); ?></span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="treeview <?php echo ($menu_active_child == 'list') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>featured/article">
										<i class="fa fa-circle-o"></i>
										<span><?php echo __('List'); ?></span>
									</a>
								</li>

								

							</ul>
						</li>

						

						
						<li class="treeview <?php echo ($menu_active == 'contacts') ? 'active' : ''; ?>">
							<a href="">
								<i class="fa fa-book"></i> <span><?php echo __('Contacto'); ?></span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="treeview <?php echo ($menu_active_child == 'list') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>contacts/list">
										<i class="fa fa-circle-o"></i>
										<span><?php echo __('List'); ?></span>
									</a>
								</li>

								<li class="treeview <?php echo ($menu_active_child == 'new') ? 'active' : ''; ?>">
									<a href="<?php echo URL::base(); ?>contacts/new">
										<i class="fa fa-circle-o"></i>
										<span><?php echo __('Novo'); ?></span>
									</a>
								</li>

							</ul>
						</li>
						<li>
							<a href="<?php echo URL::base(); ?>login/logout"><i class="fa fa-sign-out"></i> <span><?php echo __('Sair (da sessão)'); ?></span></a>
						</li>
					</ul>
				</section>
				<!-- /.sidebar -->
			</aside>
			<!-- =============================================== -->
			<?php echo $content; ?>
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b>Testemunha
				</div>
				<strong>Copyright &copy; <?php echo date('Y'); ?></strong> All rights reserved.
			</footer>

			<!-- Control Sidebar -->
			<aside class="control-sidebar control-sidebar-dark">
				<div class="tab-content">
					<div class="tab-pane" id="control-sidebar-home-tab"></div>
				</div>
			</aside>

		</div>
		<!-- ./wrapper -->
		<!-- jQuery 2.1.4 -->
		<script src="<?php echo URL::base(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="<?php echo URL::base(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
		<!-- SlimScroll -->
		<script src="<?php echo URL::base(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- FastClick -->
		<script src="<?php echo URL::base(); ?>assets/plugins/fastclick/fastclick.min.js"></script>
		<!-- AdminLTE App -->
		<script src="<?php echo URL::base(); ?>assets/dist/js/app.min.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="<?php echo URL::base(); ?>assets/dist/js/demo.js"></script>
		<script src="<?php echo URL::base(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

		<script>
			function PopupCenter(url, title, w, h) {
				// Fixes dual-screen position                         Most browsers      Firefox
				var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
				var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

				width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
				height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

				var left = ((width / 2) - (w / 2)) + dualScreenLeft;
				var top = ((height / 2) - (h / 2)) + dualScreenTop;
				var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

				// Puts focus on the newWindow
				if (window.focus) {
					newWindow.focus();
				}
			}

		</script>

		<script>

			$( "#txttitle" ).on('input', function() {
  			  if ($(this).val().length>60) {
        	alert('you have reached a limit of 60');
   			 }
			});

			$( "#myinput2" ).on('input', function() {
				if ($(this).val().length>140) {
			alert('you have reached a limit of 140');
			}
			});

		</script>

		<script>
			//tambah titik tiap kelipatan 3 di input angka (jumlah uang)
			$('.amount').keyup(function(event) {
			// skip for arrow keys
			if(event.which >= 37 && event.which <= 40){
				event.preventDefault();
			}
			$(this).val(function(index, value) {
				return value
					.replace(/\D/g, '')
					//.replace(/([0-9])([0-9]{2})$/, '$1.$2')
					.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, '.')
				;
				});
			});
		</script>

		<?php echo $custom_footer; ?>
	</body>
</html>

<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller_Backend {

	public function action_index() {
		$data['main_title'] = "Painel de controlo";
		$data['menu_active'] = "dashboard";
		// Load data from model
		$home_model = new Model_Home();

		// Count All Data
		//$data['category'] = $home_model->count_all("video","vdeoStatus");
		$data['laporan'] = $home_model->graph_laporan();
		$data['penulis'] = $home_model->graph_penulis();
		$data['keyword'] = $home_model->graph_keyword();
		
		$data['custom_footer'] = '
		<script src="/assets/plugins/raphael/raphael.min.js"></script>
		<script src="/assets/plugins/morris/morris.min.js"></script>
		<script src="/assets/plugins/chartjs/Chart.js"></script>
		<script src="/assets/plugins/fastclick/fastclick.js"></script>
		<script>
			"use strict";
			
			function changeLaporan(){
				var val = $("#laporan").val();
				var data = {date: val}
				
				$.ajax({
				  type: "POST",
				  url: "/home/laporan",
				  data: data,
				  datatype: "json",
				  success: function(data, status) {
					data = jQuery.parseJSON(data);
					loadLaporan(data);
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				  }
				});
			}
			
			function changePenulis(){
				var val = $("#penulis").val();
				var data = {date: val}
				
				$.ajax({
				  type: "POST",
				  url: "/home/penulis",
				  data: data,
				  datatype: "json",
				  success: function(data, status) {
					data = jQuery.parseJSON(data);
					loadPenulis(data);
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				  }
				});
			}
			
			function changeKeyword(){
				var val = $("#keyword").val();
				var data = {date: val}
				
				$.ajax({
				  type: "POST",
				  url: "/home/keyword",
				  data: data,
				  datatype: "json",
				  success: function(data, status) {
					data = jQuery.parseJSON(data);
					loadKeyword(data);
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				  }
				});
			}
			
			function loadLaporan(data){
				var label_laporan = [];
				var approved_laporan = [];
				var saved_laporan = [];
				
				$.each(data, function( index, value ) {
					value.approved = parseInt(value.approved);
					value.saved = parseInt(value.saved);
					
					label_laporan.push(value.name);
					approved_laporan.push(value.approved);
					saved_laporan.push(value.saved);
				});
				
				var areaChartCanvas = $("#areaLaporan").get(0).getContext("2d")
				var areaChart       = new Chart(areaChartCanvas)

				var areaChartData = {
				  labels  : label_laporan,
				  datasets: [
					{
					  label               : "Approved",
					  fillColor           : "rgba(210, 214, 222, 1)",
					  strokeColor         : "rgba(210, 214, 222, 1)",
					  pointColor          : "rgba(210, 214, 222, 1)",
					  pointStrokeColor    : "#c1c7d1",
					  pointHighlightFill  : "#fff",
					  pointHighlightStroke: "rgba(220,220,220,1)",
					  data                : approved_laporan
					},
					{
					  label               : "Saved",
					  fillColor           : "rgba(60,141,188,0.9)",
					  strokeColor         : "rgba(60,141,188,0.8)",
					  pointColor          : "#3b8bba",
					  pointStrokeColor    : "rgba(60,141,188,1)",
					  pointHighlightFill  : "#fff",
					  pointHighlightStroke: "rgba(60,141,188,1)",
					  data                : saved_laporan
					}
				  ]
				}

				var areaChartOptions = {
				  //Boolean - If we should show the scale at all
				  showScale               : true,
				  //Boolean - Whether grid lines are shown across the chart
				  scaleShowGridLines      : false,
				  //String - Colour of the grid lines
				  scaleGridLineColor      : "rgba(0,0,0,.05)",
				  //Number - Width of the grid lines
				  scaleGridLineWidth      : 1,
				  //Boolean - Whether to show horizontal lines (except X axis)
				  scaleShowHorizontalLines: true,
				  //Boolean - Whether to show vertical lines (except Y axis)
				  scaleShowVerticalLines  : true,
				  //Boolean - Whether the line is curved between points
				  bezierCurve             : true,
				  //Number - Tension of the bezier curve between points
				  bezierCurveTension      : 0.3,
				  //Boolean - Whether to show a dot for each point
				  pointDot                : false,
				  //Number - Radius of each point dot in pixels
				  pointDotRadius          : 4,
				  //Number - Pixel width of point dot stroke
				  pointDotStrokeWidth     : 1,
				  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
				  pointHitDetectionRadius : 20,
				  //Boolean - Whether to show a stroke for datasets
				  datasetStroke           : true,
				  //Number - Pixel width of dataset stroke
				  datasetStrokeWidth      : 2,
				  //Boolean - Whether to fill the dataset with a color
				  datasetFill             : true,
				  //String - A legend template
				  legendTemplate          : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
				  //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
				  maintainAspectRatio     : true,
				  //Boolean - whether to make the chart responsive to window resizing
				  responsive              : true
				}
				
				//Create the line chart
				areaChart.Line(areaChartData, areaChartOptions)				
			}
			
			function loadPenulis(data){
				var label_laporan = [];
				var approved_laporan = [];
				var saved_laporan = [];
				
				$.each(data, function( index, value ) {
					value.approved = parseInt(value.approved);
					value.saved = parseInt(value.saved);
					
					label_laporan.push(value.name);
					approved_laporan.push(value.approved);
					saved_laporan.push(value.saved);
				});
				
				var areaChartCanvas = $("#areaPenulis").get(0).getContext("2d")
				var areaChart       = new Chart(areaChartCanvas)

				var areaChartData = {
				  labels  : label_laporan,
				  datasets: [
					{
					  label               : "Approved",
					  fillColor           : "#3fb303",
					  strokeColor         : "#3fb303",
					  pointColor          : "#3fb303",
					  pointStrokeColor    : "#c1c7d1",
					  pointHighlightFill  : "#fff",
					  pointHighlightStroke: "rgba(220,220,220,1)",
					  data                : approved_laporan
					},
					{
					  label               : "Saved",
					  fillColor           : "#ffff00",
					  strokeColor         : "#ffff00",
					  pointColor          : "#ffff00",
					  pointStrokeColor    : "rgba(60,141,188,1)",
					  pointHighlightFill  : "#fff",
					  pointHighlightStroke: "rgba(60,141,188,1)",
					  data                : saved_laporan
					}
				  ]
				}

				var areaChartOptions = {
				  //Boolean - If we should show the scale at all
				  showScale               : true,
				  //Boolean - Whether grid lines are shown across the chart
				  scaleShowGridLines      : false,
				  //String - Colour of the grid lines
				  scaleGridLineColor      : "rgba(0,0,0,.05)",
				  //Number - Width of the grid lines
				  scaleGridLineWidth      : 1,
				  //Boolean - Whether to show horizontal lines (except X axis)
				  scaleShowHorizontalLines: true,
				  //Boolean - Whether to show vertical lines (except Y axis)
				  scaleShowVerticalLines  : true,
				  //Boolean - Whether the line is curved between points
				  bezierCurve             : true,
				  //Number - Tension of the bezier curve between points
				  bezierCurveTension      : 0.3,
				  //Boolean - Whether to show a dot for each point
				  pointDot                : false,
				  //Number - Radius of each point dot in pixels
				  pointDotRadius          : 4,
				  //Number - Pixel width of point dot stroke
				  pointDotStrokeWidth     : 1,
				  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
				  pointHitDetectionRadius : 20,
				  //Boolean - Whether to show a stroke for datasets
				  datasetStroke           : true,
				  //Number - Pixel width of dataset stroke
				  datasetStrokeWidth      : 2,
				  //Boolean - Whether to fill the dataset with a color
				  datasetFill             : true,
				  //String - A legend template
				  legendTemplate          : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
				  //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
				  maintainAspectRatio     : true,
				  //Boolean - whether to make the chart responsive to window resizing
				  responsive              : true
				}
				
				//Create the line chart
				areaChart.Line(areaChartData, areaChartOptions)				
			}
			
			function loadKeyword(data){
				var label_laporan = [];
				var approved_laporan = [];
				var saved_laporan = [];
				
				$.each(data, function( index, value ) {
					value.approved = parseInt(value.approved);
					value.saved = parseInt(value.saved);
					
					label_laporan.push(value.name);
					approved_laporan.push(value.approved);
					saved_laporan.push(value.saved);
				});
				
				var areaChartCanvas = $("#areaKeyword").get(0).getContext("2d")
				var areaChart       = new Chart(areaChartCanvas)

				var areaChartData = {
				  labels  : label_laporan,
				  datasets: [
					{
					  label               : "Approved",
					  fillColor           : "#3fb303",
					  strokeColor         : "#3fb303",
					  pointColor          : "#3fb303",
					  pointStrokeColor    : "#c1c7d1",
					  pointHighlightFill  : "#fff",
					  pointHighlightStroke: "rgba(220,220,220,1)",
					  data                : approved_laporan
					},
					{
					  label               : "Saved",
					  fillColor           : "#ffff00",
					  strokeColor         : "#ffff00",
					  pointColor          : "#ffff00",
					  pointStrokeColor    : "rgba(60,141,188,1)",
					  pointHighlightFill  : "#fff",
					  pointHighlightStroke: "rgba(60,141,188,1)",
					  data                : saved_laporan
					}
				  ]
				}

				var areaChartOptions = {
				  //Boolean - If we should show the scale at all
				  showScale               : true,
				  //Boolean - Whether grid lines are shown across the chart
				  scaleShowGridLines      : false,
				  //String - Colour of the grid lines
				  scaleGridLineColor      : "rgba(0,0,0,.05)",
				  //Number - Width of the grid lines
				  scaleGridLineWidth      : 1,
				  //Boolean - Whether to show horizontal lines (except X axis)
				  scaleShowHorizontalLines: true,
				  //Boolean - Whether to show vertical lines (except Y axis)
				  scaleShowVerticalLines  : true,
				  //Boolean - Whether the line is curved between points
				  bezierCurve             : true,
				  //Number - Tension of the bezier curve between points
				  bezierCurveTension      : 0.3,
				  //Boolean - Whether to show a dot for each point
				  pointDot                : false,
				  //Number - Radius of each point dot in pixels
				  pointDotRadius          : 4,
				  //Number - Pixel width of point dot stroke
				  pointDotStrokeWidth     : 1,
				  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
				  pointHitDetectionRadius : 20,
				  //Boolean - Whether to show a stroke for datasets
				  datasetStroke           : true,
				  //Number - Pixel width of dataset stroke
				  datasetStrokeWidth      : 2,
				  //Boolean - Whether to fill the dataset with a color
				  datasetFill             : true,
				  //String - A legend template
				  legendTemplate          : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
				  //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
				  maintainAspectRatio     : true,
				  //Boolean - whether to make the chart responsive to window resizing
				  responsive              : true
				}
				
				//Create the line chart
				areaChart.Line(areaChartData, areaChartOptions)				
			}
			
			var data_laporan = '.json_encode($data['laporan']).';
			loadLaporan(data_laporan);
			
			var data_penulis = '.json_encode($data['penulis']).';
			loadPenulis(data_penulis);
			
			var data_keyword = '.json_encode($data['keyword']).';
			loadKeyword(data_keyword);

		</script>';

		$view = Briliant::admin_template('home/' . Kohana::$config->load('path.main_template') . '/template', $data);

		$this->response->body($view);
	}
	
	public function action_laporan() {
		
		$post = $this->request->post();
		
		$home_model = new Model_Home();

		$data['laporan'] = $home_model->graph_laporan($post['date']);
		
		$result = json_encode($data['laporan']);
		
		echo $result;
	}
	
	public function action_penulis() {
		
		$post = $this->request->post();
		
		$home_model = new Model_Home();

		$data['penulis'] = $home_model->graph_penulis($post['date']);
		
		$result = json_encode($data['penulis']);
		
		echo $result;
	}
	
	public function action_keyword() {
		
		$post = $this->request->post();
		
		$home_model = new Model_Home();

		$data['keyword'] = $home_model->graph_keyword($post['date']);
		
		$result = json_encode($data['keyword']);
		
		echo $result;
	}

}

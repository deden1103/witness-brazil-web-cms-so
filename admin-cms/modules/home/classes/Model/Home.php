<?php defined('SYSPATH') or die('No direct script access.');

class Model_Home extends Model {

	public function count_all() {
		$return = 0;

		$results = DB::query(Database::SELECT,"SELECT mcv.mscvName cat, count(1) total
FROM tempowitness.video v
left join tempowitness.master_category_video mcv on v.vdeoMscvId=mcv.mscvId
where mscvStatus=1
group by vdeoMscvId
order by mscvName")
											->execute();
		// $results = DB::select($column)->from($table)->execute();
		$return = $results->as_array();

		return $return;
	}
	
	public function graph_laporan($date = ''){
		
		$where = "";
		if(!empty($date)){
			$where = "AND artcPublishTime >= '" . $date . " 00:00:00' AND artcPublishTime <= '" . $date . " 23:59:59'";
		}
		
		$results = DB::query(Database::SELECT,"SELECT
						count(artcId) as con,
						artcMscvId,
						mscvName as name,
						count(case WHEN artcStatus = 2 ".$where." then 1 else null end) as approved,
						count(case WHEN artcStatus = 1 ".$where." then 1 else null end) as saved
						FROM master_category_video left JOIN article ON master_category_video.mscvId=article.artcMscvId
						where mscvStatus = 1
						GROUP BY mscvId
						Order by mscvName Asc")->execute();
		
		$return = $results->as_array();

		return $return;
		
	}
	
	public function graph_penulis($date = ''){
		
		$where = "";
		if(!empty($date)){
			$where = "AND artcPublishTime >= '" . $date . " 00:00:00' AND artcPublishTime <= '" . $date . " 23:59:59'";
		}
		
		$results = DB::query(Database::SELECT,"SELECT
						count(artcId) as con,
						artcUkomIdSaved,
						ukomName as name,
						count(case WHEN artcStatus = 2 ".$where." then 1 else null end) as approved,
						count(case WHEN artcStatus = 1 ".$where." then 1 else null end) as saved
						FROM user_komunitas left JOIN article ON user_komunitas.ukomId=article.artcUkomIdSaved
						where ukomStatus = 1
						GROUP BY ukomId
						Order by approved Desc limit 20")->execute();
		
		$return = $results->as_array();

		return $return;
		
	}
	
	public function graph_keyword($date = ''){
		
		$where = "";
		if(!empty($date)){
			$where = "AND artcPublishTime >= '" . $date . " 00:00:00' AND artcPublishTime <= '" . $date . " 23:59:59'";
		}
		
		$results = DB::query(Database::SELECT,"SELECT
						count(artcId) as con,
						master_keyword_video.vkeyName as name,
						count(case WHEN artcStatus = 2 ".$where." then 1 else null end) as approved,
						count(case WHEN artcStatus = 1 ".$where." then 1 else null end) as saved
						FROM master_keyword_video left JOIN article ON article.artcKeyword LIKE CONCAT('%\"',master_keyword_video.vkeyID,'\"%')
						GROUP BY master_keyword_video.vkeyID, master_keyword_video.vkeyName
						Order by con Desc
						limit 10")->execute();
		
		$return = $results->as_array();

		return $return;
		
	}

}

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo $data['main_title']; ?>
		</h1>
	</section>

	<section class="content">
		<div class="row">
			
		  <div class="col-md-12">
			  <div class="box box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Relatório de mapas</h3>

				  <div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				  </div>
				</div>
				<div class="box-body">
				  <div class="input-group date col-md-3">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="date" class="form-control pull-right" id="laporan" onchange="changeLaporan()" name="date">
					</div>
					<div class="chart">
					<canvas id="areaLaporan" style="height:250px"></canvas>
				  </div>
				</div>
			  </div>
		  </div>
		  
		  <div class="col-md-12">
			  <div class="box box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Escritor de mapas</h3>

				  <div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				  </div>
				</div>
				<div class="box-body">
					<div class="input-group date col-md-3">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="date" class="form-control pull-right" id="penulis" onchange="changePenulis()" name="date">
					</div>
					<div class="chart">
					<canvas id="areaPenulis" style="height:250px"></canvas>
				  </div>
				</div>
			  </div>
		  </div>
		  
		  <div class="col-md-12">
			  <div class="box box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Palavra-chave de mapas</h3>

				  <div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				  </div>
				</div>
				<div class="box-body">
					<div class="input-group date col-md-3">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="date" class="form-control pull-right" id="keyword" onchange="changeKeyword()" name="date">
					</div>
					<div class="chart">
					<canvas id="areaKeyword" style="height:250px"></canvas>
				  </div>
				</div>
			  </div>
		  </div>
		

		</div>
	</section>
</div>

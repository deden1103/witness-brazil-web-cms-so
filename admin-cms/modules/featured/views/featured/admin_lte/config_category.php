
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo __('Configurar o destaque'); ?>
		</h1>
		<ol class="breadcrumb">
			
			<li><a href="#"><?php echo __('Configurar'); ?></a></li>
			<li class="active"><a href="#"><?php echo __('Configurar o destaque'); ?></a></li>
		</ol>
	</section>
    <section class="content">
        <div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
                    <div class="box-header with-border">
						<h3 class="box-title">Adicionar dados de artigos em destaque</h3>
					</div>
					<div class="box-body">
                    <form action="<?php echo URL::Base(); ?>featured/add_featured" method="post">
                       <!--  <div class="form-group">
                            <label><?php echo __('Tópico quente') ?></label>
                            <input type="text" name="name" class="form-control" required>
                        </div> -->


                        <?php 
                                $sl_category = '';
                                if(!empty($data['detail']['category_id'])) {
                                    $sl_category = $data['detail']['category_id'];
                                }
                                echo Featured::select_list('name', 1, $sl_category); 
                            ?>


                    </div>
                    <div class="box-footer">
							<button type="submit" name="save" value="save" class="btn btn-primary"><?php echo __('Guardar'); ?></button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
						</div>
					</form>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
                        <?php $level = Controller_Backend::check_level(); 
                            if ($level == 1) { ?>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nome do artigo em destaque</th>
                                    <th colspan="2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                 $no = 1;
                                 foreach ($data['ctg_category'] as $key) {
                                    echo '
                                        <tr id="'.$key['id'].'">   
                                            <td>'.$key['name'].'</td>
                                           
                                            <td>
                                                <button onclick="del(\''.$key['id'].'\')" class="btn btn-xs btn-block btn-danger"> Delete </buttin>
                                            </td>
                                        </tr>
                                    ';
                                } ?>
                            </tbody>
                        </table>
                        <?php 
                        }else{ ?>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nome do artigo em destaque</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                 $no = 1;
                                 foreach ($data['ctg_category'] as $key) {
                                    echo '
                                        <tr id="row'.$key['id'].'">   
                                            <td>'.$key['name'].'</td>
                                        </tr>
                                    ';
                                } ?>
                            </tbody>
                        </table>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>  
 <td>
                                              <!--   <a href="'.URL::Base().'hot/editcategory/' . $key['id'] . '" class="btn btn-xs btn-block btn-warning"> Update </a>
                                            </td> -->
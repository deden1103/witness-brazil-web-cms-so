					<div class="box-body">
						<table class="table table-bordered">
							<!-- List isi artikel -->
							<tr>
								<th style="text-align:center;"><?php echo __('Video Detail'); ?></th>
								<th style="text-align:center;"><?php echo __('Carregador'); ?></th>
								<th style="text-align:center;width:18%;"><?php echo __('Status'); ?></th>
								<th colspan="3" style="width:20%;text-align:center;"><?php echo __('Action'); ?></th>
							</tr>
							<?php
							if(!empty($data['list'])) {
                                                                $c_bg_list = 1;
                                                                $bg_list = '';
								foreach($data['list'] as $v_list) {
                                                                        // Background color
                                                                        if($c_bg_list % 2 == 0) {
                                                                            $bg_list = '';
                                                                        } else {
                                                                            $bg_list = 'style="background-color: #ECF0F5;"';
                                                                        }
									// Status
									$video_status = $url_live = '';
									if($v_list['status'] == 1) {
										$video_status = '<span class="label label-info">Saved</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
									} else if($v_list['status'] == 0) {
										$video_status = '<span class="label label-danger">Deleted</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
									} else if($v_list['status'] == 2) {
										$url_live = Kohana::$config->load('path.arah')."/video/{$v_list['id']}/".URL::title($v_list['title']).'.html';
										
										$video_status = '<span class="label label-success">Published</span> <b>' . date('d M Y H:i', strtotime($v_list['publish'])) . '</b>';
                                                                                $video_status .= '<br><br><b><i>Approved By :<br>' . Video::get_user_approver($v_list['id']) . '<i></b>';
                                                                        }
									
									// Publish schedule
									if($v_list['publish'] > $v_list['saved']) {
										$video_status .= '<br/></br><span class="label bg-maroon-active color-palette">Schedule</span> <b>' . date('d M Y H:i', strtotime($v_list['publish'])) . '</b>';
									}
									
									// Default Button
									$button = '
										<td>
											<a href="/video/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a>											
										</td>
										<td>
											<a href="/video/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a>
										</td>
										<td>
											<a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a>
										</td>
									';
									
									// Button If Status 0
									if($v_list['status'] == 0) {
										$button = '
											<td>
												<a href="/video/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a>
										</td>
										<td>
												<a href="javascript:void()"><button disabled class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a>
										</td>
										<td>
												<a href="javascript:void()"><button disabled class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a>
											</td>
										';
									}
									
									// BUtton jika yang membuka list != yang mengupload berita
									if($data['session_user_id'] != $v_list['user_id']) {
										$button = '
											<td>
												<a href="/video/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a>
										</td>
										<td>
												<a href="javascript:void()"><button disabled class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a>
										</td>
										<td>
												<a href="javascript:void()"><button disabled class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a>
											</td>
										';
									}
									
									$article_title = $v_list['title'];
									if(!empty($data['search'])) {
										$ex_search = explode(',',$data['search']);
										foreach($ex_search as $v_search) {
											$article_title = str_ireplace(trim($v_search), '<span style="color:red"><b><i>' . trim($v_search). '</i></b></span>', $article_title);
										}
									}
											if(!empty($url_live)){
												$article_title = "<a href='{$url_live}' target='_BLANK'>{$article_title}</a>";
											}
									
									echo '
										<tr ' . $bg_list . '>
											<td>
												<strong>' . $article_title . '</strong>
											</td>
											<td>' . $v_list['user_name'] . '</td>
											<td>' . $video_status . '</td>
											' . $button . '
										</tr>
									';
                                                                        $c_bg_list++;
								}
							}
							?>
						</table>
					</div>
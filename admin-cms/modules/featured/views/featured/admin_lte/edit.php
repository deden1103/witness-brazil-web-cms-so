<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Sala de imprensa'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Sala de imprensa'); ?></li>
			<li class="active"><a href="/laporan"><?php echo __('Edit'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Editar dados de relatório'); ?></h3>
					</div>
					<form role="form" method="post" action="<?php echo URL::Base(); ?>laporan/update">
						<div class="box-body">
							<?php
							if($data['detail']['status'] == 2) {
								?>
								<div class="alert alert-success alert-dismissable">
									<h4><i class="icon fa fa-check"></i> Alert!</h4>
									This video article was published
								</div>
								<?php
							}
							?>
							<?php
							if(!empty($data['errors'])) {
								?>
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<h4><i class="icon fa fa-ban"></i> <?php echo __('Alert!'); ?></h4>
									<?php
									foreach($data['errors'] as $v_errors) {
										echo ucfirst($v_errors) . '</br>';
									}
									?>
								</div>
								<?php
							}
							
							if(empty($data['detail']['is_edit'])) { // If detail from database
                                                            // Change format date for form detail
									if(Briliant::date_is_yyyy_mm_dd_hh_mm_ss($data['detail']['publish_time']) === true) {
										$data['detail']['publish_time'] = date('d/m/Y H:i:s', strtotime($data['detail']['publish_time']));
									}
							}
							
							// Style Background
							$style_bg = '';
							if($data['detail']['status'] == 2) {
								$style_bg = 'style="background-color: rgba(4, 255, 4, 0.29);" disabled';
							}
							
							// Status Publish
							$text_status_publish = '';
							$hidden_text_publish = '';
							if($data['detail']['status'] == 2) {
								//$text_status_publish = '<span><i>(' . __('Was Published') . ')</i></span>';
								//$hidden_text_publish = '<input type="hidden" name="publish_time" value="' . $data['detail']['publish_time'] . '" class="form-control">';
							}
							
							
							?>
							
							<!-- Hidden Text -->
							<input type="hidden" name="id" value="<?php echo !empty($data['detail']['id']) ? $data['detail']['id'] : ''; ?>" />
							<input type="hidden" name="status" value="<?php echo !empty($data['detail']['status']) ? $data['detail']['status'] : ''; ?>" />
							<input type="hidden" name="is_edit" value="1" />
							<div class="form-group">
								<label><?php echo __('Horário') ?></label>
								<div class="input-group">
									<input <?php echo $style_bg; ?> type="text" name="publish_time" value="<?php echo !empty($data['detail']['publish_time']) ? $data['detail']['publish_time'] : ''; ?>"  class="form-control publish_time">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label><?php echo __('Título') ?></label>
								<input type="text" name="title" value="<?php echo !empty($data['detail']['title']) ? $data['detail']['title'] : ''; ?>" maxlength="60" placeholder="<?php echo __('Only 60 Characters'); ?>" class="form-control">
							</div>
							
							<div class="form-group">
								<label><?php echo __('Descrição') ?></label>
								<textarea class="form-control" rows="3" name="description" maxlength="140" placeholder="<?php echo __('Only 140 Characters'); ?>"><?php echo !empty($data['detail']['description']) ? $data['detail']['description'] : ''; ?></textarea>
							</div>
							<?php 
								$sl_category = '';
								if(!empty($data['detail']['category_id'])) {
									$sl_category = $data['detail']['category_id'];
								}
								echo Laporan::select_list('category', 1, $sl_category); 
							?>
							<div class="form-group">
								<label><?php echo __('Palavra-chave') ?></label>
								<select style="width: 100%" class="form-control select2" name="keyword[]" id="" multiple>
								<?php
                                        foreach ($data['list_keyword'] as $key) {
											$selected = "";
											if(!empty($data['detail']['keyword_id'])){
												$lvl = unserialize($data['detail']['keyword_id']);
												foreach($lvl as $kk){
		
													if($key['id'] == $kk){
														$selected = "selected";
													}
												}
											}
                                            echo '
                                                <option '.$selected.' value="'.$key['id'].'">'.$key['name'].'</option>
                                            ';
                                        }
                                    ?>
								</select>
							</div>
							
							<div class="form-group">
								<label><?php echo __('Detalhes') ?></label>
								<textarea id="wysiwyg" class="form-control f_tinymce" rows="15" name="detail" minlength="3" required><?php echo !empty($data['detail']['detail']) ? $data['detail']['detail'] : ' '; ?></textarea>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Atualizar os dados'); ?></button>
							<a href="<?= URL::base() ?>laporan/search"><button type="button" class="btn btn-danger"><?php echo __('Cancelar'); ?></button></a>
						</div>
					</form>
				</div>
			</div>

			<!----chat---->
			<div class="col-md-12">
				<!-- DIRECT CHAT PRIMARY -->
				<div class="box box-primary direct-chat direct-chat-primary" >
					<div class="box-header with-border">
					<h3 class="box-title">Conversa direta</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body" >
					<!-- Conversations are loaded here -->
					<div class="direct-chat-messages socket-list-container">
						
					</div>
					<!--/.direct-chat-messages-->

					<!-- Contacts are loaded here -->
					
					<!-- /.direct-chat-pane -->
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
					
						<div class="input-group">
						<input type="text" id="m" placeholder="Digitar a mensagem ..." class="form-control">
							<span class="input-group-btn">
								<button  id= "pesan" class="btn btn-primary btn-flat">Send</button>
							</span>
						</div>
					
					</div>
					<!-- /.box-footer-->
				</div>
				<!--/.direct-chat -->
        	</div>
			
		</div>
		
	</section>
</div>

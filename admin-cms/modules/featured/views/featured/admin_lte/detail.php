<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo __('Newsroom Detail'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Sala de imprensa'); ?></li>
			<li class="active"><a href="/video"><?php echo __('Detail'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
					<div class="table-responsive">
						<table class="table">
							<?php
								//Get keyword
								if(!empty($data['detail']['keyword_id'])){
									$keyword = null;
									$keyword_id = unserialize($data['detail']['keyword_id']);
									foreach($data['list_keyword'] as $kk){
										$idK = $kk['id'];
										$nameK = $kk['name'];
										foreach($keyword_id as $k){
											if ($k == $idK) {
												$keyword .= $nameK.', ';
											}
										}
									}
								}else{
									$keyword = "-";
								}
							?>
							<tr>
								<td width="15%"><b><?php echo __('Status'); ?></b></td>
								<td width="5px">:</td>
								<td>
									<?php 
										if($data['detail']['status'] == 1) { // Saved
											echo '<span class="text-light-blue" style=" font-weight: bolder; ">** SAVED **</span>';
										} else if($data['detail']['status'] == 2) {
											echo '<span class="text-green" style=" font-weight: bolder; ">** PUBLISHED **</span>';
										} else {
											echo '<span class="text-red" style=" font-weight: bolder; ">** DELETED **</span>';
										}
									?>
								</td>
							</tr>
							<tr>
								<td width="15%"><b><?php echo __('Carregador'); ?></b</td>
								<td width="5px">:</td>
								<td><?php echo $data['detail']['ukom_name']; ?></td>
							</tr>
							<tr>
								<td width="15%"><b><?php echo __('Título'); ?></b</td>
								<td width="5px">:</td>
								<td><?php echo $data['detail']['title']; ?></td>
							</tr>
							<tr>
								<td width="15%"><b><?php echo __('Descrição'); ?></b</td>
								<td width="5px">:</td>
								<td><?php echo $data['detail']['description']; ?></td>
							</tr>
							<tr>
								<td width="15%"><b><?php echo __('Category'); ?></b</td>
								<td width="5px">:</td>
								<td><?php echo $data['detail']['category_name']; ?></td>
							</tr>
							<tr>
								<td width="15%"><b><?php echo __('Palavra-chave'); ?></b</td>
								<td width="5px">:</td>
								<td><?php echo ucwords(substr($keyword,0,-2)) ?></td>
											</tr>
								<?php
								// Image article
								$img_video = '' . __('[ No Media ]') . '';
								$url =Kohana::$config->load('path.api');
								if($data['detail']['type']==('mp4')) {
									$split_id = str_split($data['detail']['id']);
									$path_folder_image = implode('/', $split_id);
									
									$img_video = '<video width="400"  height="270" controls="controls" src="' . $url . $path_folder_image . '/' . $data['detail']['id'] .'.'. $data['detail']['type'] . '" /><source type="video/mp4" /></video>';
								
								}else if(in_array($data['detail']['type'], array("png", "jpg","jpeg"))) {
									$split_id = str_split($data['detail']['id']);
									$path_folder_image = implode('/', $split_id);
									
									$img_video = '<img width="300px" height="200px" src="' . $url . $path_folder_image . '/' . $data['detail']['id'] .'.'. $data['detail']['type'] . '" />';
								}else if(in_array($data['detail']['type'], array("aac", "mp3"))) {
									$split_id = str_split($data['detail']['id']);
									$path_folder_image = implode('/', $split_id);
								
									$img_video = '<audio  controls="controls" src="' . $url . $path_folder_image . '/' . $data['detail']['id'] .'.'. $data['detail']['type'] . '" /></audio>';
								
								}
								?>
							<tr>
								<td width="15%"><b><?php echo __('Media Preview'); ?></b></td>
								<td width="5px">:</td>
								<td>
								<?php
									echo $img_video;
									/*if(strpos($data['detail']['embed_url'], 'youtube') !== false) { // If youtube video
										$parse_url = parse_url($data['detail']['embed_url']);
										if(!empty($parse_url['query'])) { 
											parse_str($parse_url['query'], $query);
											if(!empty($query['v'])) {
												echo '<iframe width="50%" height="300px" src="https://www.youtube.com/embed/' . $query['v'] . '" frameborder="0" allowfullscreen></iframe>';
											}
										}
									} else if(strpos($data['detail']['embed_url'], 'dailymotion') !== false) { // If dailymotion video
										$ex_url_dm = explode('/video/', $data['detail']['embed_url']);
										if(!empty($ex_url_dm[1])) {
											$ex_us = explode('_', $ex_url_dm[1]);
											if(!empty($ex_us[0])) {
												echo '<iframe frameborder="0" width="50%" height="300px" src="//www.dailymotion.com/embed/video/' . $ex_us[0] . '" allowfullscreen></iframe>';
											}
										}
									}else if(strpos($data['detail']['embed_url'], 'brightcove') !== false){
										$ex_url_dm = $data['detail']['embed_url'];
											echo '<div style="position: relative; display: block; max-width: 100%;"><div style="padding-top: 56.25%;"><iframe src="'.$ex_url_dm.'" 
											allowfullscreen 
											webkitallowfullscreen 
											mozallowfullscreen 
											allow="encrypted-media" 
											style="position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; width: 100%; height: 100%;"></iframe></div></div>';
									}else {
										echo $data['detail']['embed_url'];
									}*/
								?>
								</td>
							</tr>
							<tr>
								<td width="15%"><b><?php echo __('Detalhes'); ?></b</td>
								<td width="5px">:</td>
								<td><?php echo $data['detail']['detail']; ?></td>
							</tr>
						</table>
						</div>
					</div>
					<div class="box-footer">
						<a href="javascript:history.go(-1);"><button type="button" class="btn btn-info"><?php echo __('Back'); ?></button></a>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

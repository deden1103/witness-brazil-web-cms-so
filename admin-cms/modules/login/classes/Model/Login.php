<?php defined('SYSPATH') or die('No direct script access.');

class Model_Login extends Model {
	
	public function do_login($email = '', $password = '') {
		
		$return = false;
		$exec =	DB::select(DB::expr('COUNT(1) as COUNT'))
				->from('admin')
				->where('admin.admiEmail', '=', $email)
				->where('admin.admiPassword', '=', $password)
				->where('admin.admiStatus', '=', 1)
				->execute()
				->as_array();
		if(!empty($exec[0]['COUNT'])) {
			$return = true;
		}
		return $return;
	}
	
	public function get_admin_detail($email = '', $password ='') {
		
		$return = '';
		$exec =	DB::select('admiId, admiEmail, admiRealName')
				->from('admin')
				->where('admin.admiEmail', '=', $email)
				->where('admin.admiPassword', '=', $password)
				->where('admin.admiStatus', '=', 1)
				->execute()
				->as_array();
		if(!empty($exec[0])) {
			$return = $exec[0];
		}
		return $return;
		
	}




	public function do_login_komunitas($email = '', $password = '') {
		
		$return = false;
		$exec =	DB::select(DB::expr('COUNT(1) as COUNT'))
				->from('user_komunitas')
				->where('user_komunitas.ukomEmail', '=', $email)
				->where('user_komunitas.ukomPassword', '=', $password)
				->where('user_komunitas.ukomStatus', '=', 1)
				->where('user_komunitas.isEditor', '=', 1)
				->execute()
				->as_array();
		if(!empty($exec[0]['COUNT'])) {
			$return = true;
		}
		return $return;
	}
	
	public function get_admin_detail_komunitas($email = '', $password ='') {
		
		$return = '';
		$exec =	DB::select('ukomId, ukomEmail, ukomName')
				->from('user_komunitas')
				->where('user_komunitas.ukomEmail', '=', $email)
				->where('user_komunitas.ukomPassword', '=', $password)
				->where('user_komunitas.ukomStatus', '=', 1)
				->where('user_komunitas.isEditor', '=', 1)
				->execute()
				->as_array();
		if(!empty($exec[0])) {
			$return = $exec[0];
		}
		return $return;
		
	}
	
}
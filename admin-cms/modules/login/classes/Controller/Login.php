<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Login extends Controller {

	public function action_index() {
		//echo phpinfo();
		$this->response->body(View::factory('login/login')->render());

	}

	public function action_auth() {
		$email = $this->request->post('email');
		$password = SHA1($this->request->post('password'));
		$login_model = new Model_Login();
		//print_r(phpinfo());die;
		$data_string = array('secret'=>'6Lev5LkUAAAAADAfqZz3MnLyunt-DBZq-ZUVo5FD', 'response' => $_POST['g-recaptcha-response']);
		// Android
		$ch_an = curl_init('https://www.google.com/recaptcha/api/siteverify');
		curl_setopt($ch_an, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch_an, CURLOPT_POST, count($data_string));
		curl_setopt($ch_an, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch_an, CURLOPT_SSL_VERIFYPEER, false); //Because you're attempting to connect via SSL
		curl_setopt($ch_an, CURLOPT_RETURNTRANSFER, true);
		$result_an = curl_exec($ch_an);
		$rst = json_decode($result_an, true);

		if($rst['success']===false){
		  $this->redirect('/login');
		}elseif($login_model->do_login($email, $password) === true) { // Login Sukses

			$admin_detail = $login_model->get_admin_detail($email, $password);
			ini_set("display_errors", true);
			$session = Session::instance();
			// Set session was login
			$session->set('adminLogin', 1);
			$session->set('adminId', $admin_detail['admiId']);
			$session->set('adminName', $admin_detail['admiRealName']);
			$session->set('adminEmail', $admin_detail['admiEmail']);

			$this->redirect('/home');
		}
		// elseif($login_model->do_login_komunitas($email, $password) === true){

		// 	$admin_detail = $login_model->get_admin_detail_komunitas($email, $password);
		// 	ini_set("display_errors", true);
		// 	$session = Session::instance();
		// 	$session->set('adminLogin', 1);
		// 	$session->set('adminId', $admin_detail['ukomId']);
		// 	$session->set('adminName', $admin_detail['ukomName']);
		// 	$session->set('adminEmail', $admin_detail['ukomEmail']);
		// 	$this->redirect('/home');
			
		// } 
		 else { // Login gagal
			$this->redirect('/login');
		}

	}

	public function action_logout() {
		$session = Session::instance();
		$session->delete('adminLogin');
		$session->delete('adminId');
		$session->delete('adminName');
		Session::instance()->destroy();
		$this->redirect('/login');
	}
}

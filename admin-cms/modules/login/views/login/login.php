<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo __('Log in'); ?></title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="<?php echo URL::Base(); ?>assets/bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo URL::Base(); ?>assets/dist/css/AdminLTE.min.css">
		<!-- iCheck -->
		<link rel="stylesheet" href="<?php echo URL::Base(); ?>assets/plugins/iCheck/square/blue.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="shortcut icon" href="<?php echo URL::Base(); ?>assets/favicon.ico" type="image/x-icon" />
	</head>
	<?php
	$arr=array('http://brightcove04.o.brightcove.com/4077388032001/4077388032001_5377192274001_5351516542001-vs.jpg?pubId=4077388032001&videoId=5351516542001',
'http://brightcove04.o.brightcove.com/4077388032001/4077388032001_5555601742001_5555600591001-vs.jpg?pubId=4077388032001&videoId=5555600591001',
'http://brightcove04.o.brightcove.com/4077388032001/4077388032001_5841292594001_5840223351001-vs.jpg?pubId=4077388032001&videoId=5840223351001',
'http://brightcove04.o.brightcove.com/4077388032001/4077388032001_5596280523001_5596277132001-vs.jpg?pubId=4077388032001&videoId=5596277132001',
'http://brightcove04.o.brightcove.com/4077388032001/4077388032001_5479677354001_5479664272001-vs.jpg?pubId=4077388032001&videoId=5479664272001',
'http://brightcove04.o.brightcove.com/4077388032001/4077388032001_5478678238001_5478669580001-vs.jpg?pubId=4077388032001&videoId=5478669580001');
$random_keys=array_rand($arr,1);
	?>
	<body class="hold-transition login-page" style="background-image: url(<?php echo $arr[$random_keys]; ?>);
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;">
		<div class="login-box">
			<div class="login-logo" style="    background-color: white;
    padding-top: 10px;
    margin-bottom: 0px;">
				<img src="<?php echo URL::Base(); ?>assets/images/witness-logo.png" style="width: 200px;" />
			</div>
			<!-- /.login-logo -->
			<div class="login-box-body">
				<p class="login-box-msg"><?php echo __('Iniciar sessão'); ?></p>
				<form action="<?php echo URL::Base(); ?>login/auth" method="post">
					<div class="form-group has-feedback">
						<input type="text" class="form-control" name="email" placeholder="<?php echo __('Email'); ?>">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" class="form-control" name="password" placeholder="<?php echo __('Password'); ?>">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<center>
						<div style='padding-bottom: 10px; ' class="g-recaptcha" data-sitekey="6Lev5LkUAAAAALQjqVK0b1zLT5Lk8tGk-PbtYamc"></div>
					</center>
					<div class="row">
						<div class="col-xs-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo __('Sign In'); ?></button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<script src='https://www.google.com/recaptcha/api.js'></script>
		<!-- jQuery 2.1.4 -->
		<script src="<?php echo URL::Base(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="<?php echo URL::Base(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>

<?php defined('SYSPATH') or die('No direct script access.');

class Model_Witness extends Model {
	public function count_kanal() {
		$query = DB::select(array('COUNT("artcMsctId")', 'total'), 'msctName')->from('article')->join('master_category', 'LEFT')->on('master_category.msctId', '=', 'article.artcMsctId')->group_by('msctName')->execute()->as_array();
		return $query;
	}

	public function count_all($date1 = '', $date2 = '') {

		$return = 0;

		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('article')
					->where(DB::expr('DATE(artcPublishTime)'), '>=', $date1)
					->where(DB::expr('DATE(artcPublishTime)'), '<=', $date2)
					->where('artcParentId', '=', NULL)
					->order_by('artcId', 'DESC')
					->execute()
					->as_array();

		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}

		return $return;

	}

        public function count_search($date1 = '', $date2 = '', $search = '') {
		$session = Session::instance();
		$return = 0;

		$publish = $session->get("publish_news", "artcPublishTime");

		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('article')
					->where(DB::expr('DATE('.$publish.')'), '>=', $date1)
					->where(DB::expr('DATE('.$publish.')'), '<=', $date2)
					->where('artcStatus', '!=', '0')
                                        ->where('artcParentId', '=', NULL);

                $ex_search = explode(',', $search);
                foreach($ex_search as $v_search) {
                    $query = $query->where('artcTitle', 'LIKE', '%' . trim($v_search) . '%');
                }

				$category = $session->get("category_news", "");
				if(!empty($category)){
					$query = $query->where('artcMsctId','=', $category);
				}

				$uploader = $session->get("uploader_news", "");
				if(!empty($uploader)){
					$query = $query->where('artcUserIdSaved','=', $uploader);
				}

                $query = $query->order_by('artcId', 'DESC')
					->execute()
					->as_array();


		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}

		return $return;

	}

	public function list_data($date1 = '', $date2 = '', $limit = '', $offset = '') {
		$publish = $session->get("publish_news", "artcPublishTime");
		$exec = DB::select(
					array('artcId', 'id'),
					array('artcTitle', 'title'),
					array('artcExcerpt', 'description'),
					array('artcKeyword', 'keyword'),
					array('artcDetail', 'detail'),
					array('artcThirdparty', 'thirdparty'),
					array('artcViewer', 'viewer'),
					array('artcPublishTime', 'publish'),
					array('artcSaved', 'saved'),
					array('artcStatus', 'status'),
					array('artcMsctId', 'category_id'),
					array('msctName', 'category_name'),
					array('artcUserIdSaved', 'user_id'),
					array('userRealName', 'user_name')
				)
				->from('article')
				->join('user', 'LEFT')
				->on('article.artcUserIdSaved', '=', 'user.userId')
				->join('master_category', 'LEFT')
				->on('article.artcMsctId', '=', 'master_category.msctId')
				->where(DB::expr('DATE('.$publish.')'), '>=', $date1)
				->where(DB::expr('DATE('.$publish.')'), '<=', $date2)
                                ->where('artcParentId', '=', NULL)
				->order_by('artcId', 'DESC')
				->limit($limit)
				->offset($offset)
				->execute()
				->as_array();

		if(!empty($exec)) {

			foreach($exec as $k_exec => $v_exec) {

				if(!empty($v_exec['id'])) {

					// Get tags
					$exec[$k_exec]['tags'] = 	DB::select(
													array('mstgId', 'tags_id'),
													array('mstgName', 'tags_name')
												)
												->from('article_tags')
												->join('master_tags', 'LEFT')
												->on('master_tags.mstgId', '=', 'article_tags.artgMstgId')
												->where('artgArtcId', '=', $v_exec['id'])
												->execute()
												->as_array();

					// Get images
					$exec[$k_exec]['images'] = 	DB::select(
													array('artiArimId', 'image_id'),
													array('arimFileType', 'image_type')
												)
												->from('article_image')
												->join('arsip_images', 'LEFT')
												->on('arsip_images.arimId', '=', 'article_image.artiArimId')
												->where('artiArtcId', '=', $v_exec['id'])
												->execute()
												->as_array();

                                        // Get child
                                        $exec[$k_exec]['child'] =       DB::select(
                                                                                                        array('artcId', 'child_article_id'),
                                                                                                        array('artcTitle', 'child_article_title')
                                                                                                )
                                                                                                ->from('article')
                                                                                                ->where('artcParentId', '=', $v_exec['id'])
                                                                                                ->where('artcStatus', '!=', '0')
                                                                                                ->order_by('artcId', 'ASC')
                                                                                                ->execute()
                                                                                                ->as_array();

				}

			}

		}

		return $exec;

	}

        public function list_search($date1 = '', $date2 = '', $search = '', $limit = '', $offset = '') {
		$session = Session::instance();
		$publish = $session->get("publish_news", "artcPublishTime");
		$exec = DB::select(
					array('artcId', 'id'),
					array('artcTitle', 'title'),
					array('artcExcerpt', 'description'),
					array('artcWording', 'wording'),
					array('artcKeyword', 'keyword'),
					array('artcDetail', 'detail'),
					array('artcViewer', 'viewer'),
					array('artcPublishTime', 'publish'),
					array('artcSaved', 'saved'),
					array('artcStatus', 'status'),
					array('artcMsctId', 'category_id'),
					array('artcThirdparty', 'thirdparty'),
					array('msctName', 'category_name'),
					array('artcUserIdSaved', 'user_id'),
					array('userRealName', 'user_name')
				)
				->from('article')
				->join('user', 'LEFT')
				->on('article.artcUserIdSaved', '=', 'user.userId')
				->join('master_category', 'LEFT')
				->on('article.artcMsctId', '=', 'master_category.msctId')
				->where(DB::expr('DATE('.$publish.')'), '>=', $date1)
				->where(DB::expr('DATE('.$publish.')'), '<=', $date2)
				->where('artcStatus', '!=', '0')
                                ->where('artcParentId', '=', NULL);

                $ex_search = explode(',', $search);
				if(!empty($ex_search)){
					foreach($ex_search as $v_search) {
						$exec = $exec->where('artcTitle', 'LIKE', '%' . trim($v_search) . '%');
					}
				}

				$category = $session->get("category_news", "");
				if(!empty($category)){
					$exec = $exec->where('artcMsctId','=', $category);
				}

				$uploader = $session->get("uploader_news", "");
				if(!empty($uploader)){
					$exec = $exec->where('artcUserIdSaved','=', $uploader);
				}
//echo Debug::vars((string) $exec);
                $exec = $exec->order_by('artcId', 'DESC')
				->limit($limit)
				->offset($offset)
				->execute()
                                ->as_array();

		if(!empty($exec)) {

			foreach($exec as $k_exec => $v_exec) {

				if(!empty($v_exec['id'])) {

					// Get tags
					$exec[$k_exec]['tags'] = 	DB::select(
													array('mstgId', 'tags_id'),
													array('mstgName', 'tags_name')
												)
												->from('article_tags')
												->join('master_tags', 'LEFT')
												->on('master_tags.mstgId', '=', 'article_tags.artgMstgId')
												->where('artgArtcId', '=', $v_exec['id'])
												->execute()
												->as_array();

					// Get images
					$exec[$k_exec]['images'] = 	DB::select(
													array('artiArimId', 'image_id'),
													array('arimFileType', 'image_type')
												)
												->from('article_image')
												->join('arsip_images', 'LEFT')
												->on('arsip_images.arimId', '=', 'article_image.artiArimId')
												->where('artiArtcId', '=', $v_exec['id'])
												->execute()
												->as_array();

                                        // Get child
                                        $exec[$k_exec]['child'] =       DB::select(
                                                                                                        array('artcId', 'child_article_id'),
                                                                                                        array('artcTitle', 'child_article_title')
                                                                                                )
                                                                                                ->from('article')
                                                                                                ->where('artcParentId', '=', $v_exec['id'])
                                                                                                ->where('artcStatus', '!=', '0')
                                                                                                ->order_by('artcId', 'ASC')
                                                                                                ->execute()
                                                                                                ->as_array();

				}

			}

		}

		return $exec;

	}

	public function save_data($data = '', $user_id = '') {
		// Start Transaction
		Database::instance()->begin();

		$publish_time = $saved_time = date('Y-m-d H:i:s'); // Default data now
		// if(!empty($data['sembunyi'])){
// 			$sembunyi = DB::query(Database::SELECT, "SELECT artcPublishTime  FROM `article` WHERE artcMsctId='{$data['category']}' order by `artcPublishTime` desc limit 1 OFFSET 20")->execute()->as_array();
// 			$publish_time = $sembunyi[0]["artcPublishTime"];
// 		}

		if(!empty($data['publish_time'])) {
			$publish_time = DateTime::createFromFormat('d/m/Y H:i:s', $data['publish_time']);
			$publish_time = $publish_time->format('Y-m-d H:i:s');
		} else {
			$saved_time = $publish_time;
		}

		if(empty($data['partner'])){
			$data['partner'] = 0;
		}
		if(empty($data['sembunyi'])){
			$data['sembunyi'] = 0;
		}
		if(empty($data['news'])){
			$data['news'] = 0;
		}
		if($data['category'] == 17){
			$data['news'] = 1;
		}

		$bacajuga = array_combine($data["title_bj"], $data["link_bj"]);

		// Save to table article
		$article =	DB::insert('article', array(
						//'artcStatus',
						'artcTitle',
						'artcExcerpt',
						'artcWording',
						'artcKeyword',
						'artcMsctId', // Categpry or Kanal
						'artcDetail',
						'artcThirdparty',
						'artcPartner',
						'artcPooling',
						'artcAnswers',
						'artcBacaJuga',
						'artcPublishTime',
						'artcUserIdSaved',
						'artcSaved',
						'artcHide',
						'artcNewsStatus'
					))
					->values(array(
						//2,
						$data['title'],
						$data['description'],
						$data['wording'],
						$data['keyword'],
						$data['category'],
						$data['detail'],
						$data['thirdparty'],
						$data['partner'],
						$data['pooling'],
						json_encode($data['answers']),
						json_encode($bacajuga),
						$publish_time,
						$user_id,
						$saved_time,
						$data['sembunyi'],
						$data['news']
					));
		list($lastid_article, $rows_inserted) = $article->execute();

		if(!empty($lastid_article)) {

			// Save article tags
			if(!empty($data['tags'])) {

				if(!empty($data['partner'])){
					$tags = DB::insert('article_tags', array(
								'artgArtcId',
								'artgMstgId'
							))
							->values(array(
								$lastid_article,
								'26166' //partner
							))
							->execute();
				}

				foreach($data['tags'] as $v_tags) {
					$tags = DB::insert('article_tags', array(
								'artgArtcId',
								'artgMstgId'
							))
							->values(array(
								$lastid_article,
								$v_tags
							))
							->execute();
				}
				/*
				$tags = DB::insert('article_tags', array(
								'artgArtcId',
								'artgMstgId'
							))
							->values(array(
								$lastid_article,
								$data['location']
							))
							->execute();
				*/
			}

			// Save article images
			if(!empty($data['image'])) {

				// Get id image from full url image
				$base_image = basename($data['image']).PHP_EOL;
				if(!empty($base_image)) {
					$ex_base = explode('.', $base_image);
					list($id_image, $file_type) = $ex_base;
				}

				if(!empty($id_image)) {
					$image = 	DB::insert('article_image', array(
									'artiArtcId',
									'artiArimId'
								))
								->values(array(
									$lastid_article,
									$id_image
								))
								->execute();
				}
			}
			//Engine_News::send_to_web($id_article);
			// Kohana_Engine_News::send_to_fb("http://www.example.com");

			// add crontib_pickupby table, pickup by
			if(!empty($data['pickup_id'])) {
				DB::insert('contrib_article_pickupby', array(
								'artcId',
								'userId'
							))
							->values(array(
								$data['pickup_id'],
								$user_id
							))
							->execute();
			}
		}

		// Commit transaction
		Database::instance()->commit();

	}

	public function detail_data($id = '') {

		$return = '';

		$exec = DB::select(
					array('id', 'id'),
					array('detail', 'detail')
				)
				->from('about')
				->where('id', '=', $id)
				->execute()
				->as_array();

		if(!empty($exec[0])) {

			$return = $exec[0];

		}

		return $return;

	}


	public function detail_data_pickup($id = '') {

		$return = '';

		$exec = DB::select(
					array('artcId', 'id'),
					array('artcTitle', 'title'),
					array('artcExcerpt', 'description'),
					array('artcWording', 'wording'),
					array('artcKeyword', 'keyword'),
					array('artcDetail', 'detail'),
					array('artcPooling', 'pooling'),
					array('artcAnswers', 'answers'),
					array('artcViewer', 'viewer'),
					array('artcParentId', 'parent_id'),
					array('artcPublishTime', 'publish_time'),
					array('artcSaved', 'saved'),
					array('artcStatus', 'status'),
					array('artcMsctId', 'category_id'),
					array('artcThirdparty', 'thirdparty'),
					array('artcPartner', 'partner'),
					array('artcUserIdApproved', 'user_id_approved'),
					array('msctName', 'category_name'),
					array('artcUserIdSaved', 'user_id'),
					array('userRealName', 'user_name')
				)
				->from('contrib_article')
				->join('user', 'LEFT')
				->on('contrib_article.artcUserIdSaved', '=', 'user.userId')
				->join('master_category', 'LEFT')
				->on('contrib_article.artcMsctId', '=', 'master_category.msctId')
				->where('artcId', '=', $id)
				->execute()
				->as_array();

		if(!empty($exec)) {

			foreach($exec as $k_exec => $v_exec) {

				if(!empty($v_exec['id'])) {

					// Get tags
					$exec[$k_exec]['tags'] = 	DB::select(
													array('mstgId', 'tags_id'),
													array('mstgName', 'tags_name')
												)
												->from('contrib_article_tags')
												->join('master_tags', 'LEFT')
												->on('master_tags.mstgId', '=', 'contrib_article_tags.artgMstgId')
												->where('artgArtcId', '=', $v_exec['id'])
												->execute()
												->as_array();

					// Get images
					$exec[$k_exec]['images'] = 	DB::select(
													array('artiArimId', 'image_id'),
													array('arimFileType', 'image_type')
												)
												->from('contrib_article_image')
												->join('arsip_images', 'LEFT')
												->on('arsip_images.arimId', '=', 'contrib_article_image.artiArimId')
												->where('artiArtcId', '=', $v_exec['id'])
												->execute()
												->as_array();
                                        // Get child
                                        $exec[$k_exec]['child'] =       DB::select(
                                                                                                        array('artcId', 'child_article_id'),
                                                                                                        array('artcTitle', 'child_article_title')
                                                                                                )
                                                                                                ->from('contrib_article')
                                                                                                ->where('artcParentId', '=', $v_exec['id'])
                                                                                                ->where('artcStatus', '!=', '0')
                                                                                                ->order_by('artcId', 'ASC')
                                                                                                ->execute()
                                                                                                ->as_array();

				}

			}

		}

		if(!empty($exec[0])) {

			$return = $exec[0];

		}

		return $return;

	}


	public function update_data($data = '', $user_id = '', $action='') {

		// Id article
		$id_article = $data['id'];

		// Start Transaction
		Database::instance()->begin();

		// Update date article
		$update = DB::update('about')
			->set(array('detail' => $data['detail']))
			->set(array('user_saved' => $user_id))
			->where('id', '=', $id_article)->execute();

		// Commit transaction
		Database::instance()->commit();

	}

	public function update_data_sosmed($data = '', $user_id = '', $action='') {

		// Id article
		$id_article = $data['id'];

		// Start Transaction
		Database::instance()->begin();

		// Update date article
		$update = DB::update('article')->set(array('artcDetail' => $data['detail']));
		$update = $update->where('artcId', '=', $id_article)->execute();

		if($action=='approve' && !empty($update)){
			Engine_News::send_to_web($id_article);
		}

		// Commit transaction
		Database::instance()->commit();

	}

	public function delete_data($id = '') {
		$exec = DB::select(array('artcStatus', 'status'))
				->from('article')
				->where('artcId', '=', $id)
				->execute()
				->as_array();
		if(!empty($exec)){
			if($exec[0]['status']==2){
				Engine_News::send_to_delete($id);
			}
		}

		DB::update('article')
			->set(array('artcStatus' => 0))
			->where('artcId', '=', $id)
			->execute();

	}

        public function get_title($id = '') {

            $return = '';

            $exec = DB::select(
                            array('artcTitle', 'title')
                    )
                    ->from('article')
                    ->where('artcId', '=', $id)
                    ->execute()
                    ->as_array();

            if(!empty($exec[0]['title'])) {
                    $return = $exec[0]['title'];
            }

            return $return;

        }

        public function get_category($id = '') {

            $return = '';

            $exec = DB::select(
                            array('artcMsctId', 'category_id')
                    )
                    ->from('article')
                    ->where('artcId', '=', $id)
                    ->execute()
                    ->as_array();

            if(!empty($exec[0]['category_id'])) {
                    $return = $exec[0]['category_id'];
            }

            return $return;

        }

        public function get_tags($id_article = '') {

            return DB::select(
                            array('artgMstgId', 'tag_id')
                    )
                    ->from('article_tags')
                    ->where('artgArtcId', '=', $id_article)
                    ->execute()
                    ->as_array();

        }

        public function get_publish_time($id = '') {

            $return = '';

            $exec = DB::select(
                            array('artcPublishTime', 'publish_time')
                    )
                    ->from('article')
                    ->where('artcId', '=', $id)
                    ->execute()
                    ->as_array();

            if(!empty($exec[0]['publish_time'])) {
                    $return = $exec[0]['publish_time'];
            }

            return $return;

        }

        public function save_child($data = '', $user_id = '') {

		// Start Transaction
		Database::instance()->begin();

		// Save to table article
		$article =	DB::insert('article', array(
						'artcTitle',
						'artcExcerpt',
						'artcKeyword',
						'artcMsctId', // Categpry or Kanal
						'artcThirdparty', // Categpry or Kanal
						'artcDetail',
						'artcPublishTime',
						'artcUserIdSaved',
                                                'artcParentId'
					))
					->values(array(
						$data['title'],
						$data['description'],
						$data['keyword'],
						$data['parent_category'],
						$data['thirdparty'],
						$data['detail'],
						$data['parent_publish_time'],
						$user_id,
                                                $data['parent_id']
					));
		list($lastid_article, $rows_inserted) = $article->execute();

		if(!empty($lastid_article)) {

			// Save article tags
			if(!empty($data['parent_tags'])) {

				foreach($data['parent_tags'] as $v_tags) {
					$tags = DB::insert('article_tags', array(
								'artgArtcId',
								'artgMstgId'
							))
							->values(array(
								$lastid_article,
								$v_tags
							))
							->execute();
				}

			}

			// Save article images
			if(!empty($data['image'])) {

				// Get id image from full url image
				$base_image = basename($data['image']).PHP_EOL;
				if(!empty($base_image)) {
					$ex_base = explode('.', $base_image);
					list($id_image, $file_type) = $ex_base;
				}

				if(!empty($id_image)) {
					$image = 	DB::insert('article_image', array(
									'artiArtcId',
									'artiArimId'
								))
								->values(array(
									$lastid_article,
									$id_image
								))
								->execute();
				}
			}

		}

		// Commit transaction
		Database::instance()->commit();

	}

        public function dashboard_count($status = 1) {

                $return = 0;

		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('article')
					->where('artcStatus', '=', $status)
					->where('artcParentId', '=', NULL)
					->execute()
					->as_array();

		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}

		return $return;

        }

        public function my_count_all($user_id = '') {
            $return = 0;

            $query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
                                    ->from('article')
                                    ->where('artcUserIdSaved', '=', $user_id)
                                    ->where('artcParentId', '=', NULL)
                                    ->execute()
                                    ->as_array();

            if(!empty($query[0]['COUNT'])) {
                    $return = $query[0]['COUNT'];
            }

            return $return;
        }

        public function my_count_search($user_id = '', $search = '') {
            $return = 0;

            $query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
                                    ->from('article')
                                    ->where('artcTitle', 'LIKE', '%' . $search . '%')
                                    ->where('artcUserIdSaved', '=', $user_id)
                                    ->where('artcParentId', '=', NULL)
                                    ->execute()
                                    ->as_array();

            if(!empty($query[0]['COUNT'])) {
                    $return = $query[0]['COUNT'];
            }

            return $return;
        }

        public function my_list_data($user_id = '', $limit = '', $offset = '') {

                $exec = DB::select(
					array('artcId', 'id'),
					array('artcTitle', 'title'),
					array('artcExcerpt', 'description'),
					array('artcWording', 'wording'),
					array('artcKeyword', 'keyword'),
					array('artcDetail', 'detail'),
					array('artcViewer', 'viewer'),
					array('artcThirdparty', 'thirdparty'),
					array('artcPublishTime', 'publish'),
					array('artcSaved', 'saved'),
					array('artcStatus', 'status'),
					array('artcMsctId', 'category_id'),
					array('msctName', 'category_name'),
					array('artcUserIdSaved', 'user_id'),
					array('userRealName', 'user_name')
				)
				->from('article')
				->join('user', 'LEFT')
				->on('article.artcUserIdSaved', '=', 'user.userId')
				->join('master_category', 'LEFT')
				->on('article.artcMsctId', '=', 'master_category.msctId')
				->where('artcUserIdSaved', '=', $user_id)
                ->where('artcParentId', '=', NULL)
				->order_by('artcId', 'DESC')
				->limit($limit)
				->offset($offset)
				->execute()
				->as_array();

		if(!empty($exec)) {

			foreach($exec as $k_exec => $v_exec) {

				if(!empty($v_exec['id'])) {

					// Get tags
					$exec[$k_exec]['tags'] = 	DB::select(
													array('mstgId', 'tags_id'),
													array('mstgName', 'tags_name')
												)
												->from('article_tags')
												->join('master_tags', 'LEFT')
												->on('master_tags.mstgId', '=', 'article_tags.artgMstgId')
												->where('artgArtcId', '=', $v_exec['id'])
												->execute()
												->as_array();

					// Get images
					$exec[$k_exec]['images'] = 	DB::select(
													array('artiArimId', 'image_id'),
													array('arimFileType', 'image_type')
												)
												->from('article_image')
												->join('arsip_images', 'LEFT')
												->on('arsip_images.arimId', '=', 'article_image.artiArimId')
												->where('artiArtcId', '=', $v_exec['id'])
												->execute()
												->as_array();

                                        // Get child
                                        $exec[$k_exec]['child'] =       DB::select(
                                                                                                        array('artcId', 'child_article_id'),
                                                                                                        array('artcTitle', 'child_article_title')
                                                                                                )
                                                                                                ->from('article')
                                                                                                ->where('artcParentId', '=', $v_exec['id'])
                                                                                                ->where('artcStatus', '!=', '0')
                                                                                                ->order_by('artcId', 'ASC')
                                                                                                ->execute()
                                                                                                ->as_array();

				}

			}

		}

		return $exec;

        }

        public function my_list_search($user_id = '', $search ='', $limit = '', $offset = '') {

                $exec = DB::select(
					array('artcId', 'id'),
					array('artcTitle', 'title'),
					array('artcExcerpt', 'description'),
					array('artcWording', 'wording'),
					array('artcKeyword', 'keyword'),
					array('artcDetail', 'detail'),
					array('artcViewer', 'viewer'),
					array('artcPublishTime', 'publish'),
					array('artcSaved', 'saved'),
					array('artcStatus', 'status'),
					array('artcThirdparty', 'thirdparty'),
					array('artcMsctId', 'category_id'),
					array('msctName', 'category_name'),
					array('artcUserIdSaved', 'user_id'),
					array('userRealName', 'user_name')
				)
				->from('article')
				->join('user', 'LEFT')
				->on('article.artcUserIdSaved', '=', 'user.userId')
				->join('master_category', 'LEFT')
				->on('article.artcMsctId', '=', 'master_category.msctId')
				->where('artcTitle', 'LIKE', '%' . $search . '%')
				->where('artcUserIdSaved', '=', $user_id)
                                ->where('artcParentId', '=', NULL)
				->order_by('artcId', 'DESC')
				->limit($limit)
				->offset($offset)
				->execute()
				->as_array();

		if(!empty($exec)) {

			foreach($exec as $k_exec => $v_exec) {

				if(!empty($v_exec['id'])) {

					// Get tags
					$exec[$k_exec]['tags'] = 	DB::select(
													array('mstgId', 'tags_id'),
													array('mstgName', 'tags_name')
												)
												->from('article_tags')
												->join('master_tags', 'LEFT')
												->on('master_tags.mstgId', '=', 'article_tags.artgMstgId')
												->where('artgArtcId', '=', $v_exec['id'])
												->execute()
												->as_array();

					// Get images
					$exec[$k_exec]['images'] = 	DB::select(
													array('artiArimId', 'image_id'),
													array('arimFileType', 'image_type')
												)
												->from('article_image')
												->join('arsip_images', 'LEFT')
												->on('arsip_images.arimId', '=', 'article_image.artiArimId')
												->where('artiArtcId', '=', $v_exec['id'])
												->execute()
												->as_array();

                                        // Get child
                                        $exec[$k_exec]['child'] =       DB::select(
                                                                                                        array('artcId', 'child_article_id'),
                                                                                                        array('artcTitle', 'child_article_title')
                                                                                                )
                                                                                                ->from('article')
                                                                                                ->where('artcParentId', '=', $v_exec['id'])
                                                                                                ->where('artcStatus', '!=', '0')
                                                                                                ->order_by('artcId', 'ASC')
                                                                                                ->execute()
                                                                                                ->as_array();

				}

			}

		}

		return $exec;

        }

}

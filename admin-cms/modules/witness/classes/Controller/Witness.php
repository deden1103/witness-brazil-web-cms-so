<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Witness extends Controller_Backend {

	private $custom_header_form;

	private $custom_footer_form;

	public function before() {

		parent::before();

		// Header Multiple SElect
		$this->custom_header_form = '
			<link rel="stylesheet" href="/assets/plugins/select2/select2.min.css">
			<link rel="stylesheet" href="/assets/dist/css/AdminLTE.min.css">
		';

		// Custom header for daterangepicker
		$this->custom_header_form .= '<link rel="stylesheet" href="/assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		// Tinymce
		$this->custom_footer_form = '
			<script>
				var myCms = [];
				function printToScreen(myValues) {
					var selectedItems = [];
					var x = 1;
					if (myCms.length > 0) {
						myValues.forEach(function (item, index) {
							if(item.name == "image"){
								document.getElementById("previewImage").innerHTML = "<div style=\" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); \"><center><span style=\"color: #FFFFFF;cursor: pointer;\" onclick=\"javascript:delPreview();\">X</span></center></div><img src=\""+item.value+"\" style=\"width:200px;\">";
							}
							var nama = item.name;
							var nama = nama.replace("[]", "");
							if(nama == "tags"){
								$.getJSON( "/tags/ajaxid/" + item.value, function( data ) {
									var arrJs = [];
									$(\'select[name="tags[]"] option\').each(function() {
										arrJs.push($(this).val())
									});
									$.each( data, function( key, val ) {
										var id_data = val.id;
										var sid_data = id_data.toString();
										if(arrJs.indexOf(sid_data) == -1) {
											$(\'select[name="tags[]"]\').append(\'<option value="\' + val.id + \'">\' + val.name + \'</option>\')
											selectedItems.push(val.id);
											$(".select2").select2("val", selectedItems);
										}
									}, chlabel());
								});
							}
							if(nama == "title_bj" || nama == "link_bj"){
								$( "#bj" + x ).val(item.value);
								x++;
							}else{
								$( "[name="+nama+"]" ).val(item.value);
							}
						})
					}
				}
				function showValues() {
					var data = JSON.stringify( $("form").serializeArray() );
					localStorage.setItem("cms", data);
					var data_detail = tinymce.get("wysiwyg").getContent();;
					localStorage.setItem("detail", data_detail);
				}
				function loadValues() {
					tinyMCE.activeEditor.setContent(localStorage.getItem("detail"));
					if (localStorage.getItem("cms") !== null) {
						var localValues = JSON.parse(localStorage.cms);
						if (myCms.length <= 0) {
							myCms = localValues;
						}
						printToScreen(localValues);
					}
				}
				setInterval(showValues, 300000);
			</script>
			<script src="/assets/tinymce/tinymce.min.js"></script>
			<script>
				tinymce.init({
					paste_data_images: true,
					valid_elements : \'*[*]\',
					selector: \'.f_tinymce\',
					theme: \'modern\',
                    templates: [{title: \'Berita Lainnya\', description: \'Berita Lainnya\', url: \'/template/terkait.html\'},{title: \'Button Selengkapnya\', description: \'Button Selengkapnya\', url: \'/template/selengkapnya.html\'},{title: \'Sharing\', description: \'Sharing Information\', url: \'/template/sharing.html\'},{title: \'Mutiara\', description: \'Informasi Mutiara\', url: \'/template/komunitas.html\'},{title: \'Download Arena\', description: \'Link Download Aplikasi Arena\', url: \'/template/arena.html\'}],
					plugins: [
						\'example advlist autolink lists link image charmap print preview hr anchor pagebreak\',
						\'searchreplace wordcount visualblocks visualchars code fullscreen\',
						\'insertdatetime media nonbreaking save table contextmenu directionality\',
						\'emoticons template paste textcolor colorpicker textpattern imagetools\'
					],
					toolbar1: \'insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image\',
					toolbar2: \'fullscreen preview media | forecolor backcolor emoticons | pagebreak\',
					image_advtab: true,
                                        pagebreak_separator: "<!--PAGE_SEPARATOR-->",
                                        pagebreak_split_block: true,
                                        setup: function (editor) {
                                        editor.addButton(\'my_image\', {
                                            text: \'Add Image From Gallery\',
                                            icon: false,
                                            onclick: function () {
                                                open_popup_img_tmce();
                                            }
                                        });
										editor.addButton(\'my_voice\', {
											text: \'voice to text\',
											icon: false,
											onclick: function () {
												PopupCenter("/assets/voice2text/", "google", "640", "480");
											}
										});
										editor.addButton(\'my_video\', {
											text: \'Insert Video\',
											icon: false,
											onclick: function () {
												PopupCenter("/news/insertvideo/", "Insert video", "640", "480");
											}
										});
										editor.addButton(\'insert_link\', {
											text: \'Insert Link\',
											icon: false,
											onclick: function () {
												tinyMCE.activeEditor.insertContent(\'<a href="#" style="border-left: 5px solid #ddd; background: #f8f8f8; padding: 3px 10px; display: block; margin-bottom: 5px;">Ganti judul link<br /></a>\');
											}
										});
										editor.addButton(\'change_cover\', {
											text: \'Change Cover\',
											icon: false,
											onclick: function () {
                                                open_popup_img_cover();
                                            }
										});
										editor.addButton(\'add_page\', {
											text: \'Add Page\',
											icon: false,
											onclick: function () {
												tinyMCE.activeEditor.insertContent(\'<!--PAGE_SEPARATOR--><div class="box-paging-show"><div class="box-paging"><p class="title-paging"><strong>JUDUL_PAGING</strong></p><p><img class="image-paging" src="https://sariagri.id/assets/images/logo-new.png" width="100%" caption="false" /></p><p>ISI_PAGING</p></div></div><!--PAGE_SEPARATOR-->\');
											}
										});
										editor.addButton(\'baca_juga\', {
											text: \'Baca Juga\',
											icon: false,
											onclick: function () {
												tinyMCE.activeEditor.insertContent(\'<p>[baca_juga]</p><br/>\');
											}
										});
										editor.addButton(\'link_baca_juga\', {
											text: \'link baca juga\',
											icon: false,
											onclick: function () {
												PopupCenter("/news/linkbacajuga/", "Link Baca Juga", "640", "480");
											}
										});
                                      },
				});
				function my_voice(texthtml) {
					tinyMCE.activeEditor.insertContent(texthtml);
				}
				function my_video(texthtml) {
					tinyMCE.activeEditor.insertContent(texthtml);
				}
				function link_baca_juga(texthtml) {
					tinyMCE.activeEditor.insertContent(texthtml);
				}
			</script>
		';

		// Multiple SElect
		$this->custom_footer_form .= '
			<script src="/assets/plugins/select2/select2.full.min.js"></script>
			<script>
				$(".select2").select2();
			</script>
		';

		// Custom footer for daterangepicker
		$this->custom_footer_form .= '
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="/assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				var clone_answer=\'<div class="form-group col-xs-3" ><div class="input-group"><input type="text" name="answers[]" value="" maxlength="255" placeholder="'.__('Answer').'" class="form-control"><div class="input-group-addon"><i class="fa fa-times clicked"></i></div></div></div>\';
				var clone_bacajuga =\'<div class="added"><div class="col-md-6 form-group"><input type="text" name="title_bj[]" class="form-control" placeholder="Only 165 Characters" minlength="3"></div><div class="col-md-6 form-group"><div class="input-group"><input type="text" name="link_bj[]" class="form-control" placeholder="Link" minlength="3"><div class="input-group-addon"><i class="fa fa-times clicked_bacajuga"></i></div></div></div></div>\';
				//Date range picker
				$(function() {
					$(\'#publish_time\').daterangepicker({
						format: \'DD/MM/YYYY HH:mm:ss\',
						timePicker: true,
						singleDatePicker: true,
						timePickerIncrement: 1,
						timePicker12Hour: false,
						//minDate: \'' . date('d-m-Y H:i:s') . '\'
					});
				});

				$(".clicked").click(function(){
					$(this).parent().parent().parent().remove();
				});

				function addAnswer(){
					$( "#pooling_form" ).append( clone_answer );
					$(".clicked").click(function(){
						$(this).parent().parent().parent().remove();
					});
				}

				function addBacajuga(){
					if($(".added").length < 3){
						$( ".bacajuga_form" ).append( clone_bacajuga );
						$(".clicked_bacajuga").click(function(){
							$(this).parent().parent().parent().parent().remove();
						});
					}else{
						$(".bacajuga_max").fadeIn("slow");
						setTimeout(\'$(".bacajuga_max").fadeOut("slow")\', 2000);
					}

				}

			</script>
			<!--Tinymce hidden for append image textarea-->
			<input type="hidden" id="image_popup_tmce" />
			<input type="hidden" id="image_popup_cover" />
			<input type="hidden" id="insertvideo" />
			<script>
				function open_popup_img() {
					PopupCenter("/library/index/1/0/image_popup", "google", "840", "576");
				}

				function open_popup_img_tmce() {
					PopupCenter("/library/index/1/0/image_popup_tmce", "google", "640", "480");
				}

				function open_popup_img_cover() {
					PopupCenter("/library/index/1/0/image_popup_cover", "google", "640", "480");
				}

				function image_popup_tmce() {
					var str = $(\'#image_popup_tmce\').val();
					var n = str.lastIndexOf(".");
					var res = str.substring(0, n)+"_512x351"+str.substring(n);
					tinyMCE.activeEditor.insertContent(\'<img src="\' + res + \'" width="100%" />\');
				}

				function image_popup_cover() {
					var str = $(\'#image_popup_cover\').val();
					var n = str.lastIndexOf(".");
					var res = str.substring(0, n)+"_512x351"+str.substring(n);
					tinyMCE.activeEditor.insertContent(\'<img class="image-cover" src="\' + res + \'" width="100%" />\');
				}

				function insert_video() {
					var str = $(\'#insertvideo\').val();
					tinyMCE.activeEditor.insertContent(str);
				}


				function image_popup() {
					$(\'#previewImage\').html(\'\');
					$(\'#previewImage\').append(\'<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreview();">X</span></center></div>\');
					$(\'#previewImage\').append(\'<img src="\' + $(\'#image_popup\').val() + \'" style="width:200px;"/>\');
				}

				function delPreview() {
					$(\'#previewImage\').html(\'\');
					$(\'#image_popup\').val(\'\');
				}

                                $(\'.refresh_tags\').click(function() {
                                    $(this).html(\'Loading ...\');
                                    $(\'select[name="tags[]"]\').empty();

                                    $.getJSON( "/tags/ajax", function( data ) {
                                        $.each( data, function( key, val ) {
                                            $(\'select[name="tags[]"]\').append(\'<option value="\' + val.id + \'">\' + val.name + \'</option>\')
                                        }, chlabel());
                                    });
                                })

                                $(document).ready(function(){
                                    //$(\'select[name="tags[]"]\').empty();
                                    ajax_tags()
                                });

                                $(\'select[name="tags[]"]\').change(function() {ajax_tags()})

                                function ajax_tags() {
                                    $(\'.select2-search__field\').on("keyup", function(e){
                                        if (e.which <= 90 && e.which >= 48) {
                                            var str = $(\'.select2-search__field\').val();
                                            var n = str.length;
                                            if(n >= 2) {
                                                $(\'.refresh_tags\').html(\'Loading ...\');
                                                console.log("Ajax Search : " + $(\'.select2-search__field\').val());
                                                $.getJSON( "/tags/ajax/" + $(\'.select2-search__field\').val(), function( data ) {
                                                    var arrJs = [];
                                                    $(\'select[name="tags[]"] option\').each(function() {
                                                        arrJs.push($(this).val())
                                                    });
                                                    $.each( data, function( key, val ) {
                                                        var id_data = val.id;
                                                        var sid_data = id_data.toString();
                                                        if(arrJs.indexOf(sid_data) == -1) {
                                                            $(\'select[name="tags[]"]\').append(\'<option value="\' + val.id + \'">\' + val.name + \'</option>\')
                                                        }
                                                    }, chlabel());
                                                });
                                            }
                                        }
                                    })
                                }

								// Jika Diakses dari mobile
								if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
									var oldData = "";
									setInterval(function(){
										var newData = $(\'.select2-search__field\').val();
										if(oldData != newData) {
											oldData = newData
											var n = newData.length;
                                            if(n >= 3) {
                                                $(\'.refresh_tags\').html(\'Loading ...\');
                                                console.log("Ajax Search : " + $(\'.select2-search__field\').val());
                                                $.getJSON( "/tags/ajax/" + $(\'.select2-search__field\').val(), function( data ) {
                                                    var arrJs = [];
                                                    $(\'select[name="tags[]"] option\').each(function() {
                                                        arrJs.push($(this).val())
                                                    });
                                                    $.each( data, function( key, val ) {
                                                        var id_data = val.id;
                                                        var sid_data = id_data.toString();
                                                        if(arrJs.indexOf(sid_data) == -1) {
                                                            $(\'select[name="tags[]"]\').append(\'<option value="\' + val.id + \'">\' + val.name + \'</option>\')
                                                        }
                                                    }, chlabel());
                                                });
                                            }
										}
									}, 500);
								}

                                function chlabel() {
                                    $(\'.refresh_tags\').html(\'( Click Here For Refresh Tags )\');
                                }
			</script>
		';

	}

	public function action_index() {
		// Redirect
		$this->redirect('/news/search');
	}

	public function action_search() {

		$session = Session::instance();

		if(isset($_POST['version'])) $session->set('version_news', $_POST['version']);
		$data['version'] = $version = $session->get("version_news", "");

		if(isset($_POST['publisher'])) $session->set('publish_news', $_POST['publisher']);
		$data['publish'] = $publish = $session->get("publish_news", "artcPublishTime");

		if(isset($_POST['uploader'])) $session->set('uploader_news', $_POST['uploader']);
		$data['uploader'] = $uploader = $session->get("uploader_news", "");

		if(isset($_POST['category'])) $session->set('category_news', $_POST['category']);
		$data['category'] = $category = $session->get("category_news", "");

		if(isset($_POST['search'])) $session->set('search_news', $_POST['search']);
		$data['search'] = $search = $session->get("search_news", "");

		if(isset($_GET['date_range'])) $session->set('date_range_news', $_GET['date_range']);
		$data['date_range'] = $date_range = $session->get("date_range_news", "");

		$data['main_title'] = __('Newsroom | Newsroom | News | Search');
		$data['menu_active'] = 'newsroom';
		$data['menu_active_child'] = 'news';
		$data['menu_active_child_1'] = 'list';

		// Redirect jika mengepost search agar bisa menggunakan paginasi dengan beauty url
		// $post_search = $this->request->post('search');

		/* if(!empty($post_search)) {
			$this->redirect('news/search/' . base64_encode($post_search) . '/?date_range=' . $date_range);
		} */

		// Param search
		// $data['search'] = $search = base64_decode($this->request->param('search'));

		// Custom header for daterangepicker
		$data['custom_header'] = '<link rel="stylesheet" href="/assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		// Custom footer for daterangepicker
		$data['custom_footer'] = '
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="/assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				//Date range picker
				$(function() {
					$(\'#reservation\').daterangepicker(
						{
							format: \'DD/MM/YYYY\'
						}, function (start, end) {
							window.location = "/news/search/?date_range=" + $(\'#reservation\').val();
						}
					);
				});
				function doExcel() {
					window.open(\'https://myadmin.intisari.my.id/engine/cms/excel_news.php?date_range=\' + $( "input[name=\'date_range\']" ).val() + \'&category=\' + $( "select[name=\'category\']" ).val(), \'_blank\')
				}
			</script>
		';

		// Yes No Confirm
		$data['custom_footer'] .= '
			<script type="text/javascript">
				function del_confirm(id) {
					var dialog = confirm("' . __('Are you sure for delete this data ?') . '");
					if (dialog == true) {
						window.location.href="/news/delete/" + id;
					}
				}
			</script>
		';

		// Date parameter
		/* $date_range = $this->request->post('date_range');
		$date_range = !empty($_GET['date_range']) ? $_GET['date_range'] : $date_range; */
		if(empty($date_range)) {
			$date1 = date('Y-m-d');
			$date2 = date('Y-m-d');
		} else {
			$ex_range = explode(' - ', $date_range);
			if(!empty($ex_range)) {
				$date1 = DateTime::createFromFormat('d/m/Y', $ex_range[0]);
				$date1 = $date1->format('Y-m-d');
				$date2 = DateTime::createFromFormat('d/m/Y', $ex_range[1]);
				$date2 = $date2->format('Y-m-d');
			}
		}

		// Date adding time
		$date1 = $date1 . ' 00:00:00';
		$date2 = $date2 . ' 00:00:00';

		// Page
		$page = intval($this->request->param('page'));
		if(empty($page)) {
			$page = 1;
		}

		$user_model = new Model_User();
        $data['all_user'] = $user_model->list_all();

		// Load model news
		$news_model = new Model_News();

		// Count all data
		$count_all = $news_model->count_search($date1, $date2, $search);
		$data['count_all'] = $count_all;

		// Pagination
		$pagination = 	Pagination::factory(array(
							'total_items'    		=> $count_all,
							'items_per_page'		=> 20,
							'current_page'			=> $page,
							'base_url'				=> '/news/search/',
							'view'					=> 'pagination/admin'
							// 'suffix'				=> '?date_range=' . $date_range,
						));

		$data['list'] = $news_model->list_search($date1, $date2, $search, $pagination->items_per_page, $pagination->offset);

		$data['pagination'] = $pagination->render();

		// Session
		$data['session_user_id'] = $session->get('user_id');

		$member = $session->get('member');
		if($member==2){
			$view = DVH::admin_template_sosmed('news/' . Kohana::$config->load('path.main_template') . '/list', $data);
		}else{
			$view = DVH::admin_template('news/' . Kohana::$config->load('path.main_template') . '/list', $data);
		}
		$this->response->body($view);

	}


	public function action_fb_callback() {
		Kohana_Engine_News::fb_callback();
		$this->redirect('/news/search');
	}
	public function action_new() {
		$session = Session::instance();
		$data['main_title'] = __('Newsroom | Newsroom | News | Add New Data');
		$data['menu_active'] = 'newsroom';
		$data['menu_active_child'] = 'news';
		$data['menu_active_child_1'] = 'add';

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;

		$member = $session->get('member');
		if($member==2){
			$view = DVH::admin_template_sosmed('news/' . Kohana::$config->load('path.main_template') . '/new', $data);
		}else{
			$view = DVH::admin_template('news/' . Kohana::$config->load('path.main_template') . '/new', $data);
		}
		$this->response->body($view);
	}

	public function action_save() {
		$session = Session::instance();

		// Validation
		$validation = News::validation($this->request->post());

		if($validation !== TRUE) { // Error Validation

			$data['main_title'] = __('Newsroom | Newsroom | News | Add New Data');
			$data['menu_active'] = 'newsroom';
			$data['menu_active_child'] = 'news';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['post'] = $this->request->post();

			$member = $session->get('member');
			if($member==2){
				$view = DVH::admin_template_sosmed('news/' . Kohana::$config->load('path.main_template') . '/new', $data);
			}else{
				$view = DVH::admin_template('news/' . Kohana::$config->load('path.main_template') . '/new', $data);
			}
			$this->response->body($view);

		} else { // Validation Success

			// Get user id from session

			$user_id = $session->get('user_id');

			// Load Model
			$news_model = new Model_News();

			// Save data
			$save_data = $news_model->save_data($this->request->post(), $user_id);

			$this->redirect('/news/search');
		}

	}

	public function action_about() {
		$session = Session::instance();

		$data['main_title'] = __('Witness | Sobre nós');
		$data['menu_active'] = 'witness';
		$data['menu_active_child'] = 'about';

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;

		// Paramter ID
		$id = 1;

		// Get user id from session
		$user_id = $session->get('adminId');

		// Load Model
		$news_model = new Model_Witness();

		// Detail Data
		$news_detail = $news_model->detail_data($id);

		$data['detail'] = $news_detail;

		$view = Briliant::admin_template('witness/' . Kohana::$config->load('path.main_template') . '/edit', $data);
		
		$this->response->body($view);

	}

	public function action_pms() {
		$session = Session::instance();

		$data['main_title'] = __('Witness | Pedoman Media Siber');
		$data['menu_active'] = 'witness';
		$data['menu_active_child'] = 'pms';

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;

		// Paramter ID
		$id = 2;

		// Get user id from session
		$user_id = $session->get('adminId');

		// Load Model
		$news_model = new Model_Witness();

		// Detail Data
		$news_detail = $news_model->detail_data($id);

		$data['detail'] = $news_detail;

		$view = Briliant::admin_template('witness/' . Kohana::$config->load('path.main_template') . '/edit', $data);
		
		$this->response->body($view);

	}

	public function action_layanan() {
		$session = Session::instance();

		$data['main_title'] = __('Witness | Ketentuan Layanan');
		$data['menu_active'] = 'witness';
		$data['menu_active_child'] = 'layanan';

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;

		// Paramter ID
		$id = 3;

		// Get user id from session
		$user_id = $session->get('adminId');

		// Load Model
		$news_model = new Model_Witness();

		// Detail Data
		$news_detail = $news_model->detail_data($id);

		$data['detail'] = $news_detail;

		$view = Briliant::admin_template('witness/' . Kohana::$config->load('path.main_template') . '/edit', $data);
		
		$this->response->body($view);

	}


	public function action_update() {

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('adminId');
		
		// Check permission data
		$id_parameter = $this->request->post('id');

		if($id_parameter == 1){
			$param = "about";
		}elseif ($id_parameter == 2) {
			$param = "pms";
		}elseif ($id_parameter == 3) {
			$param = "layanan";
		}

		// Load Model
		$news_model = new Model_Witness();

		// Update Data
		$update_data = $news_model->update_data($this->request->post(), $user_id);

		$this->redirect('/witness/'.$param);

	}


	public function action_pickup() {
		$session = Session::instance();

		$data['main_title'] = __('Newsroom | Newsroom | News | Pickup Data');
		$data['menu_active'] = 'newsroom';
		$data['menu_active_child'] = 'news';

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;


		// Paramter ID
		$id = $this->request->param('id');

		// Get user id from session
		$user_id = $session->get('user_id');

		// Load Model
		$news_model = new Model_News();

		// Detail Data
		$news_detail = $news_model->detail_data_pickup($id);

		$data['id'] = $news_detail;
		$data['detail'] = $news_detail;
		$member = $session->get('member');

		if($member==2){
			$view = DVH::admin_template_sosmed('news/' . Kohana::$config->load('path.main_template') . '/pickup', $data);
		}else{
			$view = DVH::admin_template('news/' . Kohana::$config->load('path.main_template') . '/pickup', $data);
		}
		$this->response->body($view);

	}



	public function action_delete() {

		// ID Parameter
		$id = $this->request->param('id');

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('user_id');

		// Check permission data
		DVH::check_permission('article', 'artcId', $id, $user_id, 'artcUserIdSaved');

		// Load Model News
		$news_model = new Model_News();

		// Change status to 0
		$news_model->delete_data($id);

		// Redirect
		$this->redirect('/news/search');

	}

	public function action_detail() {
		$session = Session::instance();

		$data['main_title'] = __('Newsroom | Newsroom | News | Detail Data');
		$data['menu_active'] = 'newsroom';
		$data['menu_active_child'] = 'news';

		$id = $this->request->param('id');

		// Load model news
		$news_model = new Model_News();

		// Detail data
		$data['detail'] = $news_model->detail_data($id);
		$member = $session->get('member');
		if($member==2){
			$view = DVH::admin_template_sosmed('news/' . Kohana::$config->load('path.main_template') . '/detail', $data);
		}else{
			$view = DVH::admin_template('news/' . Kohana::$config->load('path.main_template') . '/detail', $data);
		}
		$this->response->body($view);

	}

        public function action_child() {

            $parent_id = $this->request->param('parent_id');
            $data['post']['parent_id'] = $parent_id;

            // Get user id from session
            $session = Session::instance();
            $user_id = $session->get('user_id');

            // Check Permission
            DVH::check_permission('article', 'artcId', $parent_id, $user_id, 'artcUserIdSaved');

            // Get title news from id
            $news_model = new Model_News();
            $parent_title = $news_model->get_title($parent_id);
            $data['post']['parent_title'] = $parent_title;

            // Get category parent
            $parent_category = $news_model->get_category($parent_id);
            $data['post']['parent_category'] = $parent_category;

            // Get tags parent
            $parent_tags = $news_model->get_tags($parent_id);
            $arr_parent_tags = array();
            if(!empty($parent_tags)) {
                foreach($parent_tags as $v_partag) {
                    $arr_parent_tags[] = $v_partag['tag_id'];
                }
            }
            $data['post']['parent_tags'] = $arr_parent_tags;

            // Get parent publish time
            $parent_publish_time = $news_model->get_publish_time($parent_id);
            $data['post']['parent_publish_time'] = $parent_publish_time;

            $data['main_title'] = __('Newsroom | Newsroom | News | Add Child News');
            $data['menu_active'] = 'newsroom';
            $data['menu_active_child'] = 'news';

            $data['custom_header'] = $this->custom_header_form;
            $data['custom_footer'] = $this->custom_footer_form;
			$member = $session->get('member');
			if($member==2){
				$view = DVH::admin_template_sosmed('news/' . Kohana::$config->load('path.main_template') . '/child', $data);
			}else{
				$view = DVH::admin_template('news/' . Kohana::$config->load('path.main_template') . '/child', $data);
			}
            $this->response->body($view);

        }

        public function action_savechild() {

		// Validation
		$validation = News::validation($this->request->post());

                // Parent Id
                $parent_id = $this->request->post('parent_id');

                // Get user id from session
                $session = Session::instance();
                $user_id = $session->get('user_id');

                // Check Permission
                DVH::check_permission('article', 'artcId', $parent_id, $user_id, 'artcUserIdSaved');

		if($validation !== TRUE) { // Error Validation

			$data['main_title'] = __('Newsroom | Newsroom | News | Add Child News');
			$data['menu_active'] = 'newsroom';
			$data['menu_active_child'] = 'news';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['post'] = $this->request->post();

			$member = $session->get('member');
			if($member==2){
				$view = DVH::admin_template_sosmed('news/' . Kohana::$config->load('path.main_template') . '/child', $data);
			}else{
				$view = DVH::admin_template('news/' . Kohana::$config->load('path.main_template') . '/child', $data);
			}
			$this->response->body($view);

		} else { // Validation Success

			// Load Model
			$news_model = new Model_News();

			// Save data
			$save_data = $news_model->save_child($this->request->post(), $user_id);

			$this->redirect('/news/search');
		}

        }

        public function action_detailchild() {
            $data['main_title'] = __('Newsroom | Newsroom | News | Detail Child News');
            $data['menu_active'] = 'newsroom';
            $data['menu_active_child'] = 'news';

            $id = $this->request->param('id');

            // Load model news
            $news_model = new Model_News();

            // Detail data
            $data['detail'] = $news_model->detail_data($id);

            // Title Parent
            if(!empty($data['detail']['parent_id'])) {
                $data['detail']['parent_title'] = $news_model->get_title($data['detail']['parent_id']);
            }

            // Get user id from session
            $session = Session::instance();
            $session_user_id = $session->get('user_id');

            $data['detail']['session_user_id'] = $session_user_id;

            $data['custom_footer'] = '
			<script type="text/javascript">
				function del_confirm(id) {
					var dialog = confirm("' . __('Are you sure for delete this data ?') . '");
					if (dialog == true) {
						window.location.href="/news/delete/" + id;
					}
				}
			</script>
		';
            $member = $session->get('member');
			if($member==2){
				$view = DVH::admin_template_sosmed('news/' . Kohana::$config->load('path.main_template') . '/detailchild', $data);
			}else{
				$view = DVH::admin_template('news/' . Kohana::$config->load('path.main_template') . '/detailchild', $data);
			}
            $this->response->body($view);
        }

	public function action_insertvideo(){
		echo "
			<html>
				<head>
					<title>insert video</title>
				</head>
				<body>
					<form action='/news/insertvideo_save' method='post'>
					Insert Link Video : <br>
					<textarea name='linkvideo' rows='4' cols='70'></textarea><br>
					<input type='submit' value='Submit'>
					</form>
				</body>
			<html>
		";
	}

	public function action_insertvideo_save(){
		$post = $this->request->post('linkvideo');
		if (strpos($post, 'www.youtube.com') !== false) {
			$post = str_replace('https://www.youtube.com/watch?v=', '', $post);
			//$result = '<div class="g-ytsubscribe" data-channelid="UCR1CM8jSgR47vFpfoIvDUZQ" data-layout="default" data-count="default"></div><div class="videoWrapper"><iframe width="560" height="315" src="https://www.youtube.com/embed/'.$post.'" frameborder="0" allowfullscreen></iframe></div><p>----</p>';
			$result = '<div class="videoWrapper"><iframe width="560" height="315" src="https://www.youtube.com/embed/'.$post.'" frameborder="0" allowfullscreen></iframe></div><p>----</p>';
		} elseif (strpos($post, 'www.dailymotion.com') !== false){
			$post = str_replace('http://www.dailymotion.com/video/', '', $post);
			$post = str_replace('https://www.dailymotion.com/video/', '', $post);
			$result = '<div class="videoWrapper"><iframe frameborder="0" width="480" height="270" src="//www.dailymotion.com/embed/video/'.$post.'" allowfullscreen></iframe></div><p>----</p>';
		} else {
			echo "Bukan Youtube dan Dailymotion";
		}

		echo "
			<script src=\"https://code.jquery.com/jquery-1.9.1.min.js\"></script>
			<script>
			$( document ).ready(function() {
				//window.opener.document.getElementById(insertvideo).value = '" . $result . "';
				window.opener.my_video('" . $result . "');
				window.close();
			});
			</script>
		";
	}

	public function action_linkbacajuga(){
		echo "
			<html>
				<head>
					<title>Link Baca Juga</title>
				</head>
				<body>
					<form action='/news/linkbacajuga_save' method='post'>
					Link : <input type=\"text\" name=\"link\" size=\"50%\"><br><br>
					Judul : <input type=\"text\" name=\"judul\" size=\"70%\"><br><br>
					<input type='submit' value='Submit'>
					</form>
				</body>
			<html>
		";
	}

	public function action_linkbacajuga_save(){
		$link = $this->request->post('link');
		$judul = $this->request->post('judul');

		echo "
			<script src=\"https://code.jquery.com/jquery-1.9.1.min.js\"></script>
			<script>
			$( document ).ready(function() {
				window.opener.link_baca_juga('<a href=\"" . $link . "\" style=\"border-left:5px solid #ccc; background:#eee;padding:5px 10px; display:block; margin-bottom:10px;\">" . $judul . "</a>');
				window.close();
			});
			</script>
		";
	}

	public function action_refresh_baca_juga() {
		$article_id = $this->request->param('id');
		$web = "sariagri.id";

		//echo 'https://myadmin.intisari.my.id/engine/bacaJugaRefresh.php?web='.$web.'&id='.$article_id; exit();

		file_get_contents('https://myadmin.intisari.my.id/engine/bacaJugaRefresh.php?web='.$web.'&id='.$article_id);
		// Engine_News::send_to_web($article_id);
		file_get_contents('https://myadmin.intisari.my.id/engine/purge/cache.php?web='.$web.'&id='.$article_id.'&close=1');
		echo "<script>window.close();</script>";
	}
	public function action_tag_render() {
		$article_id = $this->request->param('id');
		$web = "sariagri.id";

		//echo 'https://myadmin.intisari.my.id/engine/bacaJugaRefresh.php?web='.$web.'&id='.$article_id; exit();

		file_get_contents('https://myadmin.intisari.my.id/engine/tagRander.php?web='.$web.'&id='.$article_id);
		// Engine_News::send_to_web($article_id);
		file_get_contents('https://myadmin.intisari.my.id/engine/purge/cache.php?web='.$web.'&id='.$article_id.'&close=1');
		echo "<script>window.close();</script>";
	}
}

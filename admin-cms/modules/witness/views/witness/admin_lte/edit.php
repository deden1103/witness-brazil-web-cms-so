<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $data['main_title']; ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Testemunha'); ?></li>
			<li class="active"><a href="/witness"><?php echo __('Testemunha'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Edit Data'); ?></h3>
					</div>
					<form role="form" method="post" action="/witness/update">
						<div class="box-body">
							<?php
								if(!empty($data['errors'])) {
							?>
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<h4><i class="icon fa fa-ban"></i> <?php echo __('Alert!'); ?></h4>
									<?php
										foreach($data['errors'] as $v_errors) {
											echo ucfirst($v_errors) . '</br>';
										}
									?>
								</div>
							<?php
								}
								$id_parameter = $data['detail']['id'];
								if($id_parameter == 1){
									$param = "about";
								}elseif ($id_parameter == 2) {
									$param = "pms";
								}elseif ($id_parameter == 3) {
									$param = "layanan";
								}
							?>
							
							<!-- Hidden Text -->
							<input type="hidden" name="id" value="<?php echo !empty($data['detail']['id']) ? $data['detail']['id'] : ''; ?>" />
							<input type="hidden" name="is_edit" value="1" />
							<div class="form-group">
								<label><?php echo __('Detalhes') ?></label>
								<textarea class="form-control f_tinymce" rows="15" name="detail" minlength="3" required><?php echo !empty($data['detail']['detail']) ? $data['detail']['detail'] : '&nbsp;'; ?></textarea>
							</div>
						  </div><!-- /.box -->
							
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Atualizar os dados'); ?></button>
							<a href="/witness/<?= $param; ?>"><button type="button" class="btn btn-danger"><?php echo __('Cancelar'); ?></button></a>
						</div>
					</form>
					<!-- /.box-header -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

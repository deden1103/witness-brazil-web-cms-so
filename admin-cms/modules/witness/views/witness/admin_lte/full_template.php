<div class="box-body">
						<table class="table table-bordered">
							<!-- List isi artikel -->
							<tr>
								<th style="width:210px;text-align:center;"><?php echo __('News Image'); ?></th>
								<th style="text-align:center;"><?php echo __('News Article'); ?></th>
								<th style="text-align:center;"><?php echo __('Carregador'); ?></th>
								<th style="width:18%;text-align:center;"><?php echo __('Status'); ?></th>
								<th colspan="2" style="width:20%;text-align:center;"><?php echo __('Ações'); ?></th>
							</tr>
							<?php
							if(!empty($data['list'])) {
                                                                $c_bg_list = 1;
                                                                $bg_list = '';
								foreach($data['list'] as $v_list) {
                                                                        // Background color
                                                                        if($c_bg_list % 2 == 0) {
                                                                            $bg_list = '';
                                                                        } else {
                                                                            $bg_list = 'style="background-color: #ECF0F5;"';
                                                                        }
									// Image article
									$img_article = '<center> ' . __('[ No Image Available ]') . ' </center>';
									if(!empty($v_list['images'][0]['image_id'])) {
										$split_id = str_split($v_list['images'][0]['image_id']);
										$path_folder_image = implode('/', $split_id);
										$img_article = '<img src="/uploads/library/' . $path_folder_image . '/' . $v_list['images'][0]['image_id'] . '_224x153.' . $v_list['images'][0]['image_type'] . '" />';
									}
									
									// Article tags
									$article_tags = '';
									if(!empty($v_list['tags'])) {
										foreach($v_list['tags'] as $v_list_tags) {
											$article_tags .= '<u>' . $v_list_tags['tags_name'] . '</u>, ';
										}
									}
									
									// Status
									$article_status = '';
									if($v_list['status'] == 1) {
										$article_status = '<span class="label label-info">Saved</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
									} else if($v_list['status'] == 0) {
										$article_status = '<span class="label label-danger">Deleted</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
									} else if($v_list['status'] == 2) {
										$url_live = Kohana::$config->load('path.arah')."/article/detail/{$v_list['id']}/".URL::title($v_list['title']).'.html';
										$article_status = '<span class="label label-success">Publish</span> <b>' . date('d M Y H:i', strtotime($v_list['publish'])). '</b>';
                                                                                $article_status .= '<br><br><b><i>Approved By :<br>' . News::get_user_approver($v_list['id']) . '<i></b>';
                                                                        }
									
									// Publish schedule
									if($v_list['publish'] > $v_list['saved']) {
										$article_status .= '<br/></br><span class="label bg-maroon-active color-palette">Schedule</span> <b>' . date('d F Y H:i', strtotime($v_list['publish'])) . '</b>';
									}
									
									$btn_prg = '';
									$btn_rbj = '<a href="javascript:void(0)" onclick="PopupCenter(\'https://myadmin.intisari.my.id/news/refresh_baca_juga/' . $v_list['id'] . '\', \'Baca Juga\', 600, 300)"><button class="btn btn-block btn-primary  btn-xs">' . __('Refresh Baca Juga') . '</button></a></br>';
									$btn_tr = '<a href="javascript:void(0)" onclick="PopupCenter(\'https://myadmin.intisari.my.id/news/tag_render/' . $v_list['id'] . '\', \'Render Tags\', 600, 300)"><button class="btn btn-block btn-primary  btn-xs">' . __('Render Tags') . '</button></a></br>';
									// if($v_list['status'] == 2){
									// 	$btn_prg = '<a href="javascript:void(0)" onclick="PopupCenter(\'http://baca.bismillah.web.id/engine/purge/cache.php?web=arah&id=' . $v_list['id'] . '\', \'Purge\', 600, 300)"><button class="btn btn-block btn-danger btn-xs">' . __('Purge Cache') . '</button></a></br>';
									// }
									
									// Default Button
									$button = '
										<td>
											<a href="/news/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
											<a href="/news/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
											<a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a> </br></br>
											<a href="/news/child/' . $v_list['id'] . '"><button class="btn btn-block btn-info btn-xs">' . __('Add Child News') . '</button></a></br></br></br>
											'.$btn_rbj.'
											'.$btn_tr.'
											'.$btn_prg.'
										</td>
									';
									
									// Button If Status 0
									if($v_list['status'] == 0) {
										$button = '
											<td>
												<a href="/news/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
												<a href="javascript:void()"><button disabled class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
												<a href="javascript:void()"><button disabled class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a> </br></br>
												<a href="javascript:void()"><button disabled class="btn btn-block btn-info btn-xs">' . __('Add Child News') . '</button></a>
											</td>
										';
									}
									
									// BUtton jika yang membuka list != yang mengupload berita
									if($data['session_user_id'] != $v_list['user_id']) {
										$button = '
											<td>
												<a href="/news/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
												<a href="javascript:void()"><button disabled class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
												<a href="javascript:void()"><button disabled class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a> </br></br>
												<a href="javascript:void()"><button disabled class="btn btn-block btn-info btn-xs">' . __('Add Child News') . '</button></a></br></br></br>
												'.$btn_rbj.'
												'.$btn_tr.'
												'.$btn_prg.'
											</td>
										';
									}
									
                                                                        // Jika berita mempunya child
                                                                        $child_news = '';
                                                                        if(!empty($v_list['child'])) {
                                                                            
                                                                            $child_news .= '<br></br>Child News : </br>';
                                                                            
                                                                            foreach ($v_list['child'] as $v_child) {
                                                                                
                                                                                $child_news .= '<div style=" background-color: #FFF5C8; margin-top: 5px; "><a target="_blank" href="/news/detailchild/' . $v_child['child_article_id'] . '"><strong>' . $v_child['child_article_title'] . '</strong></a></div>';
                                                                                
                                                                            }
                                                                            
                                                                        }
                                                                        
                                                                        $article_title = $v_list['title'];
                                                                        $article_desc = $v_list['description'];
                                                                        $article_wording = $v_list['wording'];
                                                                        if(!empty($data['search'])) {
                                                                            $ex_search = explode(',',$data['search']);
                                                                            foreach($ex_search as $v_search) {
                                                                                $article_title = str_ireplace(trim($v_search), '<span style="color:red"><b><i>' . trim($v_search). '</i></b></span>', $article_title);
																				
                                                                                $article_desc = str_ireplace(trim($v_search), '<span style="color:red"><b><i>' . trim($v_search) . '</i></b></span>', $article_desc);
                                                                            }
                                                                        }
																		if(!empty($url_live)){
																					$article_title = "<a href='{$url_live}' target='_BLANK'>{$article_title}</a>";
																				}
                                                                        
                                                                        
									echo '
										<tr ' . $bg_list . '>
											<td>' . $img_article . '</td>
											<td>
												<strong>' . $article_title . '</strong></br>
												<i>' . $article_desc . '</i> </br></br>
												Wording: <BR><i>' . $article_wording . '</i> </br></br>
												<!-- >>>> <a href="javascript:void(0)" onclick="PopupCenter(\'http://baca.bismillah.web.id/engine/autopost/twitter/post.php?id=' . $v_list['id'] . '\', \'AutoPost\', 600, 300)"><b>Post Wording To Twitter</b></a> </br> -->
												<!-- >>>> <a href="javascript:void(0)" onclick="PopupCenter(\'http://baca.bismillah.web.id/engine/autopost/tumblr/index.php?id=' . $v_list['id'] . '\', \'AutoPost\', 600, 300)"><b>Post Wording To Tumblr</b></a> </br> -->
												<!-- >>>> <a href="javascript:void(0)" onclick="PopupCenter(\'http://baca.bismillah.web.id/engine/autopost/pinterest/index.php?id=' . $v_list['id'] . '\', \'AutoPost\', 600, 300)"><b>Post Wording To Pinterest</b></a> </br> -->
												<!-- >>>> <a href="javascript:void(0)" onclick="PopupCenter(\'http://baca.bismillah.web.id/engine/autopost/linkedin/index.php?id=' . $v_list['id'] . '\', \'AutoPost\', 600, 300)"><b>Post Wording To Linkedin</b></a> </br></br> -->
												Categoria : <strong><i>' . $v_list['category_name'] . '</i></strong></br></br>
												Tags : </br>
												' . $article_tags . '
                                                                                                ' . $child_news . '
											</td>
											<td>' . $v_list['user_name'] . '</td>
											<td>' . $article_status . '</td>
											' . $button . '
										</tr>
									';
                                                                        $c_bg_list++;
								}
							}
							?>
						</table>
					</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Newsroom Article'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Sala de imprensa'); ?></li>
			<li class="active"><a href="/news"><?php echo __('News'); ?></a></li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Add New Data'); ?></h3>
					</div>
					<script>
					function submit_news(){
						return true;
						var txt = tinyMCE.activeEditor.getContent();
						if(txt.toLowerCase().indexOf("[baca_juga]")>0){
							return true;
						}else{
							alert("Anda belum memasukkan \"Baca Juga:\"");
							return false;
						}

					}
					</script>
					<button onclick="loadValues()" class="btn btn-sm btn-success" style="margin: 1%">Load Last Data</button>
					<form role="form" method="post" onsubmit="return submit_news()" action="/news/save">
						<div class="box-body">
							<?php
							if(!empty($data['errors'])) {
								?>
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<h4><i class="icon fa fa-ban"></i> <?php echo __('Alert!'); ?></h4>
									<?php
									foreach($data['errors'] as $v_errors) {
										echo ucfirst($v_errors) . '</br>';
									}
									?>
								</div>
								<?php
							}
							?>
                            <input type="hidden" name="publish_time" value="" />
							<div class="form-group">
								<label><?php echo __('Publicar Horário') ?></label>
								<div class="input-group">
									<input type="text" name="publish_time" id="publish_time" value="<?php echo !empty($data['post']['publish_time']) ? $data['post']['publish_time'] : ''; ?>" placeholder="<?php echo __('Optional'); ?>" class="form-control">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label><?php echo __('Título') ?></label>
								<input type="text" name="title" value="<?php echo !empty($data['post']['title']) ? $data['post']['title'] : ''; ?>" minlength="3" maxlength="75" placeholder="<?php echo __('Only 75 Characters'); ?>" class="form-control" required>
							</div>
							<div class="form-group">
								<label><?php echo __('Image') ?></label>
								<div id="previewImage" style="width: 100%; height: auto; margin-bottom: 10px;">
									<?php
									$val_hidden_image = '';
									// Image preview edit
									if(!empty($data['post']['image'])) {
										// Get id image from full url image
										$base_image = basename($data['post']['image']).PHP_EOL;
										if(!empty($base_image)) {
											$ex_base = explode('.', $base_image);
											list($id_image, $file_type) = $ex_base;
										}
										$split_id = str_split($id_image);
										$path_folder_image = implode('/', $split_id);
										echo '<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreview();">X</span></center></div><img src="/uploads/library/' . $path_folder_image . '/' . $id_image . '.' . $file_type . '" style="width:200px;">';
										$val_hidden_image = $data['post']['image'];
									}
									?>
								</div>
								<input type="hidden" name="image" id="image_popup" value="<?php echo $val_hidden_image; ?>" class="form-control">
								<a href="javascript:open_popup_img()"><button type="button" class="btn btn-block btn-default" id="image_popup_btn" style="width:200px"><?php echo __('Browse'); ?></button></a>
							</div>
							<div class="form-group">
								<label><?php echo __('Descrição') ?></label>
								<textarea class="form-control" rows="3" name="description" maxlength="165" placeholder="<?php echo __('Only 165 Characters'); ?>" minlength="3" required><?php echo !empty($data['post']['description']) ? $data['post']['description'] : ''; ?></textarea>
							</div>
							<div class="form-group">
								<label><?php echo __('Wording') ?></label>
								<textarea class="form-control" rows="3" name="wording" maxlength="165" placeholder="<?php echo __('Only 165 Characters'); ?>" minlength="3" required><?php echo !empty($data['post']['wording']) ? $data['post']['wording'] : ''; ?></textarea>
							</div>
							<div class="form-group">
								<label><?php echo __('Palavra-chave') ?></label>
								<textarea class="form-control" rows="3" name="keyword" maxlength="165" placeholder="<?php echo __('Only 165 Characters'); ?>" minlength="3" required><?php echo !empty($data['post']['keyword']) ? $data['post']['keyword'] : ''; ?></textarea>
							</div>
							<?php
								$sl_category = '';
								if(!empty($data['post']['category'])) {
									$sl_category = $data['post']['category'];
								}
								echo Category::select_list('category', 1, $sl_category);
							?>
							<?php
								$sl_category = '';
								if(!empty($data['post']['location'])) {
									$sl_category = $data['post']['location'];
								}
								echo Categoryip::select_list('location', 1, $sl_category);
							?>
							<?php
								$sl_category = '';
								if(!empty($data['post']['thirdparty'])) {
									$sl_category = $data['post']['thirdparty'];
								}
								echo Thirdparty::select_list('thirdparty', 1, $sl_category);
							?>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="partner" value="1"> Partner
								</label>
							</div>
							<div class="row">
								<div class="col-md-6 form-group">
									<label><?php echo __('Título "Baca Juga"') ?></label>
									<input type="text" name="title_bj[]" id="bj1" class="form-control" maxlength="165" placeholder="Only 165 Characters"><br>
									<input type="text" name="title_bj[]" id="bj2" class="form-control" maxlength="165" placeholder="Only 165 Characters">
								</div>
								<div class="col-md-6 form-group">
									<label><?php echo __('Link "Baca Juga"') ?></label>
									<input type="text" name="link_bj[]" id="bj3" class="form-control" placeholder="Link"><br>
									<input type="text" name="link_bj[]" id="bj4" class="form-control" placeholder="Link">
								</div>
								<div class="bacajuga_form"></div>
								<div class="bacajuga_max col-md-12" style="display:none;"><p style="text-align: center;background: rgba(221, 75, 57, 0.55);font-size: large;">Maximum 5</p></div>
								<div class="col-md-6 form-group">
									<button onclick="addBacajuga()" type="button" class="btn btn-warning"><?php echo __('Add'); ?></button>
								</div>
							</div>

							<div class="form-group checkbox">
								<label>
									<input type="checkbox" name="news" value="1"> <b>News</b>
								</label>
							</div>

							<div class="form-group">
								<label><?php echo __('Detalhes') ?></label>
								<textarea id="wysiwyg" class="form-control f_tinymce" rows="15" name="detail" minlength="3" required><?php echo !empty($data['post']['detail']) ? $data['post']['detail'] : '<strong style="color:#999">SariAgri - </strong>&nbsp;'; ?></textarea>
							</div>
							<div class="form-group">
								<label><?php echo __('Tags') ?></label>
                                                                <label style="float: right; cursor: pointer" class="refresh_tags"><?php echo __('( Click Here For Refresh Tags )') ?></label>
								<?php
									$msl_tags = '';
									if(!empty($data['post']['tags'])) {
										$msl_tags = $data['post']['tags'];
									}
									echo Tags::multiple('tags', 1, $msl_tags);
								?>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="sembunyi" value="1"> sembunyikan
								</label>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box box-warning collapsed-box">
							<div class="box-header with-border">
							  <h3 class="box-title">Pooling</h3>
							  <div class="box-tools pull-right">
								<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
							  </div><!-- /.box-tools -->
							</div><!-- /.box-header -->
							<div class="box-body">
								<div id="pooling_form">

								<div class="form-group">

									<input type="text" name="pooling" value="<?php echo !empty($data['post']['pooling']) ? $data['post']['pooling'] : ''; ?>" maxlength="255" placeholder="<?php echo __('Fill in the question'); ?>" class="form-control">

								</div>
								<?php
								if(!empty($data['post']['answers'])):
									$counter=0;
									foreach($data['post']['answers'] as $v):
								?>
									<div class="form-group col-xs-2" >
										<div class="input-group">
										<input type="text" name="answers[]" value="<?php echo $v; ?>" maxlength="255" placeholder="<?php echo __('Answer'); ?>" class="form-control">
										<?php if(!empty($counter)): ?>
											<div class="input-group-addon"><i class="fa fa-times clicked"></i></div>
										<?php endif; ?>
										</div>
									</div>
								<?php
										$counter++;
									endforeach;
								else:
								?>
									<div class="form-group col-xs-2" >
										<div class="input-group">
										<input type="text" name="answers[]" value="" maxlength="255" placeholder="<?php echo __('Answer'); ?>" class="form-control">
										</div>
									</div>
								<?php
								endif;
								?>
							</div><!-- /.box-body -->
							<button onclick="addAnswer()" type="button" class="btn btn-warning"><?php echo __('Add an Answer'); ?></button>
							</div><!-- /.box-body -->
						  </div><!-- /.box -->

						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Guardar os dados'); ?></button>
							<a href="/news"><button type="button" class="btn btn-danger"><?php echo __('Cancelar'); ?></button></a>
						</div>
					</form>
					<!-- /.box-header -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Newsroom News'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Sala de imprensa'); ?></li>
			<li class="active"><a href="/news"><?php echo __('News'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
						<!-- Date range -->
						<div class="form-group">
							<form method="post" id="publishForm">
							<label>Range 
							<select name="publisher" onChange="document.getElementById('publishForm').submit();">
								<option value="artcPublishTime" <?php echo ($data['publish']!='artcPublishTime'?'':'selected'); ?>>Publish</option>
								<option value="artcSaved" <?php echo ($data['publish']!='artcSaved'?'':'selected'); ?>>Saved</option>
							</select> Date
							</label>
							</form>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" name="date_range" required value="<?php echo !empty($_GET['date_range']) ? $_GET['date_range'] : date('d/m/Y') . ' - ' . date('d/m/Y') ; ?>" class="form-control pull-right" id="reservation">
							</div>
							<br/>
							<form method="post" id="catForm">
								<?php echo Category::select_list('category', 1, @$data['category'], "onChange=document.getElementById('catForm').submit();
", 'lite'); ?>
								<div style="width: 200px;float: left;">
								<select name="uploader" class="form-control" onChange="document.getElementById('catForm').submit();
">
									<option value="">--Choose Uploader--</option>
									<?php
									foreach($data['all_user'] as $v_list) {
										$uploader_name = @$data['uploader'];
										if($uploader_name==$v_list['id']) $uploader_name='selected';
										echo "<option value='{$v_list['id']}' {$uploader_name}>{$v_list['name']}</option>";
									}
									?>
								</select>
								</div>
								
								<div style="width: 200px;float: left;">
								<select name="version" class="form-control" onChange="document.getElementById('catForm').submit();
">
									<option value="" <?php echo (empty($data['version'])?'selected':''); ?>>Full version</option>
									<option value="lite" <?php echo ($data['version']=='lite'?'selected':''); ?>>Lite version</option>									
								</select>
								</div>
								
							</form>
							<!-- /.input group -->
						</div>
					</div>
					<div class="box-body">
						<a href="/news/new"><button type="button" style="width:150px" class="btn btn-success btn-sm"><?php echo __('Add New Data'); ?></button></a>
						<?php
						if(
							$data['session_user_id'] == 1 OR
							$data['session_user_id'] == 15 OR
							$data['session_user_id'] == 17 OR
							$data['session_user_id'] == 18 OR
							$data['session_user_id'] == 22 OR
							$data['session_user_id'] == 32
							) {
							?>
							<a href="javascriot:void(0)" onclick="doExcel()"><button type="button" style="width:150px" class="btn btn-warning btn-sm"><?php echo __('Convert Data To Excel'); ?></button></a>
							<?php
						}
						?>
					</div>
				</div>
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $data['count_all'] . ' ' .__('Dados encontrados'); ?></h3>
						<div class="box-tools" style="float: right;">
						<form method="post" action="/news/search?date_range=<?php echo !empty($data['date_range']) ? $data['date_range'] : ''; ?>">
							
								<div class="input-group" style="width: 150px;">
									<input type="text" name="search" class="form-control input-sm pull-right" placeholder="<?php echo __('Search'); ?>" value="<?php echo !empty($data['search']) ? $data['search'] : ''; ?>">
                                                                        <!-- <input type="hidden" name="date_range" value="<?php echo !empty($_GET['date_range']) ? $_GET['date_range'] : ''; ?>" /> -->
                                                                        <div class="input-group-btn">
										<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /.box-header -->
					<?php 
					echo !empty($data['pagination']) ? $data['pagination'] : '';
					if($data['version']=='lite'){
						include('lite_template.php');
					}else{
						include('full_template.php');
					}
					echo !empty($data['pagination']) ? $data['pagination'] : ''; 
					?>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php

// Route delete
Route::set('witness_child', 'witness/child(/<parent_id>)')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'child',
	));

// Route delete
Route::set('witness_delete', 'witness/delete(/<id>)')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'delete',
	));
	
// Route detail
Route::set('witness_detail', 'witness/detail(/<id>)')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'detail',
	));

// Route edit
Route::set('witness_edit', 'witness/edit(/<id>)')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'edit',
	));

// Route index
Route::set('witness_index', 'witness/index(/<page>)')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'index',
	));
	
// Route new
Route::set('witness_new', 'witness/new')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'new',
	));
	
// Route Save
Route::set('witness_save', 'witness/save')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'save',
	));

// Route search
Route::set('witness_search', 'witness/search(/<page>)')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'search',
	));
	
// Route Update
Route::set('witness_update', 'witness/update')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'update',
	));
	
// Route About
Route::set('witness_about', 'witness/about')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'about',
	));
	
// Route Insert Video
Route::set('witness_insertvideo', 'witness/insertvideo')
	->defaults(array(
		'controller' => 'witness',
		'action'     => 'insertvideo',
	));
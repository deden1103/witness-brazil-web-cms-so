<?php defined('SYSPATH') or die('No direct script access.');

class Model_Komunitas extends Model {
	
	public function count_all() {
		
		$return = 0;
		
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('komunitas')
					->where('komtStatus','=',1)
					->execute()
					->as_array();
		
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function list_data($limit = '', $offset = '') {
		
		$exec = DB::select(
					array('komtId', 'id'),
					array('komtName', 'name'),
					array('komtSaved', 'saved'),
					array('komtStatus', 'status')
				)
				->from('komunitas')
				->where('komtStatus','=',1)
				->order_by('komtId', 'DESC')
				->limit($limit)
				->offset($offset)
				->execute()
				->as_array();
		
		return $exec;
		
	}
	
	public function save_data($data = array(), $user_id = '') {
		
		$query =	DB::insert('komunitas', array(
						'komtName',
						'komtAdmiIdSaved'
					))
					->values(array(
						$data['name'],
						$user_id
					))
					->execute();
		return $query;
	}
	
	public function data_by_id($id = '') {
		
		$return  = array();
		
		$exec = DB::select(
					array('komtId', 'id'),
					array('komtName', 'name')
				)
				->from('komunitas')
				->where('komtStatus','=',1)
				->where('komtId','=',$id)
				->execute()
				->as_array();
		
		if(!empty($exec[0])) {
			$return = $exec[0];
		}
		
		return $return;
		
	}
	
	public function update_data($id = '', $data = '') {
		
		$query =	DB::update('komunitas')
						->set(array('komtName' => $data['name']))
						->where('komtId', '=', $id)
						->execute();
		
	}
	
	public function delete_data($id = '') {
		$query =	DB::update('komunitas')
					->set(array('komtStatus' => 0))
					->where('komtId', '=', $id)
					->execute();
	}
	
	public function count_search_data($search = '') {
		
		$return = 0;
		
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('komunitas')
					->where('komtStatus','=',1)
					->where('komunitas.komtName', 'LIKE', '%' . $search . '%')
					->execute()
					->as_array();
		
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function search_data($search = '', $limit = '', $offset = '') {
		
		
		$exec = DB::select(
					array('komtId', 'id'),
					array('komtName', 'name'),
					array('komtSaved', 'saved'),
					array('komtStatus', 'status')
				)
				->from('komunitas')
				->where('komtStatus','=',1)
				->where('komunitas.komtName', 'LIKE', '%' . $search . '%')
				->order_by('komtId', 'DESC')
				->limit($limit)
				->offset($offset)
				->execute()
				->as_array();
		
		return $exec;
		
	}
	
}
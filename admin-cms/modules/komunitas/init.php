<?php

// Route List
Route::set('komunitaslist', 'komunitas/list(/<page>)')
	->defaults(array(
		'controller' => 'komunitas',
		'action'     => 'list',
	));

// Route Search
Route::set('komunitassearch', 'komunitas/search(/<search>(/<page>))')
	->defaults(array(
		'controller' => 'komunitas',
		'action'     => 'search',
	));
	
// Route input
Route::set('komunitasinput', 'komunitas/new(/<action>(/<page>))')
	->defaults(array(
		'controller' => 'komunitas',
		'action'     => 'new',
	));
	
// Route Edit
Route::set('komunitasedit', 'komunitas/edit(/<id>)')
	->defaults(array(
		'controller' => 'komunitas',
		'action'     => 'edit',
	));
	
// Route Cahnge Password
Route::set('komunitascpas', 'komunitas/cpas(/<id>)')
	->defaults(array(
		'controller' => 'komunitas',
		'action'     => 'cpas',
	));
	
// Route Delete
Route::set('komunitasdelete', 'komunitas/delete(/<id>)')
	->defaults(array(
		'controller' => 'komunitas',
		'action'     => 'delete',
	));
	
// Route default
Route::set('komunitas', 'komunitas(/<action>(/<page>))')
	->defaults(array(
		'controller' => 'komunitas',
		'action'     => 'index',
	));
<?php defined('SYSPATH') or die('No direct script access.');

class Model_Hot extends Model {

	public function count_search($date1 = '', $date2 = '', $search = '') {
		$session = Session::instance();
		$publish = $session->get("publish_video", "artcPublishTime");
		$return = 0;
		//print_r($publish .' '. $date1 .' sampai '. $date2); exit;
		
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('article');
		
		if (!empty($date1) and !empty($date2)){
			$query = $query->where(DB::expr('DATE('.$publish.')'), '>=', $date1)
						->where(DB::expr('DATE('.$publish.')'), '<=', $date2);
		}
		
		$ex_search = explode(',', $search);
		foreach($ex_search as $v_search) {
			$query = $query->where('artcTitle', 'LIKE', '%' . trim($v_search) . '%');
		}
		
		$category = $session->get("category_video", "");
		if(!empty($category)){
			$query = $query->where('artcMscvId','=', $category);
		}
		
		// echo Debug::vars((string) $query);
		$query = $query->order_by('artcId', 'desc')
					->where('artcStatus','!=', 0)
					->execute()
					->as_array();
					
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function list_search($date1 = '', $date2 = '', $search = '', $limit = '', $offset = '') {
		$session = Session::instance();
		$publish = $session->get("publish_video", "artcPublishTime");
		
		$exec = DB::select(
					array('artcId', 'id'),
					array('artcTitle', 'title'),
					array('artcExcerpt', 'description'),
					array('artcEmbedURL', 'embed_url'),
					array('artcPublishTime', 'publish'),
					array('artcSaved', 'saved'),
					array('artcUkomIdSaved', 'savedId'),
					array('artcStatus', 'status'),
					array('artcMscvId', 'category_id'),
					array('mscvName', 'category_name'),
					array('admiRealName', 'user_name'),
					array('ukomName', 'ukom_name'),
					array('admiLvl', 'level'),
					array('artcKeyword','keyword_id'),
					array('artcMediaId','media'),
					array('artcMediaFileType','type'),
					array('artcUserIdApproved','userId')
					
				)
				->from('article')
				->join('admin', 'LEFT')
				->on('article.artcUserIdApproved', '=', 'admin.admiId')
				->join('user_komunitas', 'LEFT')
				->on('article.artcUkomIdSaved', '=', 'user_komunitas.ukomId')
				->join('master_category_video', 'LEFT')
				->on('article.artcMscvId', '=', 'master_category_video.mscvId');

		if (!empty($date1) and !empty($date2)){
			$exec = $exec->where(DB::expr('DATE('.$publish.')'), '>=', $date1)
						->where(DB::expr('DATE('.$publish.')'), '<=', $date2);
		}


		$exec = $exec->where('artcTitle', 'LIKE', '%' . $search . '%')
				->order_by('artcId', 'desc')
				->where('artcStatus','!=', 0);
				
		$ex_search = explode(',', $search);
		foreach($ex_search as $v_search) {
			$exec = $exec->where('artcTitle', 'LIKE', '%' . trim($v_search) . '%');
		}
		
		$category = $session->get("category_video", "");
		//print_r($category); exit;
		if(!empty($category)){
			$exec = $exec->where('artcMscvId','=', $category);
		}
		
		$exec = $exec->limit($limit)
				->offset($offset)
				->execute()
				->as_array();
				
		return $exec;
		
	}

	public function save_data($data = '', $user_id = '') {
		// Start Transaction
		Database::instance()->begin();
		
		$publish_time = date('Y-m-d H:i:s'); // Default data now
		if(!empty($data['publish_time'])) {
			$publish_time = DateTime::createFromFormat('d/m/Y H:i:s', $data['publish_time']);
			$publish_time = $publish_time->format('Y-m-d H:i:s');
			
			$saved_time = date('Y-m-d H:i:s');
		} else {
			$saved_time = $publish_time;
		}

		$keyword = serialize($data['keyword']);
		$video 		=	DB::insert('article', array(
						'artcTitle',
						'artcExcerpt',
						'artcDetail',
						'artcKeyword',
						'artcKeywordAlt',
						'artcMscvId', // Categpry or Kanal
						'artcEmbedURL',
						'artcPublishTime',
						'artcSaved',
						'artcAdmiIdSaved'
					))
					->values(array(
						$data['title'],
						$data['description'],
						$data['detail'],
						$keyword,
						$data['keyword_alt'],
						$data['category'],
						$data['embed_url'],
						$publish_time,
						$saved_time,
						$user_id
					));
		
		list($lastid_video_1, $rows_inserted_1) = $video->execute();

		if(!empty($lastid_video_1)) {
			// Save article images
			if(!empty($data['image'])) {
				$explode_id_image = explode("/", $data['image']);
				
				$id_image_2 = array();
				foreach($explode_id_image as $k_id_img=>$v_id_img){
					if(is_numeric(($v_id_img))){
						$id_image_2_arr = $v_id_img;
					} else {
						$id_image_2_arr = '';
					}

					array_push($id_image_2, $id_image_2_arr);
				} 
				$id_image_2_implode = implode("", $id_image_2);
					
				// Get id image from full url image
				$base_image = basename($data['image']).PHP_EOL;
				
				if(!empty($base_image)) {
					$ex_base = explode('_', $base_image);
					list($id_image, $file_type) = $ex_base;
				}

				if(!empty($id_image)) { 
					$image = 	DB::insert('video_image', array(
									'vidVdeoId',
									'vidArimId',
									'vidAdmiIdSaved'
								))
								->values(array(
									$lastid_video_1,
									$id_image_2_implode,
									$user_id
								));
					$add_image_1 = $image->execute();
				}
			}
		}
			

			// Start Transaction for portal database
			Database::instance('portal')->begin();
			
			list($lastid_video_2, $rows_inserted_2) = $video->execute('portal');
			if(!empty($lastid_video_2)) {
				// Save article images
				if(!empty($data['image'])) {
					$explode_id_image = explode("/", $data['image']);
				
					$id_image_2 = array();
					foreach($explode_id_image as $k_id_img=>$v_id_img){
						if(is_numeric(($v_id_img))){
							$id_image_2_arr = $v_id_img;
						} else {
							$id_image_2_arr = '';
						}

						array_push($id_image_2, $id_image_2_arr);
					} 
					$id_image_2_implode = implode("", $id_image_2);

					// Get id image from full url image
					$base_image = basename($data['image']).PHP_EOL;
					if(!empty($base_image)) {
						$ex_base = explode('_', $base_image);
						list($id_image, $file_type) = $ex_base;
					}

					if(!empty($id_image)) {
						$image = 	DB::insert('video_image', array(
										'vidVdeoId',
										'vidArimId',
										'vidAdmiIdSaved'
									))
									->values(array(
										$lastid_video_2,
										//$id_image_2,
										$id_image_2_implode,
										$user_id
									));
						$add_image_2 = $image->execute('portal');
					}
				}
			}

			if($add_image_1 and $add_image_2){
				// Commit transaction
				Database::instance()->commit();
				Database::instance('portal')->commit();
			}
	}
	
	public function detail_data($id = '') {
		
		$return = '';
		
		$exec = DB::select(
					array('artcId', 'id'),
					array('artcTitle', 'title'),
					array('artcExcerpt', 'description'),
					array('artcEmbedURL', 'embed_url'),
					array('artcPublishTime', 'publish_time'),
					array('artcSaved', 'saved'),
					array('artcUkomIdSaved', 'savedId'),
					array('artcStatus', 'status'),
					array('artcMscvId', 'category_id'),
					array('mscvName', 'category_name'),
					array('artcDetail', 'detail'),
					array('artcKeyword', 'keyword_id'),
					array('artcMediaId', 'media'),
					array('ukomName', 'ukom_name'),
					array('artcMediaFileType', 'type')
					

					
				)
				->from('article')
				->join('user_komunitas', 'LEFT')
				->on('article.artcUkomIdSaved', '=', 'user_komunitas.ukomId')
				->join('master_category_video', 'LEFT')
				->on('article.artcMscvId', '=', 'master_category_video.mscvId')
				->where('artcId', '=', $id)
				->execute()
				->as_array();
			
		if(!empty($exec[0])) {
			
			$return = $exec[0];
			
		}
		
		return $return;
		
	}
	
	public function update_data($data = '', $user_id = '', $action='') {
		// Id video
		$id_video = $data['id'];
		
		// Start Transaction
		Database::instance()->begin();

		

		// Update date video
		$publish_time = date('Y-m-d H:i:s'); // Default data now
		if(!empty($data['publish_time'])) {
			$publish_time = DateTime::createFromFormat('d/m/Y H:i:s', $data['publish_time']);
			$publish_time = $publish_time->format('Y-m-d H:i:s');
			
			$saved_time = date('Y-m-d H:i:s');
		} else {
			$saved_time = $publish_time;
		}
		
		if(!empty($data['finish_time'])) {
			$finish_time = DateTime::createFromFormat('d/m/Y H:i:s', $data['finish_time']);
			$finish_time = $finish_time->format('Y-m-d H:i:s');
		}

		if(!empty($data['keyword'])){
			$keyword = serialize($data['keyword']);
		}
		
		$update = DB::update('article')
			->set(array('artcTitle' => $data['title']))
			->set(array('artcExcerpt' => $data['description']))
			->set(array('artcDetail' => $data['detail']))
			->set(array('artcKeyword' => $keyword))
			->set(array('artcMscvId' => $data['category']))
			->set(array('artcPublishTime' => $publish_time))
			->set(array('artcSaved' => $saved_time));
			
			
		$update_1 = $update->where('artcId', '=', $id_video)->execute();
		
		// Commit transaction
		Database::instance()->commit();
		
	}
	
	public function delete_data($id = '') {
		
		DB::update('article')
			->set(array('artcStatus' => 0))
			->where('artcId', '=', $id)
			->execute();

	
		
	}
	public function add_category($data){
		$session = Session::instance();
		$save_keyword = DB::insert('master_hot_topic', array(
			'topicName',
			'topicAdminIdSave'
		))
		->values(array(
			$data['name'],
			$session->get('adminId')
		));
		$exec_cms = $save_keyword->execute();
	}

	public function delete_category($id){
		//Database::instance()->begin();	
		//Database::instance()->commit();
		Database::instance()->begin();
		$session = Session::instance();

			DB::delete('master_hot_topic')
				->where('hotTopicId', '=', $id)
				->execute();
				
			// $query = DB::update('master_hot_topic')
			// 		->set(array('topicStatus' => 0))
			// 		->where('hotTopicId', '=', $id);
			// $del_1 = $query->execute();		
			Database::instance()->commit();	
			return "Success";
			

	}

	public function add_keyword($data){
		$session = Session::instance();
		$save_keyword = DB::insert('master_keyword_video', array(
			'vkeyName',
			'vkeyAdmiId'
		))
		->values(array(
			$data['name'],
			$session->get('adminId')
		));
		$exec_cms = $save_keyword->execute();
	}

	public function update_keyword($data){
		Database::instance()->begin();
		$session = Session::instance();
			$query = DB::update('master_keyword_video')
					->set(array('vkeyName' => $data['name']))
					->set(array('vkeyAdmiId' => $session->get('adminId')))
					->where('vkeyId', '=', $data['id']);
			$update_1 = $query->execute();		
		Database::instance()->commit();	
		return "Success";
			
	}

	public function update_category($data){
		Database::instance()->begin();
		$session = Session::instance();
			$query = DB::update('master_hot_topic')
					->set(array('topicName' => $data['name']))
					->set(array('topicAdminIdSave' => $session->get('adminId')))
					->where('hotTopicId', '=', $data['id']);
			$update_1 = $query->execute();		
		Database::instance()->commit();	
		return "Success";
			
	}


	public function delete_keyword($id){
		Database::instance()->begin();
		$session = Session::instance();
			$query = DB::update('master_hot_topic')
					->set(array('topicStatus' => 0))
					->where('hotTopicId', '=', $id);
			$del_1 = $query->execute();		
		Database::instance()->commit();	
		return "Success";
			
	}

	public function publish_video($id_video, $cat_table, $data, $adminId,$videourl){
		Database::instance()->begin();

		$now = date('Y-m-d H:i:s');
		$key = array_keys($data);
		$val = array_values($data);
			
			$update = DB::update('article')
						->set(array('artcStatus' => '2'))
						->set(array('artcEmbedURL' => $videourl))
						->set(array('artcPublishTime' => $now))
						->set(array('artcUserIdApproved' => $adminId))
						->where('artcId', '=', $id_video);
			
			$update1 = $update->execute();
			$status = ' ,update article = '.$update1;

		// }
		// Commit transaction		
		if($update1){
			Database::instance()->commit();	
			return $status;
		}
	
	}

	public function unpublish_video($id_video, $cat_table){
		Database::instance()->begin();
			// //delete video category
			// $delete_cat_video =  DB::delete('video_cat_'.$cat_table)
			// 						->where('vdeoId', '=', $id_video)
			// 						->execute();
			// if($delete_cat_video){
			// 	//delete video front
			// 	$delete_front_video =  DB::delete('video_front_page')
			// 						->where('vdeoId', '=', $id_video)
			// 						->execute();
			// }		
			
			// if($delete_front_video){
				//update video
				$update = DB::update('article')
				->set(array('artcStatus' => '1'))
				->set(array('artcUserIdApproved' => null));
				
				$update_1 = $update->where('artcId', '=', $id_video)->execute();
			
			// }

			if($update){
				Database::instance()->commit();	
				return $update;
			}
		
	}
	public function data_chat($table = '', $artcId = ''){
	$exec = DB::select(
		array('admin.admiId', 'admiId'),
		array('user_komunitas.ukomName', 'ukom_name'),
		array('admin.admiRealName', 'admin_name'),
		array('chatFilteredMessage', 'text'),
		array('chatSaved', 'saved')
	)
	->from($table)

	->where('chatArtcId','=',$artcId)
	->join('user_komunitas', 'LEFT')
	->on('chat.chatUkomId', '=', 'user_komunitas.ukomId')
	->join('admin', 'LEFT')
	->on('chat.chatAdmiId', '=', 'admin.admiId')
	->order_by('chatsaved', 'Asc')
	->execute()
	->as_array();

	
	$arr_bulan = array(
		'',
		'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);

	if(!empty($exec)) {
		foreach($exec as $k_list => $v_list) {
			$exec[$k_list]['indo_saved'] = date('d', strtotime($v_list['saved'])) . ' ' . $arr_bulan[date('n', strtotime($v_list['saved']))] . ' ' . date('Y H:i', strtotime($v_list['saved']));
		}
	}

	return $exec;
	}
}
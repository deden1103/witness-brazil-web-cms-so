
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo __('Edit Keyword'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/home"><?php echo __('Painel de controlo'); ?></a></li>
			<li><a href="/laporan"><?php echo __('Relatório'); ?></a></li>
			<li><a href="#"><?php echo __('Config'); ?></a></li>
			<li class="active"><a href="#"><?php echo __('Edit Keyword'); ?></a></li>
		</ol>
	</section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-body">
                    <form action="<?php echo URL::Base(); ?>laporan/update_keyword" method="post">
                        <div class="form-group">
                            <label><?php echo __('Palavra-chave Name') ?></label>
                            <input autofocus type="text" name="name" class="form-control" value="<?php echo $data['ctg_keyword'][0]['name']; ?>" required>
                            <input type="hidden" name="id" value="<?php echo $data['ctg_keyword'][0]['id']; ?>">
                        </div>
                    </div>
                    <div class="box-footer">
							<button type="submit" name="save" value="update" class="btn btn-primary"><?php echo __('Guardar'); ?></button>
                            <a href="/laporan/keyword" class="btn btn-danger">Cancel</a>
						</div>
					</form>
                </div>
            </div>
        </div>
    </section>
</div>  

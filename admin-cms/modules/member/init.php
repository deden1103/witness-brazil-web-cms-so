<?php

// Route List
Route::set('memberlist', 'member/list(/<page>)')
	->defaults(array(
		'controller' => 'Members',
		'action'     => 'list',
	));

// Route Edit
Route::set('memberedit', 'member/edit(/<id>)')
	->defaults(array(
		'controller' => 'Members',
		'action'     => 'edit',
	));

//Route update
Route::set('membeupdate', 'member/update(/<id>)')
	->defaults(array(
		'controller' => 'Members',
		'action'     => 'update',
	));	

Route::set('memberdelete', 'member/delete(/<id>)')
	->defaults(array(
		'controller' => 'Members',
		'action'     => 'delete',
	));

Route::set('member', 'member(/<action>(/<page>))')
	->defaults(array(
		'controller' => 'Members',
		'action'     => 'index',
	));


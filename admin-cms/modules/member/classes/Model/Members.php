<?php defined('SYSPATH') or die('No direct script access.');

class Model_Members extends Model {
	
	public function count_all() {
		
		$return = 0;
		
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('user')
					//->where('admiStatus','=',1)
					->execute()
					->as_array();
		
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function list_data($limit = '', $offset = '') {
		
		$exec = DB::select(
					array('userId', 'id'),
					array('userFullName', 'name'),
					array('userEmail', 'email')
				)
				->from('user')
				// ->where('admiStatus','=',1)
				->order_by('userId', 'ASC')
				->limit($limit)
				->offset($offset)
				->execute()
				->as_array();
		
		return $exec;
		
	}
	
	public function save_data($data = array()) {
		$query =	DB::insert('admin', array(
						'admiRealName',
						'admiEmail',
						'admiPassword'
					))
					->values(array(
						$data['name'],
						$data['email'],
						SHA1($data['password'])))
					->execute();
		return $query;
	}
	
	public function data_by_id($id = '') {
		
		$return  = array();
		
		$exec = DB::select(
					array('userId', 'id'),
					array('userFullName', 'name'),
					array('userEmail', 'email'),
					array('userPassword', 'password')
				)
				->from('user')
				->where('userId', '=', $id)
				->execute()
				->as_array();
		if(!empty($exec[0])) {
			$return = $exec[0];
		}
		
		return $return;
		
	}
	
	public function update_data($id = '', $data = '') {
		$query =	DB::update('user')
					->set(array('userFullName' => $data['name']))
					->set(array('userEmail' => $data['email']))
					->where('userId', '=', $id)
					->execute();
	}
	
	public function delete_data($id = '') {
		// $query =	DB::update('user')
		// 			->set(array('admiStatus' => 0))
		// 			->where('admiId', '=', $id)
		// 			->execute();
		DB::delete('user')
				->where('userId', '=', $id)
				->execute();
	}
	
	public function count_search_data($search = '') {
		
		$return = 0;
		
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('admin')
					->where('admiRealName', 'LIKE', '%' . $search . '%')
					->execute()
					->as_array();
		
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function search_data($search = '', $limit = '', $offset = '') {
		
		$exec = DB::select(
					array('admiId', 'id'),
					array('admiRealName', 'name'),
					array('admiEmail', 'email')
				)
				->from('admin')
				->where('admiRealName', 'LIKE', '%' . $search . '%')
				->order_by('admiRealName', 'ASC')
				->limit($limit)
				->offset($offset)
				->execute()
				->as_array();
		
		return $exec;
		
	}
	
	public function change_password($id = '', $password = '') {
		$query =	DB::update('admin')
					->set(
						array('admiPassword' => SHA1($password))
					)
					->where('admiId', '=', $id)
					->execute();
	}

}
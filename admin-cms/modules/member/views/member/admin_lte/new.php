<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo __('Usuários'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Usuários'); ?></li>
			<li class="active"><a href="/"><?php echo __('Novo'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Novo Usuários'); ?></h3>
					</div>
					<form role="form" method="post" action="<?php echo URL::Base(); ?>users/submit">
						<div class="box-body">
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Nome') ?></label>
								<input type="text" name="name" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Email') ?></label>
								<input type="text" name="email" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Password') ?></label>
								<input type="password" name="password" class="form-control" minlength="8" required>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Guardar'); ?></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
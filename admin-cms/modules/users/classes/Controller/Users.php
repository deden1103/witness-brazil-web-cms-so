<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Users extends Controller_Backend {

	private $users_model = null;
	private $global_model = null;
	
	public function before() {
		parent::before();

		$level = Controller_Backend::check_level();
		if($level == 2){
			$this->redirect(URL::Base().'/keranjang');
		}elseif($level == 3){
			$this->redirect(URL::Base().'/home');
			
		}

		$this->users_model = new Model_Users();
		$this->global_model = new Model_Globalmodel();
		
		$this->custom_header = '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.5/dist/sweetalert2.min.css">';

		$this->custom_footer =  '<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.5/dist/sweetalert2.all.min.js"></script>';
		$this->custom_footer .=  '
		<script>
			function delUsr(swal_id){
				swal({
					title: "Are you sure?",
					text: "You won\'t be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3085d6",
					cancelButtonColor: "#d33",
					confirmButtonText: "Yes, delete it!"
				  }).then((result) => {
					if (result.value) {
						$.ajax({
							url: "/users/delete/",
							dataType: "JSON",
							type: "POST",
							data: {swal_id : swal_id}, 
							success: function(res){
								$("#row"+swal_id).remove()
								swal(
									"Deleted!",
									"Your file has been deleted.",
									"success"
								  )
							},error: function(){
								swal(
									"Something Wrong Happen !!",
									"",
									"error"
								  )
							}
						})
					}
				  })
			}
		</script>';
	}

	public function action_index() {
		$this->redirect('/users/list');
	}
	
	public function action_new() {
		$data['main_title'] = "Users New";
		$data['menu_active'] = "users";
		$data['menu_active_child'] = "new";
		$view = Briliant::admin_template('users/' . Kohana::$config->load('path.main_template') . '/new', $data);
		$this->response->body($view);
	}
	
	public function action_submit() {
		
		// Load Model Users
		$result = $this->users_model->save_data($this->request->post());
		$this->redirect('/users/list');
	}
	
	public function action_list() {
		
		// Page from parameter
		$page = intval($this->request->param('page'));
		if(empty($page)) {
			$page = 1;
		}
		
		$data['main_title'] = "Lista de usuários";
		$data['menu_active'] = "users";
		$data['menu_active_child'] = "list";
		
		$data['count_all'] = $this->users_model->count_all();
		
		// Pagination
		$pagination = Pagination::factory(array(
							'total_items'    		=> $data['count_all'],
							'items_per_page'		=> 20,
							'current_page'			=> $page,
							'base_url'				=> '/users/list/',
							'view'					=> 'pagination/admin'
						));
				
		$select = ['admiId as id','admiRealName as name','admiEmail as email','admiLvl as level'];
		$data['list'] = $this->global_model->select(
			'admin', //table
			$select, //select
			['admiStatus' => '1'], //where
			$pagination->items_per_page, //limit
			$pagination->offset, //offset
			['admiId' => 'DESC'] //order
		);
		
		$data['pagination'] =  $pagination->render();
		
		$data['custom_header'] = $this->custom_header;
		$data['custom_footer'] = $this->custom_footer;
		
		
		$view = Briliant::admin_template('users/' . Kohana::$config->load('path.main_template') . '/list', $data);
		$this->response->body($view);
	}

	public function action_search() {
		
		$data['main_title'] = "Lista de usuários";
		$data['menu_active'] = 'users';
		$data['menu_active_child'] = 'list';
		
		// Redirect jika mengepost search agar bisa menggunakan paginasi dengan beauty url
		$post_search = $this->request->post('search');
		if(!empty($post_search)) {
			$this->redirect('/users/search/' . $post_search);
		}
		
		// Param search
		$search = $this->request->param('search');
		
		// Param page
		$page = intval($this->request->param('page'));
		if(empty($page)) {
			$page = 1;
		}
		
		
		// Count All Data
		$data['count_all'] = $this->users_model->count_search_data($search);
		
		$_search = (!empty($search)) ? '/users/search/' . $search . '/' : "/users/search/";
		if (!empty($search)) {
			$_search = '/users/search/' . $search . '/';
			$where = ['admiRealName LIKE' => '%'.$search.'%','admiStatus' => '1'];
		}else{
			$_search = '/users/list/';
			$where = ['admiStatus' => '1'];
		}
		
		// Pagination
		$pagination = 	Pagination::factory(array(
							'total_items'    		=> $data['count_all'],
							'items_per_page'		=> 20,
							'current_page'			=> $page,
							'base_url'				=> $_search,
							'view'					=> 'pagination/admin'
						));
					
		$select = ['admiId as id','admiRealName as name','admiEmail as email','admiLvl as level'];
		$data['list'] = $this->global_model->select(
			'admin', //table
			$select, //select
			$where, //where
			$pagination->items_per_page, //limit 
			$pagination->offset, //offset
			['admiId' => 'DESC'] //order
		);
		
		$data['search'] = $search;
		$data['pagination'] =  $pagination->render();
	
		$data['custom_header'] = $this->custom_header;
		$data['custom_footer'] = $this->custom_footer;	
		
		$view = Briliant::admin_template('users/' . Kohana::$config->load('path.main_template') . '/list', $data);
		$this->response->body($view);
		
	}
	
	public function action_edit() {
		
		// Id from parameter
		$id = intval($this->request->param('id'));
		

		$select = ['admiId as id','admiRealName as name','admiEmail as email','admiLvl as level','phone as phone,komunitasId as komunitasId'];
		$where = ['admiId' => $id, 'admiStatus' => 1];
		$data['list'] = $this->global_model->select('admin', $select, $where);
		
		if(empty($data['list'])){
			$this->redirect('/users/list');
		}

		$data['main_title'] = "Users Edit";
		$data['menu_active'] = "users";
		$data['menu_active_child'] = "edit";
		
		$view = Briliant::admin_template('users/' . Kohana::$config->load('path.main_template') . '/edit', $data);
		$this->response->body($view);
	}
	
	public function action_update() {
		
		$id = $this->request->post('id');
		// print_r($this->request->post());die;
		$update = $this->users_model->update_data($id, $this->request->post());
		
		$this->redirect('/users/list');
	}
	
	public function action_cpas() {
		
		// Id from parameter
		$id = intval($this->request->param('id'));
		$data = $this->users_model->data_by_id($id);
		
		$data['main_title'] = "Users Edit";
		$data['menu_active'] = "users";
		$data['menu_active_child'] = "edit";
		
		$view = Briliant::admin_template('users/' . Kohana::$config->load('path.main_template') . '/cpas', $data);
		$this->response->body($view);
	}
	
	public function action_cpas_submit(){
		// Id from parameter
		$id = intval($this->request->post('id'));
		
		 // Load Model Users
		$data = $this->users_model->data_by_id($id);
		
		$data['main_title'] = "Users Edit";
		$data['menu_active'] = "users";
		$data['menu_active_child'] = "edit";
		
        $validation = Validation::factory($this->request->post())
                        ->rule('password', 'not_empty')
                        ->rule('retype_password', 'not_empty')
                        ->rule('retype_password',  'matches', array(':validation', 'retype_password', 'password'));
        
        if($validation->check()) {
            
           
			$password = $this->request->post('password');
                
			// Change password
			$this->users_model->change_password($id, $this->request->post('password'));
			
			$data['success'] = 1;
                
        } else { // Validation Error
            
            $data['errors'] = $validation->errors('validation');
            
        }

		$view = Briliant::admin_template('users/' . Kohana::$config->load('path.main_template') . '/cpas', $data);
		$this->response->body($view);
	}
	
	public function action_delete() {
		if(isset($_POST["swal_id"])){
			$id = $_POST['swal_id'];
			$update = $this->users_model->delete_data($id);
			if ($update == 1) {
				echo json_encode($update);
			}else{
				http_response_code(500);
			}
		}
	}
	
}

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo __('Usuários'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Usuários'); ?></li>
			<li class="active"><a href="#"><?php echo __('Editar'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Usuários Editar'); ?></h3>
					</div>
					<form role="form" method="post" action="<?php echo URL::Base(); ?>users/update">
						<div class="box-body">
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Name <font color="red">*</font>') ?></label>
								<input type="text" name="name" class="form-control" value="<?php echo $data['list'][0]['name']; ?>" required>
								<input type="hidden" name="id" value="<?php echo $data['list'][0]['id']; ?>">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Email <font color="red">*</font>') ?></label>
								<input type="text" name="email" class="form-control" value="<?php echo $data['list'][0]['email']; ?>" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Nível <font color="red">*</font>') ?></label>
								<select name="level" id="level" class="form-control" required>
									<option disabled>--Escolher o nível de acessos--</option>
									<option <?= $selected = ($data['list'][0]['level'] == 1) ? "selected" : "" ?> value="1">Admin</option>
									<option <?= $selected = ($data['list'][0]['level'] == 2) ? "selected" : "" ?> value="2">Redaksi</option>
									<option <?= $selected = ($data['list'][0]['level'] == 3) ? "selected" : "" ?> value="3">Editor</option>
									<option <?= $selected = ($data['list'][0]['level'] == 4) ? "selected" : "" ?> value="4">Admin Aceh</option>
								</select>
							</div>
							<font color="red">*) Requerido</font>


							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Telefone') ?></label>
								<input type="text" name="phone" class="form-control" value="<?php echo $data['list'][0]['phone']; ?>" required>
							</div>

							
							<?php 
								$komunitas_id = '';
								if(!empty($data['list'][0]['komunitasId'])) {
									$komunitas_id = $data['list'][0]['komunitasId'];
								}
								echo Userkom::select_list_komunitas_editor('komunitas_id', 1, $komunitas_id); 
							?>



							<div class="form-group">
								<label for="jjj"><?php echo __('Ativar notificações') ?></label>
								<select name="notif" id="notif" class="form-control">
									<option value="1">Sim</option>
									<option value="0">Não</option>
								</select>
							</div>
						

							<!-- <div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Community (As Editor)') ?></label>
								<select name="komunitasId" id="komunitasId" class="form-control">
									<option disabled>--Pilih Komunitas (Sebagai Editor)--</option>
									
								</select>
							</div> -->


						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Guardar'); ?></button>
							<a href="<?php echo URL::Base(); ?>users/list" class="btn btn-danger"><?php echo __('Cancelar'); ?></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
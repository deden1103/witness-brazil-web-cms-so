<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo __('Usuários'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Usuários'); ?></li>
			<li class="active"><a href="/"><?php echo __('Novo'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Novo Usuários'); ?></h3>
					</div>
					<form role="form" method="post" action="<?php echo URL::Base(); ?>users/submit">
						<div class="box-body">
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Name <font color="red">*</font>') ?></label>
								<input type="text" name="name" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Email <font color="red">*</font>') ?></label>
								<input type="text" name="email" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Nível <font color="red">*</font>') ?></label>
								<select name="level" id="level" class="form-control" required>
									<option selected disabled>--Escolher o nível de acesso--</option>
									<option value="1">Admin</option>
									<option value="2">Editorial</option>
									<option value="3">Editor</option>
									<option value="4">Admin Aceh</option>
								</select>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Password <font color="red">*</font>') ?></label>
								<input type="password" name="password" class="form-control" minlength="8" required>
							</div>
							<font color="red">*) Requerido</font>


							
							


							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Telefone') ?></label>
								<input type="text" name="phone" class="form-control" value="" required>
							</div>

							<?php 
								$komunitas_id = '';
								
								echo Userkom::select_list_komunitas_editor('komunitas_id', 1, $komunitas_id); 
							?>



							<div class="form-group">
								<label for="jjj"><?php echo __('Ativar notificações') ?></label>
								<select name="notif" id="notif" class="form-control">
									<option value="1">Sim</option>
									<option value="0">Não</option>
								</select>
							</div>

							
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Guardar'); ?></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
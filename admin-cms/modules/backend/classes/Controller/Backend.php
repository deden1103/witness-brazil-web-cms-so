<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Backend extends Controller {

	private static $level = null;
	
	public function before() {
		
		parent::before();
		
		// Check session login, apakah sudah login atau belum
		$session   = Session::instance();
		$was_login = $session->get('adminLogin');
		$admin_id = $session->get('adminId');

		if ($was_login != 1) { // Jika belum login maka redirect ke halaman login
			$this->redirect('/login');
		}

		$global_model = new Model_Globalmodel();
		$select = ['admiLvl as level'];
		$where = ['admiId' => $admin_id];
		$result = $global_model->select('admin',$select,$where);
		self::$level = $result[0]['level'];
		
		
		
	}

	public static function check_level(){
		return self::$level;
	}
	
	
} // End Welcome
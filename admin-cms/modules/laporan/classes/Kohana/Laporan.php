<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_Laporan {

	public static function validation($post) {
		
		$validation = 	Validation::factory($post)
						->rule('title', 'not_empty')
						->rule('title', 'max_length', array(':value', '65'))
						->rule('description', 'not_empty')
						->rule('description', 'max_length', array(':value', '165'))
						->rule('category', 'not_empty')
						//->rule('embed_url', 'not_empty')
						;
						
		if($validation->check()) {
			
			$return = TRUE;
		
		} else {
		
			$return = $validation->errors('validation');
		
		}
		
		return $return;
		
	}
    
    public static function get_image($video_id = '') {
        
        $return = '';
        
        $exec = DB::select(
                        array('arimId', 'image_id'),
                        array('arimFileType', 'image_type')
                )
                ->from('video_image_gif')
                ->join('arsip_images', 'LEFT')
                ->on('arsip_images.arimId', '=', 'video_image_gif.vgifArimId')
                ->where('vgifVdeoId', '=', $video_id)
                ->limit(1)
                ->execute()
                ->as_array();

        if(!empty($exec[0]['image_id'])) {
            $split = str_split($exec[0]['image_id']);
            $path = implode('/', $split);
            $return = '/uploads/library/' . $path . '/' . $exec[0]['image_id'] . '.' . $exec[0]['image_type'];
        }
        
        return $return;
        
    }
    
    public static function detail($video_id = '') {
        
        // Load MOdel
        $video_model = new Model_Video();
        
        return $video_model->detail_data($video_id);
        
    }
	
	public static function select_list($dom_name = '', $status = '', $id_selected = '', $custom = '', $version='') {
		$session = Session::instance();
		$return = '';
		
		$exec = DB::select(
					array('mscvId', 'id'),
					// array('msctAds', 'status'),
					array('mscvName', 'name')
				)
				->from('master_category_video');
				
		if($status === 0 OR $status === 1) {
			$exec = $exec->where('mscvStatus', '=', $status);
		}
		
		//$member = $session->get('member');
		//if($member == 2) {
			//$exec = $exec->where('msctAds', '=', 1);
		//}
		// $exec = $exec->order_by('msctAds', 'ASC')
		$exec = $exec->order_by('mscvName', 'ASC')
				->execute()
				->as_array();

		if($version=='lite'){
			$return .= '<div style="width: 200px;float: left;">
					<select required ' . $custom . ' class="form-control" name="' . $dom_name . '">
							<option value="">-- ' . __('Todas as categorias') . ' --</option>';
		}else{
			$return .= '<div class="form-group">
						<label>' . __('Category <font color="red">*</font>') . '</label>
						<select ' . $custom . ' class="form-control" name="' . $dom_name . '" required>
							<option value="">-- ' . __('Todas as categorias') . ' --</option>';
		}

		$status=0;
		if(!empty($exec)) {
			foreach($exec as $v_exec) {
				$selected_val = '';
				if($id_selected == $v_exec['id']) {
					$selected_val = 'selected';
				}
				$return .= '<option value="' . $v_exec['id'] . '" ' . $selected_val . '>' . $v_exec['name'] . '</option>';
			}
		}

		$return .= '	</select></div>';
		return $return;

	}
	
	public static function contact_list($dom_name = '', $status = '', $id_selected = '', $custom = '', $version='') {
		$session = Session::instance();
		$return = '';
		
		$exec = DB::select(
					array('contId', 'id'),
					array('contName', 'name'),
					array('contNomor', 'no')
				)
				->from('contact');
				
		if($status === 0 OR $status === 1) {
			$exec = $exec->where('contStatus', '=', $status);
		}
		
		//$member = $session->get('member');
		//if($member == 2) {
			//$exec = $exec->where('msctAds', '=', 1);
		//}
		// $exec = $exec->order_by('msctAds', 'ASC')
		$exec = $exec->order_by('contName', 'ASC')
				->execute()
				->as_array();

		if($version=='lite'){
			$return .= '<div style="width: 200px;float: left;">
					<select ' . $custom . ' class="form-control" name="' . $dom_name . '">
							<option value="">-- ' . __('Choose Contact') . ' --</option>';
		}else{
			$return .= '<div class="form-group">
						<label>' . __('Category') . '</label>
						<select multiple ' . $custom . ' class="form-control" name="' . $dom_name . '">
							<option value="">-- ' . __('Choose Contact') . ' --</option>';
		}

		$status=0;
		if(!empty($exec)) {
			foreach($exec as $v_exec) {
				$selected_val = '';
				if($id_selected == $v_exec['id']) {
					$selected_val = 'selected';
				}
				$return .= '<option value="' . $v_exec['id'] . '" ' . $selected_val . '>' . $v_exec['name'] . ' (' . $v_exec['no'] . ')</option>';
			}
		}

		$return .= '	</select></div>';
		return $return;

	}

	public static function slugfy($text){
		$text = str_replace('\'', '', $text);
		// replace non letter or digits by _
		$text = preg_replace('~[^\pL\d]+~u', '_', $text);
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		// trim
		$text = trim($text, '_');
		// remove duplicate -
		$text = preg_replace('~-+~', '_', $text);
		// lowercase
		$text = strtolower($text);
		if (empty($text)) {
			return 'n-a';
		}
		return $text;
	}


	public static function select_list_komunitas($dom_name = '', $status = '', $id_selected = '', $custom = '', $version='') {
		$session = Session::instance();
		$return = '';
		
		$exec = DB::select(
					array('komtId', 'id'),
					// array('msctAds', 'status'),
					array('komtName', 'name')
				)
				->from('komunitas');
				
		if($status === 0 OR $status === 1) {
			$exec = $exec->where('komtStatus', '=', $status);
		}
		
		//$member = $session->get('member');
		//if($member == 2) {
			//$exec = $exec->where('msctAds', '=', 1);
		//}
		// $exec = $exec->order_by('msctAds', 'ASC')
		$exec = $exec->order_by('komtName', 'ASC')
				->execute()
				->as_array();

		if($version=='lite'){
			$return .= '<div style="width: 200px;float: left;">
					<select required ' . $custom . ' class="form-control" name="' . $dom_name . '">
							<option value="">-- ' . __('All Community') . ' --</option>';
		}else{
			$return .= '<div class="form-group">
						<label>' . __('Category <font color="red">*</font>') . '</label>
						<select ' . $custom . ' class="form-control" name="' . $dom_name . '">
							<option value="">-- ' . __('All Community') . ' --</option>';
		}

		$status=0;
		if(!empty($exec)) {
			foreach($exec as $v_exec) {
				$selected_val = '';
				if($id_selected == $v_exec['id']) {
					$selected_val = 'selected';
				}
				$return .= '<option value="' . $v_exec['id'] . '" ' . $selected_val . '>' . $v_exec['name'] . '</option>';
			}
		}

		$return .= '	</select></div>';
		return $return;

	}
	
    
}
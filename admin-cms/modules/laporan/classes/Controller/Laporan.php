<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Laporan extends Controller_Backend {

	private $custom_header_form;
	private $custom_footer_form;
	private $video_model;
	private $global_model;
	private $daily = "";
	private $link_youtube = "";

	public function before() {


		parent::before();
		$level = Controller_Backend::check_level();


		if($level == 2){
			//$this->redirect(URL::Base().'/keranjang');
		}

		//set global model
		$this->global_model = new Model_Globalmodel();
		$this->video_model = new Model_Laporan();

		// Header Multiple SElect
		$this->custom_header_form = '
			<link rel="stylesheet" href="'.URL::Base().'assets/plugins/select2/select2.min.css">
			<link rel="stylesheet" href="'.URL::Base().'assets/dist/css/AdminLTE.min.css">
		';

		// Custom header for daterangepicker
		$this->custom_header_form .= '<link rel="stylesheet" href="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		// Tinymce
		$this->custom_footer_form = '
			<script src="'.URL::Base().'assets/tinymce/tinymce.min.js"></script>
			<script>
				tinymce.init({
					paste_data_images: true,
					valid_elements : \'*[*]\',
					selector: \'.f_tinymce\',
					theme: \'modern\',
                    templates: [{title: \'Berita Lainnya\', description: \'Berita Lainnya\', url: \'/template/terkait.html\'},{title: \'Button Selengkapnya\', description: \'Button Selengkapnya\', url: \'/template/selengkapnya.html\'},{title: \'Sharing\', description: \'Sharing Information\', url: \'/template/sharing.html\'},{title: \'Mutiara\', description: \'Informasi Mutiara\', url: \'/template/komunitas.html\'},{title: \'Download Arena\', description: \'Link Download Aplikasi Arena\', url: \'/template/arena.html\'}],
					plugins: [
						\'example advlist autolink lists link image charmap print preview hr anchor pagebreak\',
						\'searchreplace wordcount visualblocks visualchars code fullscreen\',
						\'insertdatetime media nonbreaking save table contextmenu directionality\',
						\'emoticons template paste textcolor colorpicker textpattern imagetools\'
					],
					toolbar1: \'insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image\',
					toolbar2: \'fullscreen preview media | forecolor backcolor emoticons | pagebreak | my_image\',
					image_advtab: true,
                                        pagebreak_separator: "<!--PAGE_SEPARATOR-->",
                                        pagebreak_split_block: true,
                                        setup: function (editor) {
                                        editor.addButton(\'my_image\', {
                                            text: \'Add Image From Gallery\',
                                            icon: false,
                                            onclick: function () {
                                                open_popup_img_tmce();
                                            }
                                        });
                                      },
				});
				function my_voice(texthtml) {
					tinyMCE.activeEditor.insertContent(texthtml);
				}
			</script>
		';

		// Multiple SElect
		$this->custom_footer_form .= '
			<script src="'.URL::Base().'assets/plugins/select2/select2.full.min.js"></script>
			<script>
				$(".select2").select2({
					allowClear: true,
					tags: true,
					placeholder : "-- Choose Keyword --"
				});
			</script>
		';

		// Custom footer for daterangepicker
		$this->custom_footer_form .= '
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				//Date range picker
				$(function() {
					$(\'.publish_time\').daterangepicker({
						format: \'DD/MM/YYYY HH:mm:ss\',
						timePicker: true,
						singleDatePicker: true,
						timePickerIncrement: 1,
						timePicker12Hour: false,
						//minDate: \'' . date('d-m-Y H:i:s') . '\'
					});
				});
				$("#myButton").click(function() {
					$("#kotak1").data(\'daterangepicker\').toggle();
				});
			</script>
			<!--Tinymce hidden for append image textarea-->
			<input type="hidden" id="image_popup_tmce" />
			<script>
				function open_popup_img() {
					PopupCenter("/library/index/1/0/image_popup", "google", "640", "480");
				}

				function open_popup_img_tmce() {
					PopupCenter("/library/index/1/0/image_popup_tmce", "google", "640", "480");
				}

				function image_popup_tmce() {
						var str = $(\'#image_popup_tmce\').val();
						var n = str.lastIndexOf(".");
						var res = str.substring(0, n)+str.substring(n);
						tinyMCE.activeEditor.insertContent(\'<img src="\' + res + \'" width="100%" />\');
				}

				function image_popup() {
					$(\'#previewImage\').html(\'\');
					$(\'#previewImage\').append(\'<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreview();">X</span></center></div>\');
					$(\'#previewImage\').append(\'<img src="\' + $(\'#image_popup\').val() + \'" style="width:200px;"/>\');
				}

				function delPreview() {
					$(\'#previewImage\').html(\'\');
					$(\'#image_popup\').val(\'\');
				}

				// GIF IMAGE JS FUNCTION
				function open_popup_img_gif() {
					PopupCenter("/library/index/1/0/image_popup_gif", "google", "640", "480");
				}

				function image_popup_gif() {
					$(\'#previewImageGif\').html(\'\');
					$(\'#previewImageGif\').append(\'<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreviewGif();">X</span></center></div>\');
					$(\'#previewImageGif\').append(\'<img src="\' + $(\'#image_popup_gif\').val() + \'" style="width:200px;"/>\');
				}

				function delPreviewGif() {
					$(\'#previewImageGif\').html(\'\');
					$(\'#image_popup_gif\').val(\'\');
				}

			</script>
		';

	}

	public function action_index() {

		$this->redirect('/laporan/search');

	}

	public function action_search() {

		$session = Session::instance();

		if(isset($_POST['version'])) $session->set('version_video', $_POST['version']);
		$data['version'] = $version = $session->get("version_video", "");

		if(isset($_POST['publisher'])) $session->set('publish_video', $_POST['publisher']);
		$data['publish'] = $publish = $session->get("publish_video", "artcPublishTime");

		if(isset($_POST['uploader'])) $session->set('uploader_video', $_POST['uploader']);
		$data['uploader'] = $uploader = $session->get("uploader_video", "");

		if(isset($_POST['category'])) $session->set('category_video', $_POST['category']);
		$data['category'] = $category = $session->get("category_video", "");

		if(isset($_POST['komunitas'])) $session->set('komunitas', $_POST['komunitas']);
		$data['komunitas'] = $komunitas = $session->get("komunitas", "");

		if(isset($_POST['search'])) $session->set('search_video', $_POST['search']);
		$data['search'] = $search = $session->get("search_video", "");

		if(isset($_GET['date_range'])) $session->set('date_range_video', $_GET['date_range']);
		$data['date_range'] = $date_range = $session->get("date_range_video", "");

		$data['main_title'] = __('Laporan');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'video';
		$data['menu_active_child_1'] = 'list';

		// Custom header for daterangepicker
		$data['custom_header'] = '<link rel="stylesheet" href="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker-bs3.css">';

		// Custom footer for daterangepicker
		$data['custom_footer'] = '
			<script src="'.URL::Base().'assets/plugins/select2/select2.full.min.js"></script>
			<script>
				$(document).ready(function () {
					$("#contact").select2({allowClear: true, multiple:true, placeholder : "-- Choose Contact --"});
				});
			</script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
			<script src="'.URL::Base().'assets/plugins/daterangepicker/daterangepicker.js"></script>
			<script>
				//Date range picker
				$(function() {
					$(\'#reservation\').daterangepicker(
						{
							format: \'DD/MM/YYYY\'
						}, function (start, end) {
							window.location = "/laporan/search/?date_range=" + $(\'#reservation\').val();
						}
					);
				});
			</script>
		';

		// Yes No Confirm
		$data['custom_footer'] .= '
			<script type="text/javascript">
				function del_confirm(id) {
					var dialog = confirm("' . __('Are you sure for delete this data ?') . '");
					if (dialog == true) {
						window.location.href="/laporan/delete/" + id;
					}
				}


                $(document).ready(function () {
					$(".btn-wa").click(function() {
                        var wa_desc = $(this).attr("custom-desc");
                        $(".hidden-wa").val(wa_desc);
                    })
					$(".btn-sms").click(function() {
                        var wa_desc = $(this).attr("custom-desc");
                        $(".hidden-wa").val(wa_desc);
                    })
				});
			</script>


		';

		// Date parameter
		if(empty($date_range)) {
			$date1 = "";
			$date2 = "";

		} else {
			$ex_range = explode(' - ', $date_range);
			if(!empty($ex_range)) {
				$date1 = DateTime::createFromFormat('d/m/Y', $ex_range[0]);
				$date1 = $date1->format('Y-m-d');
				$date2 = DateTime::createFromFormat('d/m/Y', $ex_range[1]);
				$date2 = $date2->format('Y-m-d');

				// Date adding time
				$date1 = $date1 . ' 00:00:00';
				$date2 = $date2 . ' 00:00:00';
			}
		}


		// Page
		$page = intval($this->request->param('page'));
		if(empty($page)) {
			$page = 1;
		}

		// Count all data
		$count_all = $this->video_model->count_search($date1, $date2, $search);
		$data['count_all'] = $count_all;

		// Pagination
		$pagination = 	Pagination::factory(array(
							'total_items'    		=> $count_all,
							'items_per_page'		=> 10,
							'current_page'			=> $page,
							'base_url'				=> '/laporan/search/',
							//'suffix'				=> '?date_range=' . $date_range,
							'view'					=> 'pagination/admin'
						));

		$dataaalist = $this->video_model->list_search($date1, $date2, $search, $pagination->items_per_page, $pagination->offset);

		foreach ($dataaalist as $key => $value) {
			$dataaalist[$key]['diedit'] = $this->video_model->getIsEditingName($value['isUserEditing']);
		}

		$data['list'] = $dataaalist;

		// print_r($data['list']);die;
		$data['list_keyword'] = $this->global_model->select(
			'master_keyword_video',['vkeyId as id', 'vkeyName as name'], //select
			['vkeyStatus >=' => 1] //table
		);
		$data['list_contact'] = $this->global_model->select(
			'contact',['contId as id', 'contName as name', 'contNomor as no'], //select
			['contStatus =' => 1] //table
		);


		$data['pagination'] = $pagination->render();

		$view = Briliant::admin_template('laporan/' . Kohana::$config->load('path.main_template') . '/list', $data);

		$this->response->body($view);

	}

	public function action_new() {
		$session = Session::instance();

		$data['main_title'] = __('Laporan | Add New Data');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'video';
		$data['menu_active_child_1'] = 'add';

		$keranjang_mentah = $this->request->post();
		if(isset($keranjang_mentah)){
			$data['post']['title'] = $this->request->post('title');
			$data['post']['detail'] =  $this->request->post('detail');
		}

		$data['custom_header'] = $this->custom_header_form;
		$data['custom_footer'] = $this->custom_footer_form;
		$data['list_keyword'] = $this->global_model->select('master_keyword_video',['vkeyId as id', 'vkeyName as name'],['vkeyStatus >=' => 1]);

		$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/new', $data);
		$this->response->body($view);

	}

	public function action_youtube(){


		$id_video = $this->request->param('id');

		$get_data_video = $this->global_model->select(
			'article', //table
			['artcId','artcTitle','artcExcerpt','artcDetail','artcKeyword','artcMscvId','artcEmbedURL','artcUkomIdSaved','artcSaved','artcPublishTime','artcMediaFileType'], //select
			['artcId' => $id_video] //where
		);

		$data['list_keyword'] = $this->global_model->select('master_keyword_video',['vkeyId as id', 'vkeyName as name'],['vkeyStatus >=' => 1]);

		if(!empty($get_data_video[0]['artcKeyword'])){
			$keyword = null;
			$keyword_id = unserialize($get_data_video[0]['artcKeyword']);

			foreach($data['list_keyword'] as $kk){
				$idK = $kk['id'];
				$nameK = $kk['name'];
				foreach($keyword_id as $k){
					if ($k == $idK) {
						$keyword .= $nameK.', ';
					}
				}
			}
		}else{
			$keyword = "";
		}

		//get video url
		$split_id = str_split($get_data_video[0]['artcId']);
		$keywords = rtrim($keyword,", ");
		$desc = $get_data_video[0]['artcExcerpt'];
		$title = $get_data_video[0]['artcTitle'];
		$url = implode('/', $split_id).'/'.$id_video.'.mp4';

		$this->action_upload($id_video,$title,$desc,$keywords,$url);
		//$this->redirect('/laporan/search');



	}
	public function action_rotate($id='', $rotate='', $type='')
    {
		$id = $_GET['id'];
		$type = $_GET['type'];
		$rotate = $_GET['rotate'];
		$split_id = str_split($id);
		$path_folder_image = implode('/', $split_id);
		//print_r($path_folder_image);die;
		// $type = "jpg";
        // Your original file
        $original   =   imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'] . "/../api/assets/library/" . $path_folder_image . '/' . $id .'.'. $type);
        // Rotate
        $rotated    =   imagerotate($original, $rotate, 0);
		// Save to a directory with a new filename
		imagejpeg($rotated, $_SERVER['DOCUMENT_ROOT'] . "/../api/assets/library/" . $path_folder_image . '/' .$id .'.'. $type);

        // Standard destroy command
        imagedestroy($rotated);
		$this->redirect('laporan/edit/'.$id);
    }

	public function action_upload($id_video='',$title='',$desc='',$keywords='',$videoPath='') {

		$pathv = $videoPath;
		require_once($_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php");

		// $OAUTH2_CLIENT_ID = '621024871990-p5rj72p424ca36e2lju7t9usa2oskcne.apps.googleusercontent.com';//'43550634816-k5ecoc0n5eu1oe2u7f8af1pt0kp6v5pd.apps.googleusercontent.com';
		// $OAUTH2_CLIENT_SECRET ='3RvcbbAGvbjl0yGBA7clUAec'; //'g-vA9QSuG12rI1Xx34Cwj3mp';

		// $OAUTH2_CLIENT_ID = '290848944953-4sulvc7mvgkhlu85232qskfl9tvne79d.apps.googleusercontent.com';//'43550634816-k5ecoc0n5eu1oe2u7f8af1pt0kp6v5pd.apps.googleusercontent.com';
		// $OAUTH2_CLIENT_SECRET ='GO0nw-QjRHqKs8xCdWw3K4fI'; //'g-vA9QSuG12rI1Xx34Cwj3mp';

		$OAUTH2_CLIENT_ID = '621024871990-orngvkf4qtmm94c4hbpu8jiaak32f5o3.apps.googleusercontent.com';//'43550634816-k5ecoc0n5eu1oe2u7f8af1pt0kp6v5pd.apps.googleusercontent.com';
		$OAUTH2_CLIENT_SECRET ='VqnufN91KSP78tS-x66OgSC-'; //'g-vA9QSuG12rI1Xx34Cwj3mp';


		$client = new Google_Client();
		$client->setClientId($OAUTH2_CLIENT_ID);
		$client->setClientSecret($OAUTH2_CLIENT_SECRET);
		$client->setScopes('https://www.googleapis.com/auth/youtube');
		$redirect =Kohana::$config->load('path.cms').'laporan/upload';
		$client->setRedirectUri($redirect);
		$client->setAccessType('offline');

		// Define an object that will be used to make all API requests.
		$youtube = new Google_Service_YouTube($client);


		if (isset($_GET['code'])) {
		  if (strval($_SESSION['state']) !== strval($_GET['state'])) {
			die('The session state did not match.');
		  }

		  $client->authenticate($_GET['code']);
		  $_SESSION['token'] = $client->getAccessToken();
		  header('Location: ' . $redirect);
		}

		if (isset($_SESSION['token'])) {
		  $client->setAccessToken($_SESSION['token']);

		}
		//print_r($client);die;

		// Check to ensure that the access token was successfully acquired.
		if ($client->getAccessToken()) {

			// REPLACE this value with the path to the file you are uploading.
			//$videoPath = "http://witness-api.hahabid.com/assets/library/".$url;

			// Define service object for making API requests.
		//print_r($client);die;
			$service = new Google_Service_YouTube($client);

			// Define the $video object, which will be uploaded as the request body.
			$video = new Google_Service_YouTube_Video();

			$videoSnippet = new Google_Service_YouTube_VideoSnippet();
			$videoSnippet->setCategoryId('22');
			$videoSnippet->setDescription($desc);
			$videoSnippet->setTitle($title);
			$videoSnippet->setTags(array($keywords));
			$video->setSnippet($videoSnippet);

			$response = $service->videos->insert(
				'snippet,status',
				$video,
				array(
				  'data' => file_get_contents($pathv),
				  'mimeType' => 'video/*',
				  'uploadType' => 'multipart'
				)
			  );


			$this->link_youtube=$response['id'];

		  $_SESSION['token'] = $client->getAccessToken();
		} else {
		// If the user hasn't authorized the app, initiate the OAuth flow
		$state = mt_rand();
		$client->setState($state);
		$_SESSION['state'] = $state;
		$authUrl = $client->createAuthUrl();
		$this->redirect($authUrl);

	  }

	}

	public function action_whatsapp() {

		require_once($_SERVER['DOCUMENT_ROOT']."/twilio-php-master/src/Twilio/autoload.php");

		// Your Account SID and Auth Token from twilio.com/console
		$sid = 'AC1ba7289c2d43d31f2a4799f4434e50e1';
		$token = 'c31e831e6600c7def7ffd707b1a2cade';
		$client = new Twilio\Rest\Client($sid, $token);
		$from = 'whatsapp:+14155238886';
		// print_r($_POST); die;
		// Use the client to do fun stuff like send text messages!
		foreach($_POST['contact'] as $to){
			$client->messages->create(
				// the number you'd like to send the message to
				"whatsapp:" . $to,
				array(
					// A Twilio phone number you purchased at twilio.com/console
					'from' => $from,
					// the body of the text message you'd like to send
					'body' => $this->request->post('description')
				)
			);
		}
		// print_r($client); die;
		$this->redirect('laporan');
	}

	public function action_sms() {
		header('location: sms:/open?addresses='.implode(',',$_POST['contact']).'&body='.$this->request->post('description'));
		exit;
		require_once($_SERVER['DOCUMENT_ROOT']."/twilio-php-master/src/Twilio/autoload.php");

		// Your Account SID and Auth Token from twilio.com/console
		$sid = 'AC1ba7289c2d43d31f2a4799f4434e50e1';
		$token = 'c31e831e6600c7def7ffd707b1a2cade';
		$client = new Twilio\Rest\Client($sid, $token);
		$from = '+12055768305';
		// print_r($_POST); die;
		// Use the client to do fun stuff like send text messages!
		foreach($_POST['contact'] as $to){
			$client->messages->create(
				// the number you'd like to send the message to
				$to,
				array(
					// A Twilio phone number you purchased at twilio.com/console
					'from' => $from,
					// the body of the text message you'd like to send
					'body' => $this->request->post('description')
				)
			);
		}
		// print_r($client); die;
		$this->redirect('laporan');
	}

	public function action_redirect() {
		print_r($_POST); die;
	}


	public function action_save() {
		$session = Session::instance();

		// Validation
		$validation = Video::validation($this->request->post());

		if($validation !== TRUE) { // Error Validation

			$data['main_title'] = __('Video | Add New Data');
			$data['menu_active'] = 'video';
			$data['menu_active_child'] = 'video';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['post'] = $this->request->post();
			$view = Briliant::admin_template('video/' . Kohana::$config->load('path.main_template') . '/new', $data);
			$this->response->body($view);

		} else { // Validation Success

			// Get user id from session login
			$user_id = $session->get('adminId');
			$post = $this->request->post();

			// Save Data
			// print_r($this->request->post());exit;
			$save_data = $this->video_model->save_data($post, $user_id);

			// Redirect
			$this->redirect('/video/search');

		}

	}

	public function action_detail() {
		$session = Session::instance();

      $data['main_title'] = __('Laporan | Detail Data');
      $data['menu_active'] = 'video';
      $data['menu_active_child'] = 'video';

      $id = $this->request->param('id');

            // Detail data
			$data['detail'] = $this->video_model->detail_data($id);
			$data['list_keyword'] = $this->global_model->select('master_keyword_video',['vkeyId as id', 'vkeyName as name'],['vkeyStatus >=' => 1]);

			$view = Briliant::admin_template('laporan/' . Kohana::$config->load('path.main_template') . '/detail', $data);
            $this->response->body($view);

	}

	public function action_edit() {
		$session = Session::instance();

		$data['main_title'] = __('Laporan | Edit Data');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'video';

		$data['custom_header'] = $this->custom_header_form;

		// Paramter ID
		$id = $this->request->param('id');

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('adminId');

		// Detail Data
		$video_detail = $this->video_model->detail_data($id);

		if($video_detail['isEditing'] == 1){
			if($video_detail['isUserEditing'] == $user_id){
				
			}else{
				$this->redirect('/laporan/search');
				return false;
			}
		}
		$updateIsEditing = $this->video_model->update_isEditing($id);
		// print_r($video_detail); die;
		$data['detail'] = $video_detail;


		$data['list_keyword'] = $this->global_model->select('master_keyword_video',['vkeyId as id', 'vkeyName as name'],['vkeyStatus >=' => 1]);
		$data['custom_footer'] = $this->custom_footer_form;
		$data['custom_footer'] = $data['custom_footer'].'<script type="text/javascript" src="/assets/js/new/socket.io.min.js"></script>
			<script>
				var socket = io(\'https://witness-so.tempo.co\');

				$.ajax({
					dataType: "json",
					url: "/laporan/dataChat/'.$id.'",
					success: function(historyChat){
						console.log(\'test\');
						if(historyChat.length > 0) {
							$.each(historyChat, function(keyHis, valHis) {
								if(valHis.admiId == '.$user_id.') { // From Me
									$(\'.socket-list-container\').append(\'<div class="direct-chat-msg">\'+

																			\'<div class="direct-chat-info clearfix">\'+
																				\'<span class="direct-chat-name pull-left">\'+ valHis.admin_name +\'</span>\'+
																				\'<span class="direct-chat-timestamp pull-right">\'+valHis.indo_saved+\'</span>\'+
																			\'</div>\'+
																			\'<div class="direct-chat-text">\'
																				+ valHis.text +
																			\'</div>\'+
																		\'<div>\');
								}else {
									$(\'.socket-list-container\').append(\'<div class="direct-chat-msg right">\'+
																			\'<div class="direct-chat-info clearfix">\'+
																				\'<span class="direct-chat-name pull-left">\'+ valHis.ukom_name +\'</span>\'+
																				\'<span class="direct-chat-timestamp pull-right">\'+valHis.indo_saved+\'</span>\'+
																			\'</div>\'+
																			\'<div class="direct-chat-text">\'
																				+ valHis.text +
																			\'</div>\'+
																	\'<div>\');



								}

							});
						}
					}
			    });

				socket.emit(\'join\',\''.$data['detail']['savedId'].'\',\''.$user_id.'\',\''.$id.'\',true);

				$("#pesan").click(function() {
					socket.emit(\'sendchat\', $(\'#m\').val());
					$(\'#m\').val(\'\');
					return false;
				});

				socket.on(\'updatechat\',function(id,date,nama,text){
					console.log(id);
					console.log(date);
					console.log(nama);
					console.log(text);
					if(id == '.$user_id.') {
						console.log(\'right\');
							$(\'.socket-list-container\').append(\'<div class="direct-chat-msg">\'+

																			\'<div class="direct-chat-info clearfix">\'+
																				\'<span class="direct-chat-name pull-left">\'+ nama +\'</span>\'+
																				\'<span class="direct-chat-timestamp pull-right">\'+date+\'</span>\'+
																			\'</div>\'+
																			\'<div class="direct-chat-text">\'
																				+ text +
																			\'</div>\'+
																		\'<div>\');
					}else{
							$(\'.socket-list-container\').append(\'<div class="direct-chat-msg right">\'+
																			\'<div class="direct-chat-info clearfix">\'+
																				\'<span class="direct-chat-name pull-left">\'+ nama +\'</span>\'+
																				\'<span class="direct-chat-timestamp pull-right">\'+date+\'</span>\'+
																			\'</div>\'+
																			\'<div class="direct-chat-text">\'
																				+ text +
																			\'</div>\'+
																	\'<div>\');



					}

				})


			</script>
		';


		$view = Briliant::admin_template('laporan/' . Kohana::$config->load('path.main_template') . '/edit', $data);

		$this->response->body($view);

	}

	public function action_dataChat(){
		$artcId = $this->request->param('id');
		$data['list_chat'] = $this->video_model->data_chat('chat', $artcId);
		//print_r($data['list_chat']);die;
		echo json_encode($data['list_chat']);


	}


	public function action_update() {
		// print_r($this->request->post());die;
		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('adminId');

		// Check Permission
		$id_parameter = $this->request->post('id');

		// Validation
		$validation = Laporan::validation($this->request->post());

		if($validation !== TRUE) { // Error Validation

			$data['main_title'] = __('Newsroom | Newsroom | News | Edit Data');
			$data['menu_active'] = 'newsroom';
			$data['menu_active_child'] = 'video';

			$data['custom_header'] = $this->custom_header_form;
			$data['custom_footer'] = $this->custom_footer_form;

			$data['errors'] = $validation;

			$data['detail'] = $this->request->post();
			if(!empty($data['detail']['category'])) {
				$data['detail']['category_id'] = $data['detail']['category'];
			}

			$view = Briliant::admin_template('laporan/' . Kohana::$config->load('path.main_template') . '/edit', $data);

			$this->response->body($view);

		} else { // Validation Success

			// Update Data
			// print_r($this->request->post()); die;
			$update_data = $this->video_model->update_data($this->request->post(), $user_id);

			$updateIsEditing = $this->video_model->update_isNoEditing($id_parameter);


			$this->redirect('/laporan/search');

		}

	}

	public function action_delete() {

		// ID Paramter
		$id = $this->request->param('id');

		// Get user id from session
		$session = Session::instance();
		$user_id = $session->get('user_id');

		// Check Permission
		// DVH::check_permission('video', 'vdeoId', $id, $user_id, 'vdeoUserIdSaved');

		// Change status to 0
		$this->video_model->delete_data($id);

		// Redirect
		$this->redirect('/laporan/search');

	}

	public function action_publish(){
		$session = Session::instance();
		$level = Controller_Backend::check_level();
		if($level == 3 or $level == 1){
			$id_video = $this->request->param('id');
			$now = date('Y-m-d H:i:s');
			$adminId = $session->get('adminId');
			$table_name = $this->request->param('cat');

			$get_data_video = $this->global_model->select(
				'article', //table
				['artcId','artcTitle','artcExcerpt','artcDetail','artcKeyword','artcMscvId','artcEmbedURL','artcUkomIdSaved','artcSaved','artcPublishTime'], //select
				['artcId' => $id_video] //where
			);
			$onesignalid = $this->global_model->select(
				'user_komunitas', //table
				['ukomOneSignal'], //select
				['ukomId' => $get_data_video[0]['artcUkomIdSaved']] //where
			);
			// var_dump($onesignalid[0]['ukomOneSignal']);die();
			$publish =  $this->video_model->publish_video($id_video, $table_name, $get_data_video[0], $adminId,$videourl='');
			$this->sendpushnotif($get_data_video[0]['artcTitle'],$onesignalid[0]['ukomOneSignal']);
			if($publish){
				// Redirect

				$this->redirect('/laporan/search');
			}
		}
	}

	public function action_dailymontion($id_video='',$title='',$desc='',$keywords='',$path='') {

		require_once($_SERVER['DOCUMENT_ROOT'].'/Dailymotion.php');

		$apikey = '64ebaf8460715ef37efe';//'d1118c2db9a757f079e6';//'2c5a9d45f25bf7e570e4';;
		$apisecret ='4cc9d2a06581f204117b81f8f89467fea2138ce5';//'dc900f36257c5935d201eb7c858a09c67f94fb62'; //'bb9352c4a0946550ddfa2de7489f5a4daee25044';
		$user = 'tempowitness@gmail.com';//'arran2796@gmail.com';//'rannyasti08@gmail.com';
		$password = 'TempoProklamasi72!';//'cendrawasih59*';//'cendrawasih5*';

		$videotitle = $title;
		$videocategory = "news";
		$videotags =$keywords;
		$videodescription = $desc;
		$api = new Dailymotion();
		$url=Kohana::$config->load('path.api').$path;
		$api->setGrantType(Dailymotion::GRANT_TYPE_PASSWORD, $apikey, $apisecret, array('write','delete'), array('username' => $user, 'password' => $password));
		$result = $api->call('video.create', array('url' => $url, 'title' => $videotitle , 'channel' => $videocategory , 'tags' => $videotags, 'description' => $videodescription, 'published' => true));
		//print_r($result); die;
		$this->daily = $result['id'];

	}

	public function action_publishvideo(){

		$session = Session::instance();
		$level = Controller_Backend::check_level();
		if($level == 3 or $level == 1){
			$id_video = $this->request->param('id');
			$now = date('Y-m-d H:i:s');
			$adminId = $session->get('adminId');
			$table_name = $this->request->param('cat');

			$get_data_video = $this->global_model->select(
				'article', //table
				['artcId','artcTitle','artcExcerpt','artcDetail','artcKeyword','artcMscvId','artcEmbedURL','artcUkomIdSaved','artcSaved','artcPublishTime','artcMediaFileType'], //select
				['artcId' => $id_video] //where
			);

			$data['list_keyword'] = $this->global_model->select('master_keyword_video',['vkeyId as id', 'vkeyName as name'],['vkeyStatus >=' => 1]);

			if(!empty($get_data_video[0]['artcKeyword'])){
				$keyword = null;
				$keyword_id = unserialize($get_data_video[0]['artcKeyword']);

				foreach($data['list_keyword'] as $kk){
					$idK = $kk['id'];
					$nameK = $kk['name'];
					foreach($keyword_id as $k){
						if ($k == $idK) {
							$keyword .= $nameK.', ';
						}
					}
				}
			}else{
				$keyword = "";
			}

			//get video url
			$split_id = str_split($get_data_video[0]['artcId']);
			$keywords = rtrim($keyword,", ");
			$desc = $get_data_video[0]['artcExcerpt'];
			$title = $get_data_video[0]['artcTitle'];
			$url = implode('/', $split_id).'/'.$id_video.'.'. $get_data_video[0]['artcMediaFileType'];
			$videoPath = Kohana::$config->load('path.api').$url;
		    //print_r($videoPath);die;
			//upload Dailymotion
			$this->action_dailymontion($id_video,$title,$desc,$keywords,$url);
			$videourl = 'http://www.dailymotion.com/video/'.$this->daily;

			//upload youtube
			//$this->action_upload($id_video,$title,$desc,$keywords,$videoPath);

			//$videourl = 'https://www.youtube.com/watch?v='.$this->link_youtube;


			$publish =  $this->video_model->publish_video($id_video, $table_name, $get_data_video[0], $adminId,$videourl);

			if($publish){
				// Redirect
				$this->redirect('/laporan/search');
			}
		}

	}

	public function action_unpublish(){
		$level = Controller_Backend::check_level();
		if($level == 3 or $level == 1){
			$id_video = $this->request->param('id');
			$table_name = $this->request->param('cat');
			$unpublish = $this->video_model->unpublish_video($id_video, $table_name);
			if($unpublish){
				// Redirect
				$this->redirect('/laporan/search');
			}
		}
	}

	public function action_category(){
		$data['main_title'] = __('Config | Configurar a categoria');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'config';
		$data['menu_active_child_1'] = 'category';

		$data['ctg_category'] = $this->global_model->select(
			'master_category_video', //table
			['mscvId as id', 'mscvName as name'], //select
			['mscvStatus >=' => 1],
			'', //limit
			'', //offset
			['mscvId' => 'DESC'] //order
		); //where

		$data['custom_header'] = '
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.min.css">
		';
		$data['custom_footer'] = '
			<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.all.min.js"></script>
		';

		//alert
		$data['custom_footer'] .= '
		<script>
			function del(id){
				var swal_id = id
				swal({
					title: "Are you sure?",
					text: "You won\'t be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3085d6",
					cancelButtonColor: "#d33",
					confirmButtonText: "Yes, delete it!"
				  }).then((result) => {
					if (result.value) {
						$.ajax({
							url: "/video/delete_category/",
							dataType: "JSON",
							type: "POST",
							data: {swal_id : swal_id},
							success: function(res){
								$("#row"+swal_id).remove()
								swal(
									"Deleted!",
									"Your file has been deleted.",
									"success"
								  )
							},error: function(){
								swal(
									"Something Wrong Happen !!",
									"",
									"error"
								  )
							}
						})

					}
				  })
			}
		</script>
		';

		$view = Briliant::admin_template('laporan/' . Kohana::$config->load('path.main_template') . '/config_category', $data);
		//print_r($data); exit;

		$this->response->body($view);
	}

	public function action_editcategory(){
		$data['main_title'] = __('Config | Configurar a categoria');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'config';
		$data['menu_active_child_1'] = 'category';

		$id = $this->request->param('id');
		$data['ctg_category'] = $this->global_model->select(
				'master_category_video',
				['mscvId as id', 'mscvName as name'],
				['mscvId' => $id,'mscvStatus >=' => 1]
			);

		$view = Briliant::admin_template('laporan/' . Kohana::$config->load('path.main_template') . '/edit_category', $data);
		//print_r($data); exit;

		$this->response->body($view);
	}

	function action_add_category(){
		$session = Session::instance();
		$save = $this->request->post('save');
		if (isset($save)) {
			$data = $this->request->post();
			$save_data = $this->video_model->add_category($data);

			$this->redirect('/laporan/category');
		}
	}

	function action_update_category(){

		$session = Session::instance();
		$save = $this->request->post('save');
		if (isset($save)) {
			$data = $this->request->post();
			$update = $this->video_model->update_category($data);


			$this->redirect('/laporan/category');
		}
	}

	function action_delete_category() {
		if(isset($_POST["swal_id"])){
			$id = $_POST['swal_id'];
			$update = $this->video_model->delete_category($id);
			if ($update == "Success") {
				echo json_encode($update);
			}else{
				http_response_code(500);
			}
		}
	}

	public function action_keyword(){
		$data['main_title'] = __('Config | Config Keyword');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'config';
		$data['menu_active_child_1'] = 'keyword';

		$data['ctg_keyword'] = $this->global_model->select(
					'master_keyword_video', //from
					['vkeyId as id', 'vkeyName as name'], //select
					['vkeyStatus >=' => 1], //where
					'', //limit
					'', //offset
					['vkeyId' => 'DESC'] //order
				);

		$data['custom_header'] = '
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.min.css">
		';
		$data['custom_footer'] = '
			<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.all.min.js"></script>
		';

		//alert
		$data['custom_footer'] .= '
		<script>
			function del(id){
				var swal_id = id
				swal({
					title: "Are you sure?",
					text: "You won\'t be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3085d6",
					cancelButtonColor: "#d33",
					confirmButtonText: "Yes, delete it!"
				  }).then((result) => {
					if (result.value) {
						$.ajax({
							url: "/laporan/delete_keyword/",
							dataType: "JSON",
							type: "POST",
							data: {swal_id : swal_id},
							success: function(res){
								$("#row"+swal_id).remove()
								swal(
									"Deleted!",
									"Your file has been deleted.",
									"success"
								  )
							},error: function(){
								swal(
									"Something Wrong Happen !!",
									"",
									"error"
								  )
							}
						})

					}
				  })
			}
		</script>
		';

		$view = Briliant::admin_template('laporan/' . Kohana::$config->load('path.main_template') . '/config_keyword', $data);
		//print_r($data); exit;

		$this->response->body($view);
	}

	public function action_editkeyword(){
		$data['main_title'] = __('Config | Config Keyword');
		$data['menu_active'] = 'video';
		$data['menu_active_child'] = 'config';
		$data['menu_active_child_1'] = 'keyword';

		$id = $this->request->param('id');
		$data['ctg_keyword'] = $this->global_model->select(
				'master_keyword_video',
				['vkeyId as id', 'vkeyName as name'],
				['vkeyId' => $id,'vkeyStatus >=' => 1]
			);

		$view = Briliant::admin_template('laporan/' . Kohana::$config->load('path.main_template') . '/edit_keyword', $data);
		//print_r($data); exit;

		$this->response->body($view);
	}

	function action_add_keyword(){
		$session = Session::instance();
		$save = $this->request->post('save');
		if (isset($save)) {
			$data = $this->request->post();
			$save_data = $this->video_model->add_keyword($data);
			$this->redirect('/video/keyword');
		}
	}

	function action_update_keyword(){
		$save = $this->request->post('save');
		if (isset($save)) {
			/* $id = $_POST['id'];
			$data = array('vkeyName' => $this->request->post('name'),'vkeyAdmiId' => $session->get('adminId'));
			$update = $this->global_model->update('master_keyword_video',$data,['vkeyId' => $id]); */
			$data = $this->request->post();
			$update_data = $this->video_model->update_keyword($data);
			$this->redirect('/laporan/keyword');
		}
	}

	function action_delete_keyword() {
		if(isset($_POST["swal_id"])){
			$id = $_POST['swal_id'];
			$update = $this->video_model->delete_keyword($id);
			if ($update == "Success") {
				echo json_encode($update);
			}else{
				http_response_code(500);
			}
		}
	}

	 function sendpushnotif($laporanName,$osid) {
		$content = array(
			"en" => 'Laporan '.$laporanName.' Anda Telah Disetujui',
		);

		$fields = array(
			'app_id' => "04f752ea-ae43-42a1-8488-75b945eb9447",
			'include_player_ids' => [$osid],
			'data' => array(
				"foo" => "bar",
			),
			'contents' => $content,

		);

		$fields = json_encode($fields);
		// print("\nJSON sent:\n");
		// print($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json; charset=utf-8',
			'Authorization: Basic OTQ4MDBjYTktMDVmYS00NjM1LTkyNjktNDZkMmVjYmNkODY4',
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
    }


	public function action_aftercancel(){
		
		$id_video = $this->request->param('id');
		$updateIsEditing = $this->video_model->update_isNoEditing($id_video);
		$this->redirect('/laporan/search');
		
	}


	public function action_setnullisediting(){
		return 'testing ya';
	}

}

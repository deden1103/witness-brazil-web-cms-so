<div class="box-body">
	<table class="table table-bordered table-striped">
		<tr>
			<th style="width:20%;text-align:center;"><?php echo __('Meios de comunicação'); ?></th>
			<th style="text-align:center;"><?php echo __('Detalhes'); ?></th>
			<th style="width:15%;text-align:center;"><?php echo __('Carregador'); ?></th>
			<th style="text-align:center;width:18%;"><?php echo __('Status'); ?></th>
			<th colspan="2" style="width:10%;text-align:center;"><?php echo __('Ações'); ?></th>
		</tr>
		<?php
		if(!empty($data['list'])) {
			foreach($data['list'] as $v_list) {
				$now = date('Y-m-d H:i:s');
				$url = Kohana::$config->load('path.api');
				// Image article
				$img_video = '' . __('[ No Media ]') . '';

				if(in_array($v_list['type'], array("mp4", "mov"))) {
					$split_id = str_split($v_list['id']);
					$path_folder_image = implode('/', $split_id);
					$img_video = '<video width="400"  height="270" controls="controls" src="' . $url . $path_folder_image . '/' . $v_list['id'] .'.'. $v_list['type'] . '" /><source type="video/mp4" /></video>';

				}else if(in_array($v_list['type'], array("png", "jpg","jpeg"))) {
					$split_id = str_split($v_list['id']);
					$path_folder_image = implode('/', $split_id);
					$img_video = '<img height="200px" src="' . $url . $path_folder_image . '/' . $v_list['id'] .'.'. $v_list['type'] . '" />';
				}else if(in_array($v_list['type'], array("mp3","aac"))) {
					$split_id = str_split($v_list['id']);
					$path_folder_image = implode('/', $split_id);
					$img_video = '<br><audio  controls="controls" src="' . $url . $path_folder_image . '/' . $v_list['id'] .'.'. $v_list['type'] . '" /></audio>';

				}


				//level
				if ($v_list['level'] == 1) {
					$level = "Admin";
				}elseif ($v_list['level'] == 2) {
					$level = "Redaksi";
				}else{
					$level = "Editor";
				}

				if(strtotime($v_list['publish']) > strtotime($now)) {
					//$video_status .= '<br/></br><span class="label bg-maroon-active color-palette">Schedule</span> <b>' . date('d M Y H:i', strtotime($v_list['publish'])) . '</b>';
				}
				// Status
				$video_status = '';
				if($v_list['status'] == 1) {
					$video_status = '<span class="label label-info">Elaborado</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
				} else if($v_list['status'] == 0) {
					$video_status = '<span class="label label-danger">Deleted</span> <b>' . date('d M Y H:i', strtotime($v_list['saved'])) . '</b>';
				} else if($v_list['status'] == 2) {
					//$url_live = Kohana::$config->load('path.arah')."/video/{$v_list['id']}/".URL::title($v_list['title']).'.html';
					$video_status = '<span class="label label-success">Published</span> <b>'.ucfirst($v_list['user_name']).'-'.$level.'<br>' . date('d M Y H:i', strtotime($v_list['publish'])) . '</b>';
					//$video_status .= '<br><br><b><i>Approved By :<br>' . Video::get_user_approver($v_list['id']) . '<i></b>';
				}

				if($v_list['isEditing'] == 1){
					$video_status .= '<br /> Editing By: <b>' .$v_list['diedit'].'</b>';
				}

				// Publish schedule


				if(in_array($v_list['type'], array("mp4", "mov"))){
					$v_publish = 'publishvideo';

				}else{
					$v_publish = 'publish';
				}

				// Default Button
				$level = Controller_Backend::check_level();
				$category = Laporan::slugfy($v_list['category_name']);
				$portal =Kohana::$config->load('path.portal').$v_list['id'].'/'.Laporan::slugfy($v_list['title']);
				if($level == 3 or $level == 1){
					if($v_list['status'] == 2){
						$button = '
						<a href="'.URL::Base().'laporan/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
						<a href="'.URL::Base().'laporan/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
						<a href="'.URL::Base().'laporan/unpublish/' . $v_list['id'] . '/'.$category.'"><button class="btn btn-block btn-danger btn-xs">' . __('Unpublish') . '</button></a></br>
						<a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a><br><br>
					';
					// <button class="btn btn-block btn-info btn-xs btn-wa" custom-desc="' . $v_list['title'] . ' - ' . $portal . '" data-toggle="modal" data-target="#myModal">' . __('Enviar Whatsapp') . '</button></br>
					// <button class="btn btn-block btn-primary btn-xs btn-sms" custom-desc="' . $v_list['title'] . ' - ' . $portal . '" data-toggle="modal" data-toggle="modal" data-target="#myModal1">' . __('Enviar SMS') . '</button></br>
						
					}else{
							$button = '
							<a href="'.URL::Base().'laporan/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
							<a href="'.URL::Base().'laporan/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
							<a href="'.URL::Base().'laporan/'.$v_publish.'/' .$v_list['id'].'"><button class="btn btn-block btn-info btn-xs">' . __('Publicar') . '</button></a></br>
							<a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a><br><br>
							';

					}

				}else{
					$button = '
							<a href="'.URL::Base().'laporan/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
							<a href="'.URL::Base().'laporan/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
							<a href="javascript:del_confirm(' . $v_list['id'] . ')"><button class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a><br><br>
					';
				}



				// Button If Status 0
				if($v_list['status'] == 0) {
					$button = '
							<a href="'.URL::Base().'laporan/detail/' . $v_list['id'] . '"><button class="btn btn-block btn-success btn-xs">' . __('Pré-visualização') . '</button></a> </br>
							<a href="javascript:void()"><button disabled class="btn btn-block btn-warning btn-xs">' . __('Editar') . '</button></a> </br>
							<a href="javascript:void()"><button disabled class="btn btn-block btn-danger btn-xs">' . __('Apagar') . '</button></a>
					';
				}

				$article_title = $v_list['title'];
				if(!empty($data['search'])) {
					$ex_search = explode(',',$data['search']);
					foreach($ex_search as $v_search) {
						$article_title = str_ireplace(trim($v_search), '<span style="color:red"><b><i>' . trim($v_search). '</i></b></span>', $article_title);
					}
				}
				// if(!empty($url_live)){
				// 	$article_title = "<a href='{$url_live}' target='_BLANK'>{$article_title}</a>";
				// }
				//Get keyword
				if(!empty($v_list['keyword_id'])){
					$keyword = null;
					$keyword_id = unserialize($v_list['keyword_id']);
					//print_r($keyword_id);
					// foreach($keyword_id as $k){
					// 	// echo $k.'<br>';
					// 	print_r($k);
					// }
					foreach($data['list_keyword'] as $kk){
						$idK = $kk['id'];
						$nameK = $kk['name'];
						foreach($keyword_id as $k){
							if ($k == $idK) {
								$keyword .= $nameK.', ';
							}
						}
					}
				}else{
					$keyword = "-";
				}


				//media
				if (empty($v_list['media'])) {
					$media = "NO MEDIA";
				}


				echo '
					<tr>
						<td><center>'. $img_video .'</center></td>
						<td>
							<strong>' . $article_title . '</strong></br>
							<i>' . $v_list['description'] . '</i> </br></br>
							Category : <strong><i>' . $v_list['category_name'] . '</i></strong><br>
							Keyword : '.ucwords(substr($keyword,0,-2)).'

						</td>
						<td>
							' . ucfirst($v_list['ukom_name']) . '</br>(' .$v_list['community_name']. ')
						</td>
						<td>' . $video_status . '
							
						</td>
						<td>' . $button . '</td>
					</tr>
				';
			}
		}
		?>
	</table>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form role="form" method="post" action="<?php echo URL::Base(); ?>laporan/whatsapp">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Send Whatsapp</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label><?php echo __('Select Contact') ?></label>
					<select style="width: 100%" class="form-control" name="contact[]" id="" multiple>
						<?php
							foreach($data['list_contact'] as $k){
								echo '<option value="'.ucfirst($k['no']).'">'.ucfirst($k['name']).' ('.ucfirst($k['no']).')</option>';
							}
						?>
					</select>
				</div>
				<div class="form-group">
					<!-- <label><?php echo __('Message') ?></label> -->
					<!--<input type="hidden"  class="form-control" name="description" value="<?php //echo $v_list['title'].' - '.$portal; ?>"  placeholder="<?php // echo $v_list['title'].' - '.$portal; ?>">-->
					<input type="hidden"  class="form-control hidden-wa" name="description" value="">

                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Send</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form role="form" method="post" action="<?php echo URL::Base(); ?>laporan/sms">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Send SMS</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label><?php echo __('Select Contact') ?></label>
					<select style="width: 100%" class="form-control" name="contact[]" id="" multiple>
						<?php
							foreach($data['list_contact'] as $k){
								echo '<option value="'.ucfirst($k['no']).'">'.ucfirst($k['name']).' ('.ucfirst($k['no']).')</option>';
							}
						?>
					</select>
				</div>
				<div class="form-group">
					<!-- <label><?php echo __('Message') ?></label> -->
					<input type="hidden"  class="form-control hidden-wa" name="description" value="">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Send</button>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Video'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><a href='/home'><?php echo __('Home'); ?></a></li>
			<li class="active"><a href="/video"><?php echo __('Video'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Add New Data'); ?></h3>
					</div>
					<form role="form" method="post" onsubmit="return submit_video()" action="<?php echo URL::Base(); ?>video/save">
						<div class="box-body">
							<?php
							if(!empty($data['errors'])) {
								?>
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<h4><i class="icon fa fa-ban"></i> <?php echo __('Alert!'); ?></h4>
									<?php
									foreach($data['errors'] as $v_errors) {
										echo ucfirst($v_errors) . '</br>';
									}
									?>
								</div>
								<?php
							}
							?>
							<div class="form-group">
								<label><?php echo __('Horário') ?></label>
								<div class="input-group">
									<input type="text" name="publish_time" id="kotak1" value="<?php echo !empty($data['post']['publish_time']) ? $data['post']['publish_time'] : ''; ?>" placeholder="<?php echo __('Optional'); ?>"  required class="form-control publish_time">
									<div class="input-group-addon">
										<i class="fa fa-calendar" id="myButton"></i>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label><?php echo __('Título') ?></label>
								<input maxlength="60" type="text" id= "txttitle" name="title" value="<?php echo !empty($data['post']['title']) ? $data['post']['title'] : ''; ?>"  placeholder="<?php echo __('Only 60 Characters'); ?>" required  class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Image') ?></label>
								<div id="previewImage" style="width: 100%; height: auto; margin-bottom: 10px;">
									<?php
									$val_hidden_image = '';
									// Image preview edit
									if(!empty($data['post']['image'])) {
										// Get id image from full url image
										$base_image = basename($data['post']['image']).PHP_EOL;
										if(!empty($base_image)) {
											$ex_base = explode('.', $base_image);
											list($id_image, $file_type) = $ex_base;
										}
										$split_id = str_split($id_image);
										$path_folder_image = implode('/', $split_id);
										echo '<div style=" position: absolute; width: 20px; height: 20px; left: 186px; margin-top: 4px; background-color: rgb(255, 0, 0); "><center><span style="color: #FFFFFF;cursor: pointer;" onclick="javascript:delPreview();">X</span></center></div><img src="uploads/library/' . $path_folder_image . '/' . $id_image . '.' . $file_type . '" style="width:200px;">';
										$val_hidden_image = $data['post']['image'];
									}
									?>
								</div>
								<input type="hidden" name="image" id="image_popup" value="<?php echo $val_hidden_image; ?>" class="form-control">
								<a href="javascript:open_popup_img()"><button type="button" class="btn btn-block btn-default" id="image_popup_btn" style="width:200px"><?php echo __('Browse'); ?></button></a>
							</div>
							<div class="form-group">
								<label><?php echo __('Descrição') ?></label>
								<textarea class="form-control" rows="3" name="description" maxlength="140"  id="myinput2" placeholder="<?php echo __('Only 140 Characters'); ?>" required><?php echo !empty($data['post']['description']) ? $data['post']['description'] : ''; ?></textarea>
							</div>
							<?php 
								$sl_category = '';
								if(!empty($data['post']['category'])) {
									$sl_category = $data['post']['category'];
								}
								echo Video::select_list('category', 1, $sl_category); 
							?>
							<div class="form-group">
								<label><?php echo __('Palavra-chave') ?></label>
								<select style="width: 100%" class="form-control select2" name="keyword[]" id="" multiple>
									<?php 
										foreach($data['list_keyword'] as $k){
											echo '<option value="'.$k['id'].'">'.ucfirst($k['name']).'</option>';
										}
									?>
								</select>
							</div>
							<div class="form-group">
								<label><?php echo __('Palavra-chave Alternative') ?></label>
								<input maxlength="255" type="text" name="keyword_alt" value="<?php echo !empty($data['post']['keyword_alt']) ? $data['post']['keyword_alt'] : ''; ?>"  placeholder="<?php echo __('Only 255 Characters'); ?>"  class="form-control">
							</div>
							<div class="form-group">
								<label><?php echo __('Video URL') ?></label>
								<textarea class="form-control" rows="3" name="embed_url" placeholder="<?php echo __('Video URL From Youtube or Other Online Media Player'); ?>"><?php echo !empty($data['post']['embed_url']) ? $data['post']['embed_url'] : ''; ?></textarea>
							</div>
							
							<div class="form-group">
								<label><?php echo __('Detalhes') ?></label>
								<textarea id="wysiwyg" class="form-control f_tinymce" rows="15" name="detail" minlength="3" required><?php echo !empty($data['post']['detail']) ? $data['post']['detail'] : ' '; ?></textarea>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Guardar os dados'); ?></button>
							<a href="<?php URL::Base(); ?>/video"><button type="button" class="btn btn-danger"><?php echo __('Cancelar'); ?></button></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>

<?php defined('SYSPATH') or die('No direct script access.');

class Model_Globalmodel extends Model {
    
    public function select($table, $select, $where = '', $limit = '', $offset = '', $order = ''){
		$exec = DB::select()
			->select_array($select)
			->from($table);
			if(!empty($where)){
				foreach ($where as $key => $value) {
					$check = explode(" ",$key);
					if(!empty($check[1])){
						$field = $check[0];
						$op = $check[1];
					}else{
						$field = $check[0];
						$op = "=";
					}
					$exec = $exec->where($field,$op,$value);
				}
			}

			if(!empty($limit)){
				$exec = $exec->limit($limit);
			}

			if(!empty($limit) && !empty($offset)){
				$exec = $exec->limit($limit)
							->offset($offset);
			}

			if(!empty($order)){
				foreach($order as $k => $v){
					$exec = $exec->order_by($k, $v);
				}
			}

			$exec = $exec->execute()->as_array();
			return $exec;
	}

	public function save($table, $data){
		Database::instance()->begin();

		$key = array_keys($data);
		$val = array_values($data);

		$save =	DB::insert($table, $key)
					->values($val)->execute();

		// Commit transaction
		Database::instance()->commit();	
		return $save;		

	}

	public function update($table, $data, $where = '' ){
		//print_r($data);exit;
		Database::instance()->begin();
		$update = DB::update($table)
					->set($data);
				if(!empty($where)){
					foreach ($where as $key => $value) {
						$check = explode(" ",$key);
						if(!empty($check[1])){
							$field = $check[0];
							$op = $check[1];
						}else{
							$field = $check[0];
							$op = "=";
						}
						$update = $update->where($field,$op,$value);
					}
				}
				$update = $update->execute();

		// Commit transaction
		Database::instance()->commit();
		return $update;
	}

	public function delete($table, $where = ''){
		Database::instance()->begin();
		$delete = DB::delete($table);
		if(!empty($where)){
			foreach ($where as $key => $value) {
				$check = explode(" ",$key);
				if(!empty($check[1])){
					$field = $check[0];
					$op = $check[1];
				}else{
					$field = $check[0];
					$op = "=";
				}
				$delete = $delete->where($field,$op,$value);
			}
		}
		$delete = $delete->execute();

		// Commit transaction
		Database::instance()->commit();
		return $delete;
	}
}
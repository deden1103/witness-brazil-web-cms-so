<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo __('Contacto'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Contacto'); ?></li>
			<li class="active"><a href="#"><?php echo __('Editar'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo __('Contacto Editar'); ?></h3>
					</div>
					<form role="form" method="post" action="<?php echo URL::Base(); ?>contacts/update">
						<div class="box-body">
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Nome') ?></label>
								<input type="text" name="name" class="form-control" value="<?php echo $data['list'][0]['name']; ?>" required>
								<input type="hidden" name="id" value="<?php echo $data['list'][0]['id']; ?>">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1"><?php echo __('Número') ?></label>
								<input type="text" name="nomor" class="form-control" value="<?php echo $data['list'][0]['nomor']; ?>" required>
							</div>
							
						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-primary"><?php echo __('Guardar'); ?></button>
							<a href="<?php echo URL::Base(); ?>contacts/list" class="btn btn-danger"><?php echo __('Cancelar'); ?></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo __('Contacto Lista'); ?>
		</h1>
		<ol class="breadcrumb">
			<li><?php echo __('Contacto'); ?></li>
			<li class="active"><a href="/contacts/list"><?php echo __('Lista'); ?></a></li>
		</ol>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $data['count_all'] . ' ' . __('Dados'); ?></h3>
						<div class="box-tools" style="float: right;">
							<form method="post" action="<?php echo URL::Base(); ?>contacts/search">
								<div class="input-group" style="width: 150px;">
									<input type="text" name="search" class="form-control input-sm pull-right" placeholder="<?php echo __('Search'); ?>" value="<?php echo !empty($data['search']) ? $data['search'] : ''; ?>">
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<!-- List isi artikel -->
							<tr>
								<th style="width:210px;text-align:center;"><?php echo __('Nome'); ?></th>
								<th style="text-align:center;"><?php echo __('Number'); ?></th>
								<th colspan="3" style="width:30%;text-align:center;"><?php echo __('Actions'); ?></th>
							</tr>
							<?php
							if(!empty($data['list'])) {
								foreach($data['list'] as $v_list) {
									//$level = ($v_list['level'] == 1) ? "Admin" : ($v_list['level'] == 2) ? "Redaksi" : "Editor";
									
									echo '
										<tr id="row'.$v_list['id'].'">
											<td>' . $v_list['name'] . '</td>
											<td>' . $v_list['nomor'] . '</td>
											<td><a href="'.URL::Base().'contacts/edit/' . $v_list['id'] . '"><button class="btn btn-block btn-warning btn-xs">' . __('Edit') . '</button></a></td>
											<td><button class="btn btn-block btn-xs btn-danger" onclick="delUsr('.$v_list['id'].')">' . __('Apagar') . '</button>
											</td>
										</tr>
									';
								}
							}
							?>
						</table>
					</div>
					<?php echo !empty($data['pagination']) ? $data['pagination'] : ''; ?>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php

// Route List
Route::set('contactslist', 'contacts/list(/<page>)')
	->defaults(array(
		'controller' => 'Contacts',
		'action'     => 'list',
	));

// Route Search
Route::set('contactssearch', 'contacts/search(/<search>(/<page>))')
	->defaults(array(
		'controller' => 'Contacts',
		'action'     => 'search',
	));
	
// Route input
Route::set('contactsinput', 'contacts/new(/<action>(/<page>))')
	->defaults(array(
		'controller' => 'Contacts',
		'action'     => 'new',
	));
	
// Route Edit
Route::set('contactsedit', 'contacts/edit(/<id>)')
	->defaults(array(
		'controller' => 'Contacts',
		'action'     => 'edit',
	));
	
// Route Cahnge Password
Route::set('contactscpas', 'contacts/cpas(/<id>)')
	->defaults(array(
		'controller' => 'Contacts',
		'action'     => 'cpas',
	));
	
// Route Delete
Route::set('contactsdelete', 'contacts/delete(/<id>)')
	->defaults(array(
		'controller' => 'Contacts',
		'action'     => 'delete',
	));
	
// Route default
Route::set('contacts', 'contacts(/<action>(/<page>))')
	->defaults(array(
		'controller' => 'Contacts',
		'action'     => 'index',
	));
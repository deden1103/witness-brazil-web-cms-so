<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Contacts extends Controller_Backend {

	private $users_model = null;
	private $global_model = null;
	
	public function before() {
		parent::before();

		$level = Controller_Backend::check_level();
		if($level == 2){
			// $this->redirect(URL::Base().'/keranjang');
		}elseif($level == 3){
			$this->redirect(URL::Base().'/home');
			
		}

		$this->users_model = new Model_Contacts();
		$this->global_model = new Model_Globalmodel();
		
		$this->custom_header = '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.5/dist/sweetalert2.min.css">';

		$this->custom_footer =  '<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.5/dist/sweetalert2.all.min.js"></script>';
		$this->custom_footer .=  '
		<script>
			function delUsr(swal_id){
				swal({
					title: "Are you sure?",
					text: "You won\'t be able to revert this!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3085d6",
					cancelButtonColor: "#d33",
					confirmButtonText: "Yes, delete it!"
				  }).then((result) => {
					if (result.value) {
						$.ajax({
							url: "/contacts/delete/",
							dataType: "JSON",
							type: "POST",
							data: {swal_id : swal_id}, 
							success: function(res){
								$("#row"+swal_id).remove()
								swal(
									"Deleted!",
									"Your file has been deleted.",
									"success"
								  )
							},error: function(){
								swal(
									"Something Wrong Happen !!",
									"",
									"error"
								  )
							}
						})
					}
				  })
			}
		</script>';
	}

	public function action_index() {
		$this->redirect('/contacts/list');
	}
	
	public function action_new() {
		$data['main_title'] = "Contacs New";
		$data['menu_active'] = "contacts";
		$data['menu_active_child'] = "new";
		$view = Briliant::admin_template('contacts/' . Kohana::$config->load('path.main_template') . '/new', $data);
		$this->response->body($view);
	}
	
	public function action_submit() {
		
		// Load Model Users
		$result = $this->users_model->save_data($this->request->post());
		$this->redirect('/contacts/list');
	}
	
	public function action_list() {
		
		// Page from parameter
		$page = intval($this->request->param('page'));
		if(empty($page)) {
			$page = 1;
		}
		
		$data['main_title'] = "Contacts List";
		$data['menu_active'] = "contacts";
		$data['menu_active_child'] = "list";
		
		$data['count_all'] = $this->users_model->count_all();
		
		// Pagination
		$pagination = Pagination::factory(array(
							'total_items'    		=> $data['count_all'],
							'items_per_page'		=> 20,
							'current_page'			=> $page,
							'base_url'				=> '/contacts/list/',
							'view'					=> 'pagination/contacts'
						));
				
		$select = ['contId as id','contName as name','contNomor as nomor'];
		$data['list'] = $this->global_model->select(
			'contact', //table
			$select, //select
			['contStatus' => '1'], //where
			$pagination->items_per_page, //limit
			$pagination->offset, //offset
			['contId' => 'DESC'] //order
		);
		
		$data['pagination'] =  $pagination->render();
		
		$data['custom_header'] = $this->custom_header;
		$data['custom_footer'] = $this->custom_footer;
		
		
		$view = Briliant::admin_template('contacts/' . Kohana::$config->load('path.main_template') . '/list', $data);
		$this->response->body($view);
	}

	public function action_search() {
		
		$data['main_title'] = "Contacts List";
		$data['menu_active'] = 'contacts';
		$data['menu_active_child'] = 'list';
		
		// Redirect jika mengepost search agar bisa menggunakan paginasi dengan beauty url
		$post_search = $this->request->post('search');
		if(!empty($post_search)) {
			$this->redirect('/contacts/search/' . $post_search);
		}
		
		// Param search
		$search = $this->request->param('search');
		
		// Param page
		$page = intval($this->request->param('page'));
		if(empty($page)) {
			$page = 1;
		}
		
		
		// Count All Data
		$data['count_all'] = $this->users_model->count_search_data($search);
		
		$_search = (!empty($search)) ? '/contacts/search/' . $search . '/' : "/contacts/search/";
		if (!empty($search)) {
			$_search = '/contacts/search/' . $search . '/';
			$where = ['contName LIKE' => '%'.$search.'%','contStatus' => '1'];
		}else{
			$_search = '/contacts/list/';
			$where = ['contStatus' => '1'];
		}
		
		// Pagination
		$pagination = 	Pagination::factory(array(
							'total_items'    		=> $data['count_all'],
							'items_per_page'		=> 20,
							'current_page'			=> $page,
							'base_url'				=> $_search,
							'view'					=> 'pagination/contacts'
						));
					
		$select = ['contId as id','contName as name','contNomor as nomor'];
		$data['list'] = $this->global_model->select(
			'contact', //table
			$select, //select
			$where, //where
			$pagination->items_per_page, //limit 
			$pagination->offset, //offset
			['contId' => 'DESC'] //order
		);
		
		$data['search'] = $search;
		$data['pagination'] =  $pagination->render();
	
		$data['custom_header'] = $this->custom_header;
		$data['custom_footer'] = $this->custom_footer;	
		
		$view = Briliant::admin_template('contacts/' . Kohana::$config->load('path.main_template') . '/list', $data);
		$this->response->body($view);
		
	}
	
	public function action_edit() {
		
		// Id from parameter
		$id = intval($this->request->param('id'));
		

		$select = ['contId as id','contName as name','contNomor as nomor'];
		$where = ['contId' => $id, 'contStatus' => 1];
		$data['list'] = $this->global_model->select('contact', $select, $where);
		
		if(empty($data['list'])){
			$this->redirect('/contacts/list');
		}

		$data['main_title'] = "Contacts Edit";
		$data['menu_active'] = "contacts";
		$data['menu_active_child'] = "edit";
		
		$view = Briliant::admin_template('contacts/' . Kohana::$config->load('path.main_template') . '/edit', $data);
		$this->response->body($view);
	}
	
	public function action_update() {
		
		$id = $this->request->post('id');
		$update = $this->users_model->update_data($id, $this->request->post());
		
		$this->redirect('/contacts/list');
	}
	
	public function action_cpas() {
		
		// Id from parameter
		$id = intval($this->request->param('id'));
		$data = $this->users_model->data_by_id($id);
		
		$data['main_title'] = "Users Edit";
		$data['menu_active'] = "users";
		$data['menu_active_child'] = "edit";
		
		$view = Briliant::admin_template('users/' . Kohana::$config->load('path.main_template') . '/cpas', $data);
		$this->response->body($view);
	}
	
	public function action_cpas_submit(){
		// Id from parameter
		$id = intval($this->request->post('id'));
		
		 // Load Model Users
		$data = $this->users_model->data_by_id($id);
		
		$data['main_title'] = "Users Edit";
		$data['menu_active'] = "users";
		$data['menu_active_child'] = "edit";
		
        $validation = Validation::factory($this->request->post())
                        ->rule('password', 'not_empty')
                        ->rule('retype_password', 'not_empty')
                        ->rule('retype_password',  'matches', array(':validation', 'retype_password', 'password'));
        
        if($validation->check()) {
            
           
			$password = $this->request->post('password');
                
			// Change password
			$this->users_model->change_password($id, $this->request->post('password'));
			
			$data['success'] = 1;
                
        } else { // Validation Error
            
            $data['errors'] = $validation->errors('validation');
            
        }

		$view = Briliant::admin_template('users/' . Kohana::$config->load('path.main_template') . '/cpas', $data);
		$this->response->body($view);
	}
	
	public function action_delete() {
		if(isset($_POST["swal_id"])){
			$id = $_POST['swal_id'];
			$update = $this->users_model->delete_data($id);
			if ($update == 1) {
				echo json_encode($update);
			}else{
				http_response_code(500);
			}
		}
	}
	
}

<?php defined('SYSPATH') or die('No direct script access.');

class Model_Contacts extends Model {
	
	public function count_all() {
		$return = 0;
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('contact')
					->where('contStatus','=',1)
					->execute()
					->as_array();
					
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function save_data($data = array()) {
		$query =	DB::insert('contact', array(
						'contName',
						'contNomor'
					))
					->values(array(
						$data['name'],
						$data['nomor']
						))
					->execute();
		return $query;
	}
	
	public function data_by_id($id = '') {
		
		$return  = array();
		
		$exec = DB::select(
					array('contId', 'id'),
					array('contName', 'name'),
					array('contNomor', 'nomor')
				)
				->from('contact')
				->where('contId', '=', $id)
				->execute()
				->as_array();
		if(!empty($exec[0])) {
			$return = $exec[0];
		}
		
		return $return;
		
	}
	
	public function update_data($id = '', $data = '') {
		$query =	DB::update('contact')
					->set(array('contName' => $data['name']))
					->set(array('contNomor' => $data['nomor']))
					->where('contId', '=', $id)
					->execute();
	}
	
	public function delete_data($id = '') {
		$query =	DB::update('contact')
					->set(array('contStatus' => 0))
					->where('contId', '=', $id)
					->execute();
		return $query;			
	}
	
	public function count_search_data($search = '') {
		
		$return = 0;
		
		$query =	DB::select(array(DB::expr('COUNT(1)'), 'COUNT'))
					->from('contact')
					->where('contName', 'LIKE', '%' . $search . '%')
					->execute()
					->as_array();
				
		if(!empty($query[0]['COUNT'])) {
			$return = $query[0]['COUNT'];
		}
		
		return $return;
		
	}
	
	public function change_password($id = '', $password = '') {
		$query =	DB::update('admin')
					->set(
						array('admiPassword' => SHA1($password))
					)
					->where('admiId', '=', $id)
					->execute();
	}

}
<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Classlayout {
    public function masterview($datas='', $file_view, $content='', $title='', $footer='', $header=''){

        $CI =& get_instance();

        $data['session'] = $CI->session->userdata();
        $data['current_url'] =  base_url(uri_string());
        $data['header'] = $header;
        $data['title'] = $title;
        $data['content'] = '';
        $data['footer'] = $footer;
        $data['datas'] = $datas;

        if(is_array($content)){
            foreach($content as $k=>$v){
                if(!isset($v) OR empty($v)){
                    $v = array();
                }
                $data['content'] .= $CI->load->view($k, $v, true);
            }
        }
        $CI->load->view("/partials/header", $data);
        $CI->load->view($file_view, $data);
        $CI->load->view("/partials/footer", $data);
    }

}

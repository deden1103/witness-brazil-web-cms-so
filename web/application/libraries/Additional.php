<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Additional {
    public function cut_text($text='', $num_char=''){
        $cut_text = substr($text, 0, $num_char);
        if ($text{$num_char - 1} != ' ') { // jika huruf ke 50 (50 - 1 karena index dimulai dari 0) buka  spasi
            $new_pos = strrpos($cut_text, ' '); // cari posisi spasi, pencarian dari huruf terakhir
            $cut_text = substr($text, 0, $new_pos);
        }

        return $cut_text. '...';
    }

    public function slugify($text) {
		$text = str_replace('\'', '', $text);
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		// trim
		$text = trim($text, '-');
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		// lowercase
		$text = strtolower($text);
		if (empty($text)) {
			return 'n-a';
		}
		return $text;
	}

	public function indonesian_date($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
		if (trim($timestamp) == '') {
			$timestamp = time();
		} elseif (!ctype_digit($timestamp)) {
			$timestamp = strtotime($timestamp);
		}
		# remove S (st,nd,rd,th) there are no such things in indonesia :p
		$date_format = preg_replace("/S/", "", $date_format);
		$pattern     = array(
			'/Mon[^day]/',
			'/Tue[^sday]/',
			'/Wed[^nesday]/',
			'/Thu[^rsday]/',
			'/Fri[^day]/',
			'/Sat[^urday]/',
			'/Sun[^day]/',
			'/Monday/',
			'/Tuesday/',
			'/Wednesday/',
			'/Thursday/',
			'/Friday/',
			'/Saturday/',
			'/Sunday/',
			'/Jan[^uary]/',
			'/Feb[^ruary]/',
			'/Mar[^ch]/',
			'/Apr[^il]/',
			'/May/',
			'/Jun[^e]/',
			'/Jul[^y]/',
			'/Aug[^ust]/',
			'/Sep[^tember]/',
			'/Oct[^ober]/',
			'/Nov[^ember]/',
			'/Dec[^ember]/',
			'/January/',
			'/February/',
			'/March/',
			'/April/',
			'/June/',
			'/July/',
			'/August/',
			'/September/',
			'/October/',
			'/November/',
			'/December/'
		);
		$replace     = array(
			'Sen',
			'Sel',
			'Rab',
			'Kam',
			'Jum',
			'Sab',
			'Min',
			'Senin',
			'Selasa',
			'Rabu',
			'Kamis',
			'Jumat',
			'Sabtu',
			'Minggu',
			'Jan',
			'Feb',
			'Mar',
			'Apr',
			'Mei',
			'Jun',
			'Jul',
			'Ags',
			'Sep',
			'Okt',
			'Nov',
			'Des',
			'Januari',
			'Februari',
			'Maret',
			'April',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$date        = date($date_format, $timestamp);
		$date        = preg_replace($pattern, $replace, $date);
		$date        = "{$date} {$suffix}";
		return $date;
	}

	function getImage($getEcommerce = array(), $table_images, $loop_object, $id_images){
		$CI =& get_instance();
		$CI->load->model('m_global');
		$table_images = $table_images;
		$select_images = 'd.'.$id_images.',e.arimId, e.arimTitle, e.arimCaption, e.arimFileType';
		
		$join_images = 	[
		
			'arsip_images e, RIGHT' => 'd.'.$id_images.'= e.arimId'
		
		];
		
		foreach($getEcommerce as $k=>$v){
			$where_images = 	[
									'e.arimId' => $v[$loop_object],
									'e.arimActive' => 1
								];
			$getEcommerceImage= $CI->m_global->get($table_images, $select_images, $join_images, $where_images);
			//print_r($getEcommerceImage);

			if(!empty($getEcommerceImage)){
				$getEcommerce_arimId = $getEcommerceImage[0]['arimId'];
				$getEcommerce_arimFileType = $getEcommerceImage[0]['arimFileType'];
				$getEcommerceImage_split_id = implode("/", str_split($getEcommerce_arimId));
				$getEcommerce[$k]['images']['title'] =  $getEcommerceImage[0]['arimTitle'];
				$getEcommerce[$k]['images']['caption'] =  $getEcommerceImage[0]['arimCaption'];
				$getEcommerce[$k]['images']['src'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'.'.$getEcommerce_arimFileType;
				$getEcommerce[$k]['images']['src_224x153'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_224x153.'.$getEcommerce_arimFileType;
				$getEcommerce[$k]['images']['src_263x180'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_263x180.'.$getEcommerce_arimFileType;
				$getEcommerce[$k]['images']['src_300x206'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_300x206.'.$getEcommerce_arimFileType;
				$getEcommerce[$k]['images']['src_512x351'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_512x351.'.$getEcommerce_arimFileType;
				$getEcommerce[$k]['images']['src_683x468'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_683x468.'.$getEcommerce_arimFileType;
				$getEcommerce[$k]['images']['src_840x576'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_840x576.'.$getEcommerce_arimFileType;
			}
		} //die;

		return $getEcommerce;
	}

	function getImage_more($getEcommerce = array(), $table_images, $loop_object, $id_images){
		$CI =& get_instance();
		$CI->load->model('m_global');
		$table_images = $table_images;
		$select_images = 'd.'.$id_images.',e.arimId, e.arimTitle, e.arimCaption, e.arimFileType';
		
		$join_images = 	[
		
			'arsip_images e, RIGHT' => 'd.'.$id_images.'= e.arimId'
		
		];
		
		foreach($getEcommerce as $k=>$v){
			foreach($v['images'] as $k_image=>$v_image){
				$where_images = 	[
										'e.arimId' => $v_image[$loop_object],
										'e.arimActive' => 1
									];
				$getEcommerceImage= $CI->m_global->get($table_images, $select_images, $join_images, $where_images);
				//print_r($getEcommerceImage);

				if(!empty($getEcommerceImage)){
					$getEcommerce_arimId = $getEcommerceImage[0]['arimId'];
					$getEcommerce_arimFileType = $getEcommerceImage[0]['arimFileType'];
					$getEcommerceImage_split_id = implode("/", str_split($getEcommerce_arimId));
					$getEcommerce[$k]['images'][$k_image]['title'] =  $getEcommerceImage[0]['arimTitle'];
					$getEcommerce[$k]['images'][$k_image]['caption'] =  $getEcommerceImage[0]['arimCaption'];
					$getEcommerce[$k]['images'][$k_image]['src'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'.'.$getEcommerce_arimFileType;
					$getEcommerce[$k]['images'][$k_image]['src_224x153'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_224x153.'.$getEcommerce_arimFileType;
					$getEcommerce[$k]['images'][$k_image]['src_263x180'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_263x180.'.$getEcommerce_arimFileType;
					$getEcommerce[$k]['images'][$k_image]['src_300x206'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_300x206.'.$getEcommerce_arimFileType;
					$getEcommerce[$k]['images'][$k_image]['src_512x351'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_512x351.'.$getEcommerce_arimFileType;
					$getEcommerce[$k]['images'][$k_image]['src_683x468'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_683x468.'.$getEcommerce_arimFileType;
					$getEcommerce[$k]['images'][$k_image]['src_840x576'] = URL_IMG.$getEcommerceImage_split_id.'/'.$getEcommerce_arimId.'_840x576.'.$getEcommerce_arimFileType;
				}
			}
		}

		return $getEcommerce;
	}
	
}
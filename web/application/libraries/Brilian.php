<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Brilian {
	
	public function get_api($url = '', $header = '') {
		//echo "<div style='display:none'> \n";
		$file_name = str_replace('/', '_', $url) . '.json';
		//echo "/mnt/data/html/wirausahabrilian/cache/api/" . $file_name;
		if($header == '' AND file_exists('/mnt/data/cache/api/' . $file_name)) {
			//echo " = read cache file \n";
			//echo "</div> \n";
			$data = file_get_contents('/mnt/data/cache/api/' . $file_name);
			return json_decode($data, TRUE);
		} else {
			//echo "<div style='display:none'> \n";
			//echo " = curl api " . $url;
			//echo "</div> \n";
			
			/*
			$text_header = '';
			if(!empty($header)) {
				foreach($header as $v_header) {
					$text_header .= '-H "' . $v_header . '" ';
				}
			}
			$exec = exec('curl ' . $text_header . URL_REST . '/' . $url);
			return json_decode($exec, TRUE);
			*/
			
			// Diganti ke versi lama karena curl tidak bisa membaca spasi di url
			//error_log($url."/n",3,'get_api_old.log');
			//return exec("php -f /mnt/data/html/wirausahabrilian/rest/");
			return $this->get_api_old($url, $header);
		}
	}
	
	public function get_api_old($url = '', $header = '') {
		$data = array();
		if (!empty($url)) {
			$ch = curl_init();
			if(!empty($header)){
				curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			}
			curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, URL_REST . '/' . $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			$data = curl_exec($ch);
			$data = json_decode($data, TRUE);
			curl_close($ch);
		}
		return $data;
	}
	
	public function post_api($url = '', $header = '', $request = '') {
		$data = array();
		if (!empty($url)) {
			$ch      = curl_init($url);
			$options = array(
				CURLOPT_RETURNTRANSFER 		=> true, // return web page
				CURLOPT_HEADER 				=> false, // don't return headers
				CURLOPT_FOLLOWLOCATION 		=> false, // follow redirects
				// CURLOPT_ENCODING       	=> "utf-8",           // handle all encodings
				CURLOPT_AUTOREFERER 		=> true, // set referer on redirect
				CURLOPT_CONNECTTIMEOUT 		=> 20, // timeout on connect
				CURLOPT_TIMEOUT 			=> 20, // timeout on response
				CURLOPT_POST 				=> 1, // i am sending post data
				CURLOPT_POSTFIELDS 			=> $request, // this are my post vars
				CURLOPT_SSL_VERIFYHOST 		=> 0, // don't verify ssl
				CURLOPT_SSL_VERIFYPEER 		=> false, //
				CURLOPT_VERBOSE 			=> 1,
				CURLOPT_HTTPHEADER 			=> $header
				
			);
			curl_setopt_array($ch, $options);
			$data       = curl_exec($ch);
			$curl_errno = curl_errno($ch);
			$curl_error = curl_error($ch);
			//echo $curl_errno;
			//echo $curl_error;
			curl_close($ch);
		}
		return json_decode($data, TRUE);
	}
	
	public function slugify($text) {
		$text = str_replace('\'', '', $text);
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		// trim
		$text = trim($text, '-');
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		// lowercase
		$text = strtolower($text);
		if (empty($text)) {
			return 'n-a';
		}
		return $text;
	}
	
	public function indonesian_date($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
		if (trim($timestamp) == '') {
			$timestamp = time();
		} elseif (!ctype_digit($timestamp)) {
			$timestamp = strtotime($timestamp);
		}
		# remove S (st,nd,rd,th) there are no such things in indonesia :p
		$date_format = preg_replace("/S/", "", $date_format);
		$pattern     = array(
			'/Mon[^day]/',
			'/Tue[^sday]/',
			'/Wed[^nesday]/',
			'/Thu[^rsday]/',
			'/Fri[^day]/',
			'/Sat[^urday]/',
			'/Sun[^day]/',
			'/Monday/',
			'/Tuesday/',
			'/Wednesday/',
			'/Thursday/',
			'/Friday/',
			'/Saturday/',
			'/Sunday/',
			'/Jan[^uary]/',
			'/Feb[^ruary]/',
			'/Mar[^ch]/',
			'/Apr[^il]/',
			'/May/',
			'/Jun[^e]/',
			'/Jul[^y]/',
			'/Aug[^ust]/',
			'/Sep[^tember]/',
			'/Oct[^ober]/',
			'/Nov[^ember]/',
			'/Dec[^ember]/',
			'/January/',
			'/February/',
			'/March/',
			'/April/',
			'/June/',
			'/July/',
			'/August/',
			'/September/',
			'/October/',
			'/November/',
			'/December/'
		);
		$replace     = array(
			'Sen',
			'Sel',
			'Rab',
			'Kam',
			'Jum',
			'Sab',
			'Min',
			'Senin',
			'Selasa',
			'Rabu',
			'Kamis',
			'Jumat',
			'Sabtu',
			'Minggu',
			'Jan',
			'Feb',
			'Mar',
			'Apr',
			'Mei',
			'Jun',
			'Jul',
			'Ags',
			'Sep',
			'Okt',
			'Nov',
			'Des',
			'Januari',
			'Februari',
			'Maret',
			'April',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$date        = date($date_format, $timestamp);
		$date        = preg_replace($pattern, $replace, $date);
		$date        = "{$date} {$suffix}";
		return $date;
	}
	
	public function is_wirausaha($uuid = '', $token = '') {
		$return = false;
		// Jika dia blm login return false
		// Jila dia sudah login maka ambil detail user nya dan cek is_wirausaha nya
		// Jika is wirausaha = 0 return false
		// Jika is wirausaha = 1 return true
		return $return;
	}
	
	public function check_access($uuid = 'web', $version = 'web', $token = '') { //check access login n wirausaha
		$is_wirausaha = $this->get_api('user/wirausaha', array('uuid: ' . $uuid, 'version: ' . $version, 'token: '.$token));	
		
		if (!empty($is_wirausaha['message']['token']) AND $is_wirausaha['message']['token'] == 'invalid token') { // Belum Login arahkan ke login
			echo "<script>alert('Anda Harus Login Terlebih Dahulu Untuk Akses Menu Ini')</script>";
			echo "<script>window.location.href = '/account/login'</script>";
			exit();
		} else if($is_wirausaha['data']['is_wirausaha'] != 1) { // Bukan wirausaha maka redirect ke edit profile
			echo "<script>alert('Anda Harus Melengkapi Profil Anda Terlebih Dahulu Untuk Akses Selengkapnya')</script>";
			echo "<script>window.location.href = '/edit'</script>";
			exit();
		}
	}
	
	public function check_access_login($uuid = 'web', $version = 'web', $token = '') { //check access login
		$is_wirausaha = $this->get_api('user/wirausaha', array('uuid: ' . $uuid, 'version: ' . $version, 'token: '.$token));	
		
		if (!empty($is_wirausaha['message']['token']) AND $is_wirausaha['message']['token'] == 'invalid token') { // Belum Login arahkan ke login
			echo "<script>alert('Anda Harus Login Terlebih Dahulu Untuk Akses Menu Ini')</script>";
			echo "<script>window.location.href = '/account/login'</script>";
			exit();
			
		} /*else if($is_wirausaha['data']['is_wirausaha'] != 1) { // Bukan wirausaha maka redirect ke edit profile
			echo "<script>alert('Anda Harus Melengkapi Profil Anda Terlebih Dahulu Untuk Akses Menu Ini')</script>";
			echo "<script>window.location.href = '/edit'</script>";
			exit();
		}*/
	}
}

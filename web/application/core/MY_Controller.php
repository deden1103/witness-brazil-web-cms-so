<?php
defined('BASEPATH') or exit('No direct script access allowed');

//call in application/core
class Syams extends CI_Controller{

    public function __construct(){
      parent::__construct();
    }

    public function kanal(){
      $color = array(
        'home',
        'travel',
        'tech',
        'lifestyle',
        'fashion',
        'sports',
        'food',
        'market'
      );

      $this->load->model('M_global');
      $data['list_kanal'] = $this->M_global->get('master_category_video','','','mscvStatus = 1','','','mscvId ASC');

      $key_color = 0;
      foreach($data['list_kanal'] as $k=>$v){
        $data['list_kanal'][$k]["color"] = $color[$key_color];
        $data['list_kanal'][$k]["link"] = "/kanal/type/" . $v["mscvId"] . "/" .preg_replace('/\W+/', '-', strtolower($v["mscvName"])) . ".html";

        if($key_color == (sizeof($color)-1)){
          $key_color = 0;
        }else{
          $key_color++;
        }
      }

      return $data['list_kanal'];
    }


     public function hot_topic(){
      $color = array(
        // 'home',
        'travel',
        'tech',
        'lifestyle',
        'fashion',
        'sports',
        'food',
        'market'
      );

      $this->load->model('M_global');
      $data['list_topic'] = $this->M_global->get('master_hot_topic','',["master_keyword_video, left" => "master_hot_topic.topicName = master_keyword_video.vkeyId"],'topicStatus > 0 ','','','topicSort ASC');

      $key_color = 0;
      foreach($data['list_topic'] as $k=>$v){
        $data['list_topic'][$k]["color"] = $color[$key_color];
        $data['list_topic'][$k]["link"] = "/kanal/keyword/" . $v["vkeyId"] . "/" .preg_replace('/\W+/', '-', strtolower($v["vkeyName"])) . ".html";

        if($k == (sizeof($color)-1)){
          $key_color = 0;
        }else{
          $key_color++;
        }
      }

      return $data['list_topic'];
    }


}

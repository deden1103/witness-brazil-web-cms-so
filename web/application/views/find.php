		<!-- Content ============================================= -->
		<section id="content">
			<div class="content-wrap" style="margin-top: -60px">
				<div class="container clearfix">
					<div class="row clearfix">
						<div class="col-lg-8">
							<div class="clearfix" style="margin-top: -4.5px;" id="boxContent">
								<!-- Trending Areas ============================================= -->
								<h4 class="mb-2 ls1 uppercase t700">Find Articles <?= !empty($datas["keyword"]) ? " With Keyword = ".$datas["keyword"]:"" ?></h4>
								<div class="line line-xs line-sports"></div>
								<?php foreach($datas["list_news"] as $v){ ?>
								<div class="ipost mb-4 mb-lg-4 row clearfix cumi" data-publish-time="<?= $v["artcPublishTime"] ?>">
									<div class="col-md-5">
										<div class="entry-image mb-0">
											<a href="<?= $v["link"] ?>"><img src="<?= $v["image"] ?>" alt="Image"></a>
											<!-- <div class="entry-categories"><a href="#" class="bg-sports">sports</a></div> -->
										</div>
									</div>
									<div class="col-md-7">
										<div class="entry-title mt-lg-0 mt-3">
											<h3><a href="<?= $v["link"] ?>"><?= $v["artcTitle"] ?></a></h3>
										</div>
										<ul class="entry-meta clearfix">
											<?php
												if($v["isShowUser"]==0){
											?>
												<li><span>by</span> <a href="<?= $v["link_writer"] ?>"><?= $v["ukomName"] ?></a></li>
											<?php
												}else{}
											?>
											<li><i class="icon-line-clock"></i><a href="<?= $v["link_publishtime"] ?>"><?= $v["showPublishTime"] ?></a></li>
										</ul>
										<div class="entry-content mt-0">
											<p><?= $v["artcExcerpt"] ?></p>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
							<!-- Infinity Scroll Loader ============================================= -->
							<div class="row clearfix">
								<div class="col-md-12 text-center">
									<button class="mt-4 button button-dark bg-dark button-rounded ls1 uppercase load-next-portfolio btn-load-more" onclick="loadmore()">Load more...</button>
								</div>
							</div>
						</div>

						<!-- Second Sidebar ============================================= -->
						<div class="col-lg-4 sticky-sidebar-wrap mt-5 mt-lg-0">
							<div class="sticky-sidebar">

								<!-- Sidebar Widget 2 ============================================= -->
								<div class="widget clearfix">
									<h4 class="mb-2 ls1 uppercase t700">Recent Posts</h4>
									<div class="line line-xs line-home"></div>
									<!-- Post Article -->
									<?php foreach($datas["list_recent"] as $v){ ?>
									<article class="spost pt-0 notopborder clearfix">
										<div class="entry-image">
											<a href="<?= $v["link"] ?>"><img src="<?= $v["image"] ?>" alt=""></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4 class="t600"><a href="<?= $v["link"] ?>"><?= $v["artcTitle"] ?></a></h4>
											</div>
											<ul class="entry-meta clearfix">
												<?php
													if($v["isShowUser"]==0){
												?>
													<li><span>by</span> <a href="<?= $v["link_writer"] ?>"><?= $v["ukomName"] ?></a></li>
												<?php
													}else{}
												?>
												<li><i class="icon-time"></i><a href="<?= $v["link_publishtime"] ?>"><?= $v["showPublishTime"] ?></a></li>
												<li><a href="<?= $v["link_kanal"] ?>"><?= $v["mscvName"] ?></a></li>
											</ul>
										</div>
									</article>
									<?php } ?>
								</div>
							</div>

						</div>
					</div>

				</div>

			</div>

		</section><!-- #content end -->

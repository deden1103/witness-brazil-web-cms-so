<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0"
   xmlns:content="http://purl.org/rss/1.0/modules/content/"
   xmlns:wfw="http://wellformedweb.org/CommentAPI/"
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:atom="http://www.w3.org/2005/Atom"
   xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
   xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
>
   <channel>
      <title>Tempo Witness</title>
    <atom:link href="https://witness.tempo.co/rss.xml" rel="self" type="application/rss+xml" />
      <link>https://witness.tempo.co/</link>
      <description>Tempo Witness</description>
	   <language>id</language>
      <lastBuildDate><?php echo date("D, d M Y H:i:s", strtotime('now')); ?> +0700</lastBuildDate>
      <updated><?php echo date("D, d M Y H:i:s", strtotime('now')); ?> +0700</updated>
      <?php
         if(!empty($rss)) {
            foreach($rss as $v_rss) { ?>
               <item>
                  <guid><?php echo 'https://witness.tempo.co'.$v_rss['link']; ?></guid>
                  <link><?php echo 'https://witness.tempo.co'.$v_rss['link']; ?></link>
                  <title><![CDATA[<?php echo $v_rss['artcTitle']; ?>]]></title>
                  <pubDate><?php echo date("D, d M Y H:i:s", strtotime($v_rss['artcPublishTime'])); ?> +0700</pubDate>
                  <image>
                     <url><?php echo $v_rss['image']; ?></url>
                  </image>
                  <description><![CDATA[<?php echo $v_rss['artcExcerpt']; ?>]]></description>
                  <content:encoded><![CDATA[<?php echo strip_tags($v_rss['artcDetail']); ?>]]></content:encoded>
               </item>
      <?php
            }
         }
      ?>
   </channel>
</rss>

<?php /*
$str = '<?xml version="1.0" encoding="UTF-8"?>';
$str .= '<rss version="2.0"
   xmlns:content="http://purl.org/rss/1.0/modules/content/"
   xmlns:wfw="http://wellformedweb.org/CommentAPI/"
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:atom="http://www.w3.org/2005/Atom"
   xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
   xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
>';
$str .= '<channel>';
$str .= '<title>Tempo Witness</title>';
$str .= '<atom:link href="http://betahita.id/rss.xml" rel="self" type="application/rss+xml" />';
$str .= '<link>http://betahita.id/</link>';
$str .= '<description>Betahita.id</description>';
$str .= '<language>id</language>';
   if(!empty($rss)) {
      foreach($rss as $v_rss) {
      $str .= '<item>';
      $str .= '<guid>'.$v_rss["link"].'</guid>';
      $str .= '<link>'.$v_rss["link"].'</link>';
      $str .= '<title><![CDATA['.$v_rss["newsTitle"].']]></title>';
      $str .= '<pubDate>'.date("D, d M Y H:i:s", strtotime($v_rss["newsPublishTime"])).' +0700</pubDate>';
      $str .= '<image>';
      $str .= '<url>'.$v_rss["image"].'</url>';
      $str .= '</image>';
      $str .= '<description><![CDATA['.$v_rss["newsExcerpt"].']]></description>';
      $str .= '<content:encoded><![CDATA['.$v_rss["newsDetail"].']]></content:encoded>';
      $str .= '</item>';
      }
   }
$str .= '</channel>';
$str .= '</rss>';
echo $str;
*/
?>
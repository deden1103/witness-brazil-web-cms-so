<!-- Content
============================================= -->
<section id="content">

  <div class="content-wrap" style="margin-top: -50px">

    <div class="container clearfix">

      <!-- Post Content
      ============================================= -->
      <div class="postcontent nobottommargin clearfix">

        <div class="single-post nobottommargin">

          <!-- Single Post
          ============================================= -->
          <div class="entry clearfix">

            <!-- Entry Title
            ============================================= -->
            <!-- <div class="entry-title">
              <h2>Tentang Kami</h2>
            </div> -->
            <!-- .entry-title end -->

            <!-- Entry Content
            ============================================= -->
            <div class="entry-content notopmargin">

              <p><?= $datas["isi"] ?></p>

              <div class="clear"></div>

            </div>
          </div><!-- .entry end -->
        </div>

      </div><!-- .postcontent end -->

    </div>
  </div>
</section><!-- #content end -->

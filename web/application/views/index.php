		<!-- Content ============================================= -->
		<section id="content"> <?php //print_r($datas["latest"]); die; ?>

			<div class="content-wrap" style="margin-top: -60px">

				<div class="container clearfix">

					<div class="row clearfix">

						<!-- Posts Area ============================================= -->
						<div class="col-lg-8">

							<!-- Tab Menu ============================================= -->
							<nav class="navbar navbar-expand-lg navbar-light p-0">
								<h4 class="mb-0 pr-2 ls1 uppercase t700" style="padding-top: 6.5px">Artikel Unggulan</h4>
							</nav>
							<!-- Tab Content ============================================= -->
							<div class="line line-xs line-dark"></div>
							<div class="tab-content" id="nav-tabContent1">

								<!-- Tab Content 1 ============================================= -->
								<div class="tab-pane fade show active" id="" role="" aria-labelledby="">
									<div class="row clearfix">
										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												if(!empty($datas["featured"][0])){
													?>

											<article class="entry nobottomborder nobottommargin">
												<div class="entry-image mb-3">
													<a href="<?= $datas["featured"][0]["link"] ?>"><img src="<?= $datas["featured"][0]["image"] ?>" alt="Image 3" style="height: 300px !important; object-fit: cover;"></a>
												</div>
												<div class="entry-title">
													<h3><a href="<?= $datas["featured"][0]["link"] ?>"><?= $datas["featured"][0]["artcTitle"] ?></a></h3>
												</div>
												<ul class="entry-meta clearfix">
													<?php
														if($datas["featured"] [0]["isShowUser"]==0){
													?>
														<li><span>by</span> <a href="<?= $datas["featured"] [0]["link_writer"] ?>"><?= $datas["featured"][0]["ukomName"] ?></a></li>
													<?php
														}else{}
													?>
													<li><i class="icon-time"></i><a href="<?= $datas["featured"][0]["link_publishtime"] ?>"><?= $datas["featured"][0]["showPublishTime"] ?></a></li>
												</ul>
												<div class="entry-content clearfix">
													<p><?= $datas["featured"][0]["artcExcerpt"] ?></p>
												</div>
											</article>

												<?php
												}
											?>

										</div>

										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												foreach($datas["featured"] as $key=>$val){
													if($key > 0){
											?>
											<article class="spost clearfix">
												<div class="entry-image">
													<a href="<?= $val["link"] ?>"><img src="<?= $val["image"] ?>" alt=""></a>
												</div>
												<div class="entry-c">
													<div class="entry-title">
														<h4 class="t600"><a href="<?= $val["link"] ?>"><?= $val["artcTitle"] ?></a></h4>
													</div>
													<ul class="entry-meta clearfix">
														<?php
															if($val["isShowUser"]==0){
														?>
															<li><span>by</span> <a href="<?= $val["link_writer"] ?>"><?= $val["ukomName"] ?></a></li>
														<?php
															}else{}
														?>
														<li><i class="icon-time"></i><a href="<?= $val["link_publishtime"] ?>"><?= $val["showPublishTime"] ?></a></li>
													</ul>
												</div>
											</article>
										<?php
												}
											}
										?>

										</div>
									</div>
								</div>


							</div><!-- Tab End -->
							

							<nav class="navbar navbar-expand-lg navbar-light p-0">
								<h4 class="mb-0 pr-2 ls1 uppercase t700">Artikel Terbaru</h4>
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler1" aria-controls="navbarToggler1" aria-expanded="false" aria-label="Toggle navigation">
									<i class="icon-line-menu"></i>
								</button>

								<div class="collapse navbar-collapse justify-content-between" id="navbarToggler1">
									<div></div>
									<ul class="nav nav-sm navbar-nav mr-md-auto mr-lg-0 mt-2 mt-lg-0 align-self-end" role="tablist">
										<li class="nav-item">
											<a class="nav-link bg-color-home active" id="nav-outdoor-tab" data-toggle="tab" href="#navi-1" role="tab" aria-selected="true">Semua</a>
										</li>
										<li class="nav-item">
											<a class="nav-link bg-color-travel" id="nav-outdoor-tab" data-toggle="tab" href="#navi-2" role="tab" aria-selected="true">Foto</a>
										</li>
										<li class="nav-item">
											<a class="nav-link bg-color-tech" id="nav-outdoor-tab" data-toggle="tab" href="#navi-3" role="tab" aria-selected="true">Video</a>
										</li>
										<li class="nav-item">
											<a class="nav-link bg-color-lifestyle" id="nav-outdoor-tab" data-toggle="tab" href="#navi-4" role="tab" aria-selected="true">Audio</a>
										</li>
										<li class="nav-item">
											<a class="nav-link bg-color-fashion" id="nav-outdoor-tab" data-toggle="tab" href="#navi-5" role="tab" aria-selected="true">Teks</a>
										</li>
									</ul>
								</div>
							</nav>
							<!-- Tab Content ============================================= -->
							<div class="line line-xs line-dark"></div>
							<div class="tab-content" id="nav-tabContent">
							<?php 
								if(!empty($_GET['sort'])){
							?>
								<form>
									<div class="btn-group btn-group-toggle" data-toggle="buttons">
										<label for="" class="btn btn-outline-secondary px-3 t600 ls0 nott">
											<input type="radio" name="redirect" id="website-cost-responsive-no" value="<?= base_url() ?>"> Terbaru
										</label>
										<label for="" class="btn btn-outline-secondary px-3 t600 active ls0 nott">
											<input type="radio" name="redirect" id="website-cost-responsive-yes" value="<?= base_url() ?>?sort=terdekat"> Terdekat
										</label>
									</div>
								</form>
							<?php
							}else{
							?>
								<form>
									<div class="btn-group btn-group-toggle" data-toggle="buttons">
										<label for="" class="btn btn-outline-secondary px-3 t600 active ls0 nott">
											<input type="radio" name="redirect" id="website-cost-responsive-no" value="<?= base_url() ?>"> Terbaru
										</label>
										<label for="" class="btn btn-outline-secondary px-3 t600 ls0 nott">
											<input type="radio" name="redirect" id="website-cost-responsive-yes" value="<?= base_url() ?>?sort=terdekat"> Terdekat
										</label>
									</div>
								</form>
							<?php } ?>

								<!-- Tab Content 1 ============================================= -->
								<div class="tab-pane fade show active" id="navi-1" role="tabpanel" aria-labelledby="nav-outdoor-tab">
									<div class="row clearfix">
										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												if(!empty($datas["all"][0])){
													?>

											<article class="entry nobottomborder nobottommargin">
												<div class="entry-image mb-3">
													<a href="<?= $datas["all"][0]["link"] ?>"><img src="<?= $datas["all"][0]["image"] ?>" alt="Image 3"></a>
												</div>
												<div class="entry-title">
													<h3><a href="<?= $datas["all"][0]["link"] ?>"><?= $datas["all"][0]["artcTitle"] ?></a></h3>
												</div>
												<ul class="entry-meta clearfix">
													<?php
														if($datas["all"][0]["isShowUser"]==0){
													?>
														<li><span>by</span> <a href="<?= $datas["all"][0]["link_writer"] ?>"><?= $datas["all"][0]["ukomName"] ?></a></li>
													<?php
														}else{}
													?>
													<li><i class="icon-time"></i><a href="<?= $datas["all"][0]["link_publishtime"] ?>"><?= $datas["all"][0]["showPublishTime"] ?></a></li>
												</ul>
												<div class="entry-content clearfix">
													<p><?= $datas["all"][0]["artcExcerpt"] ?></p>
												</div>
											</article>

												<?php
												}
											?>

										</div>

										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												foreach($datas["all"] as $key=>$val){
													if($key > 0){
											?>
											<article class="spost clearfix">
												<div class="entry-image">
													<a href="<?= $val["link"] ?>"><img src="<?= $val["image"] ?>" alt=""></a>
												</div>
												<div class="entry-c">
													<div class="entry-title">
														<h4 class="t600"><a href="<?= $val["link"] ?>"><?= $val["artcTitle"] ?></a></h4>
													</div>
													<ul class="entry-meta clearfix">
														<?php
															if($val["isShowUser"]==0){
														?>
															<li><span>by</span> <a href="<?= $val["link_writer"] ?>"><?= $val["ukomName"] ?></a></li>
														<?php
															}else{}
														?>
														<li><i class="icon-time"></i><a href="<?= $val["link_publishtime"] ?>"><?= $val["showPublishTime"] ?></a></li>
													</ul>
												</div>
											</article>
										<?php
												}
											}
										?>

										</div>
									</div>
								</div>

								<!-- Tab Content 2 ============================================= -->
								<div class="tab-pane fade" id="navi-2" role="tabpanel" aria-labelledby="nav-outdoor-tab">
									<div class="row clearfix">
										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												if(!empty($datas["image"][0])){
													?>

											<article class="entry nobottomborder nobottommargin">
												<div class="entry-image mb-3">
													<a href="<?= $datas["image"][0]["link"] ?>"><img src="<?= $datas["image"][0]["image"] ?>" alt="Image 3"></a>
												</div>
												<div class="entry-title">
													<h3><a href="<?= $datas["image"][0]["link"] ?>"><?= $datas["image"][0]["artcTitle"] ?></a></h3>
												</div>
												<ul class="entry-meta clearfix">
													<?php
														if($datas["image"][0]["isShowUser"]==0){
													?>
														<li><span>by</span> <a href="<?= $datas["image"][0]["link_writer"] ?>"><?= $datas["image"][0]["ukomName"] ?></a></li>
													<?php
														}else{}
													?>
													<li><i class="icon-time"></i><a href="<?= $datas["image"][0]["link_publishtime"] ?>"><?= $datas["image"][0]["showPublishTime"] ?></a></li>
												</ul>
												<div class="entry-content clearfix">
													<p><?= $datas["image"][0]["artcExcerpt"] ?></p>
												</div>
											</article>

												<?php
												}
											?>

										</div>

										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												foreach($datas["image"] as $key=>$val){
													if($key > 0){
											?>
											<article class="spost clearfix">
												<div class="entry-image">
													<a href="<?= $val["link"] ?>"><img src="<?= $val["image"] ?>" alt=""></a>
												</div>
												<div class="entry-c">
													<div class="entry-title">
														<h4 class="t600"><a href="<?= $val["link"] ?>"><?= $val["artcTitle"] ?></a></h4>
													</div>
													<ul class="entry-meta clearfix">
														<?php
															if($val["isShowUser"]==0){
														?>
														<li><span>by</span> <a href="<?= $val["link_writer"] ?>"><?= $val["ukomName"] ?></a></li>
														<?php
															}else{}
														?>
														<li><i class="icon-time"></i><a href="<?= $val["link_publishtime"] ?>"><?= $val["showPublishTime"] ?></a></li>
													</ul>
												</div>
											</article>
										<?php
												}
											}
										?>

										</div>
									</div>
								</div>

								<!-- Tab Content 3 ============================================= -->
								<div class="tab-pane fade" id="navi-3" role="tabpanel" aria-labelledby="nav-outdoor-tab">
									<div class="row clearfix">
										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												if(!empty($datas["video"][0])){
													?>

											<article class="entry nobottomborder nobottommargin">
												<div class="entry-image mb-3">
													<a href="<?= $datas["video"][0]["link"] ?>"><img src="<?= $datas["video"][0]["image"] ?>" alt="Image 3"></a>
												</div>
												<div class="entry-title">
													<h3><a href="<?= $datas["video"][0]["link"] ?>"><?= $datas["video"][0]["artcTitle"] ?></a></h3>
												</div>
												<ul class="entry-meta clearfix">
													<?php
														if($datas["video"][0]["isShowUser"]==0){
													?>
														<li><span>by</span> <a href="<?= $datas["video"][0]["link_writer"] ?>"><?= $datas["video"][0]["ukomName"] ?></a></li>
													<?php
														}else{}
													?>
													<li><i class="icon-time"></i><a href="<?= $datas["video"][0]["link_publishtime"] ?>"><?= $datas["video"][0]["showPublishTime"] ?></a></li>
												</ul>
												<div class="entry-content clearfix">
													<p><?= $datas["video"][0]["artcExcerpt"] ?></p>
												</div>
											</article>

												<?php
												}
											?>

										</div>

										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												foreach($datas["video"] as $key=>$val){
													if($key > 0){
											?>
											<article class="spost clearfix">
												<div class="entry-image">
													<a href="<?= $val["link"] ?>"><img src="<?= $val["image"] ?>" alt=""></a>
												</div>
												<div class="entry-c">
													<div class="entry-title">
														<h4 class="t600"><a href="<?= $val["link"] ?>"><?= $val["artcTitle"] ?></a></h4>
													</div>
													<ul class="entry-meta clearfix">
														<?php
															if($val["isShowUser"]==0){
														?>
														<li><span>by</span> <a href="<?= $val["link_writer"] ?>"><?= $val["ukomName"] ?></a></li>
														<?php
															}else{}
														?>
														<li><i class="icon-time"></i><a href="<?= $val["link_publishtime"] ?>"><?= $val["showPublishTime"] ?></a></li>
													</ul>
												</div>
											</article>
										<?php
												}
											}
										?>

										</div>
									</div>
								</div>

								<!-- Tab Content 5 ============================================= -->
								<div class="tab-pane fade" id="navi-5" role="tabpanel" aria-labelledby="nav-outdoor-tab">
									<div class="row clearfix">
										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												if(!empty($datas["teks"][0])){
													?>

											<article class="entry nobottomborder nobottommargin">
												<div class="entry-image mb-3">
													<a href="<?= $datas["teks"][0]["link"] ?>"><img src="<?= $datas["teks"][0]["image"] ?>" alt="Image 3"></a>
												</div>
												<div class="entry-title">
													<h3><a href="<?= $datas["teks"][0]["link"] ?>"><?= $datas["teks"][0]["artcTitle"] ?></a></h3>
												</div>
												<ul class="entry-meta clearfix">
													<?php
														if($datas["teks"][0]["isShowUser"]==0){
													?>
														<li><span>by</span> <a href="<?= $datas["teks"][0]["link_writer"] ?>"><?= $datas["teks"][0]["ukomName"] ?></a></li>
													<?php
														}else{}
													?>
													<li><i class="icon-time"></i><a href="<?= $datas["teks"][0]["link_publishtime"] ?>"><?= $datas["teks"][0]["showPublishTime"] ?></a></li>
												</ul>
												<div class="entry-content clearfix">
													<p><?= $datas["teks"][0]["artcExcerpt"] ?></p>
												</div>
											</article>

												<?php
												}
											?>

										</div>

										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												foreach($datas["teks"] as $key=>$val){
													if($key > 0){
											?>
											<article class="spost clearfix">
												<div class="entry-image">
													<a href="<?= $val["link"] ?>"><img src="<?= $val["image"] ?>" alt=""></a>
												</div>
												<div class="entry-c">
													<div class="entry-title">
														<h4 class="t600"><a href="<?= $val["link"] ?>"><?= $val["artcTitle"] ?></a></h4>
													</div>
													<ul class="entry-meta clearfix">
														<?php
															if($val["isShowUser"]==0){
														?>
															<li><span>by</span> <a href="<?= $val["link_writer"] ?>"><?= $val["ukomName"] ?></a></li>
														<?php
															}else{}
														?>
														<li><i class="icon-time"></i><a href="<?= $val["link_publishtime"] ?>"><?= $val["showPublishTime"] ?></a></li>
													</ul>
												</div>
											</article>
										<?php
												}
											}
										?>

										</div>
									</div>
								</div>

								<!-- Tab Content 4 ============================================= -->
								<div class="tab-pane fade" id="navi-4" role="tabpanel" aria-labelledby="nav-outdoor-tab">
									<div class="row clearfix">
										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												if(!empty($datas["audio"][0])){
													?>

											<article class="entry nobottomborder nobottommargin">
												<div class="entry-image mb-3">
													<a href="<?= $datas["audio"][0]["link"] ?>"><img src="<?= $datas["audio"][0]["image"] ?>" alt="Image 3"></a>
												</div>
												<div class="entry-title">
													<h3><a href="<?= $datas["audio"][0]["link"] ?>"><?= $datas["audio"][0]["artcTitle"] ?></a></h3>
												</div>
												<ul class="entry-meta clearfix">
													<?php
														if($datas["audio"][0]["isShowUser"]==0){
													?>
													<li><span>by</span> <a href="<?= $datas["audio"][0]["link_writer"] ?>"><?= $datas["audio"][0]["ukomName"] ?></a></li>
													<?php
														}else{}
													?>
													<li><i class="icon-time"></i><a href="<?= $datas["audio"][0]["link_publishtime"] ?>"><?= $datas["audio"][0]["showPublishTime"] ?></a></li>
												</ul>
												<div class="entry-content clearfix">
													<p><?= $datas["audio"][0]["artcExcerpt"] ?></p>
												</div>
											</article>

												<?php
												}
											?>

										</div>

										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												foreach($datas["audio"] as $key=>$val){
													if($key > 0){
											?>
											<article class="spost clearfix">
												<div class="entry-image">
													<a href="<?= $val["link"] ?>"><img src="<?= $val["image"] ?>" alt=""></a>
												</div>
												<div class="entry-c">
													<div class="entry-title">
														<h4 class="t600"><a href="<?= $val["link"] ?>"><?= $val["artcTitle"] ?></a></h4>
													</div>
													<ul class="entry-meta clearfix">
														<?php
															if($val["isShowUser"]==0){
														?>
															<li><span>by</span> <a href="<?= $val["link_writer"] ?>"><?= $val["ukomName"] ?></a></li>
														<?php
															}else{}
														?>
														<li><i class="icon-time"></i><a href="<?= $val["link_publishtime"] ?>"><?= $val["showPublishTime"] ?></a></li>
													</ul>
												</div>
											</article>
										<?php
												}
											}
										?>

										</div>
									</div>
								</div>

							</div><!-- Tab End -->

							<?php /*
							<nav class="navbar navbar-expand-lg navbar-light p-0">
								<h4 class="mb-0 pr-2 ls1 uppercase t700">Artikel Per Wilayah</h4>
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler2" aria-controls="navbarToggler2" aria-expanded="false" aria-label="Toggle navigation">
									<i class="icon-line-menu"></i>
								</button>

								<div class="collapse navbar-collapse justify-content-between" id="navbarToggler2">
									<div></div>
									<ul class="nav nav-sm navbar-nav mr-md-auto mr-lg-0 mt-2 mt-lg-0 align-self-end" role="tablist">
										<?php
											foreach($datas["list_kanal"] as $k=>$v){
												if($k < 5){
										?>
										<li class="nav-item">
											<a class="nav-link bg-color-<?= $v["color"] ?> <?php if($k == 0){echo "active";} ?>" id="nav-outdoor-tab" data-toggle="tab" href="#nav-<?= $v["mscvId"] ?>" role="tab" aria-selected="true"><?= $v["mscvName"] ?></a>
										</li>
										<?php
												}
											}
										?>
									</ul>
								</div>
							</nav>
							<!-- Tab Content ============================================= -->
							<div class="line line-xs line-dark"></div>
							<div class="tab-content" id="nav-tabContent">

								<?php
									foreach($datas["latest"] as $k=>$v){
										if($k < 5){
								?>

								<!-- Tab Content 1 ============================================= -->
								<div class="tab-pane fade <?= ($k == 0 ? "show active" : "") ?>" id="nav-<?= (!empty($datas["latest"][$k][0]) ? $datas["latest"][$k][0]["artcMscvId"] : $datas["list_kanal"][$k]["mscvId"]) ?>" role="tabpanel" aria-labelledby="nav-outdoor-tab">
									<div class="row clearfix">
										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												if(!empty($datas["latest"][$k][0])){
													?>

											<article class="entry nobottomborder nobottommargin">
												<div class="entry-image mb-3">
													<a href="<?= $datas["latest"][$k][0]["link"] ?>"><img src="<?= $datas["latest"][$k][0]["image"] ?>" alt="Image 3"></a>
													<div class="entry-categories"><a href="<?= $datas["list_kanal"][$k]["link"] ?>" class="bg-<?= $datas["list_kanal"][$k]["color"] ?>"><?= $datas["list_kanal"][$k]["mscvName"] ?></a></div>
												</div>
												<div class="entry-title">
													<h3><a href="<?= $datas["latest"][$k][0]["link"] ?>"><?= $datas["latest"][$k][0]["artcTitle"] ?></a></h3>
												</div>
												<ul class="entry-meta clearfix">
													<?php
														if($datas["latest"][$k][0]["isShowUser"]==0){
													?>
														<li><span>by</span> <a href="<?= $datas["latest"][$k][0]["link_writer"] ?>"><?= $datas["latest"][$k][0]["ukomName"] ?></a></li>
													<?php
														}else{}
													?>
													<li><i class="icon-time"></i><a href="<?= $datas["latest"][$k][0]["link_publishtime"] ?>"><?= $datas["latest"][$k][0]["showPublishTime"] ?></a></li>
												</ul>
												<div class="entry-content clearfix">
													<p><?= $datas["latest"][$k][0]["artcExcerpt"] ?></p>
												</div>
											</article>

												<?php
												}
											?>

										</div>

										<div class="col-lg-6">
											<!-- Post Article -->
											<?php
												foreach($v as $key=>$val){
													if($key > 0){
											?>
											<article class="spost clearfix">
												<div class="entry-image">
													<a href="<?= $val["link"] ?>"><img src="<?= $val["image"] ?>" alt=""></a>
												</div>
												<div class="entry-c">
													<div class="entry-title">
														<h4 class="t600"><a href="<?= $val["link"] ?>"><?= $val["artcTitle"] ?></a></h4>
													</div>
													<ul class="entry-meta clearfix">
														<?php
															if($val["isShowUser"]==0){
														?>
															<li><span>by</span> <a href="<?= $val["link_writer"] ?>"><?= $val["ukomName"] ?></a></li>
														<?php
															}else{}
														?>
														<li><i class="icon-time"></i><a href="<?= $val["link_publishtime"] ?>"><?= $val["showPublishTime"] ?></a></li>
													</ul>
												</div>
											</article>
										<?php
												}
											}
										?>

										</div>
									</div>
								</div>

								<?php
										}
									}
								?>

							</div><!-- Tab End -->

							<!-- Ad ============================================= -->
							<!-- <a href="#"><img src="<?= base_url() ?>assets/demos/news/images/ad/728x90.jpg" width="728" alt="Ad" class="mt-5 mt-lg-2 mb-4 mb-lg-3 aligncenter"></a> -->

							<!-- Articles ============================================= -->
							<!-- <div class="row clearfix">
								<?php
									if(!empty($datas["latest"][5])){
										?>

								<div class="col-md-6 mt-4">
									<article class="ipost">
										<div class="entry-image mb-3">
											<a href="<?= $datas["latest"][5][0]["link"]; ?>"><img src="<?= $datas["latest"][5][0]["image"]; ?>" alt="Image 3" style="max-height: 222px;"></a>
											<div class="entry-categories"><a href="<?= $datas["list_kanal"][5]["link"] ?>" class="bg-<?= $datas["list_kanal"][5]["color"] ?>"><?= $datas["list_kanal"][5]["mscvName"] ?></a></div>
										</div>
										<div class="entry-title">
											<h3><a href="<?= $datas["latest"][5][0]["link"]; ?>"><?= $datas["latest"][5][0]["artcTitle"]; ?></a></h3>
										</div>
										<ul class="entry-meta clearfix">
											<li><span>by</span> <a href="<?= $datas["latest"][5][0]["link_writer"]; ?>"><?= $datas["latest"][5][0]["ukomName"]; ?></a></li>
											<li><i class="icon-time"></i><a href="<?= $datas["latest"][5][0]["link_publishtime"]; ?>"><?= $datas["latest"][5][0]["artcPublishTime"] ?></a></li>
										</ul>
										<div class="entry-content mt-0 clearfix">
											<p><?= $datas["latest"][5][0]["artcExcerpt"] ?></p>
										</div>
									</article>
								</div>
								<?php
									}

									if(!empty($datas["latest"][6])){
										?>

									<div class="col-md-6 mt-4">
										<article class="ipost">
											<div class="entry-image mb-3">
												<a href="<?= $datas["latest"][6][0]["link"]; ?>"><img src="<?= $datas["latest"][6][0]["image"]; ?>" alt="Image 3" style="max-height: 222px;"></a>
												<div class="entry-categories"><a href="<?= $datas["list_kanal"][6]["link"] ?>" class="bg-<?= $datas["list_kanal"][6]["color"] ?>"><?= $datas["list_kanal"][6]["mscvName"] ?></a></div>
											</div>
											<div class="entry-title">
												<h3><a href="<?= $datas["latest"][6][0]["link"]; ?>"><?= $datas["latest"][6][0]["artcTitle"]; ?></a></h3>
											</div>
											<ul class="entry-meta clearfix">
												<li><span>by</span> <a href="<?= $datas["latest"][6][0]["link_writer"]; ?>"><?= $datas["latest"][6][0]["ukomName"]; ?></a></li>
												<li><i class="icon-time"></i><a href="<?= $datas["latest"][6][0]["link_publishtime"]; ?>"><?= $datas["latest"][6][0]["artcPublishTime"] ?></a></li>
											</ul>
											<div class="entry-content mt-0 clearfix">
												<p><?= $datas["latest"][6][0]["artcExcerpt"] ?></p>
											</div>
										</article>
									</div>
									<?php
									}
								?>

								<div class="col-md-6 topmargin-sm">

								<?php
									foreach($datas["latest"][5] as $k=>$v){
										if($k > 0){
								?>
									<article class="spost clearfix">
										<div class="entry-image">
											<a href="<?= $v["link"]; ?>"><img src="<?= $v["image"]; ?>" alt=""></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4 class="t500"><a href="<?= $v["link"]; ?>"><?= $v["artcTitle"] ?></a></h4>
											</div>
											<ul class="entry-meta clearfix">
												<li><span>by</span> <a href="<?= $val["link_writer"] ?>"><?= $v["ukomName"]; ?></a></li>
												<li><i class="icon-time"></i><a href="<?= $val["link_publishtime"] ?>"><?= $v["artcPublishTime"] ?></a></li>
											</ul>
										</div>
									</article>
								<?php
										}
									}
								?>

								</div>


								<div class="col-md-6 topmargin-sm">

									<?php
										foreach($datas["latest"][6] as $k=>$v){
											if($k > 0){
									?>

										<article class="spost clearfix">
											<div class="entry-image">
												<a href="<?= $v["link"]; ?>"><img src="<?= $v["image"]; ?>" alt=""></a>
											</div>
											<div class="entry-c">
												<div class="entry-title">
													<h4 class="t500"><a href="<?= $v["link"]; ?>"><?= $v["artcTitle"] ?></a></h4>
												</div>
												<ul class="entry-meta clearfix">
													<li><span>by</span> <a href="<?= $val["link_writer"] ?>"><?= $v["ukomName"]; ?></a></li>
													<li><i class="icon-time"></i><a href="<?= $val["link_publishtime"] ?>"><?= $v["showPublishTime"] ?></a></li>
												</ul>
											</div>
										</article>

									<?php
											}
										}
									?>

								</div>
							</div> --> */ ?>

							<div class="clearfix mt-5" id="boxContent">
								<!-- Trending Areas
								============================================= -->
								<h4 class="mb-2 ls1 uppercase t700">Artikel</h4>
								<div class="line line-xs line-sports"></div>

								<?php foreach($datas["article"] as $v){ ?>
								<div class="ipost mb-4 mb-lg-4 row clearfix cumi" data-publish-time="<?= $v["artcPublishTime"] ?>">
									<div class="col-md-5">
										<div class="entry-image mb-0">
											<a href="<?= $v["link"] ?>"><img src="<?= $v["image"] ?>" alt="Image"></a>
											<div class="entry-categories"><a href="<?= $v["link_kanal"] ?>" class="bg-sports"><?= $v["mscvName"] ?></a></div>
										</div>
									</div>
									<div class="col-md-7">
										<div class="entry-title mt-lg-0 mt-3">
											<h3><a href="<?= $v["link"] ?>"><?= $v["artcTitle"] ?></a></h3>
										</div>
										<ul class="entry-meta clearfix">
											<?php
												if($v["isShowUser"]==0){
											?>
												<li><span>by</span> <a href="<?= $v["link_writer"] ?>"><?= $v["ukomName"] ?></a></li>
											<?php
												}else{}
											?>
											<li><i class="icon-line-clock"></i><a href="<?= $v["link_publishtime"] ?>"><?= $v["showPublishTime"] ?></a></li>
											<!-- <li><a href="#"><i class="icon-camera-retro"></i></a></li> -->
										</ul>
										<div class="entry-content mt-0">
											<p><?= $v["artcExcerpt"] ?></p>
										</div>
									</div>
								</div>
								<?php } ?>

							</div>

							<!-- Infinity Scroll Loader
							============================================= -->
							<div class="row clearfix">
								<div class="col-md-12 text-center">
									<div class="page-load-status hovering-load-status">
										<div class="css3-spinner infinite-scroll-request">
											<div class="css3-spinner-ball-pulse-sync">
												<div></div>
												<div></div>
												<div></div>
											</div>
										</div>
									</div>

									<!-- Infinity Scroll Button
									============================================= -->
									<button class="mt-4 button button-dark bg-dark button-rounded ls1 uppercase load-next-portfolio btn-load-more" onclick="loadmore()">Load more...</button>
								</div>
							</div>
						</div>

						<!-- Top Sidebar Area
						============================================= -->
						<div class="col-lg-4 sticky-sidebar-wrap mt-5 mt-lg-0">
							<div class="sticky-sidebar" style="padding-top: 6.5px">
								<!-- Sidebar Widget 1
								============================================= -->
								<div class="widget widget_links clearfix">
									<h4 class="mb-2 ls1 uppercase t700">Wilayah</h4>
									<div class="line line-xs line-sports"></div>
									<ul>
										<?php foreach($datas["popular"] as $v){ ?>
											<li><a href="<?= $v["link"] ?>" class="d-flex justify-content-between align-items-center"><?= $v["mscvName"] ?><span class="badge bg-<?= $v["color"] ?>"><?= $v["sum"] ?></span></a></li>
										<?php } ?>
										<!-- <li><a href="#" class="d-flex justify-content-between align-items-center">World<span class="badge bg-sports">14</span></a></li>
										<li><a href="#" class="d-flex justify-content-between align-items-center">Technology<span class="badge bg-travel">21</span></a></li>
										<li><a href="#" class="d-flex justify-content-between align-items-center">Entertainment<span class="badge bg-info">32</span></a></li>
										<li><a href="#" class="d-flex justify-content-between align-items-center">Sports<span class="badge bg-fashion">52</span></a></li>
										<li><a href="#" class="d-flex justify-content-between align-items-center">Media<span class="badge bg-success">14</span></a></li>
										<li><a href="#" class="d-flex justify-content-between align-items-center">Politics<span class="badge bg-travel">91</span></a></li>
										<li><a href="#" class="d-flex justify-content-between align-items-center">Business<span class="badge bg-market">32</span></a></li> -->
									</ul>
								</div>
							</div>
						</div> <!-- Sidebar End -->
					</div>
				</div> <!-- Container End -->

				<!-- <div class="container clearfix">

					<div class="row clearfix">
						Second Posts Area
						============================================= -
						 <div class="col-lg-8">

						</div>

						Second Sidebar
						=============================================
						<div class="col-lg-4 sticky-sidebar-wrap mt-5 mt-lg-0">
							<div class="sticky-sidebar">

								Sidebar Widget 3 =============================================
								<div class="widget clearfix">
									<a href="#"><img src="<?= base_url() ?>assets/demos/news/images/ad/336x280.jpg" width="336" alt="Ad" class="img-responsive aligncenter"></a>
								</div>

								Sidebar Widget 2
								============================================= 
								 <div class="widget clearfix">
									<h4 class="mb-2 ls1 uppercase t700">Recent Posts</h4>
									<div class="line line-xs line-home"></div>
									<article class="spost pt-0 notopborder clearfix">
										<div class="entry-image">
											<a href="#"><img src="<?= base_url() ?>assets/demos/news/images/posts/fashion/small/2.jpg" alt=""></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4 class="t600"><a href="#">UK government weighs Tesla's Model.</a></h4>
											</div>
											<ul class="entry-meta clearfix">
												<li><span>by</span> <a href="#">John Doe</a></li>
												<li><i class="icon-time"></i><a href="#">11 Mar 2016</a></li>
											</ul>
										</div>
									</article>
									<article class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="<?= base_url() ?>assets/demos/news/images/posts/travel/small/3.jpg" alt=""></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4 class="t600"><a href="#">MIT's new robot glove can give you extra fingers.</a></h4>
											</div>
											<ul class="entry-meta clearfix">
												<li><span>by</span> <a href="#">John Doe</a></li>
												<li><i class="icon-time"></i><a href="#">11 Mar 2016</a></li>
											</ul>
										</div>
									</article>
									<article class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="<?= base_url() ?>assets/demos/news/images/posts/sports/small/1.jpg" alt=""></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4 class="t600"><a href="#">You can now listen to headphones through your hoodie.</a></h4>
											</div>
											<ul class="entry-meta clearfix">
												<li><span>by</span> <a href="#">John Doe</a></li>
												<li><i class="icon-time"></i><a href="#">11 Mar 2016</a></li>
											</ul>
										</div>
									</article>
									<article class="spost clearfix">
										<div class="entry-image">
											<a href="#"><img src="<?= base_url() ?>assets/demos/news/images/posts/fashion/small/4.jpg" alt=""></a>
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4 class="t600"><a href="#">How would you change Kobo's Aura HD e-reader?</a></h4>
											</div>
											<ul class="entry-meta clearfix">
												<li><span>by</span> <a href="#">John Doe</a></li>
												<li><i class="icon-time"></i><a href="#">11 Mar 2016</a></li>
											</ul>
										</div>
									</article>
								</div>

								Sidebar Widget 3
								=============================================


								Sidebar Widget 4
								=============================================
								<div id="instagram" class="widget clearfix">
									<h4 class="mb-2 ls1 uppercase t700">Instagram Feeds</h4>
									<div class="line line-xs line-home"></div>

									<div id="instagram-photos" class="instagram-photos masonry-thumbs mt-2 grid-3" data-user="5834720953" data-count="9" data-type="user"></div>
								</div>
							</div>

						</div>
					</div>

				</div> -->

			</div>

		</section>

<?php
  //set headers to NOT cache a page
//   header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
//   header("Pragma: no-cache"); //HTTP 1.0

  //or, if you DO want a file to cache, use:
//   header("Cache-Control: max-age=2592000"); //30days (60sec * 60min * 24hours * 30days)

?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<title><?= $datas["meta_title"] ?>Witness</title>
	<link rel="shortcut icon" type="image/png" href="<?= base_url() ?>assets/demos/news/images/logo-s.png"/>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<meta name="title" content="<?= (isset($datas["article_detail"]["artcTitle"])) ? $datas["article_detail"]["artcTitle"] : "witness.com: Berita Terkini dan Terbaru" ?>">
	<meta name="description" content="<?= (isset($datas["article_detail"]["artcExcerpt"])) ? $datas["article_detail"]["artcExcerpt"] : "witness.com: Berita Terkini dan Terbaru" ?>">
	<meta name="news_keywords" content="<?= (isset($datas["article_detail"]["artcExcerpt"])) ? $datas["article_detail"]["artcExcerpt"] : "witness.com: Berita Terkini dan Terbaru" ?>">
	<meta name="keywords" content="<?= (isset($datas["article_detail"]["artcExcerpt"])) ? $datas["article_detail"]["artcExcerpt"] : "witness.com: Berita Terkini dan Terbaru" ?>">

	<!-- <meta name='twitter:image' content='https://witness.com/images/def-img.jpg' /> -->
	<!-- EOF OF TWITTER CARD-->
	<meta content="index,follow" name="googlebot-news" />
	<meta content="index,follow" name="googlebot" />
	<meta content="all" name="robots" />
	<meta content="divertal" name="author" />
	<meta content="id" name="language" />
	<meta content="id" name="geo.country" />
	<meta http-equiv="content-language" content="In-Id" />
	<meta content="Indonesia" name="geo.placename" />
	<meta name="mobile-web-app-capable" content="yes">
	

	<meta property="og:site_name" content="witness.com">
	<meta property="og:type" content="article">
	<meta property="og:url" content="<?= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>">
	<meta property="og:title" content="<?= (isset($datas["article_detail"]["artcTitle"])) ? $datas["article_detail"]["artcTitle"] : "witness.com: Berita Terkini dan Terbaru" ?>">
	<meta property="og:description" content="<?= (isset($datas["article_detail"]["artcExcerpt"])) ? $datas["article_detail"]["artcExcerpt"] : "witness.com: Berita Terkini dan Terbaru" ?>">
	<meta property="og:image" content="<?= (isset($datas["share_img"])) ? $datas["share_img"] : "https://witness.com/images/def-img.jpg" ?>">
	<meta property="og:image:width" content="500" />
	<meta property="og:image:height" content="500" />

	<!-- START OF TWITTER CARD-->
                <meta name='twitter:card' content="summary_large_image" />
                <meta name='twitter:site' content='@tempowitness' />
                <meta name='twitter:creator' content='@tempowitness' />
                <meta name='twitter:title' content="<?= (isset($datas["article_detail"]["artcTitle"])) ? $datas["article_detail"]["artcTitle"] : "witness.com: Berita Terkini dan Terbaru" ?>" />
                <meta name='twitter:description' content="<?= (isset($datas["article_detail"]["artcExcerpt"])) ? $datas["article_detail"]["artcExcerpt"] : "witness.com: Berita Terkini dan Terbaru" ?>" />
                <meta name='twitter:image' content="<?= (isset($datas["share_img"])) ? $datas["share_img"] : "https://witness.com/images/def-img.jpg" ?>" />


	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="<?= (isset($datas["article_detail"]["artcTitle"])) ? $datas["article_detail"]["artcTitle"] : "witness.com: Berita Terkini dan Terbaru" ?>">
	<link rel="alternate" hreflang="id" href="<?= base_url() ?>" />
	<link rel="canonical" href="<?= base_url() ?>">
	<link rel="amphtml" href="<?= base_url() ?>">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="theme-color" content="#000000">
	<!-- <meta property="fb:app_id" content="1666804756877797" /> -->
	<meta property="fb:app_id" content="332404380172618" />
	<!-- <meta name="msvalidate.01" content="E09B0E18A6A4CACCAB6BFB2BE6113D1E" /> -->
	<meta name="yandex-verification" content="a386e1134a40bd15" />
	<!-- <meta name="google-site-verification" content="RyuYt38MaF-rFRfgN0kg_1iq_6nOe4_G9jpsuOYhYCY" /> -->

	<!-- Stylesheets ============================================= -->
	<link href="//fonts.googleapis.com/css?family=Roboto+Slab:300,400,700%7CRoboto:400,500,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/style.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/dark.css" type="text/css" />

	<link rel="stylesheet" href="<?= base_url() ?>assets/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/one-page/css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?= base_url() ?>assets/css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Theme Color Stylesheet -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/demos/news/css/colors.css" type="text/css" />
	<link rel="stylesheet" href="<?= base_url() ?>assets/demos/news/css/fonts.css" type="text/css" />

	<!-- News Demo Specific Stylesheet -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/demos/news/news.css" type="text/css" />
	<!-- / -->

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/include/rs-plugin/css/navigation.css">

	<style>
		/* Revolution Slider Styles */
		.hesperiden .tp-tab { border-bottom: 0; }
		.hesperiden .tp-tab:hover,
		.hesperiden .tp-tab.selected { background-color: #E5E5E5; }

	</style>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23817453-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-23817453-1');
		// gtag('config', 'UA-148802225-1');
	</script>

</head>

<body class="stretched no-transition">

	<!-- Document Wrapper ============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header ============================================= -->
		<header id="header" class="sticky-style-2" style="max-height: 131px">

			<div class="container clearfix">
				<div class="row justify-content-between clearfix">
					<div class="col-md-4 col-sm-3 d-none d-sm-inline-flex align-self-center">
						<a href="https://www.facebook.com/Tempo-Witness-106288950760662/" target="_BLANK" class="social-icon inline-block si-small si-rounded si-dark si-mini si-facebook">
							<i class="icon-facebook"></i>
							<i class="icon-facebook"></i>
						</a>
						<a href="https://twitter.com/tempowitness" target="_BLANK" class="social-icon inline-block si-small si-rounded si-dark si-mini si-twitter">
							<i class="icon-twitter"></i>
							<i class="icon-twitter"></i>
						</a>
						<a href="https://www.instagram.com/tempo.witness/" target="_BLANK" class="social-icon inline-block si-small si-rounded si-dark si-mini si-instagram">
							<i class="icon-instagram"></i>
							<i class="icon-instagram"></i>
						</a>
						<a href="https://www.youtube.com/channel/UC6Dc1Lv5dt1XeeN2fzo_7mg" target="_BLANK" class="social-icon inline-block si-small si-rounded si-dark si-mini si-youtube">
							<i class="icon-youtube"></i>
							<i class="icon-youtube"></i>
						</a>
					</div>

					<div class="col-md-4 col-sm-5 col-8 align-self-center">
						<!-- Logo ============================================= -->
						<div id="logo" class="divcenter nomargin">
							<a href="/" class="standard-logo"><img class="divcenter" src="<?= base_url() ?>assets/images/logo.png" alt="Canvas Logo" style="max-height: 70px;padding: 10px 0;"></a>
							<a href="/" class="retina-logo"><img class="divcenter" src="<?= base_url() ?>assets/images/logo.png" alt="Canvas Logo" style="max-height: 70px;padding: 10px 0;"></a>
						</div><!-- #logo end -->
					</div>

					<div class="col-sm-4 col-4 align-self-center nomargin">
						<ul class="nav justify-content-end">
							<li class="nav-item">
								<!-- <a class="nav-link uppercase t500" href="#">Search</a> -->


								

								

								
								<!-- <div style="display:inline;">
									<form style="display:inline;" action="<?= base_url('find/keyword') ?>" method="get">
										<input type="text" id="q" name="q" placeholder="" >
										<input type="submit" value="Cari...">
									</form>
								</div> -->
								

							</li>
							<li class="entry-categories mt-3 d-none d-sm-inline-block" style="position: relative; left: auto;">
								<a class="date-today bg-dark uppercase t500"></a>
							</li>
						</ul>
					</div>

				</div>
			</div>

			<div id="header-wrap">
				<!-- Primary Navigation ============================================= -->
				<nav id="primary-menu" class="with-arrows style-2 clearfix">

					<div class="container clearfix">

						<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

						<ul>
							<li class="menu-color-home"><a href="<?= base_url(); ?>"><div>Beranda</div></a></li>
							<?php
								foreach($datas['list_hot_topic'] as $k=>$v){
									if($k < 7){
							?>
										<li class="menu-color-<?= $v["color"] ?> <?= ($datas["kanal_select"] == $v["hotTopicId"]) ? "active" : "" ?>"><a href="<?= $v["link"] ?>"><div><?= $v["vkeyName"]?></div></a></li>
							<?php
									}
									if($k == 7){
							?>
									<li class="menu-color-<?= $v["color"] ?>"><a href="#"><div>Other</div></a>
										<div class="mega-menu-content" style="width: 180px">
											<ul>
							<?php }
									if($k >= 7){
							?>
														<li><a href="<?= $v["link"] ?>"><div><?= $v["vkeyName"]?></div></a></li>
							<?php
									}
									if($k == (sizeof($datas['list_kanal'])-1)){
							?>
											</ul>
										</div>
									</li>
							<?php
									}
								}
							?>
						</ul>

						<!-- Mobile Menu ============================================= -->
						<ul class="mobile-primary-menu">
							<?php
								foreach($datas['list_hot_topic'] as $k=>$v){
									if($k < 7){
							?>
										<li class="menu-color-<?= $v["color"] ?> <?= ($datas["kanal_select"] == $v["hotTopicId"]) ? "active" : "" ?>"><a style="background-color: white;" href="<?= $v["link"] ?>"><div><?= $v["vkeyName"]?></div></a></li>
							<?php
									}
									if($k == 7){
							?>
									<li class="menu-color-<?= $v["color"] ?>"><a href="#"><div>Other</div></a>
										<div class="mega-menu-content" style="width: 180px">
											<ul>
							<?php }
									if($k >= 7){
							?>
														<li><a href="<?= $v["link"] ?>"><div><?= $v["vkeyName"]?></div></a></li>
							<?php
									}
									if($k == (sizeof($datas['list_kanal'])-1)){
							?>
											</ul>
										</div>
									</li>
							<?php
									}
								}
							?>
						</ul>

						<!-- Top Search ============================================= -->
						<div id="top-search">
							<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
							<form action="<?= base_url('find/keyword') ?>" method="post">
								<input type="text" name="q" class="form-control" value="" placeholder="Cari &amp; Tekan Enter..">
							</form>
						</div>
						<!-- #top-search end -->

						<!-- Bookmark ============================================= -->
						<div id="top-cart">
							<!-- <a href="#" id="top-cart-trigger"><i class="icon-bookmark-empty t600"></i></a> -->
						</div>

					</div>

				</nav><!-- #primary-menu end -->

			</div>

		</header><!-- #header end -->

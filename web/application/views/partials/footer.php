<!-- Footer
============================================= -->
<footer id="footer" class="dark" style="background-color: #1f2024;">

    <div class="container">

        <div class="footer-widgets-wrap row clearfix">

            <div class="col-lg-3 col-sm-6 mb-5 mb-lg-0">

            </div>

            <div class="col-lg-3 col-sm-6 mb-5 mb-lg-0">
                <!-- <h4 class="mb-3 mb-sm-4">Tag Cloud</h4>
                <div class="tagcloud"> -->
                <div class="widget widget_links clearfix">
                    <ul>
                        <li><a href="<?=base_url()?>info?id=1">Tentang Kami</a></li>
                        <li><a href="<?=base_url()?>info?id=2">Pedoman Media Siber</a></li>
                        <li><a href="<?=base_url()?>info?id=3">Ketentuan Layanan</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6 mb-5 mb-sm-0">
                <div class="widget widget_links clearfix">
                    <!-- <h4 class="mb-3 mb-sm-4">Blogroll</h4> -->
                    <ul>
                        <li><a href="https://www.tempo.co/">TEMPO.CO</a></li>
                        <li><a href="https://majalah.tempo.co">Majalah TEMPO</a></li>
                        <li><a href="https://magz.tempo.co">TEMPO English Magazine</a></li>
                        <li><a href="https://koran.tempo.co">Koran TEMPO</a></li>
                        <li><a href="http://tempo-institute.org/">TEMPO Institute</a></li>
                        <li><a href="https://indonesiana.id">Indonesiana</a></li>
                        <li><a href="https://store.tempo.co">TEMPO Store</a></li>
                        <li><a href="https://en.tempo.co">TEMPO.CO English</a></li>
                    </ul>
                </div>
            </div>

            <!-- <div class="col-lg-3 col-sm-6 mb-0">
                <div class="widget widget_links clearfix">
                    <h4 class="mb-3 mb-sm-4">Download in Mobile</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus beatae esse iure est, quam libero!</p>
                    <a href="#" class="button button-light text-dark btn-block text-center bg-white nott ls0 button-rounded button-xlarge noleftmargin"><i class="icon-apple"></i>App Store</a>
                    <a href="#" class="button button-light text-dark btn-block text-center bg-white nott ls0 button-rounded button-xlarge noleftmargin"><i class="icon-googleplay"></i>Google Play</a>
                </div>
            </div> -->

        </div>

    </div>

    <!-- Copyrights ============================================= -->
    <div id="copyrights" class="">

        <div class="container clearfix">

            <div class="row justify-content-center">
                <div class="col-md-6 align-self-center">
                    Copyrights &copy; <?= date("Y") ?> All Rights Reserved.
                    <br>
                    <!-- <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div> -->
                </div>

                <div class="col-md-6 align-self-center">
                    <div class="copyrights-menu fright copyright-links m-0 clearfix">
                        <!-- <a href="#">Home</a>/<a href="#">About</a>/<a href="#">Features</a>/<a href="#">Portfolio</a>/<a href="#">FAQs</a>/<a href="#">Contact</a> -->
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- #copyrights end -->

</footer>
<!-- #footer end -->

</div>
<!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script src="<?= base_url() ?>assets/js/jquery.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?= base_url() ?>assets/js/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script src="<?= base_url() ?>assets/js/functions.js"></script>

<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
<script src="<?= base_url() ?>assets/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="<?= base_url() ?>assets/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script>
$(function() {
    $("input[name$='redirect']").change(function() {
        window.location = $(this).val();
    });
})
</script>
<script>
function PopupCenter(url, title, w, h) {
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    if (window.focus) {
        newWindow.focus();
    }
}
</script>
<!-- ADD-ONS JS FILES -->
<script>
    var tpj = jQuery;
    var revapi19;
    tpj(document).ready(function() {
        if (tpj("#rev_slider_19_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_19_1");
        } else {
            revapi19 = tpj("#rev_slider_19_1").show().revolution({
                sliderType: "carousel",
                jsFileLocation: "include/rs-plugin/js/",
                sliderLayout: "fullwidth",
                dottedOverlay: "none",
                delay: 7000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "on",
                    tabs: {
                        style: "hesperiden",
                        enable: true,
                        width: 260,
                        height: 80,
                        min_width: 260,
                        wrapper_padding: 25,
                        wrapper_color: "#F5F5F5",
                        wrapper_opacity: "1",
                        tmp: '<div class="tp-tab-content">  <span class="tp-tab-date">{{param1}}</span>  <span class="tp-tab-title font-secondary">{{title}}</span> <span class="tp-tab-date tp-tab-para">{{param2}}</span></div><div class="tp-tab-image"></div>',
                        visibleAmount: 9,
                        hide_onmobile: false,
                        hide_under: 480,
                        hide_onleave: false,
                        hide_delay: 200,
                        direction: "horizontal",
                        span: true,
                        position: "outer-bottom",
                        space: 0,
                        h_align: "left",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 0
                    }
                },
                carousel: {
                    horizontal_align: "center",
                    vertical_align: "center",
                    fadeout: "on",
                    vary_fade: "on",
                    maxVisibleItems: 3,
                    infinity: "on",
                    space: 0,
                    stretch: "off",
                    showLayersAllTime: "off",
                    easing: "Power3.easeInOut",
                    speed: "800"
                },
                responsiveLevels: [1140, 992, 768, 576],
                visibilityLevels: [1140, 992, 768, 576],
                gridwidth: [850, 700, 400, 300],
                gridheight: [580, 600, 500, 400],
                lazyType: "single",
                shadow: 0,
                spinner: "off",
                stopLoop: "on",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                disableProgressBar: "off",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }
    }); /* Revolution Slider End */

    // Navbar on hover
    $('.nav.tab-hover a.nav-link').hover(function() {
        $(this).tab('show');
    });

    // Current Date
    // var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    //     month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    //     a = new Date();
    var weekday = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
        month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        a = new Date();

    // jQuery('.date-today').html(weekday[a.getDay()] + ', ' + month[a.getMonth()] + ' ' + a.getDate());
    jQuery('.date-today').html(weekday[a.getDay()] + ', ' + a.getDate() + ' ' + month[a.getMonth()] + ' ' + a.getFullYear());

    // Infinity Scroll
    jQuery(window).on('load', function() {
        var $container = $('.infinity-wrapper');

        $container.infiniteScroll({
            path: '.load-next-portfolio',
            button: '.load-next-portfolio',
            scrollThreshold: false,
            history: false,
            status: '.page-load-status'
        });

        $container.on('load.infiniteScroll', function(event, response, path) {
            var $items = $(response).find('.infinity-loader');
            // append items after images loaded
            $items.imagesLoaded(function() {
                $container.append($items);
                $container.isotope('insert', $items);
                setTimeout(function() {
                    SEMICOLON.widget.loadFlexSlider();
                }, 1000);
            });
        });

    });

    $('#oc-news').owlCarousel({
        items: 1,
        margin: 20,
        dots: false,
        nav: true,
        navText: ['<i class="icon-angle-left"></i>', '<i class="icon-angle-right"></i>'],
        responsive: {
            0: {
                items: 1,
                dots: true,
            },
            576: {
                items: 1,
                dots: true
            },
            768: {
                items: 2,
                dots: true
            },
            992: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });
</script>
<?= (isset($datas["custome_footer"]) ? $datas["custome_footer"] : "") ?>
</body>

</html>

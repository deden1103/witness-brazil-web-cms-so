<!-- Content
============================================= -->
<section id="content">

  <div class="content-wrap" style="margin-top: -50px">

    <div class="container clearfix">

      <!-- Post Content
      ============================================= -->
      <div class="postcontent nobottommargin clearfix">

        <div class="single-post nobottommargin">

          <!-- Single Post
          ============================================= -->
          <div class="entry clearfix">

            <!-- Entry Title
            ============================================= -->
            <div class="entry-title">
              <h2><?= $datas["article_detail"]["artcTitle"] ?></h2>
            </div><!-- .entry-title end -->

            <!-- Entry Meta
            ============================================= -->
            <ul class="entry-meta clearfix">
              <li><i class="icon-calendar3"></i> <?= $datas["article_detail"]["tanggal"]." ".date("H:i", strtotime($datas["article_detail"]["artcPublishTime"])) ?></li>
              <?php
                if($datas["article_detail"]["isShowUser"]==0){
              ?>
                <li><a href="<?= $datas["article_detail"]["link_writer"] ?>"><i class="icon-user"></i> <?= $datas["article_detail"]["ukomName"] ?></a></li>
              <?php
                }else{}
              ?>
              <!-- <li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li> -->
              <!-- <li><a href="#"><i class="icon-comments"></i> 43 Comments</a></li> -->
              <!-- <li><a href="#"><i class="icon-camera-retro"></i></a></li> -->
            </ul><!-- .entry-meta end -->

            <!-- Entry Image
            ============================================= -->
            <div class="entry-image">
              <a href="#"><?= $datas['article_detail']['image'] ?></a>
            </div><!-- .entry-image end -->

            <!-- Entry Content
            ============================================= -->
            <div class="entry-content notopmargin">

              <p><?= $datas["article_detail"]["artcDetail"] ?></p>

              <!-- Tag Cloud
              ============================================= -->
              <div class="tagcloud clearfix bottommargin">
                <?php foreach($datas["article_detail"]["list_keyword"] as $v){ ?>
                  <a href="<?= $v["link_keyword"] ?>"><?= $v["name"] ?></a>
                <?php } ?>
              </div><!-- .tagcloud end -->

              <div class="clear"></div>

              <!-- Post Single - Share
              ============================================= -->
              <div class="si-share noborder clearfix">
                <span>Share this Post:</span>
                <div>
                  <a href="javascript:void(0)" onclick="PopupCenter('/\/www.facebook.com/sharer/sharer.php?u=<?= $datas['currentURL'] ?>%20%23TempoWitness', 'Share Facebook', 600, 400)" class="social-icon si-borderless si-facebook">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                  </a>
                  <a href="javascript:void(0)" onclick="PopupCenter('/\/twitter.com/share?url=<?= $datas['currentURL'] ?>&text=<?= $datas['article_detail']['artcTitle'] ?>&via=tempowitness&hashtags=TempoWitness', 'Share Twitter', 600, 400)" class="social-icon si-borderless si-twitter">
                    <i class="icon-twitter"></i>
                    <i class="icon-twitter"></i>
                  </a>
                  <a href="javascript:void(0)" onclick="PopupCenter('/\/api.whatsapp.com/send?text=<?= $datas['currentURL'] ?>%20%23TempoWitness', 'Share Whatsapp', 600, 400)" class="social-icon si-borderless si-evernote">
                    <i class="icon-whatsapp"></i>
                    <i class="icon-whatsapp"></i>
                  </a>
                  <!-- <a href="https://plus.google.com/share?url=<?= $datas["currentURL"] ?>" target="_BLANK" class="social-icon si-borderless si-gplus">
                    <i class="icon-gplus"></i>
                    <i class="icon-gplus"></i>
                  </a> -->
                  <!-- <a href="#" target="_BLANK" class="social-icon si-borderless si-rss">
                    <i class="icon-rss"></i>
                    <i class="icon-rss"></i>
                  </a> -->
                  <a href="mailto:?subject=<?= $datas["article_detail"]["artcTitle"] ?>&body=<?= $datas["currentURL"] ?>" target="_BLANK" class="social-icon si-borderless si-email3">
                    <i class="icon-email3"></i>
                    <i class="icon-email3"></i>
                  </a>
                </div>
              </div><!-- Post Single - Share End -->

            </div>
          </div><!-- .entry end -->
        </div>

      </div><!-- .postcontent end -->

      <!-- Sidebar
      ============================================= -->
      <div class="sidebar nobottommargin col_last clearfix">
        <div class="sidebar-widgets-wrap">
		
          <div class="widget clearfix">

            <div class="tabs nobottommargin clearfix" id="sidebar-tabs">

              <ul class="tab-nav clearfix">
                <li><a href="#tabs-2">Recent</a></li>
              </ul>

              <div class="tab-container">

                
				
                <div class="tab-content clearfix" id="tabs-2">
                  <div id="recent-post-list-sidebar">
					
					<?php foreach($datas['article_detail']["list_news"] as $v){ ?>
                    <div class="spost clearfix">
                      <div class="entry-image">
                        <a href="<?= $v["link"] ?>" class="nobg"><img class="rounded-circle" src="<?= $v["image"] ?>" alt=""></a>
                      </div>
                      <div class="entry-c">
                        <div class="entry-title">
                          <h4><a href="<?= $v["link"] ?>"><?= $v["artcTitle"] ?></a></h4>
                        </div>
                        <ul class="entry-meta">
                          <li><?= $v["showPublishTime"] ?></li>
                        </ul>
                      </div>
                    </div>
					<?php } ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- .sidebar end -->

    </div>
  </div>
</section><!-- #content end -->

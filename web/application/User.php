<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('m_global');
        $this->load->library('additional');
        $this->load->library('brilian');
    }
	
    public function auth() {
        $post = $this->input->post();
        $auth = $this->m_global->get(
            'user u',
            'u.userId, u.userFullName',
            [],
            [
                'u.userEmail' => $post['username'],
                'u.userPassword' => SHA1($post['password'])
            ],
            '',
            '',
            ''
        );
        if(!empty($auth)) {
            $this->session->set_userdata(array(
                'userId' => $auth[0]['userId'],
                'userFullName' => $auth[0]['userFullName'],
            ));
            redirect($post['current_url'] . '?_' . rand(), 'refresh');
        } else {
            redirect($post['current_url'] . '?_' . rand(), 'refresh');
        }
    }

    public function register($id='', $user_full_name=''){
        if(!empty($id)){
            $table = 'user a';
            $select = 'a.userId as user_id, a.userFullName as user_full_name, a.userMsgdId as user_gender, a.userBornDate as born_date, 
                        a.userPhone as user_phone, a.userEmail as user_email, a.userImgURL as user_image_url, 
                        a.userSaved as user_saved';
            $where = [
                        'a.userStatus !=' => 0,
                        'a.userId =' => $id
                    ];
            $data_user = $this->m_global->get($table, $select, $join = '', $where);
            $data = array(
                'data_user' => $data_user
            );
            $this->load->view('partials/user/profile', $data);
        } else {
            $this->load->view('partials/user/register');
        }
    }

    public function registered($id=''){
        $post = $this->input->post();
        $extension = pathinfo($_FILES['user_image']['name'], PATHINFO_EXTENSION);
        $file_get_content = file_get_contents($_FILES['user_image']['tmp_name']);
        $base64_encode = base64_encode($file_get_content);
        $base64_img_url = 'data:image/' . $extension . ';base64,' . $base64_encode;
        //print_r($_FILES['user_image']); die;
        //print_r($base64_img_url); die;

        $user_born_date = $post['bornYear'].'-'.$post['bornMonth'].'-'.$post['bornDay'];
        
        $table = 'user';

        if(!empty($id)){
            if(!empty($post['user_password'])){
                $data_update = array(
                    'userFullName' => $post['user_full_name'],
                    'userMsgdId' => $post['gender'],
                    'userEmail' => $post['user_email'],
                    'userPassword' => SHA1($post['user_password']),
                    'userPhone' => $post['user_phone'],
                    'userBornDate' => $user_born_date,
                    'userImgURL' => $base64_img_url
                );
            } else {
                $data_update = array(
                    'userFullName' => $post['user_full_name'],
                    'userMsgdId' => $post['gender'],
                    'userEmail' => $post['user_email'],
                    'userPhone' => $post['user_phone'],
                    'userBornDate' => $user_born_date,
                    'userImgURL' => $base64_img_url
                );
            }

            $update = $this->m_global->update_post($id, $table, $data_update);

            if($update){
                $this->session->set_flashdata('message', '<div class="left-40 alertMsg" style="width: 100%">
                    <ul class="konsultan">
                        <li style="background: color: #3c763d; background-color: #dff0d8; border-color: #d6e9c6; border-radius: 0px; text-align: center;">
                            <a class="fade">
                            <div class="foto-konsultan"><img src="'.base_url().'assets/images/fotoku.jpg"></div>
                            <div style="display: block" class="text-konsultan">
                                <h2 class="title-konsultan">Sukses!</h2>
                                <p>Anda telah merubah data di akun i-pangan</p>
                            </div>
                            </a>
                        </li>
                    </ul>
                </div>');
    
            } else {
    
                $this->session->set_flashdata('message', '<div class="left-40 alertMsg" style="width: 100%">
                    <ul class="konsultan">
                        <li style="color: #a94442; background-color: #f2dede; border-color: #ebccd1; border-radius: 0px; text-align: center;">
                            <a class="fade">
                            <div class="foto-konsultan"><img src="<?= base_url() ?>assets/images/fotoku.jpg"></div>
                            <div style="display: block" class="text-konsultan">
                                <h2 class="title-konsultan">Gagal!</h2>
                                <p>Maaf! gagal merubah data, silahkan dicoba kembali!</p>
                            </div>
                            </a>
                        </li>
                    </ul>
                </div>');
            }

        } else {
            $data_register = array(
                'userFullName' => $post['user_full_name'],
                'userMsgdId' => $post['gender'],
                'userEmail' => $post['user_email'],
                'userPassword' => SHA1($post['user_password']),
                'userPhone' => $post['user_phone'],
                'userBornDate' => $user_born_date,
                'userImgURL' => $base64_img_url
            );

            $register = $this->m_global->save_post($table, $data_register);

            if($register){
                $this->session->set_flashdata('message', '<div class="left-40 alertMsg" style="width: 100%">
                    <ul class="konsultan">
                        <li style="background: color: #3c763d; background-color: #dff0d8; border-color: #d6e9c6; border-radius: 0px; text-align: center;">
                            <a class="fade">
                            <div class="foto-konsultan"><img src="'.base_url().'assets/images/fotoku.jpg"></div>
                            <div style="display: block" class="text-konsultan">
                                <h2 class="title-konsultan">Sukses!</h2>
                                <p>Anda telah terdaftar di akun i-pangan</p>
                            </div>
                            </a>
                        </li>
                    </ul>
                </div>');
    
            } else {
    
                $this->session->set_flashdata('message', '<div class="left-40 alertMsg" style="width: 100%">
                    <ul class="konsultan">
                        <li style="color: #a94442; background-color: #f2dede; border-color: #ebccd1; border-radius: 0px; text-align: center;">
                            <a class="fade">
                            <div class="foto-konsultan"><img src="<?= base_url() ?>assets/images/fotoku.jpg"></div>
                            <div style="display: block" class="text-konsultan">
                                <h2 class="title-konsultan">Gagal!</h2>
                                <p>Maaf! anda belum terdaftar di akun i-pangan</p>
                            </div>
                            </a>
                        </li>
                    </ul>
                </div>');
            }
        }

        redirect("/home", 'refresh');
    }
    
    public function logout() {
        $this->session->unset_userdata(array('userId', 'userFullName'));
        redirect('/?_' . rand(), 'refresh');
    }
    
    public function loginm() {
        $data = array();
        $this->load->view('partials/user/login-mobile', $data);
    }
    
    public function forgot() {
        $data = array();
        $this->load->view('partials/user/forgot', $data);
    }
    
    public function forgotpost() {
        $email = $this->input->post('email');
        $forgot = $this->brilian->post_api('http://rest.i-pangan.com/auth/sendemail', array(), array('email' => $email));
        if($forgot['success']) {
            $this->session->set_flashdata('message', '<div class="left-40 alertMsg" style="width: 100%">
                    <ul class="konsultan">
                        <li style="background: color: #3c763d; background-color: #dff0d8; border-color: #d6e9c6; border-radius: 0px; text-align: center;">
                            <a class="fade">
                            <div class="foto-konsultan"><img src="'.base_url().'assets/images/fotoku.jpg"></div>
                            <div style="display: block" class="text-konsultan">
                                <h2 class="title-konsultan">Sukses!</h2>
                                <p>Silahkan cek email anda untuk mendapatkan password baru</p>
                            </div>
                            </a>
                        </li>
                    </ul>
                </div>');
        } else {
            $this->session->set_flashdata('message', '<div class="left-40 alertMsg" style="width: 100%">
                    <ul class="konsultan">
                        <li style="color: #a94442; background-color: #f2dede; border-color: #ebccd1; border-radius: 0px; text-align: center;">
                            <a class="fade">
                            <div class="foto-konsultan"><img src="<?= base_url() ?>assets/images/fotoku.jpg"></div>
                            <div style="display: block" class="text-konsultan">
                                <h2 class="title-konsultan">Gagal!</h2>
                                <p>Maaf! anda belum terdaftar di akun i-pangan</p>
                            </div>
                            </a>
                        </li>
                    </ul>
                </div>');
        }
        redirect("/home", 'refresh');
    }
    
}
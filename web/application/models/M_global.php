<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_global extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function get($table, $select, $join = '', $where = '', $limit = '', $offset = '', $order = '', $group = '', $or_where=''){

		$this->db->select($select);
		//Join
		if(!empty($join)){
			foreach ($join as $k => $v) {
				$explode = explode(",",$k);
				$table_join = $explode[0];
				$type = $explode[1];
				$on = $v;
				$this->db->join($table_join, $on, $type);
			}
		}

		//where
		if(!empty($where)){
			$this->db->where($where);
		}

		if($table == 'article'){
			$this->db->where("onlyForAceh",0);
		}

		if(!empty($or_where)){
			$this->db->or_where($or_where);
		}
		//limit
		if(!empty($limit)){
			$this->db->limit($limit, $offset);
		}
		//group
		if(!empty($group)){
			$this->db->group_by($group);
		}
		//order
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$result =  $this->db->get($table)->result_array();
        
        return $result;
	}

	function getarticle($table, $select, $join = '', $where = '', $limit = '', $offset = '', $order = '', $group = '', $or_where=''){

		$this->db->select($select);
		//Join
		if(!empty($join)){
			foreach ($join as $k => $v) {
				$explode = explode(",",$k);
				$table_join = $explode[0];
				$type = $explode[1];
				$on = $v;
				$this->db->join($table_join, $on, $type);
			}
		}
		$this->db->join("master_category_video", "master_category_video.mscvId = article.artcMscvId", "left");
		$this->db->join("user_komunitas", "user_komunitas.ukomId = article.artcUkomIdSaved", "left");
		$this->db->join("admin", "admin.admiId = article.artcUserIdApproved", "left");

		//where
		if(!empty($where)){
			$this->db->where($where);
		}

		if(!empty($where)){
			$this->db->where($where);
			$this->db->where("onlyForAceh",0);
		}else{
			$this->db->where("onlyForAceh",0);
		}



		if(!empty($or_where)){
			$this->db->or_where($or_where);
		}
		//limit
		if(!empty($limit)){
			$this->db->limit($limit, $offset);
		}
		//group
		if(!empty($group)){
			$this->db->group_by($group);
		}
		//order
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$result =  $this->db->get($table)->result_array();
		foreach($result as $k=>$v){
			$image_ = str_split($v['artcId']);
			$image = implode("/", $image_);
			if($v["artcMediaFileType"] == "jpg" || $v["artcMediaFileType"] == "jpeg" || $v["artcMediaFileType"] == "png"){
				// $result[$k]['image'] = "https://witness-api.hahabid.com/assets/library/" . $image . "/" . $v['artcId'] . "." . $v['artcMediaFileType'];
				$result[$k]['image'] = "https://witness.tempo.co/source/index.php?image=/" . $image . "/" . $v['artcId'] . "." . $v['artcMediaFileType'] . "&size=300&dimension=width&quality=100";
			}elseif($v["artcMediaFileType"] == "aac"){
				$result[$k]['image'] = "/assets/demos/news/images/logo-sound.png";
			}elseif($v["artcMediaFileType"] == "mp4"){
				// $id_youtube = explode("?v=",$result[$k]['artcEmbedURL']);
				// $result[$k]['image'] = "https://img.youtube.com/vi/" . $id_youtube[1] . "/0.jpg";
				$id_youtube = explode("video/",$result[$k]['artcEmbedURL']);
				//$url = "https://api.dailymotion.com/video/" . $id_youtube[1] . "?fields=thumbnail_large_url";
				//$json_thumbnail = file_get_contents($url);
				//$get_thumbnail = json_decode($json_thumbnail, TRUE);
				//$result[$k]['image'] = $get_thumbnail['thumbnail_large_url'];
				// $result[$k]['image'] = 'https://www.dailymotion.com/thumbnail/video/' . $id_youtube[1];


				$id=$id_youtube[1]; // ID DAILYMOTION EXAMPLE
				$thumbnail_medium_url='https://api.dailymotion.com/video/'.$id.'?fields=thumbnail_medium_url';
				$json_thumbnail = file_get_contents($thumbnail_medium_url);
				$get_thumbnail = json_decode($json_thumbnail, TRUE);
				$thumb=$get_thumbnail['thumbnail_medium_url'];
				$result[$k]['image'] = $thumb;


				//print_r($get_thumbnail);die;
			}else{
				$result[$k]['image'] = "/assets/demos/news/images/logo-nofile.png";
			}

			$result[$k]["link"] = "/article/detail/" . $v["artcId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["artcTitle"])) . ".html";
			$result[$k]["link_kanal"] = "/kanal/type/" . $v["artcMscvId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["mscvName"])) . ".html";
			// $result[$k]["link_writer"] = "/kanal/writer/" . $v["admiId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["admiRealName"])) . ".html";
			$result[$k]["link_writer"] = "/kanal/writer/" . $v["ukomId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["ukomName"])) . ".html";
			$result[$k]["link_publishtime"] = "/kanal/publishtime/" . DATE("Y-m-d",strtotime($v["artcPublishTime"])) . ".html";
			// $result[$k]["showPublishTime"] = DATE("d M Y", strtotime($v["artcPublishTime"]));
			$result[$k]["showPublishTime"] = $this->tanggal_indo($v["artcPublishTime"]);
		}
        
        // print_r($this->tanggal_indo($v["artcPublishTime"]));
        
    return $result;
	}

	function getfeatured($table, $select, $join = '', $where = '', $limit = '', $offset = '', $order = '', $group = '', $or_where=''){

		$this->db->select($select);
		//Join
		if(!empty($join)){
			foreach ($join as $k => $v) {
				$explode = explode(",",$k);
				$table_join = $explode[0];
				$type = $explode[1];
				$on = $v;
				$this->db->join($table_join, $on, $type);
			}
		}
		$this->db->join("master_featured_article", "master_featured_article.featName = article.artcId", "left");
		$this->db->join("master_category_video", "master_category_video.mscvId = article.artcMscvId", "left");
		$this->db->join("user_komunitas", "user_komunitas.ukomId = article.artcUkomIdSaved", "left");
		$this->db->join("admin", "admin.admiId = article.artcUserIdApproved", "left");

		//where
		if(!empty($where)){
			$this->db->where($where);
		}

		if(!empty($or_where)){
			$this->db->or_where($or_where);
		}
		//limit
		if(!empty($limit)){
			$this->db->limit($limit, $offset);
		}
		//group
		if(!empty($group)){
			$this->db->group_by($group);
		}
		//order
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$result =  $this->db->get($table)->result_array();
		foreach($result as $k=>$v){
			$image_ = str_split($v['artcId']);
			$image = implode("/", $image_);
			if($v["artcMediaFileType"] == "jpg" || $v["artcMediaFileType"] == "jpeg" || $v["artcMediaFileType"] == "png"){
				// $result[$k]['image'] = "https://witness-api.hahabid.com/assets/library/" . $image . "/" . $v['artcId'] . "." . $v['artcMediaFileType'];
				$result[$k]['image'] = "https://witness.tempo.co/source/index.php?image=/" . $image . "/" . $v['artcId'] . "." . $v['artcMediaFileType'] . "&size=300&dimension=width&quality=100";
			}elseif($v["artcMediaFileType"] == "aac"){
				$result[$k]['image'] = "/assets/demos/news/images/logo-sound.png";
			}elseif($v["artcMediaFileType"] == "mp4"){
				// $id_youtube = explode("?v=",$result[$k]['artcEmbedURL']);
				// $result[$k]['image'] = "https://img.youtube.com/vi/" . $id_youtube[1] . "/0.jpg";
				$id_youtube = explode("video/",$result[$k]['artcEmbedURL']);
				// $url = "https://api.dailymotion.com/video/" . $id_youtube[1] . "?fields=thumbnail_large_url";
				// $json_thumbnail = file_get_contents($url);
				// $get_thumbnail = json_decode($json_thumbnail, TRUE);
				// $result[$k]['image'] = $get_thumbnail['thumbnail_large_url'];
				// $result[$k]['image'] = 'https://www.dailymotion.com/thumbnail/video/' . $id_youtube[1];

				$id=$id_youtube[1]; // ID DAILYMOTION EXAMPLE
				$thumbnail_medium_url='https://api.dailymotion.com/video/'.$id.'?fields=thumbnail_medium_url';
				$json_thumbnail = file_get_contents($thumbnail_medium_url);
				$get_thumbnail = json_decode($json_thumbnail, TRUE);
				$thumb=$get_thumbnail['thumbnail_medium_url'];
				$result[$k]['image'] = $thumb;
	
			}else{
				$result[$k]['image'] = "/assets/demos/news/images/logo-nofile.png";
			}

			$result[$k]["link"] = "/article/detail/" . $v["artcId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["artcTitle"])) . ".html";
			$result[$k]["link_kanal"] = "/kanal/type/" . $v["artcMscvId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["mscvName"])) . ".html";
			$result[$k]["link_writer"] = "/kanal/writer/" . $v["ukomId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["ukomName"])) . ".html";
			$result[$k]["link_publishtime"] = "/kanal/publishtime/" . DATE("Y-m-d",strtotime($v["artcPublishTime"])) . ".html";
			// $result[$k]["showPublishTime"] = DATE("d M Y", strtotime($v["artcPublishTime"]));
			$result[$k]["showPublishTime"] = $this->tanggal_indo($v["artcPublishTime"]);
		}
        
        // print_r($this->db->last_query());
        // print_r($result);
        
    return $result;
	}

	public function search($table, $select, $join = '', $where = '', $limit = '', $offset = '', $order = '', $group = '', $or_where='', $where_2='', $where_3='', $where_4=''){
		$this->db->select($select);
		$this->db->from($table);
		/*if(!empty($where)){
			$this->db->where($where);
		}*/

		if(!empty($join)){
			foreach ($join as $k => $v) {
				$explode = explode(",",$k);
				$table_join = $explode[0];
				$type = $explode[1];
				$on = $v;
				$this->db->join($table_join, $on, $type);
			}
		}

		if(!empty($or_where) && !empty($where)){
			$this->db->where($where_2);
			if(!empty($where_3)){
				$this->db->where($where_3);
			}

			if(!empty($where_4)){
				$this->db->where($where_4);
			}
			$this->db->group_start();
			$this->db->where($where);
			$this->db->or_where($or_where);
			$this->db->group_end();
		} else {
			$this->db->where($where_2);
		}

		//limit
		if(!empty($limit)){
			$this->db->limit($limit, $offset);
		}

		//order
		if (!empty($order)) {
			$this->db->order_by($order);
		}

		$result = $this->db->get()->result_array();

		return $result;
	}

	public function getCounter($table, $select, $where){
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);

		return $this->db->count_all_results();
	}

	public function getCounterSearch($table, $select, $where, $where_2='', $where_3='', $or_where=''){
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where_2);
		$this->db->where($where_3);
			$this->db->group_start();
				$this->db->where($where);
			$this->db->or_where($or_where);
		$this->db->group_end();

		return $this->db->count_all_results();
	}

	function save_post($table, $data_register){
		return $this->db->insert($table, $data_register);
	}

	function save_arsip_images($table, $data_register){
		$this->db->insert($table, $data_register);
		$insert_id = $this->db->insert_id();

		return $insert_id;
	}

	function update_post($id, $table, $data_update){
		$this->db->where('userId', $id);
		return $this->db->update($table, $data_update);
	}

	function update_post_global($id, $table, $data_update, $table_id=''){
		$this->db->where($table_id, $id);
		return $this->db->update($table, $data_update);
	}

	public function get_all_images($inid)
    {
        $arr_img = array();
        $query = $this->db->select('*')->from('investasi_images')
        ->join('arsip_images', 'investasi_images.investasiArsipImageId=arsip_images.arimId', 'LEFT')
        ->where("investasiId", $inid)->get()->result();
        foreach ($query as $r) {
            $split_id = str_split($r->investasiArsipImageId);
            $path_folder_image = implode('/', $split_id);
            $arr_img = array(
                'img1' => "http://cdn.i-pangan.com/library/"."{$path_folder_image}/{$r->investasiArsipImageId}_224x153.{$r->arimFileType}",
                'img2' => "http://cdn.i-pangan.com/library/"."{$path_folder_image}/{$r->investasiArsipImageId}_263x180.{$r->arimFileType}",
                'img3' => "http://cdn.i-pangan.com/library/"."{$path_folder_image}/{$r->investasiArsipImageId}_300x206.{$r->arimFileType}",
                'img4' => "http://cdn.i-pangan.com/library/"."{$path_folder_image}/{$r->investasiArsipImageId}_512x351.{$r->arimFileType}",
                'img5' => "http://cdn.i-pangan.com/library/"."{$path_folder_image}/{$r->investasiArsipImageId}_683x468.{$r->arimFileType}",
                'img6' => "http://cdn.i-pangan.com/library/"."{$path_folder_image}/{$r->investasiArsipImageId}_840x576.{$r->arimFileType}"
            );

        }

        return $arr_img;
    }

	
	function tanggal_indo($date, $cetak_hari = false){
		$hari = array('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');
		$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	
		$tahun = substr($date, 0, 4);
		$bulan = substr($date, 5, 2);
		$tgl   = substr($date, 8, 2);
	
		$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;	

		if ($cetak_hari) {
			$num = date('N', strtotime($result));
			$result = $hari[$num] . ', ' . $result;
			return($result);
		}
		return($result);
	}




	function getarticle_aceh($table, $select, $join = '', $where = '', $limit = '', $offset = '', $order = '', $group = '', $or_where=''){

		$this->db->select($select);
		//Join
		if(!empty($join)){
			foreach ($join as $k => $v) {
				$explode = explode(",",$k);
				$table_join = $explode[0];
				$type = $explode[1];
				$on = $v;
				$this->db->join($table_join, $on, $type);
			}
		}
		$this->db->join("master_category_video", "master_category_video.mscvId = article.artcMscvId", "left");
		$this->db->join("user_komunitas", "user_komunitas.ukomId = article.artcUkomIdSaved", "left");
		$this->db->join("admin", "admin.admiId = article.artcUserIdApproved", "left");

		//where
		$this->db->where("ukomProvinsi",11);
		$this->db->or_where("onlyForAceh",1);
		if(!empty($where)){
			$this->db->where($where);
		}

		if(!empty($or_where)){
			$this->db->or_where($or_where);
		}
		//limit
		if(!empty($limit)){
			$this->db->limit($limit, $offset);
		}
		//group
		if(!empty($group)){
			$this->db->group_by($group);
		}
		//order
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$result =  $this->db->get($table)->result_array();
		foreach($result as $k=>$v){
			$image_ = str_split($v['artcId']);
			$image = implode("/", $image_);
			if($v["artcMediaFileType"] == "jpg" || $v["artcMediaFileType"] == "jpeg" || $v["artcMediaFileType"] == "png"){
				// $result[$k]['image'] = "https://witness-api.hahabid.com/assets/library/" . $image . "/" . $v['artcId'] . "." . $v['artcMediaFileType'];
				$result[$k]['image'] = "https://witness.tempo.co/source/index.php?image=/" . $image . "/" . $v['artcId'] . "." . $v['artcMediaFileType'] . "&size=300&dimension=width&quality=100";
			}elseif($v["artcMediaFileType"] == "aac"){
				$result[$k]['image'] = "/assets/demos/news/images/logo-sound.png";
			}elseif($v["artcMediaFileType"] == "mp4"){
				// $id_youtube = explode("?v=",$result[$k]['artcEmbedURL']);
				// $result[$k]['image'] = "https://img.youtube.com/vi/" . $id_youtube[1] . "/0.jpg";
				$id_youtube = explode("video/",$result[$k]['artcEmbedURL']);
				//$url = "https://api.dailymotion.com/video/" . $id_youtube[1] . "?fields=thumbnail_large_url";
				//$json_thumbnail = file_get_contents($url);
				//$get_thumbnail = json_decode($json_thumbnail, TRUE);
				//$result[$k]['image'] = $get_thumbnail['thumbnail_large_url'];
				// $result[$k]['image'] = 'https://www.dailymotion.com/thumbnail/video/' . $id_youtube[1];


				$id=$id_youtube[1]; // ID DAILYMOTION EXAMPLE
				$thumbnail_medium_url='https://api.dailymotion.com/video/'.$id.'?fields=thumbnail_medium_url';
				$json_thumbnail = file_get_contents($thumbnail_medium_url);
				$get_thumbnail = json_decode($json_thumbnail, TRUE);
				$thumb=$get_thumbnail['thumbnail_medium_url'];
				$result[$k]['image'] = $thumb;


				//print_r($get_thumbnail);die;
			}else{
				$result[$k]['image'] = "/assets/demos/news/images/logo-nofile.png";
			}

			$result[$k]["link"] = "/article/detail/" . $v["artcId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["artcTitle"])) . ".html";
			$result[$k]["link_kanal"] = "/kanal/type/" . $v["artcMscvId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["mscvName"])) . ".html";
			// $result[$k]["link_writer"] = "/kanal/writer/" . $v["admiId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["admiRealName"])) . ".html";
			$result[$k]["link_writer"] = "/kanal/writer/" . $v["ukomId"] . "/" . preg_replace('/\W+/', '-', strtolower($v["ukomName"])) . ".html";
			$result[$k]["link_publishtime"] = "/kanal/publishtime/" . DATE("Y-m-d",strtotime($v["artcPublishTime"])) . ".html";
			// $result[$k]["showPublishTime"] = DATE("d M Y", strtotime($v["artcPublishTime"]));
			$result[$k]["showPublishTime"] = $this->tanggal_indo($v["artcPublishTime"]);
		}
        
        // print_r($this->tanggal_indo($v["artcPublishTime"]));
        
    return $result;
	}
}

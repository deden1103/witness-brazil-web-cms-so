
<?php
defined('BASEPATH') or exit('No direct script access allowed');

//call in application/core/MY_Controller
class Find extends Syams{
    
    private $field_select;
    public function __construct(){
        parent::__construct();
        $this->field_select = 'artcId, artcTitle, artcExcerpt, artcMediaFileType, artcEmbedURL, artcMscvId, artcPublishTime, isShowUser, mscvName, ukomId, ukomName';
    }

    public function index(){

    }

    public function type($param){
      $xx = serialize($param);
      // var_dump($xx);die()
        $data["custome_footer"] = '
          <script>
            var last_date = $(".cumi").last().attr("data-publish-time");
            function loadmore(){
              $.getJSON({url: "/kanal/loadmore/artcMscvId/'.$param.'/" + last_date, success: function(result){
                result.forEach(function(val, index){
                  $("#boxContent").append(\''.
                    '<div class="ipost mb-4 mb-lg-4 row clearfix cumi" data-publish-time="\' + val.artcPublishTime + \'">'.
                      '<div class="col-md-5">'.
                        '<div class="entry-image mb-0">'.
                          '<a href="\' + val.link + \'"><img src="\' + val.image + \'" alt="Image"></a>'.
                        '</div>'.
                      '</div>'.
                      '<div class="col-md-7">'.
                        '<div class="entry-title mt-lg-0 mt-3">'.
                          '<h3><a href="\' + val.link + \'">\' + val.artcTitle + \'</a></h3>'.
                        '</div>'.
                        '<ul class="entry-meta clearfix">'.
                          '<li><span>by</span> <a href="\' + val.link_writer + \'">\' + val.ukomName + \'</a></li>'.
                          '<li><i class="icon-line-clock"><a href="\' + val.link_publishtime + \'"></i>\' + val.showPublishTime + \'</a></li>'.
                        '</ul>'.
                        '<div class="entry-content mt-0">'.
                          '\' + val.artcExcerpt + \''.
                        '</div>'.
                      '</div>'.
                    '</div>'.
                  '\');
                  last_date = val.artcPublishTime;
                });
                if(result.length == 0){
                  $(".btn-load-more").hide();
                  swal({
                    title: \'Perhatian!\',
                    text: \'Data telah ditampilkan semua\',
                    icon: \'warning\'
                  });
                }
              }});
            }
          </script>
        ';
        $data["list_hot_topic"] = parent::hot_topic();
        $data["list_kanal"] = parent::kanal();
        $data["kanal_select"] = $param;
        $data["meta_title"] = "";

        $this->load->model('M_global');
        $data["list_news"] = $this->M_global->getarticle('article',$this->field_select,"",'artcStatus = 2 AND artcMscvId = '.$param,'10','','artcPublishTime Desc');
        $data["list_recent"] = $this->M_global->getarticle('article',$this->field_select,"",'artcStatus = 2','10','','artcPublishTime Desc');

        $this->classlayout->masterview($data, "kanal");
    }

    public function writer($param){
        $data["custome_footer"] = '
          <script>
            var last_date = $(".cumi").last().attr("data-publish-time");
            function loadmore(){
              $.getJSON({url: "/kanal/loadmore/admiId/'.$param.'/" + last_date, success: function(result){
                result.forEach(function(val, index){
                  $("#boxContent").append(\''.
                    '<div class="ipost mb-4 mb-lg-4 row clearfix cumi" data-publish-time="\' + val.artcPublishTime + \'">'.
                      '<div class="col-md-5">'.
                        '<div class="entry-image mb-0">'.
                          '<a href="\' + val.link + \'"><img src="\' + val.image + \'" alt="Image"></a>'.
                        '</div>'.
                      '</div>'.
                      '<div class="col-md-7">'.
                        '<div class="entry-title mt-lg-0 mt-3">'.
                          '<h3><a href="\' + val.link + \'">\' + val.artcTitle + \'</a></h3>'.
                        '</div>'.
                        '<ul class="entry-meta clearfix">'.
                          '<li><span>by</span> <a href="\' + val.link_writer + \'">\' + val.ukomName + \'</a></li>'.
                          '<li><i class="icon-line-clock"><a href="\' + val.link_publishtime + \'"></i>\' + val.showPublishTime + \'</a></li>'.
                        '</ul>'.
                        '<div class="entry-content mt-0">'.
                          '\' + val.artcExcerpt + \''.
                        '</div>'.
                      '</div>'.
                    '</div>'.
                  '\');
                  last_date = val.artcPublishTime;
                });
                if(result.length == 0){
                  $(".btn-load-more").hide();
                  swal({
                    title: \'Perhatian!\',
                    text: \'Data telah ditampilkan semua\',
                    icon: \'warning\'
                  });
                }
              }});
            }
          </script>
        ';

        $data["list_hot_topic"] = parent::hot_topic();
        $data["list_kanal"] = parent::kanal();
        $data["kanal_select"] = $param;
        $data["meta_title"] = "";

        $this->load->model('M_global');
        $data["list_news"] = $this->M_global->getarticle('article',$this->field_select,"",'artcStatus = 2 AND ukomId = '.$param,'10','','artcPublishTime Desc');
        $data["list_recent"] = $this->M_global->getarticle('article',$this->field_select,"",'artcStatus = 2','10','','artcPublishTime Desc');

        $this->classlayout->masterview($data, "kanal");
    }

    // public function keyword($search){
    public function keyword(){
      $searchText = strip_tags(htmlspecialchars($this->input->post("q"), ENT_QUOTES));
      // $searchText = $search;
        // echo $searchText; die();
        $data['keyword'] = $searchText;
        $param = '';
        $data["custome_footer"] = '
          <script>
            var last_date = $(".cumi").last().attr("data-publish-time");
            function loadmore(){
              $.getJSON({url: "/find/loadmore/'.$searchText.'/" + last_date, success: function(result){
                result.forEach(function(val, index){
                  $("#boxContent").append(\''.
                    '<div class="ipost mb-4 mb-lg-4 row clearfix cumi" data-publish-time="\' + val.artcPublishTime + \'">'.
                      '<div class="col-md-5">'.
                        '<div class="entry-image mb-0">'.
                          '<a href="\' + val.link + \'"><img src="\' + val.image + \'" alt="Image"></a>'.
                        '</div>'.
                      '</div>'.
                      '<div class="col-md-7">'.
                        '<div class="entry-title mt-lg-0 mt-3">'.
                          '<h3><a href="\' + val.link + \'">\' + val.artcTitle + \'</a></h3>'.
                        '</div>'.
                        '<ul class="entry-meta clearfix">'.
                          '<li><span>by</span> <a href="\' + val.link_writer + \'">\' + val.ukomName + \'</a></li>'.
                          '<li><i class="icon-line-clock"><a href="\' + val.link_publishtime + \'"></i>\' + val.showPublishTime + \'</a></li>'.
                        '</ul>'.
                        '<div class="entry-content mt-0">'.
                          '\' + val.artcExcerpt + \''.
                        '</div>'.
                      '</div>'.
                    '</div>'.
                  '\');
                  last_date = val.artcPublishTime;
                });
                if(result.length == 0){
                  $(".btn-load-more").hide();
                  swal({
                    title: \'Perhatian!\',
                    text: \'Data telah ditampilkan semua\',
                    icon: \'warning\'
                  });
                }
              }});
            }
          </script>
        ';
        $data["list_hot_topic"] = parent::hot_topic();
        $data["list_kanal"] = parent::kanal();
        $data["kanal_select"] = $param;
        $data["meta_title"] = "";

        $this->load->model('M_global');
        // $data["list_news"] = $this->M_global->getarticle('article','',"",'artcStatus = 2 AND artcKeyword REGEXP ".*;s:[0-9]+:\"'.$param.'\".*"','10','','artcPublishTime Desc');
        
        $data["list_news"] = $this->M_global->getarticle('article',$this->field_select,"",'artcStatus = 2 AND artcTitle LIKE "%'.$searchText.'%"','10','','artcPublishTime Desc');
            
        // echo $this->db->last_query();
        $data["list_recent"] = $this->M_global->getarticle('article',$this->field_select,"",'artcStatus = 2','10','','artcPublishTime Desc');

        $this->classlayout->masterview($data, "find");
    }

    public function publishtime($param){
        $data["custome_footer"] = '
          <script>
            var last_date = $(".cumi").last().attr("data-publish-time");
            function loadmore(){
              $.getJSON({url: "/kanal/loadmore/artcPublishTime/'.$param.'/" + last_date, success: function(result){
                result.forEach(function(val, index){
                  $("#boxContent").append(\''.
                    '<div class="ipost mb-4 mb-lg-4 row clearfix cumi" data-publish-time="\' + val.artcPublishTime + \'">'.
                      '<div class="col-md-5">'.
                        '<div class="entry-image mb-0">'.
                          '<a href="\' + val.link + \'"><img src="\' + val.image + \'" alt="Image"></a>'.
                        '</div>'.
                      '</div>'.
                      '<div class="col-md-7">'.
                        '<div class="entry-title mt-lg-0 mt-3">'.
                          '<h3><a href="\' + val.link + \'">\' + val.artcTitle + \'</a></h3>'.
                        '</div>'.
                        '<ul class="entry-meta clearfix">'.
                          '<li><span>by</span> <a href="\' + val.link_writer + \'">\' + val.ukomName + \'</a></li>'.
                          '<li><i class="icon-line-clock"></i><a href="\' + val.link_publishtime + \'">\' + val.showPublishTime + \'</a></li>'.
                        '</ul>'.
                        '<div class="entry-content mt-0">'.
                          '\' + val.artcExcerpt + \''.
                        '</div>'.
                      '</div>'.
                    '</div>'.
                  '\');
                  last_date = val.artcPublishTime;
                });
                if(result.length == 0){
                  $(".btn-load-more").hide();
                  swal({
                    title: \'Perhatian!\',
                    text: \'Data telah ditampilkan semua\',
                    icon: \'warning\'
                  });
                }
              }});
            }
          </script>
        ';

        $data["list_kanal"] = parent::kanal();
        $data["kanal_select"] = $param;
        $data["meta_title"] = "";

        $this->load->model('M_global');
        $data["list_news"] = $this->M_global->getarticle('article',$this->field_select,"",'artcStatus = 2 AND DATE(artcPublishTime) = "'.$param.'"','10','','artcPublishTime Desc');
        $data["list_recent"] = $this->M_global->getarticle('article',$this->field_select,"",'artcStatus = 2','10','','artcPublishTime Desc');

        $this->classlayout->masterview($data, "kanal");
    }

    public function loadMore($searchText,$date = ''){
    //   if(!empty($date)){
        $date = str_replace("%20"," ",$date);

        $this->load->model('M_global');
        // if($type == "artcPublishTime"){
        //   $data["load_more"] = $this->M_global->getarticle('article','',"",'artcStatus = 2 AND artcPublishTime < "' . DATE($date) . '" AND DATE(artcPublishTime) = "' . $kanal . '"','10','','artcPublishTime DESC');
        // }elseif($type == "artcKeyword"){
        //   $data["load_more"] = $this->M_global->getarticle('article','',"",'artcStatus = 2 AND artcPublishTime < "' . DATE($date) . '" AND artcKeyword REGEXP ".*;s:[0-9]+:\"'.$kanal.'\".*"','10','','artcPublishTime Desc');
        // }else{
          $data["load_more"] = $this->M_global->getarticle('article',$this->field_select,"",'artcStatus = 2 AND artcTitle LIKE "%'.$searchText.'%" AND artcPublishTime < "' . DATE($date) . '"','10','','');
        // }

        print_r(json_encode($data["load_more"]));
    //   }else{
    //     redirect(base_url());
    //   }
    }
}

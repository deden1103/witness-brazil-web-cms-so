
<?php
defined('BASEPATH') or exit('No direct script access allowed');

//call in application/core/MY_Controller
class Info extends Syams
{
    private $field_select;
    public function __construct()
    {
        parent::__construct();
        $this->field_select =
            'artcId, artcTitle, artcExcerpt, artcMediaFileType, artcEmbedURL, artcMscvId, artcPublishTime, isShowUser, mscvName, ukomId, ukomName';
    }

    public function index()
    {
        $id=$this->input->get('id');
        $qu=$this->db->select('detail')->from('about')->where('id',$id)->get()->result();
        $data["isi"] = $qu[0]->detail;
        // print_r($data["isi"]);die;
        $this->classlayout->masterview($data, 'about');
    }
}



<?php
defined('BASEPATH') or exit('No direct script access allowed');

//call in application/core/MY_Controller
class Rss extends Syams{

    private $field_select;
    public function __construct(){
        parent::__construct();
        $this->field_select = 'artcId, artcTitle, artcDetail, artcExcerpt, artcMediaFileType, artcEmbedURL, artcMscvId, artcPublishTime, isShowUser, mscvName, ukomId, ukomName';
    }

    public function index(){
        header('Content-Type: text/xml');
        $limit = 20;
        if(!empty($_GET['limit'])) {
            $limit = intval($_GET['limit']);
        }
        $data = array();
        // $data['rss'] = $this->M_article->listv2("", 20, 'artcPublishTime DESC');
        $this->load->view('rss', $data);
    }

    
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

//call in application/core/MY_Controller
class Home extends Syams{

    public function __construct(){
        parent::__construct();
        $this->load->model('M_global');
        $this->load->model('M_home');

    }

    public function index(){
        $data["custome_footer"] = '
          <script>
            var last_date = $(".cumi").last().attr("data-publish-time");
            function loadmore(){
              $.getJSON({url: "/home/loadmore/" + last_date, success: function(result){
                result.forEach(function(val, index){
                  $("#boxContent").append(\''.
                    '<div class="ipost mb-4 mb-lg-4 row clearfix cumi" data-publish-time="\' + val.artcPublishTime + \'">'.
                      '<div class="col-md-5">'.
                        '<div class="entry-image mb-0">'.
                          '<a href="\' + val.link + \'"><img src="\' + val.image + \'" alt="Image"></a>'.
                          '<div class="entry-categories"><a href="\' + val.link_kanal + \'" class="bg-sports">\' + val.mscvName + \'</a></div>'.
                        '</div>'.
                      '</div>'.
                      '<div class="col-md-7">'.
                        '<div class="entry-title mt-lg-0 mt-3">'.
                          '<h3><a href="\' + val.link + \'">\' + val.artcTitle + \'</a></h3>'.
                        '</div>'.
                        '<ul class="entry-meta clearfix">'.
                          '<li><span>by</span> <a href="\' + val.link_writer + \'">\' + val.ukomName + \'</a></li>'.
                          '<li><i class="icon-line-clock"><a href="\' + val.link_publishtime + \'"></i>\' + val.showPublishTime + \'</a></li>'.
                        '</ul>'.
                        '<div class="entry-content mt-0">'.
                          '\' + val.artcExcerpt + \''.
                        '</div>'.
                      '</div>'.
                    '</div>'.
                  '\');
                  last_date = val.artcPublishTime;
                });
                if(result.length == 0){
                  $(".btn-load-more").hide();
                }
              }});
            }
          </script>
        ';

          $data["list_hot_topic"] = parent::hot_topic();
          $data["list_kanal"] = parent::kanal();
          $data["kanal_select"] = "";
          $data["meta_title"] = "";

          $data["popular"] = $this->M_global->get('article','artcMscvId, count(artcId) as sum','','artcStatus = 2','','','sum DESC','artcMscvId');
          $data["featured"] = $this->M_global->getfeatured('article','artcId, artcTitle, artcExcerpt, artcMediaFileType, artcEmbedURL, artcMscvId, artcPublishTime, isShowUser, mscvName, ukomId, ukomName','','master_featured_article.featStatus = 1','5','','master_featured_article.featSort Asc, master_featured_article.createDate Desc');
          
        $loc = $this->input->GET('sort', TRUE);

        $field_select = 'artcId, artcTitle, artcExcerpt, artcMediaFileType, artcEmbedURL, artcMscvId, artcPublishTime, isShowUser, mscvName, ukomId, ukomName';

        if(!empty($loc)){
          // $getloc = json_decode(file_get_contents("http://ipinfo.io/"));
          // $coordinates = explode(",", $getloc->loc); // -> '32,-72' becomes'32','-72'
          $PublicIP = $this->get_client_ip();
          $json     = file_get_contents("http://ipinfo.io/$PublicIP/geo");
          $json     = json_decode($json, true);
          $coordinates = explode(",", $json['loc']); // -> '32,-72' becomes'32','-72'
          // print_r($PublicIP.'<br>'); // latitude
          // print_r($coordinates[0].'<br>'); // latitude
          // print_r($coordinates[1].'<br>'); // longitude
          // print_r($loc); // loc
          // die;
          $data['all'] = $this->M_global->getarticle('article',$field_select.',(6371 * acos(cos(radians(' .$coordinates[0] . ')) * cos(radians(article.latitude)) * cos(radians(article.longitude) - radians(' . $coordinates[1] . ')) + sin(radians(' .$coordinates[0]. ')) * sin(radians(article.latitude)))) AS distance',"","artcStatus = 2",'6','','distance ASC, artcPublishTime Desc');
          // $data['all'] = $this->M_global->getarticle('article','*,round((rad2deg(acos(sin(deg2rad(' .$coordinates[0] . ') * sin(deg2rad(article.latitude)) + cos(deg2rad(' .$coordinates[0] . ')) * cos(deg2rad(article.latitude)) * cos(deg2rad(' . $coordinates[1] . ' - article.longitude)))) * 60 * 1.1515 * 1.609344),2) AS distance',"","artcStatus = 2",'6','','distance ASC, artcPublishTime Desc');
          // print_r($data['all']); die;

          $data['video'] = $this->M_global->getarticle('article',$field_select.',(6371 * acos(cos(radians(' .$coordinates[0] . ')) * cos(radians(article.latitude)) * cos(radians(article.longitude) - radians(' . $coordinates[1] . ')) + sin(radians(' .$coordinates[0]. ')) * sin(radians(article.latitude)))) AS distance',"","(artcEmbedURL IS NOT NULL AND artcEmbedURL <> '') AND artcStatus = 2",'6','','distance ASC, artcPublishTime Desc');

          $data['image'] = $this->M_global->getarticle('article',$field_select.',(6371 * acos(cos(radians(' .$coordinates[0] . ')) * cos(radians(article.latitude)) * cos(radians(article.longitude) - radians(' . $coordinates[1] . ')) + sin(radians(' .$coordinates[0]. ')) * sin(radians(article.latitude)))) AS distance',"","(heightImage IS NOT NULL AND heightImage <> '') AND artcStatus = 2",'6','','distance ASC, artcPublishTime Desc');

          $data['teks'] = $this->M_global->getarticle('article',$field_select.',(6371 * acos(cos(radians(' .$coordinates[0] . ')) * cos(radians(article.latitude)) * cos(radians(article.longitude) - radians(' . $coordinates[1] . ')) + sin(radians(' .$coordinates[0]. ')) * sin(radians(article.latitude)))) AS distance',"","(artcMediaFileType IS NULL AND artcMediaFileType <> '') AND artcStatus = 2",'6','','distance ASC, artcPublishTime Desc');

          $data['audio'] = $this->M_global->getarticle('article',$field_select.',(6371 * acos(cos(radians(' .$coordinates[0] . ')) * cos(radians(article.latitude)) * cos(radians(article.longitude) - radians(' . $coordinates[1] . ')) + sin(radians(' .$coordinates[0]. ')) * sin(radians(article.latitude)))) AS distance',"","artcMediaFileType = 'aac' AND artcStatus = 2",'6','','distance ASC, artcPublishTime Desc');
          // print_r($data['video']); die;
          //print_r($data["featured"]); die;

          foreach($data["list_kanal"] as $k=>$v){
            if($k < 7){
              $data['latest'][] = $this->M_global->getarticle('article',$field_select.',(6371 * acos(cos(radians(' .$coordinates[0] . ')) * cos(radians(article.latitude)) * cos(radians(article.longitude) - radians(' . $coordinates[1] . ')) + sin(radians(' .$coordinates[0]. ')) * sin(radians(article.latitude)))) AS distance',"",'artcStatus = 2 AND artcMscvId = '.$v["mscvId"],'6','','distance ASC, artcPublishTime Desc');
            }

            $data["list_kanal"][$k]["sum"] = 0;

            foreach($data["popular"] as $val){
              if($val["artcMscvId"] == $v["mscvId"]){
                $data["list_kanal"][$k]["sum"] = $val["sum"];
              }
            }
          }

          $data["popular"] = $data["list_kanal"];
          $sum = array_column($data["popular"], 'sum');
          array_multisort($sum, SORT_DESC, $data["popular"]);

          $id_latest = array();

          foreach($data["latest"] as $k=>$v) {
            foreach($v as $key=>$val) {
              array_push($id_latest,$val["artcId"]);
            }
          }

          if(empty($id_latest)){
            $data['article'] = $this->M_global->getarticle('article',$field_select.',(6371 * acos(cos(radians(' .$coordinates[0] . ')) * cos(radians(article.latitude)) * cos(radians(article.longitude) - radians(' . $coordinates[1] . ')) + sin(radians(' .$coordinates[0]. ')) * sin(radians(article.latitude)))) AS distance',"",'artcStatus = 2','6','','distance');
          }else{
            $data['article'] = $this->M_global->getarticle('article',$field_select.',(6371 * acos(cos(radians(' .$coordinates[0] . ')) * cos(radians(article.latitude)) * cos(radians(article.longitude) - radians(' . $coordinates[1] . ')) + sin(radians(' .$coordinates[0]. ')) * sin(radians(article.latitude)))) AS distance',"","artcId NOT IN ( '" . implode( "', '" , $id_latest ) . "' ) AND artcStatus = 2",'6','','distance');
          }
        }else{
            
          $data['all'] = $this->M_global->getarticle('article',$field_select,"","artcStatus = 2",'6','','artcPublishTime Desc');

          $data['video'] = $this->M_global->getarticle('article',$field_select,"","(artcEmbedURL IS NOT NULL AND artcEmbedURL <> '') AND artcStatus = 2",'6','','artcPublishTime Desc');

          $data['image'] = $this->M_global->getarticle('article',$field_select,"","(heightImage IS NOT NULL AND heightImage <> '') AND artcStatus = 2",'6','','artcPublishTime Desc');

          $data['teks'] = $this->M_global->getarticle('article',$field_select,"","(artcMediaFileType IS NULL AND artcMediaFileType <> '') AND artcStatus = 2",'6','','artcPublishTime Desc');

          $data['audio'] = $this->M_global->getarticle('article',$field_select,"","artcMediaFileType = 'aac' AND artcStatus = 2",'6','','artcPublishTime Desc');
          // print_r($data['video']); die;
          //print_r($data["featured"]); die;

          foreach($data["list_kanal"] as $k=>$v){
            if($k < 7){
              $data['latest'][] = $this->M_global->getarticle('article',$field_select,"",'artcStatus = 2 AND artcMscvId = '.$v["mscvId"],'6','','artcPublishTime Desc');
            }

            $data["list_kanal"][$k]["sum"] = 0;

            foreach($data["popular"] as $val){
              if($val["artcMscvId"] == $v["mscvId"]){
                $data["list_kanal"][$k]["sum"] = $val["sum"];
              }
            }
          }

          $data["popular"] = $data["list_kanal"];
          $sum = array_column($data["popular"], 'sum');
          array_multisort($sum, SORT_DESC, $data["popular"]);

          $id_latest = array();

          foreach($data["latest"] as $k=>$v) {
            foreach($v as $key=>$val) {
              array_push($id_latest,$val["artcId"]);
            }
          }

          if(empty($id_latest)){
            $data['article'] = $this->M_global->getarticle('article',$field_select,"",'artcStatus = 2','6','','artcPublishTime Desc');
          }else{
            $data['article'] = $this->M_global->getarticle('article',$field_select,"","artcId NOT IN ( '" . implode( "', '" , $id_latest ) . "' ) AND artcStatus = 2",'6','','artcPublishTime Desc');
          }
        }
        $this->classlayout->masterview($data, "index");

    }

    function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } else if (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = 'UNKNOWN';
        }
    
        return $ipaddress;
    }

    public function loadMore($date){
      $date = str_replace("%20"," ",$date);

      $this->load->model('M_global');
      $loc = $this->input->GET('sort', TRUE);

      $field_select = 'artcId, artcTitle, artcExcerpt, artcMediaFileType, artcEmbedURL, artcMscvId, artcPublishTime, isShowUser, mscvName, ukomId, ukomName';

      if(!empty($loc)){
        // $getloc = json_decode(file_get_contents("http://ipinfo.io/"));
        // $coordinates = explode(",", $getloc->loc); // -> '32,-72' becomes'32','-72'
        $PublicIP = $this->get_client_ip();
        $json     = file_get_contents("http://ipinfo.io/$PublicIP/geo");
        $json     = json_decode($json, true);
        $coordinates = explode(",", $json['loc']);
        $data["load_more"] = $this->M_global->getarticle('article',$field_select.',(6371 * acos(cos(radians(' .$coordinates[0] . ')) * cos(radians(article.latitude)) * cos(radians(article.longitude) - radians(' . $coordinates[1] . ')) + sin(radians(' .$coordinates[0]. ')) * sin(radians(article.latitude)))) AS distance',"",'artcStatus = 2 AND artcPublishTime < "' . DATE($date) . '"',6,0,'distance, artcPublishTime DESC');
      }else{
        $data["load_more"] = $this->M_global->getarticle('article',$field_select,"",'artcStatus = 2 AND artcPublishTime < "' . DATE($date) . '"',6,0,'artcPublishTime DESC');
      }
      print_r(json_encode($data["load_more"]));
    }
    
    public function rss() {
        
        // $cms = "https://cdn.betahita.id/";

        // $join1 = array(
        //   "master_category_video, left" => "master_category_video.mscvId = article.artcMscvId",
        //   "user_komunitas, left" => "user_komunitas.ukomId = article.artcUkomIdSaved",
        //   "admin, left" => "admin.admiId = article.artcUserIdApproved",
        // );

        // $data['rss'] = $this->M_global->get('article','',$join1,'artcStatus = 2',20,'','artcPublishTime Desc');
        // foreach($data['rss'] as $k=>$v){
        //     if(strlen($data['rss'][$k]["newsExcerpt"]) > 165){
        //         $data['rss'][$k]["newsExcerpt"] = substr($data['rss'][$k]["newsExcerpt"], 0, 165).'...';
        //     }
        //     $data['rss'][$k]["link"] = "https://betahita.id/news/detail/" . $v["newsId"] . "/" .preg_replace('/\W+/', '-', strtolower($v["newsTitle"])) . ".html?v=".time();
        //     $data['rss'][$k]["publish"] = $this->M_global->tanggal_indo($v["newsPublishTime"], true);
        //     $image_ = str_split($data['rss'][$k]['neimArimId']);
        //     $image = implode("/", $image_);
        //     $data['rss'][$k]['image'] = $cms . $image . "/" . $data['rss'][$k]['neimArimId'] . "." . $data['rss'][$k]['arimFileType'];
        // }
        $data['rss'] = $this->M_global->getarticle('article','',"","artcStatus = 2",20,'','artcPublishTime Desc');
        // print_r($data); die;
        // $limit = 20;
        // if(!empty($_GET['limit'])) {
        //     $limit = intval($_GET['limit']);
        // }
        // $data = array();
        
        // $data['rss']= $this->M_global->getarticle('article',
		// 	'article.artcId,article.artcTitle,article.artcExcerpt,article.artcDetail,article_image.artiArimId,artcPublishTime,article.artcMsctId,master_category.msctName,user.userRealName',
		// 	"",'artcStatus = 2',$limit,'','artcPublishTime DESC');

        header("Content-Type: application/rss+xml");
        $this->load->view('rss', $data);
    }


    public function aceh_rss() {
        
      // $cms = "https://cdn.betahita.id/";

      // $join1 = array(
      //   "master_category_video, left" => "master_category_video.mscvId = article.artcMscvId",
      //   "user_komunitas, left" => "user_komunitas.ukomId = article.artcUkomIdSaved",
      //   "admin, left" => "admin.admiId = article.artcUserIdApproved",
      // );

      // $data['rss'] = $this->M_global->get('article','',$join1,'artcStatus = 2',20,'','artcPublishTime Desc');
      // foreach($data['rss'] as $k=>$v){
      //     if(strlen($data['rss'][$k]["newsExcerpt"]) > 165){
      //         $data['rss'][$k]["newsExcerpt"] = substr($data['rss'][$k]["newsExcerpt"], 0, 165).'...';
      //     }
      //     $data['rss'][$k]["link"] = "https://betahita.id/news/detail/" . $v["newsId"] . "/" .preg_replace('/\W+/', '-', strtolower($v["newsTitle"])) . ".html?v=".time();
      //     $data['rss'][$k]["publish"] = $this->M_global->tanggal_indo($v["newsPublishTime"], true);
      //     $image_ = str_split($data['rss'][$k]['neimArimId']);
      //     $image = implode("/", $image_);
      //     $data['rss'][$k]['image'] = $cms . $image . "/" . $data['rss'][$k]['neimArimId'] . "." . $data['rss'][$k]['arimFileType'];
      // }

      $page = 1;
      if ($this->input->get('p')) {
        $page = $this->input->get('p');
      }
      $rows = 20;
      $offset = ($page - 1) * $rows;


      $data['rss'] = $this->M_global->getarticle_aceh('article','',"","artcStatus = 2",$rows,$offset,'artcPublishTime Desc');
      // print_r($data); die;
      // $limit = 20;
      // if(!empty($_GET['limit'])) {
      //     $limit = intval($_GET['limit']);
      // }
      // $data = array();
      
      // $data['rss']= $this->M_global->getarticle('article',
  // 	'article.artcId,article.artcTitle,article.artcExcerpt,article.artcDetail,article_image.artiArimId,artcPublishTime,article.artcMsctId,master_category.msctName,user.userRealName',
  // 	"",'artcStatus = 2',$limit,'','artcPublishTime DESC');

      header("Content-Type: application/rss+xml");
      $this->load->view('rss', $data);
  }
}

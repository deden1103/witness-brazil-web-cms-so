
<?php
defined('BASEPATH') or exit('No direct script access allowed');

//call in application/core/MY_Controller
class Article extends Syams{

    private $field_select;
    public function __construct(){
        parent::__construct();
        $this->field_select = 'artcId, artcTitle, artcDetail, artcExcerpt, artcMediaFileType, artcEmbedURL, artcMscvId, artcPublishTime, isShowUser, mscvName, ukomId, ukomName';
    }

    public function index(){

    }

    public function detail($id){
		$data["list_hot_topic"] = parent::hot_topic();
        $data["list_kanal"] = parent::kanal();
        $data["currentURL"] = current_url();

        $this->load->model('M_global');
        $article = $this->M_global->getarticle('article','','','artcId = '.$id,'1');
        //print_r($article); die;
        $data['article_detail'] = $article[0];
        $data['article_detail']['tanggal'] = $this->M_global->tanggal_indo($article[0]["artcPublishTime"]);
        $data["meta_title"] = $article[0]["artcTitle"] . " | ";

        $image_ = str_split($data['article_detail']['artcId']);
        $image = implode("/", $image_);

        if($data['article_detail']["artcMediaFileType"] == "jpg" || $data['article_detail']["artcMediaFileType"] == "jpeg" || $data['article_detail']["artcMediaFileType"] == "png"){
          // $data['article_detail']['image'] = "<img src=\"http://witness-api.hahabid.com/assets/library/" . $image . "/" . $data['article_detail']['artcId'] . "." . $data['article_detail']['artcMediaFileType'] . "\">";
          $data['article_detail']['image'] = "<img src=\"http://witness.tempo.co/source/index.php?image=/" . $image . "/" . $data['article_detail']['artcId'] . "." . $data['article_detail']['artcMediaFileType'] . "&size=1000&dimension=width&quality=100\">";
          $data['share_img'] = "//witness.tempo.co/source/index.php?image=/" . $image . "/" . $data['article_detail']['artcId'] . "." . $data['article_detail']['artcMediaFileType'] . "&size=800&dimension=width&quality=100";
        }elseif($data['article_detail']["artcMediaFileType"] == "aac"){
          $data['article_detail']['image'] = "<audio controls><source src='http://witness-api.hahabid.com/assets/library/" . $image . "/" . $data['article_detail']['artcId'] . "." . $data['article_detail']['artcMediaFileType'] . "' type='audio/" . $data['article_detail']['artcMediaFileType'] . "'></audio>";
        }elseif($data['article_detail']["artcMediaFileType"] == "mp4" || $data['article_detail']["artcMediaFileType"] == "mov"){
          // $id_youtube = explode("?v=",$data["article_detail"]['artcEmbedURL']);
          // $data['article_detail']['image'] = '<div class=""><iframe class="embed-responsive-item" src="//www.youtube.com/embed/' . $id_youtube[1] . '" allowfullscreen></iframe></div>';
            $id_youtube = explode("video/",$data["article_detail"]['artcEmbedURL']);
            $this->load->library('Mobile_Detect');
            $detect = new Mobile_Detect();
            if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
                $data['article_detail']['image'] = '<div class=""><iframe src="https://www.dailymotion.com/embed/video/' . $id_youtube[1] . '" allowfullscreen="" allow="autoplay" width="100%" height="200" frameborder="0"></iframe></div>';
            } else {
                $data['article_detail']['image'] = '<div class=""><iframe src="https://www.dailymotion.com/embed/video/' . $id_youtube[1] . '" allowfullscreen="" allow="autoplay" width="100%" height="400" frameborder="0"></iframe></div>';
            }
            $url = "https://api.dailymotion.com/video/" . $id_youtube[1] . "?fields=thumbnail_large_url";
            $json_thumbnail = file_get_contents($url);
            $get_thumbnail = json_decode($json_thumbnail, TRUE);
            // $result[$k]['image'] = $get_thumbnail['thumbnail_large_url'];
            $data['share_img'] = "https://www.dailymotion.com/thumbnail/video/" . $id_youtube[1];
            // $data['share_img'] = $get_thumbnail['thumbnail_large_url'];
        }else{
          $data['article_detail']['image'] = '';
        }

        $id_keyword = array();
        if(!empty($data['article_detail']['artcKeyword'])){
          foreach(unserialize($data['article_detail']['artcKeyword']) as $k=>$v) {
            array_push($id_keyword,$v);
          }
        }

        $data["kanal_select"] = $data['article_detail']['artcMscvId'];
        $data['article_detail']['list_keyword'] = $this->M_global->get('master_keyword_video',['vkeyId as id', 'vkeyName as name'],"","vkeyId IN ( '" . implode( "', '" , $id_keyword ) . "' )");
        foreach($data['article_detail']['list_keyword'] as $k=>$v){
          $data['article_detail']['list_keyword'][$k]['link_keyword'] = "/kanal/keyword/" . $v["id"] . "/" . preg_replace('/\W+/', '-', strtolower($v["name"])) . ".html";
        }
		
		$data['article_detail']["list_news"] = $this->M_global->getarticle('article',$this->field_select,"",'artcStatus = 2 AND artcMscvId = '.$data['article_detail']['mscvId'],'10','','artcPublishTime Desc');

        $this->classlayout->masterview($data, "article-detail");
    }
}

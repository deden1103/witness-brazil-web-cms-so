<?php
require_once("./Image.php");

$default_image ="./default.png";//set a default image
$image_name = ""; //name of the requesting image
$size = 1000;//Default size for the image, if image size has not been set
$dimension = "";//Default value is width and you can select this value from width and height 
$quality = 100;//Default image quality 

if(isset($_GET["image"])){ $image_name = $_GET["image"]; }
if(isset($_GET["size"])){ $size = $_GET["size"]; }
if(isset($_GET["dimension"])){ $dimension = $_GET["dimension"]; }
if(isset($_GET["quality"])){ $quality = $_GET["quality"]; }

$image_path = "/usr/share/nginx/html/witness/api/assets/library/".$image_name;

$imgLib = new Image();
$imgLib->export_image($default_image, $image_path, $size, $dimension, $quality);
?>